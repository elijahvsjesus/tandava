window.addEvent('domready', function(){
	$$('form').each(function(form){
		form.addEvent('click', function(event){
			var event  = new Event(event),
				target = $(event.target);
				
			if(target.hasClass('toggle-state'))
			{
				var toggle = Json.evaluate(target.getProperty('rel')),
					token  =  this.getElements('input').filter(function(e){return e.getProperty('name')=='_token'})[0].getValue();
				
				this.send({
					data: {
						id: target.getParent().getParent().getElement('input.id').getProperty('value'),
						action: toggle.state[toggle.toggle],
						_token: token
					},
					method: 'post',
					onComplete: function(){
						target.addClass('icon-toggle-' + toggle.state[toggle.toggle]).getParent().getParent().addClass('state-' + toggle.state[toggle.toggle]);
						toggle.toggle = toggle.toggle == 0 ? 1 : 0;
						target.removeClass('icon-toggle-' + toggle.state[toggle.toggle]).getParent().getParent().removeClass('state-' + toggle.state[toggle.toggle]);
						target.setProperty('rel', Json.toString(toggle).replace(/"/g, "'"));
					}
				});
			}
		});
	});
});