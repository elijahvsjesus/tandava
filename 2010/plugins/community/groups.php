<?php
/**
 * @category	Plugins
 * @package		JomSocial
 * @copyright (C) 2008 by Slashes & Dots Sdn Bhd - All rights reserved!
 * @license		GNU/GPL, see LICENSE.php
 */
// no direct access
defined('_JEXEC') or die('Restricted access');

require_once( JPATH_ROOT . DS . 'components' . DS . 'com_community' . DS . 'libraries' . DS . 'core.php');

if(!class_exists('plgCommunityGroups'))
{
	class plgCommunityGroups extends CApplications
	{
		var $name 		= "My Groups Application";
		var $_name		= 'groups';
		var $_path		= '';
		var $_user		= '';
		var $_my		= '';
		
		function plgCommunityGroups(& $subject, $config)
	    {
			$this->_user	= CFactory::getRequestUser();
		
			parent::__construct($subject, $config);
	    }
	
		/**
		 * AJAX test method.
		 * 
		 * @param JAXResponse  A referenced JAXResponse object. Used to send response to the AJAX calls.
		 * return JAXResponse  Returns the JAXResponse so that Community will be able to display it out.
		 * 	 	 	 	 
		 **/
		function ajaxShowGroupInfo(&$response , $arg)
		{
			$uri	= JURI::base();
			$groupModel		= CFactory::getModel( 'groups' );
			$avatarModel	= CFactory::getModel( 'avatar' );
			$my				=& JFactory::getUser();
			
			$group			=& JTable::getInstance( 'Group' , 'CTable' );
			$group->load( $arg );
	
			ob_start();
	?>
			<div id="groupInfo">
				<h3><?php echo $group->name; ?></h3>
				<span class="created">
					<?php echo JHTML::_('date', $group->created, JText::_('DATE_FORMAT_LC2'));?>
				</span>
				<span class="groupImg">
					<img border="0" class="jomTips" src="<?php echo $group->getAvatar('thumb'); ?>" alt="<?php echo CTemplate::escape($group->name); ?>" title="<?php echo CTemplate::escape($group->name); ?>::" />
				</span>
				<div class="groupDetails">
					<span class="creator"><?php echo JText::_('PLG_GROUPS GROUP CREATOR');?></span>
					<span>:</span>
					<span><?php echo CTemplate::escape($group->getOwnerName()); ?></span>
				</div>
				<div class="groupDetails">
					<span class="email"><?php echo JText::_('PLG_GROUPS CONTACT EMAIL');?></span>
					<span>:</span>
					<span><?php echo $group->email; ?></span>
				</div>
				<div class="groupDetails">
					<span class="website"><?php echo JText::_('PLG_GROUPS CONTACT WEBSITE');?></span>
					<span>:</span>
					<span><?php echo $group->website; ?></span>
				</div>
				<div style="clear: both;"></div>
				<div style="margin: 10px 0 0 0; padding: 5px;">
					<?php echo $group->description; ?>
				</div>
			</div>
	<?php
			$content	= ob_get_contents();
			ob_end_clean();
			$response->addAssign('cWindowContent' , 'innerHTML' , $content);
	
			$buttons	= '<form name="joingroup" action="index.php?option=com_community&view=groups&task=joingroup" method="post">';
			$buttons	.= '<input type="submit" value="' . JText::_('PLG_GROUPS JOIN GROUP') . '" class="button" name="Submit" />';
			$buttons	.= '<input type="hidden" name="groupid" value="' . $group->id . '" />';
			$buttons	.= '&nbsp;';
			$buttons	.= '<input onclick="cWindowHide();return false;" type="button" value="' . JText::_('PLG_GROUPS CANCEL') . '" class="button" />';
			$buttons	.= '</form>';
			
			$response->addScriptCall('cWindowActions', $buttons);
			return $response;
		}
	
		/**
		 * This method is triggered by Jom Social automatically when the profile page is called.
		 * 
		 **/
		function onProfileDisplay()
		{
			JPlugin::loadLanguage( 'plg_groups', JPATH_ADMINISTRATOR );
		
			$config	= CFactory::getConfig();
					
			if( !$config->get('enablegroups') )
			{
				return JText::_('PLG_GROUPS GROUP DISABLED');	
			}
			
			$creategroups = $config->get('creategroups');
			
			$uri	= JURI::base();
	
			$document	=& JFactory::getDocument();
			$css		= $uri	. 'plugins/community/groups/style.css';
			$document->addStyleSheet($css);	
			
			$groupsModel		= CFactory::getModel( 'groups' );	
			$avatarModel		= CFactory::getModel( 'avatar' );
			
			$user				= CFactory::getRequestUser();
			$groups				= $groupsModel->getGroups( $user->id , 10 , true );	
			
			$mainframe =& JFactory::getApplication();
			$caching = $this->params->get('cache', 1);		
			if($caching)
			{
				$caching = $mainframe->getCfg('caching');
			}
			
			$cache =& JFactory::getCache('plgCommunityGroups');
			$cache->setCaching($caching);
			$callback = array('plgCommunityGroups', '_getGroupHTML');		
			$content = $cache->call($callback, $creategroups, $groups, $user->id, $groupsModel);
			return $content; 
		}
		
		function _getGroupHTML($creategroups, $groups, $userId, $groupsModel){
			$my					= CFactory::getUser();
			ob_start();		
	?>
			<script type="text/javascript" language="javascript">
			// Show Group Information box
			function cAppsShowGroupInfo(groupId){
				var ajaxCall = "jax.call('community', 'plugins,groups,ajaxShowGroupInfo', '" + groupId + "');";
				cWindowShow( ajaxCall, '<?php echo JText::_('PLG_GROUPS VIEWING GROUP'); ?>', 450, 250);
			}
			</script>
			<div id="application-group">
	
	
			<?php
			if($groups){
			?>
				<div class="application-info">
					<?php echo JText::_('PLG_GROUPS GROUP JOINED'); ?>
				</div>
	
				<ul class="cThumbList clrfix">
				<?php
				foreach( $groups as $group )
				{
					$table		=& JTable::getInstance( 'Group' , 'CTable' );
					$table->load( $group->id );
					$avatar		= $table->getAvatar('thumb');
					$members	= $groupsModel->getMembersId( $group->id );
				?>
					<li>
						<a href="<?php echo CUrl::build( 'groups' , 'viewgroup' , array( 'groupid' => $group->id ) , true ); ?>">
						<img border="0" class="jomTips avatar" src="<?php echo $avatar; ?>" alt="<?php echo CTemplate::escape($group->name); ?>" title="<?php echo CTemplate::escape($group->name); ?>::<?php echo CTemplate::escape($group->description); ?>" />
						</a>
					</li>
				<?php
				}
				?>
				</ul>
			<?php
			}else{
			?>
		        <div class="nopost">
		        	<img class="icon-nopost" src="<?php echo JURI::base(); ?>plugins/community/groups/favicon.png" alt="" />
		        	<span class="content-nopost"><?php echo JText::_('PLG_GROUPS NO GROUP JOINED'); ?></span>
		        </div>
			<?php
			}
			?>
			<div class="clr"></div>
			</div>
			
			<?php if($my->id == $userId && $creategroups ) : ?>
			<div class="app-box-footer">	
				<a href="<?php echo CRoute::_('index.php?option=com_community&view=groups&task=create');?>" style="text-indent: 0;">
					<?php echo JText::_('PLG_GROUPS CREATE NEW GROUP');?>
				</a>
				<div class="clr"></div>
			</div>
			<?php endif; ?>
			<div class="app-box-footer">
				<a href="<?php echo CRoute::_('index.php?option=com_community&view=groups&userid=' . $my->id );?>" style="text-indent: 0;"><?php echo JText::_('PLG_GROUPS SHOW ALL GROUPS'); ?></a>
				<div class="clr"></div>
			</div>
	<?php
			$contents	= ob_get_contents();
			ob_end_clean();
			return $contents;
		}
	}	
}