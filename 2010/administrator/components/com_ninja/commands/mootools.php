<?php defined( 'KOOWA' ) or die( 'Restricted access' );
/**
 * @version		$Id: mootools.php 434 2010-08-17 15:32:50Z stian $
 * @category	Napi
 * @copyright	Copyright (C) 2007 - 2010 NinjaForge. All rights reserved.
 * @license		GNU GPLv2 <http://www.gnu.org/licenses/old-licenses/gpl-2.0.html>
 * @link	 	http://ninjaforge.com
 */

class ComNinjaCommandMootools extends KEventHandler 
{ 
	public function onDispatcherAfterDispatch($args) 
	{
		$document = JFactory::getDocument();
		$script = KRequest::get('session.application.scripts.mootools', 'string', false);
		if(!$script) return true;

		if(array_key_exists($script, $document->_scripts)) unset($document->_scripts[$script]);
		return true;
	}
	
	public function onApplicationAfterRender()
	{
		$document = JFactory::getDocument();
		$script = KRequest::get('session.application.scripts.mootools', 'string', false);
		$jquery = KRequest::get('session.application.scripts.jquery', 'string', false);
		if(!$script) return true;

		if(isset($document->_scripts[$script]))
		{
			$mootools = '<script type="text/javascript" src="'.$script.'"></script>';
			$jquery   = '<script type="text/javascript" src="'.$jquery.'"></script>';
			$body = str_replace(array('  '.$mootools."\n", $jquery), array('', $mootools."\n  ".$jquery), JResponse::getBody());
			JResponse::setBody($body);
			
			$prioritize = KRequest::get('session.application.mootools.prioritize', 'boolean');
			if($prioritize) $document->_scripts = array_merge(array($script => 'text/javascript'), $document->_scripts);
		}

		//if(array_key_exists($script, $document->_scripts)) unset($document->_scripts[$script]);
		return true;
	}
}