<?php defined( 'KOOWA' ) or die( 'Restricted access' );
/**
 * @version		$Id: view.php 465 2010-09-01 13:55:13Z stian $
 * @package		Ninja
 * @copyright	Copyright (C) 2010 NinjaForge. All rights reserved.
 * @license 	GNU GPLv2 <http://www.gnu.org/licenses/old-licenses/gpl-2.0.html>
 * @link     	http://ninjaforge.com
 */
 
 /**
 * View Controller
 */
class ComNinjaControllerView extends ComDefaultControllerDefault
{

	/**
	 * The redirect mesage
	 *
	 * @var string
	 */
	protected $_message;

	protected $_uploadDestination = false;
	
	/**
	 * Boolean that decides wether or not to auto encode result messages to json.
	 *
	 * If using HMVC tricks, make sure to set this pass array('auto_json_result' => false) in 
	 * the controller configuration array
	 *
	 * @var boolean
	 */
	protected $_auto_json_result = true;

	/**
	 * Constructor
	 *
	 * @param array An optional associative array of configuration settings.
	 */
	public function __construct(KConfig $config)
	{
		//If the list limit is set to 5, set it to 10
		$limit = KFactory::get('lib.joomla.application')->getCfg('list_limit');
	
		if($limit == 5) {
			$config->append(array(
				'request' => array(
					'limit' => 10
				)
			));
		}
	
		parent::__construct($config);

		//Wether or not to json encode result
		$this->_auto_json_result = $config->auto_json_result;

		// Register extra actions
		$this->registerActionAlias('disable', 'enable');
		
		//Set model states on order action in order to get the right offset
		$this->registerFunctionBefore('order' , 'loadState');
			
		//Register redirect messages
		$this->registerFunctionAfter('add',			'setMessage')
				->registerFunctionAfter('apply',		'setMessage')	
				->registerFunctionAfter('delete',		'setMessage')
				->registerFunctionAfter('disable',	'setMessage')
				->registerFunctionAfter('enable',		'setMessage')
				->registerFunctionAfter('save',		'setMessage');
				
		//Load states from state cookie
		$this->registerFunctionBefore('browse' , 'loadStateCookie')
		     ->registerFunctionBefore('read'   , 'loadStateCookie');		

		//$this->registerFunctionBefore('upload'   , 'loadState');
		
		// Check the referrer
		$this->registerFunctionBefore('cancel', 'checkReferrer');
		
		// Set the form layout
		$this->registerFunctionBefore('read', 'setLayout');
		
		if(KRequest::type() == 'FLASH') KRequest::set('post.action', KRequest::get('get.action', 'cmd'));
	}
	
	/**
	 * Push the request data into the model state
	 *
	 * @param	string		The action to execute
	 * @return	mixed|false The value returned by the called method, false in error case.
	 * @throws 	KControllerException
	 */
	public function execute($action, $data = null)
	{
		$result = parent::execute($action, $data);

		if(
			KRequest::type() == 'AJAX' &&
			KRequest::get('get.format', 'cmd') == 'json' &&
			!is_string($result) &&
			$this->_redirect_message &&
			$this->_auto_json_result
		) {
			$data = is_object($result) && method_exists($result, 'getData') ? $result->getData() : $result;
			//@TODO get rid of msg legacy, use message in the future
			$result = json_encode(array(
				'msg' => $this->_redirect_message,
				'message' => $this->_redirect_message,
				'result' => $data
			));
			
			//Workaround for KDispatcherAbstract::_actionForward
			//@TODO submit patch, or similar for this limitation
			//@see KDispatcheAbstract line 43 - 45 where the forward is made if request isn't POST
			//@see 	also, KDispatcherAbstract::_actionForward shouldn't fire an read or browse action
			//		if the context->result is set
			if(KRequest::method() != 'GET') {
				$identifier = clone $this->getIdentifier();
				$identifier->path = array();
				$identifier->name = 'dispatcher';
				$dispatcher = KFactory::get($identifier)->unregisterFunctionAfter('dispatch', 0);
			}
		}

		return $result;
	}
	
	/**
     * Initializes the default configuration for the object
     *
     * Called from {@link __construct()} as a first step of object instantiation.
     *
     * @param 	object 	An optional KConfig object with configuration options.
     * @return void
     */
    protected function _initialize(KConfig $config)
    {
    	$config->append(array(
    		'auto_json_result'	=> true,
        ));

        parent::_initialize($config);
    }
	
	/**
	 * Display the view
	 *
	 * @return void
	 */
	public function displayView(KCommandContext $context)
	{
		$view = KFactory::get($this->getView());

		if($view instanceof KViewTemplate) {
			KFactory::get($view->getTemplate())->addFilters(array(KFactory::get('admin::com.ninja.template.filter.document')));
		}
		
		return parent::displayView($context);
	}
	
	/**
	 * Sets the layout to 'form' if view is singular
	 *
	 * @author stian didriksen <stian@ninjaforge.com>
	 * @return void
	 */
	public function setLayout()
	{
		if(!KRequest::has('get.layout') && KInflector::isSingular(KRequest::get('get.view', 'cmd')))
		{
			KRequest::set('get.layout', 'form');
		}
	}
	
	/**
	 * Filter that handles loading of the model state from the session
	 *
	 * @return void
	 */
	public function loadStateCookie()
	{
		$model   = KFactory::get($this->getModel());
		$identifier = $model->getIdentifier();
		$id 	 = implode('-', array($identifier->type, $identifier->package, $identifier->name));
		if(!$state = KRequest::get('cookie.' . $id, 'string', false)) return;
		$request 	= KRequest::get('request', 'string');
		$state 		= json_decode($state, true);
		
		//Set the state in the model
		$model->set( KHelperArray::merge($state, $request));
	}

	
	/**
	 * Generic method to modify the order level of items
	 *
	 * @return KDatabaseRow 	A row object containing the reordered data
	 */
	protected function _actionOrder()
	{
		$order	= KRequest::get('post.order', 'int');
		$model	= KFactory::get($this->getModel());
		$offset	= $model->getState()->offset;
		$table	= KFactory::get($model->getTable());
		
		//Change the order
		foreach (json_decode(KRequest::get('post.ordering', 'string')) as $ordering)
		{
			$row = $table
				->select($ordering->id)->setData(array('ordering' => $ordering->ordering+$offset))->save();
		}
		
		if(in_array('lft', $table->getColumns())) {
			$table->fetchRow()->rebuild();
		}

		return $row;
	}
	
	/**
	 * Duplicate a row item
	 *
	 * @return KDatabaseRow 	The new, duplicated row object
	 */
	protected function _actionDuplicate()
	{
		$order	= KRequest::get('post.order', 'int');
		$model	= KFactory::get($this->getModel());
		$offset	= $model->getState()->offset;
		$table	= KFactory::get($model->getTable());
		
		//Change the order
		foreach (json_decode(KRequest::get('post.ordering', 'string')) as $ordering)
		{
			$row = $table
				->select($ordering->id)->setData(array('ordering' => $ordering->ordering+$offset))->save();
		}
		
		if(in_array('lft', $table->getColumns())) {
			$table->fetchRow()->rebuild();
		}

		return $row;
	}
	
	/**
	 * Generic method to set model states
	 *
	 * @return KDatabaseRow 	A row object containing the reordered data
	 */
	protected function _setModelState()
	{
		parent::_setModelState();

		if($search = KRequest::get('post.search', 'string')) $this->getModel()->setState('search', $search);
		return $this;
	}
	
	/**
	 * Method to extract the name of a discreet installation sql file from the installation manifest file.
	 *
	 * @access	protected
	 * @param	array	$files 	The object to process
	 * @return	mixed	Number of queries processed or False on error
	 * @since	1.5
	 */
	protected function parseSQLFiles($files, $path = null, $driver = 'mysql', $charset = 'utf8')
	{
		// Initialize variables
		$files = (array) $files;
		if ( !$path ) $path = dirname(KFactory::tmp('lib.joomla.application')->getpath('admin')) . DS . 'sql';
		$db = KFactory::get('lib.joomla.database');
		$dbDriver = strtolower($db->get('name'));
		if ($dbDriver == 'mysqli') {
			$dbDriver = 'mysql';
		}
		$dbCharset = ($db->hasUTF()) ? 'utf8' : '';

		// Get the name of the sql file to process
		foreach ($files as $file)
		{
			if( $charset == $dbCharset && $driver == $dbDriver) {
				// Check that sql files exists before reading. Otherwise raise error for rollback
				if ( !file_exists( $path.DS.$file ) ) {
					return false;
				}
				$buffer = file_get_contents($path.DS.$file);
				
				// Graceful exit and rollback if read not successful
				if ( $buffer === false ) {
					return false;
				}

				// Create an array of queries from the sql file
				jimport('joomla.installer.helper');
				$queries = JInstallerHelper::splitSql($buffer);
				if (count($queries) == 0) {
					// No queries to process
					return 0;
				}

				// Process each query in the $queries array (split out of sql file).
				foreach ($queries as $query)
				{
					$query = trim($query);
					if ($query != '' && $query{0} != '#') {
						$db->setQuery($query);
						
						if (!$db->query()) {
							JError::raiseWarning(1, JText::_('SQL Error')." ".$db->stderr(true));
							return false;
						}
					}
				}
			}
		}

		return (int) count($queries);
	}
	
	/**
	 * Filter that gets the redirect URL from the sesison and sets it in the
	 * controller
	 *
	 * @return void
	 */
	public function setMessage(KCommandContext $context)
	{
		$defaults = array('url' => null, 'message' => null);
		$redirect = array_merge($defaults, $this->getRedirect());
		if(!$redirect['message'])
		{
			$message = new KObject;
			$message->count = count((array) KRequest::get('post.id', 'int', 1));
			$message->action=  ' ' . KFactory::tmp('admin::com.ninja.helper.inflector')->verbalize(KRequest::get('post.action', 'cmd', $this->getAction())) . '.';
			$message->name	= KFactory::get($this->getModel())->getIdentifier()->name;

			$message->singular = KInflector::humanize(KInflector::singularize($message->name)) . $message->action;
			$message->plural   = KInflector::humanize(KInflector::pluralize($message->name)) . $message->action;
			
			$redirect['message'] = sprintf(JText::_($message->count > 1 ? '%s ' . $message->plural : $message->singular), $message->count);
			$this->setRedirect($redirect['url'], $redirect['message']);
		}
	}
	
	
	public function setPermissions(ArrayObject $args)
	{
		$identifier = $this->getIdentifier();
		$prefix		= $identifier->type.'_'.$identifier->package.'.'.$identifier->name.'.';
		$data		= $args['result']->getData();
		
		$data = array(
			'name' => $prefix.$data['id'],
			'title' => $data['title'],
			'rules'=> json_encode($data['access'])
		);

		$assets	= KFactory::get('admin::com.ninja.helper.access')->models->assets; 
		$model	= KFactory::get($assets);
		$table  = KFactory::get($model->getTable());
		
		$query = KFactory::get('lib.koowa.database.query')->where('name', '=', $data['name']);
		$table
			->fetchRow($query)
			->setData($data)
			->save();
		
		
	}
	
	/**
	 * Get's the redirect URL from the referrer and saves in the session.
	 *
	 * @return void
	 */
	public function checkReferrer()
	{
		$referrer = KRequest::get('session.com.dispatcher.referrer', 'raw');
		$referrer = KFactory::tmp('lib.koowa.http.uri', array('uri' => $referrer))->getQuery(1);
		
		$current  = KRequest::get('get.view', 'cmd');
		$view	  = $current;
		if(isset($referrer['view'])) $view = $referrer['view'];
		
		if($current != $view) return;
		
		$view  = KInflector::pluralize($view);
		$query = array_merge(KRequest::url()->getQuery(1), array('view' => $view));
		if(isset($query['id'])) unset($query['id']);
		KRequest::url()->query = $query;

		KRequest::set('session.com.dispatcher.referrer', (string)KRequest::url());
	}
	
	public function getUploadDestination()
	{
		return false;
	}
	
	protected function _actionUpload()
	{
		if(!$destination = $this->getUploadDestination()) return;
		jimport('joomla.filesystem.file');

		$result = array();
		
		$result['time'] = date('r');
		$result['addr'] = substr_replace(gethostbyaddr($_SERVER['REMOTE_ADDR']), '******', 0, 6);
		$result['agent'] = $_SERVER['HTTP_USER_AGENT'];
		
		if (count($_GET)) {
			$result['get'] = $_GET;
		}
		if (count($_POST)) {
			$result['post'] = $_POST;
		}
		if (count($_FILES)) {
			$result['files'] = $_FILES;
		}
		
		// Validation
		
		$error = false;
		
		if (!isset($_FILES['Filedata']) || !is_uploaded_file($_FILES['Filedata']['tmp_name'])) {
			$error = 'Invalid Upload';
		}
		
		
		// Processing
		
		/**
		 * Its a demo, you would move or process the file like:
		 *
		 * move_uploaded_file($_FILES['Filedata']['tmp_name'], '../uploads/' . $_FILES['Filedata']['name']);
		 * $return['src'] = '/uploads/' . $_FILES['Filedata']['name'];
		 *
		 */

		$upload = $destination.$_FILES['Filedata']['name'];
		$uploaddir = dirname(JPATH_ROOT.$destination.$_FILES['Filedata']['name']);
		JFile::upload($_FILES['Filedata']['tmp_name'], $uploaddir.'/avatar.png');
		KRequest::set('files.uploaded', dirname($upload).'/avatar.png', 'string');

		if ($error) {
		
			$return = array(
				'status' => '0',
				'error' => $error
			);
		
		} else {
		
			$return = array(
				'status' => '1',
				'name' => $_FILES['Filedata']['name'],
				'tmp_name' => $_FILES['Filedata']['tmp_name'],
				'destination' => $destination,
				'uploaded' => KRequest::root().KRequest::get('files.uploaded', 'string'),
				'test' => JPATH_ROOT.KRequest::get('files.uploaded', 'string')
			);
		
			// Our processing, we get a hash value from the file
			$return['hash'] = @md5_file(realpath(JPATH_ROOT.$destination.$_FILES['Filedata']['name']));
			
			// ... and if available, we get image data
			$info = @getimagesize(JPATH_ROOT.KRequest::get('files.uploaded', 'string'));
		
			if ($info) {
				$return['width'] = $info[0];
				$return['height'] = $info[1];
				$return['mime'] = $info['mime'];
			}
		
		}
		
		
		// Output
		
		/**
		 * Again, a demo case. We can switch here, for different showcases
		 * between different formats. You can also return plain data, like an URL
		 * or whatever you want.
		 *
		 * The Content-type headers are uncommented, since Flash doesn't care for them
		 * anyway. This way also the IFrame-based uploader sees the content.
		 */
		
		if (isset($_REQUEST['response']) && $_REQUEST['response'] == 'xml') {
			// header('Content-type: text/xml');
		
			// Really dirty, use DOM and CDATA section!
			echo '<response>';
			foreach ($return as $key => $value) {
				echo "<$key><![CDATA[$value]]></$key>";
			}
			echo '</response>';
		} else {
			// header('Content-type: application/json');
			echo json_encode($return);
		}
		
		//KFactory::get('lib.koowa.application')->close();		
	}
	
	/**
	 * Get the real action that is was/will be performed relevant for acl checks.
	 *
	 * @return	 string Action name
	 */
	public function getRealAction()
	{
		$action = $this->getAction();
		if(empty($action))
		{
			switch(KRequest::method())
			{
				case 'GET'    :
				{
					//Determine if the action is browse or read based on the view information
					$view   = KRequest::get('get.view', 'cmd');
					$action = KInflector::isPlural($view) ? 'browse' : 'read';	
				} break; 
				
				case 'POST'   :
				{
					//If an action override exists in the post request use it
					if(!$action = KRequest::get('post.action', 'cmd')) {
						$action = 'add';
					}	
				} break;
				
				case 'PUT'    : $action = 'edit'; break;
				case 'DELETE' : $action = 'delete';	break;
			}
		}
		if($action == 'apply') $action = 'save';
		if($action == 'save')  $action = (bool) KRequest::get('get.id', 'int') ? 'edit' : 'add';

		return $action;
	}

	/**
	 * Enable/Disable action
	 *
	 * @TODO this is legacy, refactor toolbar to support the new RESTful structure in koowa controllers
	 */
	protected function _actionEnable($data)
	{
		$data['enabled'] = $this->getAction() == 'enable';

		return $this->execute('edit', $data);
	}
}