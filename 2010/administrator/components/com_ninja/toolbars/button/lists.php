<?php defined( 'KOOWA' ) or die( 'Restricted access' );
/**
 * @version		$Id: lists.php 434 2010-08-17 15:32:50Z stian $
 * @category	NinjaForge Plugin Manager
 * @copyright	Copyright (C) 2007 - 2010 NinjaForge. All rights reserved.
 * @license		GNU GPLv2 <http://www.gnu.org/licenses/old-licenses/gpl-2.0.html>
 * @link     	http://ninjaforge.com
 */

/**
 * Install button class for a toolbar
 * 
 * @author		Stian Didriksen <stian@ninjaforge.com>
 * @category	Napi
 * @package		Napi_Toolbar
 * @subpackage	Button
 */
class ComNinjaBoardToolbarButtonLists extends NToolbarButtonLink
{
	public function __construct(KConfig $options)
	{
		$options->text	= 'Lists';
		parent::__construct($options);
	}
	
	public function getLink()
	{
		$query['view']	= 'lists';
		return parent::getLink($query);
	}
	public function render()
	{
		return parent::render();
	}
}