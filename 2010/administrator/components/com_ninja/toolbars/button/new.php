<?php defined( 'KOOWA' ) or die( 'Restricted access' );
/**
* @version      $Id: new.php 434 2010-08-17 15:32:50Z stian $
* @category		Napi
* @package		Napi_Toolbar
* @subpackage	Button
* @copyright	Copyright (C) 2010 NinjaForge. All rights reserved.
* @license      GNU GPLv2 <http://www.gnu.org/licenses/old-licenses/gpl-2.0.html>
*/

/**
 * New
 *
 * Takes you to a �Add Item� form
 * 
 * @author		Stian Didriksen <stian@ninjaforge.com>
 * @category	Napi
 * @package		Napi_Toolbar
 * @subpackage	Button
 */
class ComNinjaToolbarButtonNew extends ComNinjaToolbarButtonAbstract
{
	public function render()
	{				
		$option = KRequest::get('get.option', 'cmd');
		$view	= KInflector::singularize(KRequest::get('get.view', 'cmd'));
		$link	= 'index.php?option='.$option.'&view='.$view;

		$this->attribs->set(array(
			'class' => 'toolbar',
			'href'  => $this->_parent->createRoute($link)
		));
		
		return parent::render();
	}
}