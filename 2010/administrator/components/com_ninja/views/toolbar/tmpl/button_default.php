<? /** $Id: button_default.php 347 2010-06-10 20:24:50Z stian $ */ ?>
<? defined( 'KOOWA' ) or die( 'Restricted access' ) ?>

<? if($img = KFactory::get('admin::com.ninja.helper')->img('/32/'.@$name.'.png')) : ?>

	<? KFactory::get('admin::com.ninja.helper')->css('#toolbar-box .icon-32-'.@$name.' { background-image: url('.$img.'); }') ?>
	
<? endif ?>

<td class="button" id="<?= @$id ?>">
	<a <?= KHelperArray::toString(@$attribs)  ?>>
		<span class="icon-32-<?= @$name ?>"></span>
		<?= @text(@$text) ?>
	</a>
</td>