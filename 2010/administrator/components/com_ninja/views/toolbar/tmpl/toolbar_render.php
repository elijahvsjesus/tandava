<? /** $Id: toolbar_render.php 369 2010-06-23 12:48:36Z stian $ */ ?>
<? defined( 'KOOWA' ) or die( 'Restricted access' ) ?>

<div class="toolbar" id="toolbar-<?= KFactory::get($this->getView())->getName() ?>">
	<table class="toolbar">
		<tr>
			<? foreach($buttons as $button) : ?>
				<?= $button->render() ?>
			<? endforeach ?>
		</tr>
	</table>
</div>