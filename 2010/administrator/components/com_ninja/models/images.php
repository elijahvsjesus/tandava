<?php defined( 'KOOWA' ) or die( 'Restricted access' );
/**
 * @version		$Id: images.php 434 2010-08-17 15:32:50Z stian $
 * @category	Ninja
 * @copyright	Copyright (C) 2007 - 2010 NinjaForge. All rights reserved.
 * @license		GNU GPLv2 <http://www.gnu.org/licenses/old-licenses/gpl-2.0.html>
 * @link     	http://ninjaforge.com
 */

class ComNinjaModelImages extends KModelAbstract
{

	/**
	 * Array of the images
	 *
	 * @var array
	 */
	protected $_images = array();

	public function __construct(KConfig $options)
    {
    	parent::__construct($options);
    	
    	//lets get our states
       	$this->_state
       	     ->insert('folder', 'dirname', JPATH_ROOT.'/images/stories/')
       	     ->insert('optgroup', 'boolean', true);
    }
        
    public function getItem()
    {
    	return $this->_item;
    }
    
    public function getList()
    {
    	if(!isset($this->_list))
    	{
    		jimport( 'joomla.filesystem.folder' );
			jimport( 'joomla.filesystem.file' );
	
			// path to images directory
			$identifier = $this->getIdentifier();
			$path		= $this->_state->folder;
			$filter		= '\.png$|\.gif$|\.jpg$|\.bmp$|\.ico$';
			$uripath	= str_replace(JPATH_ROOT, KRequest::root(), $path);

			$root 		=  basename($path);
			if(!JFolder::exists($path)) JFolder::create($path);
			$files		= JFolder::files($path, $filter, true, true);
			$this->_list = array ();
			$optgroup = $root;
			if ( is_array($files) )
			{
				foreach ($files as $file)
				{
					if (($f = basename(dirname($file))) !== $optgroup)
					{
						if($this->_state->optroup) $this->_list[] = JHTML::_('select.optgroup', $f);
					}
					$filepath = str_replace($root.'/', '', $f.'/');
					$this->_list[] = JHTML::_('select.option', $filepath . basename($file), basename($file));
					$this->_images[]	   = $uripath . '/' . $filepath . basename($file);
					if ($f !== $optgroup) $optgroup = $f;
				}
			}
		}
    
    	return $this->_list;
    }
    
    public function getImages()
    {
    	if(!isset($this->_images)) parent::getList();

		return $this->_images;
    }
}