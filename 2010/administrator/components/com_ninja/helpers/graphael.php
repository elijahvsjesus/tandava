<?php defined( 'KOOWA' ) or die( 'Restricted access' );
/**
 * @version		$Id: graphael.php 434 2010-08-17 15:32:50Z stian $
 * @package		Ninja
 * @copyright	Copyright (C) 2010 NinjaForge. All rights reserved.
 * @license 	GNU GPLv2 <http://www.gnu.org/licenses/old-licenses/gpl-2.0.html>
 * @link     	http://ninjaforge.com
 */
 
 /**
 * Class for creating g.raphael graphics (charts mostly)
 *
 * @author		Stian Didriksen <stian@ninjaforge.com>
 */
class ComNinjaHelperGraphael extends KTemplateHelperAbstract
{
	/**
	 * Constructor
	 *
	 * @param array An optional associative array of configuration settings.
	 */
	public function __construct(KConfig $config)
	{
		parent::__construct($config);
	
		KFactory::get('admin::com.ninja.helper')->js('/raphael.js');
		KFactory::get('admin::com.ninja.helper')->js('/g.raphael.js');
	}
	
	/**
	 * Renders a piechart
	 *
	 * @param array An optional associative array of configuration settings.
	 */
	public function piechart($config = array())
	{
		KFactory::get('admin::com.ninja.helper')->js('/g.pie.js');
	}
}