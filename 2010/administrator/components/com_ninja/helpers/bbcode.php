<?php defined( 'KOOWA' ) or die( 'Restricted access' );

require_once dirname(__FILE__) . '/bbcode/stringparser_bbcode.class.php';

/**
 * Shortcut to bbcode
 * 
 * General usage: KFactory::tmp('admin::com.ninja.helper.htmlpurifier')->purify($html);
 *
 * Usage in template layouts: <?= @$helper('htmlpurifier.purify', @template('default_items')) ?>
 *
 * You can configure HTMLPurifier during construct or as a second parameter in the function() method.
 *
 * Factory: KFactory::tmp('admin::com.ninja.helper.htmlpurifier', array('HTML.Doctype' => 'XHTML 1.0 Transitional'))->purify($html);
 *
 * Static: NinjaHelperHtmlpurifier::purify($html, array('HTML.Doctype' => 'XHTML 1.0 Transitional'));
 *
 * HTMLPurifier docs here: http://htmlpurifier.org/docs
 *
 * @author Stian Didriksen
 */


// Unify line breaks of different operating systems
function convertlinebreaks ($text) {
    return preg_replace ("/\015\012|\015|\012/", "\n", $text);
}

// Remove everything but the newline charachter
function bbcode_stripcontents ($text) {
    return preg_replace ("/[^\n]/", '', $text);
}

// Function to include images
function do_bbcode_img ($action, $attributes, $content, $params, $node_object) {
    if ($action == 'validate') {
        if (substr ($content, 0, 5) == 'data:' || substr ($content, 0, 5) == 'file:'
          || substr ($content, 0, 11) == 'javascript:' || substr ($content, 0, 4) == 'jar:') {
            return false;
        }
        return true;
    }
    return '<img src="'.htmlspecialchars($content).'" alt="">';
}

class ComNinjaHelperBbcode extends StringParser_BBCode implements KTemplateHelperInterface
{
	/**
	 * The object identifier
	 * 
	 * Public access is allowed via __get() with $identifier. The identifier
	 * is only available of the object implements the KObjectIndetifiable
	 * interface
	 *
	 * @var KIdentifierInterface
	 */
	protected $_identifier;

	/**
	 * Constructor.
	 *
	 * @param 	object 	An optional KConfig object with configuration options
	 */
	public function __construct( KConfig $config = null) 
	{ 
		//Set the identifier before initialise is called
		if($this instanceof KObjectIdentifiable) {
			$this->_identifier = $config->identifier;
		}
		
		if($config) {
			$this->_initialize($config);
		}
		

		$this->addFilter(STRINGPARSER_FILTER_PRE, 'convertlinebreaks');
		
		$this->addParser(array ('block', 'inline', 'link', 'listitem'), 'htmlspecialchars');
		$this->addParser(array ('block', 'inline', 'link', 'listitem'), 'nl2br');
		$this->addParser('list', 'bbcode_stripcontents');
		
		//Tables
		$this->addCode('td', 'simple_replace', null, array('start_tag' => '<td>', 'end_tag' => '</td>'), 'inline', array('listitem', 'block', 'inline', 'link'), array());
		$this->addCode('th', 'simple_replace', null, array('start_tag' => '<th>', 'end_tag' => '</th>'), 'inline', array('listitem', 'block', 'inline', 'link'), array());
		$this->addCode('tr', 'simple_replace', null, array('start_tag' => '<tr>', 'end_tag' => '</tr>'), 'inline', array('listitem', 'block', 'inline', 'link'), array());
		$this->addCode('table', 'simple_replace', null, array('start_tag' => '<table>', 'end_tag' => '</table>'), 'inline', array('listitem', 'block', 'inline', 'link'), array());
		
		$this->addCode('b', 'simple_replace', null, array('start_tag' => '<strong>', 'end_tag' => '</strong>'), 'inline', array('listitem', 'block', 'inline', 'link'), array());
		$this->addCode('u', 'simple_replace', null, array ('start_tag' => '<span class="underline" style="text-decoration:underline;">', 'end_tag' => '</span>'), 'inline', array ('listitem', 'block', 'inline', 'link'), array ());
		$this->addCode('i', 'simple_replace', null, array ('start_tag' => '<em>', 'end_tag' => '</em>'), 'inline', array ('listitem', 'block', 'inline', 'link'), array ());
		$this->addCode('url', 'usecontent?', array($this, 'replaceURL'), array('usecontent_param' => 'default'), 'link', array('listitem', 'block', 'inline'), array ('link'));
		$this->addCode('link', 'callback_replace_single', array($this, 'replaceURL'), array(), 'link', array('listitem', 'block', 'inline'), array('link'));
		$this->addCode('color', 'usecontent?', array($this, 'replaceColor'), array('usecontent_param' => 'default'), 'link', array ('listitem', 'block', 'inline'), array('color'));
		$this->addCode('img', 'usecontent', 'do_bbcode_img', array (), 'image', array ('listitem', 'block', 'inline', 'link'), array ());
		$this->addCode('bild', 'usecontent', 'do_bbcode_img', array (), 'image', array ('listitem', 'block', 'inline', 'link'), array ());
		$this->setOccurrenceType('img', 'image');
		$this->setOccurrenceType('bild', 'image');
		$this->addCode('quote', 'simple_replace', null, array('start_tag' => '<blockquote>', 'end_tag' => '</blockquote>'), 'inline', array('listitem', 'block', 'inline', 'link'), array());
		$this->addCode('code', 'simple_replace', null, array('start_tag' => '<pre>', 'end_tag' => '</pre>'), 'inline', array('listitem', 'block', 'inline', 'link'), array());
		//$this->setMaxOccurrences ('image', 2);
		$this->addCode('list', 'simple_replace', null, array ('start_tag' => '<ul>', 'end_tag' => '</ul>'), 'list', array ('block', 'listitem'), array ());
		$this->addCode('*', 'simple_replace', null, array ('start_tag' => '<li>', 'end_tag' => '</li>'), 'listitem', array ('list'), array ());
		$this->setCodeFlag('*', 'closetag', BBCODE_CLOSETAG_OPTIONAL);
		$this->setCodeFlag('*', 'paragraphs', true);
		$this->setCodeFlag('list', 'paragraph_type', BBCODE_PARAGRAPH_BLOCK_ELEMENT);
		$this->setCodeFlag('list', 'opentag.before.newline', BBCODE_NEWLINE_DROP);
		$this->setCodeFlag('list', 'closetag.before.newline', BBCODE_NEWLINE_DROP);
		$this->setRootParagraphHandling(true);
	}
	
	/**
	 * Initializes the options for the object
	 *
	 * Called from {@link __construct()} as a first step of object instantiation.
	 *
	 * @param 	object 	An optional KConfig object with configuration options.
	 * @return 	void
	 */
	protected function _initialize(KConfig $config)
	{
		//do nothing
	}
	
	/**
	 * Get the object identifier
	 * 
	 * @return	KIdentifier	
	 */
	public function getIdentifier()
	{
		return $this->_identifier;
	}
	
	public function replaceURL($action, $attributes, $content, $params, $node_object)
	{
	    if (!isset ($attributes['default'])) {
	        $url = $content;
	        $text = htmlspecialchars ($content);
	    } else {
	        $url = $attributes['default'];
	        $text = $content;
	    }
	    if ($action == 'validate') {
	        if (substr ($url, 0, 5) == 'data:' || substr ($url, 0, 5) == 'file:'
	          || substr ($url, 0, 11) == 'javascript:' || substr ($url, 0, 4) == 'jar:') {
	            return false;
	        }
	        return true;
	    }
	    return '<a href="'.htmlspecialchars ($url).'">'.$text.'</a>';
	}
	
	public function replaceColor($action, $attributes, $content, $params, $node_object)
	{
	    if (!isset($attributes['default'])) {
	        $color = $content;
	        $text = htmlspecialchars ($content);
	    } else {
	        $color = $attributes['default'];
	        $text = $content;
	    }
	    return '<span style="color:'.htmlspecialchars ($color).'">'.$text.'</span>';
	}
	
	/**
	 * A wrapper arond the parser function so it works with the koowa helpers
	 *
	 * @author Stian Didriksen
	 */
	public function parse($config = array())
	{
		return parent::parse($config['text']);
	}
}