<?php defined( 'KOOWA' ) or die( 'Restricted access' );
/**
 * @version		$Id: date.php 434 2010-08-17 15:32:50Z stian $
 * @package		Ninja
 * @copyright	Copyright (C) 2010 NinjaForge. All rights reserved.
 * @license 	GNU GPLv2 <http://www.gnu.org/licenses/old-licenses/gpl-2.0.html>
 * @link     	http://ninjaforge.com
 */
 
 /**
 * Date helper, gives you things like facebook style '2 hours ago'
 *
 * @author		Stian Didriksen <stian@ninjaforge.com>
 */
class ComNinjaHelperDate extends KTemplateHelperAbstract
{

	/**
	 * Gives facebook style, more human readable datetime presentations
	 *
	 * Past example: 2 days ago
	 *
	 * Future example: 35 seconds from now
	 *
	 * @author Stian Didriksen <stian@ninjaforge.com>
	 * @param $date in datetime format
	 * @return string
	 */
	public function beautiful($config = array())//$date, $title = true)
	{
		$config = new KConfig($config);
		
		$config->append(array(
			'date'	=> null,
			'title'	=> true
		));
		
	    if(empty($config->date)) return JText::_('No date provided');
	    
	    //@TODO this is legacy, cleanup this stuff
	    $date	= $config->date;
	    $title	= $config->date;
	    
	    $periods         = array('second', 'minute', 'hour', 'day', 'week', 'month', 'year');
	    $lengths         = array(60, 60, 24, 7, 4.35, 12, 10);
	
	    $now             = strtotime(gmdate('Y-m-d H:i:s'));
	    $unix_date       = strtotime($date);		
		
		// check validity of date
	    if(empty($unix_date)) return;
	
	    // is it future date or past date
	    if($now >= $unix_date)
	    {    
	        $difference	= $now - $unix_date;
	        $tense		= 'ago';
	    }
	    else
	    {
	        $difference	= $unix_date - $now;
	        $tense		= 'from now';
	    }
	    
	    for($i = 0; $difference >= $lengths[$i] && $i < 6; $i++)
	    {
	        $difference /= $lengths[$i];
	    }
	    $difference = round($difference);
	    
	    if($difference != 1) $periods[$i].= 's';

		$html  = sprintf(JText::_('%s '.$periods[$i].' '.$tense), $difference);
		
		if($title) return '<span title="'.$this->format($date).'">' . $html . '</span>';
		
		return  $html;
	}
	
	/**
	 * Formats the date according to the current locale and timezone offset
	 *
	 * @author Stian Didriksen <stian@ninjaforge.com>
	 * @param $date in datetime format
	 * @return string
	 */
	public function format($date, $format = false, $offset = false)
	{
		if(!$offset)
		{
			$user	= KFactory::get('lib.koowa.user');
			$offset	= $user->_params->get('timezone', KFactory::get('lib.joomla.config')->getValue('offset'));
		}
		
		if(!$format) $format = JText::_('DATE_FORMAT_LC2');
		
		return KFactory::get('lib.koowa.template.helper.date')->format(array(
			'date'		=>	$date,
			'format'	=>	$format,
			'offset'	=>	$offset
		));
	}
}