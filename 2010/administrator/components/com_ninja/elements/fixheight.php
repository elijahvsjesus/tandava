<?php defined( 'KOOWA' ) or die( 'Restricted access' );
/**
 * @version		$Id: fixheight.php 434 2010-08-17 15:32:50Z stian $
 * @category	Napi
 * @package		Napi_Parameter
 * @copyright	Copyright (C) 2007 - 2010 NinjaForge. All rights reserved.
 * @license		GNU GPLv2 <http://www.gnu.org/licenses/old-licenses/gpl-2.0.html>
 * @link     	http://ninjaforge.com
 */

class ComNinjaElementFixHeight extends ComNinjaElementAbstract
{
	public function fetchElement($name, $value, &$node, $control_name)
	{
		
		$html = "\n".'<button name="'.$name.'" id="'.$control_name.$this->group.$name.'" type="button" onclick="update(this)">'.JText::_((string)$node['label']).'</button>';

		return $html;
	}
}
