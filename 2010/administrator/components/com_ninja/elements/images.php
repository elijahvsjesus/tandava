<?php defined( 'KOOWA' ) or die( 'Restricted access' );
/**
 * @version		$Id: images.php 434 2010-08-17 15:32:50Z stian $
 * @category	Napi
 * @package		Napi_Parameter
 * @copyright	Copyright (C) 2007 - 2010 NinjaForge. All rights reserved.
 * @license		GNU GPLv2 <http://www.gnu.org/licenses/old-licenses/gpl-2.0.html>
 * @link     	http://ninjaforge.com
 */

class ComNinjaElementImages extends ComNinjaElementAbstract
{
	public function fetchElement($name, $value, &$node, $control_name)
	{
		$path = 'images/stories';
		if(isset($this->node['path'])) $path = (string) $this->node['path'];

		return KFactory::get('admin::com.ninja.helper.select')->images(array('path' => JPATH_ROOT.'/'.$path, 'name' => $this->name, 'selected' => $value, 'id' => $this->id));
	}
}
