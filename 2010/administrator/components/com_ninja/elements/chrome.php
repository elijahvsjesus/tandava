<?php defined( 'KOOWA' ) or die( 'Restricted access' );
/**
 * @version		$Id: chrome.php 434 2010-08-17 15:32:50Z stian $
 * @category	Napi
 * @package		Napi_Parameter
 * @copyright	Copyright (C) 2007 - 2010 NinjaForge. All rights reserved.
 * @license		GNU GPLv2 <http://www.gnu.org/licenses/old-licenses/gpl-2.0.html>
 * @link     	http://ninjaforge.com
 */

class ComNinjaElementChrome extends ComNinjaElementAbstract
{
	public function fetchElement($name, $value, &$node, $control_name)
	{
		$options = KFactory::get('admin::com.ninja.model.module_chrome')->client(0)->optgroup('<OPTGROUP>')->getList();	

		return JHTML::_('select.genericlist', $options, $this->name, array('class' => 'value'), 'id', 'title', $value, $this->id, false);
	}
}
