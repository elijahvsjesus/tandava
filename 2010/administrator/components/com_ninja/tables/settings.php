<?php defined( 'KOOWA' ) or die( 'Restricted access' );
/**
 * @version		$Id: settings.php 434 2010-08-17 15:32:50Z stian $
 * @category	Ninja
 * @copyright	Copyright (C) 2007 - 2010 NinjaForge. All rights reserved.
 * @license		GNU GPLv2 <http://www.gnu.org/licenses/old-licenses/gpl-2.0.html>
 * @link     	http://ninjaforge.com
 */

class ComNinjaTableSettings extends KDatabaseTableDefault
{
	/**
	 * The path to the settings xml
	 *
	 * @var dir
	 */
	protected $_xml_path;
	
	/**
	 * Array over xml defaults, separated by path to avoid the same xml doc being parsed queried more than once
	 *
	 * @var array
	 */
	protected $_xml_defaults_cache = array();

	public function __construct(KConfig $config)
	{
		$config->append(array(
			'filters'	=> array(
				'params' => 'json'
			)
		));
	
		parent::__construct($config);


		$this->_xml_path = $config->xml_path;

	   		 
	   	foreach($this->getColumns() as $field)
	   	{
	   		if($field->name == 'default')
	   		{
	   			$field->unique = 1;
	   			break;
	   		}
	   		
	   	}
	}
	
	/**
	 * Initializes the options for the object
	 *
	 * Called from {@link __construct()} as a first step of object instantiation.
	 *
	 * @param 	object 	An optional KConfig object with configuration options.
	 * @return  void
	 */
	protected function _initialize(KConfig $config)
	{
		$package = $this->_identifier->package;
		$name	 = KInflector::singularize($this->_identifier->name);
		
		$config->append(array(
			'xml_path'        => JPATH_ADMINISTRATOR.'/components/com_'.$package.'/views/'.$name.'/tmpl/'.$name.'.xml',

		));
		
		parent::_initialize($config);
	}

	/**
	 * Table select method
	 *
	 * The name of the resulting row(set) class is based on the table class name
	 * eg <Mycomp>Table<Tablename> -> <Mycomp>Row(set)<Tablename>
	 * 
	 * This function will return an empty rowset if called without a parameter.
	 *
	 * @param	mixed	KDatabaseQuery, query string, array of row id's, or an id or null
	 * @param 	integer	The database fetch mode. Default FETCH_ROWSET.
	 * @return	KDatabaseRow or KDatabaseRowset depending on the mode. By default will 
	 * 			return a KDatabaseRowset 
	 */
	public function select( $query = null, $mode = KDatabase::FETCH_ROWSET)
	{
		$result = parent::select($query, $mode);
		if($mode == KDatabase::FETCH_FIELD) return $result;

		$defaults = $this->_getDefaultsFromXML();

		
		if(is_a($result, 'KDatabaseRowInterface')) $result = array($result);
		
		
		foreach($result as $row)
		{
			$params = json_decode($row->params, true);
			if(!is_array($params)) $params = array();
			
			$params = new KConfig($params);
			$params->append($defaults);
			$row->params = $params;
		}
		
		return is_array($result) ? $result[0] : $result;
	}
	
	/**
	 * Get the default values from the xml document
	 *
	 * @author	Stian Didriksen <stian@ninjaforge.com>
	 * @return  array 	key/value data from the doc
	 */
	protected function _getDefaultsFromXML()
	{
		if(!isset($this->_xml_defaults_cache[$this->_xml_path]))
		{
			$xml		= simplexml_load_file($this->_xml_path);
			$values	= array();

			foreach($xml->children() as $i => $group)
			{
				$value = array();
				foreach($group->children() as $i => $element)
				{				
					if(!$element['default']) continue;
					$value[(string)$element['name']] = (string)$element['default'];
				}
				if(count($value) < 1) continue;
				$values[(string)$group['name']] = $value;
				
			}
			
			$this->_xml_defaults_cache[$this->_xml_path] = $values;
		}
		
		return $this->_xml_defaults_cache[$this->_xml_path];
	}
	
	/**
	 * Get the path to the xml file
	 *
	 * @return dir
	 */
	public function getXMLPath()
	{
		return $this->_xml_path;
	}
}