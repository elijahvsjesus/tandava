<?php die("Access Denied"); ?>
a:4:{s:4:"body";s:45853:"
<div class="rt-joomla ">
	<div class="rt-blog">

				<h1 class="rt-pagetitle">Ingressos para o Tandava Gathering</h1>
		
				<div class="rt-description">
										<p>O <strong>Tandava Gathering </strong>é um encontro sem fins lucrativos. Todos os custos do festival são repartidos entre os participantes.</p>
<p>O valor dos ingressos é utilizado para contratação dos artistas, compra de material para oficinas, aluguel de local e estrutura.</p>
<p>O custo total programado para a edição 2010 é de aproximadamente R$23.000,00 e por isso estão colocados a venda 425 ingressos divididos em 3 lotes:</p>
<h4>Preços</h4>
<ul class="bullet-a">
<li><span style="text-decoration: line-through;">1º Lote (175 Ingressos): R$ 50,00 <em>(Até 30 de Setembro)</em></span></li>
<li><span style="text-decoration: line-through;">2º Lote (150 Ingressos): R$ 60,00 <em>(Até 31 de Outubro)</em></span></li>
<li>3º e <strong>último</strong> Lote (100 Ingressos): R$ 70,00 <em>(Até 12 de Novembro)</em></li>
</ul>
<p>Faça parte do <strong>Tandava Gathering 2010</strong>, confira abaixo como adquirir os ingressos.</p>
<blockquote>
<p>"Não&nbsp; é&nbsp;nada melhor ou maior de tudo que você&nbsp;já&nbsp;viu."</p>
</blockquote>
<p><cite><a href="index.php?option=com_content&amp;view=section&amp;id=6&amp;Itemid=164">Tandava  Gathering</a></cite>, Edição 2009.</p><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://elijah.com.br/tandava/2010/ingressos.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>					</div>
		
				<div class="rt-leading-articles">
										
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/ingressos/86-onde-comprar.html">Onde Comprar</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<div class="rt-article-content">
<p>Em Curitiba os ingressos podem ser encontrados na loja <strong>Sica Moda Alternativa</strong>:</p>
<ul class="bullet-a">
<li><strong>Loja Sica Moda Alternativa</strong><br />Rua Trajano Reis, 111 - São Francisco.<br />Horário de Atend.: Terça a Sexta 11h às 19h / Sábado e Domingo 11h às 16h.</li>
</ul>
<p>Muitos dos núcleos que iniciaram o Tandava continuam colaborando nessa edição, desta vez no papel de parceiros. Espalhados por diversas localidades eles garantem uma vasta distribuição dos ingressos além de Curitiba.</p>
<p>Segue abaixo uma relação para que você interessado possa não só conseguir ingressos, mas principalmente conseguir mais informações sobre o festival.</p>
<h2 style="visibility: visible;">Paraná</h2>
<h4 style="visibility: visible;">Curitiba</h4>
<ul class="bullet-a">
<li><strong>Ahlan Droid</strong> (
 <script language='JavaScript' type='text/javascript'>
 <!--
 var prefix = 'm&#97;&#105;lt&#111;:';
 var suffix = '';
 var attribs = '';
 var path = 'hr' + 'ef' + '=';
 var addy36879 = 'dj&#97;hl&#97;ndr&#111;&#105;d' + '&#64;';
 addy36879 = addy36879 + 'h&#111;tm&#97;&#105;l' + '&#46;' + 'c&#111;m';
 document.write( '<a ' + path + '\'' + prefix + addy36879 + suffix + '\'' + attribs + '>' );
 document.write( addy36879 );
 document.write( '<\/a>' );
 //-->
 </script><script language='JavaScript' type='text/javascript'>
 <!--
 document.write( '<span style=\'display: none;\'>' );
 //-->
 </script>Este endereço de e-mail está protegido contra spambots. Você deve habilitar o JavaScript para visualizá-lo.
 <script language='JavaScript' type='text/javascript'>
 <!--
 document.write( '</' );
 document.write( 'span>' );
 //-->
 </script>)<br />Água Verde.<br />Tel: (41) 9676-7800<br />Links: <a target="_blank" href="http://www.myspace.com/djahlandroid">MySpace</a></li>
<br />
<li><strong>Felipe Kantek</strong> (
 <script language='JavaScript' type='text/javascript'>
 <!--
 var prefix = 'm&#97;&#105;lt&#111;:';
 var suffix = '';
 var attribs = '';
 var path = 'hr' + 'ef' + '=';
 var addy43659 = 'k&#97;nt&#101;k' + '&#64;';
 addy43659 = addy43659 + 'm&#101;tr&#111;p&#111;l&#105;s' + '&#46;' + '&#97;rt' + '&#46;' + 'br';
 document.write( '<a ' + path + '\'' + prefix + addy43659 + suffix + '\'' + attribs + '>' );
 document.write( addy43659 );
 document.write( '<\/a>' );
 //-->
 </script><script language='JavaScript' type='text/javascript'>
 <!--
 document.write( '<span style=\'display: none;\'>' );
 //-->
 </script>Este endereço de e-mail está protegido contra spambots. Você deve habilitar o JavaScript para visualizá-lo.
 <script language='JavaScript' type='text/javascript'>
 <!--
 document.write( '</' );
 document.write( 'span>' );
 //-->
 </script>)<br />Centro.<br />Tel: (41) 9965-2313<br />Links: <a target="_blank" href="http://www.myspace.com/kantek">MySpace</a> - <a target="_blank" href="http://www.facebook.com/djkantek">Facebook</a> - <a target="_blank" href="http://www.twitter.com/kantek">Twitter</a></li>
<br />
<li><strong>Rodrigo "Psapo" Schultz</strong> (
 <script language='JavaScript' type='text/javascript'>
 <!--
 var prefix = 'm&#97;&#105;lt&#111;:';
 var suffix = '';
 var attribs = '';
 var path = 'hr' + 'ef' + '=';
 var addy36864 = 'djps&#97;p&#111;' + '&#64;';
 addy36864 = addy36864 + 'h&#111;tm&#97;&#105;l' + '&#46;' + 'c&#111;m';
 document.write( '<a ' + path + '\'' + prefix + addy36864 + suffix + '\'' + attribs + '>' );
 document.write( addy36864 );
 document.write( '<\/a>' );
 //-->
 </script><script language='JavaScript' type='text/javascript'>
 <!--
 document.write( '<span style=\'display: none;\'>' );
 //-->
 </script>Este endereço de e-mail está protegido contra spambots. Você deve habilitar o JavaScript para visualizá-lo.
 <script language='JavaScript' type='text/javascript'>
 <!--
 document.write( '</' );
 document.write( 'span>' );
 //-->
 </script>)<br />Bairro Mossunguê.<br />Tel: (41) 91811978 / (41) 8888-8001<br />Links: <a target="_blank" href="http://www.orkut.com.br/Main#Profile?rl=ls&amp;uid=7566503494211634731">Orkut</a> - <a target="_blank" href="http://www.myspace.com/djpsapo">MySpace</a> - <a target="_blank" href="http://www.twitter.com/djpsapo">Twitter</a></li>
<br />
<li><strong>Alejandro Bargueno</strong> (
 <script language='JavaScript' type='text/javascript'>
 <!--
 var prefix = 'm&#97;&#105;lt&#111;:';
 var suffix = '';
 var attribs = '';
 var path = 'hr' + 'ef' + '=';
 var addy13723 = '&#97;l&#101;j&#97;ndr&#111;b&#97;rg&#117;&#101;n&#111;' + '&#64;';
 addy13723 = addy13723 + 'h&#111;tm&#97;&#105;l' + '&#46;' + 'c&#111;m';
 document.write( '<a ' + path + '\'' + prefix + addy13723 + suffix + '\'' + attribs + '>' );
 document.write( addy13723 );
 document.write( '<\/a>' );
 //-->
 </script><script language='JavaScript' type='text/javascript'>
 <!--
 document.write( '<span style=\'display: none;\'>' );
 //-->
 </script>Este endereço de e-mail está protegido contra spambots. Você deve habilitar o JavaScript para visualizá-lo.
 <script language='JavaScript' type='text/javascript'>
 <!--
 document.write( '</' );
 document.write( 'span>' );
 //-->
 </script>)<br />Bairro Champagnat.<br />Tel: (41) 9612-2826<br />Links: <a target="_blank" href="http://soundcloud.com/tip_tronic">Soundcloud</a> - <a target="_blank" href="http://www.orkut.com.br/Main#Profile?uid=5906662220881259873">Orkut</a><a target="_blank" href="http://www.twitter.com/kantek"></a></li>
<br />
<li><strong>Gabriel Mello </strong>(
 <script language='JavaScript' type='text/javascript'>
 <!--
 var prefix = 'm&#97;&#105;lt&#111;:';
 var suffix = '';
 var attribs = '';
 var path = 'hr' + 'ef' + '=';
 var addy98560 = 'psyl&#111;c&#105;b&#105;npr&#111;j&#101;ct' + '&#64;';
 addy98560 = addy98560 + 'h&#111;tm&#97;&#105;l' + '&#46;' + 'c&#111;m';
 document.write( '<a ' + path + '\'' + prefix + addy98560 + suffix + '\'' + attribs + '>' );
 document.write( addy98560 );
 document.write( '<\/a>' );
 //-->
 </script><script language='JavaScript' type='text/javascript'>
 <!--
 document.write( '<span style=\'display: none;\'>' );
 //-->
 </script>Este endereço de e-mail está protegido contra spambots. Você deve habilitar o JavaScript para visualizá-lo.
 <script language='JavaScript' type='text/javascript'>
 <!--
 document.write( '</' );
 document.write( 'span>' );
 //-->
 </script>)<br />Bairro Cristo Rei.<br />Tel: (41) 8461-3354<br />Links: <a target="_blank" href="http://www.orkut.com.br/Main#Profile?uid=10137116061140916781">Orkut 1</a> - <a target="_blank" href="http://www.orkut.com.br/Main#Profile?uid=11614065982556444558">Orkut 2</a> - <a target="_blank" href="http://www.myspace.com/startinguplive">MySpace</a> - <a target="_blank" href="http://soundcloud.com/starting-up-live">Soundcloud</a> - <a target="_blank" href="http://www.facebook.com/home.php#!/profile.php?id=100001272074352">Facebook</a></li>
<br />
<li><strong>Fernanda D'Avilla </strong>(
 <script language='JavaScript' type='text/javascript'>
 <!--
 var prefix = 'm&#97;&#105;lt&#111;:';
 var suffix = '';
 var attribs = '';
 var path = 'hr' + 'ef' + '=';
 var addy90815 = 'd&#97;v&#105;l&#97;f&#101;f&#101;' + '&#64;';
 addy90815 = addy90815 + 'h&#111;tm&#97;&#105;l' + '&#46;' + 'c&#111;m';
 document.write( '<a ' + path + '\'' + prefix + addy90815 + suffix + '\'' + attribs + '>' );
 document.write( addy90815 );
 document.write( '<\/a>' );
 //-->
 </script><script language='JavaScript' type='text/javascript'>
 <!--
 document.write( '<span style=\'display: none;\'>' );
 //-->
 </script>Este endereço de e-mail está protegido contra spambots. Você deve habilitar o JavaScript para visualizá-lo.
 <script language='JavaScript' type='text/javascript'>
 <!--
 document.write( '</' );
 document.write( 'span>' );
 //-->
 </script>)<br />Bairro Cristo Rei.<br />Tel: (41) 9653-8935<br />Links: <a target="_blank" href="http://www.orkut.com.br/Main#Profile?rl=ttp&amp;uid=17357190155344533774">Orkut</a> - <a target="_blank" href="http://www.myspace.com/djfernanda">MySpace</a> - <a target="_blank" href="http://www.facebook.com/home.php#!/profile.php?id=100000981554277">Facebook</a></li>
<br />
<li><strong>Lafayete Carneiro </strong>(
 <script language='JavaScript' type='text/javascript'>
 <!--
 var prefix = 'm&#97;&#105;lt&#111;:';
 var suffix = '';
 var attribs = '';
 var path = 'hr' + 'ef' + '=';
 var addy85203 = 'l&#97;f&#97;321' + '&#64;';
 addy85203 = addy85203 + 'h&#111;tm&#97;&#105;l' + '&#46;' + 'c&#111;m';
 document.write( '<a ' + path + '\'' + prefix + addy85203 + suffix + '\'' + attribs + '>' );
 document.write( addy85203 );
 document.write( '<\/a>' );
 //-->
 </script><script language='JavaScript' type='text/javascript'>
 <!--
 document.write( '<span style=\'display: none;\'>' );
 //-->
 </script>Este endereço de e-mail está protegido contra spambots. Você deve habilitar o JavaScript para visualizá-lo.
 <script language='JavaScript' type='text/javascript'>
 <!--
 document.write( '</' );
 document.write( 'span>' );
 //-->
 </script>)<br />Bairro Campina do Siqueira.<br />Tel: (41) 9675-4535</li>
<br />
<li><strong>Cezar de Araujo Santos aka DJ Kazz </strong>(
 <script language='JavaScript' type='text/javascript'>
 <!--
 var prefix = 'm&#97;&#105;lt&#111;:';
 var suffix = '';
 var attribs = '';
 var path = 'hr' + 'ef' + '=';
 var addy35841 = 'c&#101;z&#97;r.s&#97;nt&#111;s' + '&#64;';
 addy35841 = addy35841 + 'ym&#97;&#105;l' + '&#46;' + 'c&#111;m';
 document.write( '<a ' + path + '\'' + prefix + addy35841 + suffix + '\'' + attribs + '>' );
 document.write( addy35841 );
 document.write( '<\/a>' );
 //-->
 </script><script language='JavaScript' type='text/javascript'>
 <!--
 document.write( '<span style=\'display: none;\'>' );
 //-->
 </script>Este endereço de e-mail está protegido contra spambots. Você deve habilitar o JavaScript para visualizá-lo.
 <script language='JavaScript' type='text/javascript'>
 <!--
 document.write( '</' );
 document.write( 'span>' );
 //-->
 </script>)<br />Cidade Industrial.<br />Tel: (41) 9903-8154 / (41) 3095-4688<br />Links: <a target="_blank" href="http://www.orkut.com.br/Main#Profile?rl=mp&amp;uid=15558153954449337127">Orkut</a> - <a target="_blank" href="http://www.facebook.com/home.php#!/profile.php?id=100000870310449">Facebook</a> - <a target="_blank" href="http://www.soundcloud.com/djkazz">Soundcloud</a></li>
<br />
<li><strong>Joel Guglielmini </strong>(
 <script language='JavaScript' type='text/javascript'>
 <!--
 var prefix = 'm&#97;&#105;lt&#111;:';
 var suffix = '';
 var attribs = '';
 var path = 'hr' + 'ef' + '=';
 var addy29252 = 'j&#111;&#101;lg&#117;gl&#105;&#101;lm&#105;n&#105;' + '&#64;';
 addy29252 = addy29252 + 'h&#111;tm&#97;&#105;l' + '&#46;' + 'c&#111;m';
 document.write( '<a ' + path + '\'' + prefix + addy29252 + suffix + '\'' + attribs + '>' );
 document.write( addy29252 );
 document.write( '<\/a>' );
 //-->
 </script><script language='JavaScript' type='text/javascript'>
 <!--
 document.write( '<span style=\'display: none;\'>' );
 //-->
 </script>Este endereço de e-mail está protegido contra spambots. Você deve habilitar o JavaScript para visualizá-lo.
 <script language='JavaScript' type='text/javascript'>
 <!--
 document.write( '</' );
 document.write( 'span>' );
 //-->
 </script>)<br />Tel: (41) 9929-9747<br />Links: <a target="_blank" href="http://www.orkut.com.br/Main#Profile?uid=15791433437215101125">Orkut</a> - <a target="_blank" href="http://www.myspace.com/joelguglielmini">MySpace</a> - <a target="_blank" href="http://www.twitter.com/joelguglielmini">Twitter</a></li>
</ul>
<h4 style="visibility: visible;">São José dos Pinhais</h4>
<ul class="bullet-a">
<li><strong>Rafahell </strong>(
 <script language='JavaScript' type='text/javascript'>
 <!--
 var prefix = 'm&#97;&#105;lt&#111;:';
 var suffix = '';
 var attribs = '';
 var path = 'hr' + 'ef' + '=';
 var addy92032 = 's&#101;xt&#97;_&#101;_s&#97;b&#97;d&#111;' + '&#64;';
 addy92032 = addy92032 + 'h&#111;tm&#97;&#105;l' + '&#46;' + 'c&#111;m';
 document.write( '<a ' + path + '\'' + prefix + addy92032 + suffix + '\'' + attribs + '>' );
 document.write( addy92032 );
 document.write( '<\/a>' );
 //-->
 </script><script language='JavaScript' type='text/javascript'>
 <!--
 document.write( '<span style=\'display: none;\'>' );
 //-->
 </script>Este endereço de e-mail está protegido contra spambots. Você deve habilitar o JavaScript para visualizá-lo.
 <script language='JavaScript' type='text/javascript'>
 <!--
 document.write( '</' );
 document.write( 'span>' );
 //-->
 </script>)<br />Tel: (41) 9929-3185 / (41) 8422-6777<br />Links: <a target="_blank" href="http://www.orkut.com.br/Main#Profile?uid=11984508187892384100">Orkut</a> - <a target="_blank" href="http://www.myspace.com/afahell">MySpace</a> - <a target="_blank" href="http://www.facebook.com/RafahellHenrique">Facebook</a></li>
<br />
<li><strong>Chucky aka Luiz Gustavo Kochanny </strong>(
 <script language='JavaScript' type='text/javascript'>
 <!--
 var prefix = 'm&#97;&#105;lt&#111;:';
 var suffix = '';
 var attribs = '';
 var path = 'hr' + 'ef' + '=';
 var addy64550 = 'lgk&#111;ch&#97;nny' + '&#64;';
 addy64550 = addy64550 + 'h&#111;tm&#97;&#105;l' + '&#46;' + 'c&#111;m';
 document.write( '<a ' + path + '\'' + prefix + addy64550 + suffix + '\'' + attribs + '>' );
 document.write( addy64550 );
 document.write( '<\/a>' );
 //-->
 </script><script language='JavaScript' type='text/javascript'>
 <!--
 document.write( '<span style=\'display: none;\'>' );
 //-->
 </script>Este endereço de e-mail está protegido contra spambots. Você deve habilitar o JavaScript para visualizá-lo.
 <script language='JavaScript' type='text/javascript'>
 <!--
 document.write( '</' );
 document.write( 'span>' );
 //-->
 </script>)<br />Tel: (41) 7811-7755<br />Links: <a target="_blank" href="http://www.orkut.com.br/Main#Profile?rl=mp&amp;uid=5697523841202609930">Orkut</a></li>
<br />
<li><strong>Maicon Luiz Ribeiro da Silva </strong>(
 <script language='JavaScript' type='text/javascript'>
 <!--
 var prefix = 'm&#97;&#105;lt&#111;:';
 var suffix = '';
 var attribs = '';
 var path = 'hr' + 'ef' + '=';
 var addy24564 = 'm&#97;&#105;c&#111;nc&#117;r&#105;t&#105;b&#97;' + '&#64;';
 addy24564 = addy24564 + 'h&#111;tm&#97;&#105;l' + '&#46;' + 'c&#111;m';
 document.write( '<a ' + path + '\'' + prefix + addy24564 + suffix + '\'' + attribs + '>' );
 document.write( addy24564 );
 document.write( '<\/a>' );
 //-->
 </script><script language='JavaScript' type='text/javascript'>
 <!--
 document.write( '<span style=\'display: none;\'>' );
 //-->
 </script>Este endereço de e-mail está protegido contra spambots. Você deve habilitar o JavaScript para visualizá-lo.
 <script language='JavaScript' type='text/javascript'>
 <!--
 document.write( '</' );
 document.write( 'span>' );
 //-->
 </script>)<br />Tel: (41) 8841-7226<br />Links: <a target="_blank" href="http://www.orkut.com.br/Main#Profile?uid=12877644444843431040">Orkut</a> - <a target="_blank" href="http://twitter.com/Maicon_Curitiba">Twitter</a></li>
</ul>
<h4 style="visibility: visible;">Campo Largo</h4>
<ul class="bullet-a">
<li><strong>Marcelo Schiavon Ramos </strong>(
 <script language='JavaScript' type='text/javascript'>
 <!--
 var prefix = 'm&#97;&#105;lt&#111;:';
 var suffix = '';
 var attribs = '';
 var path = 'hr' + 'ef' + '=';
 var addy22906 = 'psys&#101;&#101;dr&#101;c&#111;rds' + '&#64;';
 addy22906 = addy22906 + 'h&#111;tm&#97;&#105;l' + '&#46;' + 'c&#111;m';
 document.write( '<a ' + path + '\'' + prefix + addy22906 + suffix + '\'' + attribs + '>' );
 document.write( addy22906 );
 document.write( '<\/a>' );
 //-->
 </script><script language='JavaScript' type='text/javascript'>
 <!--
 document.write( '<span style=\'display: none;\'>' );
 //-->
 </script>Este endereço de e-mail está protegido contra spambots. Você deve habilitar o JavaScript para visualizá-lo.
 <script language='JavaScript' type='text/javascript'>
 <!--
 document.write( '</' );
 document.write( 'span>' );
 //-->
 </script>)<br />Tel: (41) 3393-3937 / (41) 2913-6698<br />Links: <a target="_blank" href="http://www.orkut.com.br/Main#Profile?uid=3101381545169315055">Orkut</a> - <a target="_blank" href="http://www.myspace.com/urucubacadark">MySpace</a> - <a target="_blank" href="http://www.facebook.com/profile.php?id=688364190">Facebook</a></li>
</ul>
<h4 style="visibility: visible;">Guarapuava</h4>
<ul class="bullet-a">
<li><strong>João Gabriel de Oliveira </strong>(
 <script language='JavaScript' type='text/javascript'>
 <!--
 var prefix = 'm&#97;&#105;lt&#111;:';
 var suffix = '';
 var attribs = '';
 var path = 'hr' + 'ef' + '=';
 var addy57192 = 'b&#105;b&#105;.dj' + '&#64;';
 addy57192 = addy57192 + 'h&#111;tm&#97;&#105;l' + '&#46;' + 'c&#111;m';
 document.write( '<a ' + path + '\'' + prefix + addy57192 + suffix + '\'' + attribs + '>' );
 document.write( addy57192 );
 document.write( '<\/a>' );
 //-->
 </script><script language='JavaScript' type='text/javascript'>
 <!--
 document.write( '<span style=\'display: none;\'>' );
 //-->
 </script>Este endereço de e-mail está protegido contra spambots. Você deve habilitar o JavaScript para visualizá-lo.
 <script language='JavaScript' type='text/javascript'>
 <!--
 document.write( '</' );
 document.write( 'span>' );
 //-->
 </script>)<br />Tel: (42) 8412-6332<br />Links: <a target="_blank" href="http://www.orkut.com.br/Main#Profile?uid=12603969666220510463">Orkut</a> - <a target="_blank" href="http://www.facebook.com/#!/profile.php?id=1146217263">Facebook</a></li>
<br />
<li><strong>Dayani Leite </strong>(
 <script language='JavaScript' type='text/javascript'>
 <!--
 var prefix = 'm&#97;&#105;lt&#111;:';
 var suffix = '';
 var attribs = '';
 var path = 'hr' + 'ef' + '=';
 var addy75921 = 'd&#97;y&#97;_s&#105;st&#97;' + '&#64;';
 addy75921 = addy75921 + 'y&#97;h&#111;&#111;' + '&#46;' + 'c&#111;m' + '&#46;' + 'br';
 document.write( '<a ' + path + '\'' + prefix + addy75921 + suffix + '\'' + attribs + '>' );
 document.write( addy75921 );
 document.write( '<\/a>' );
 //-->
 </script><script language='JavaScript' type='text/javascript'>
 <!--
 document.write( '<span style=\'display: none;\'>' );
 //-->
 </script>Este endereço de e-mail está protegido contra spambots. Você deve habilitar o JavaScript para visualizá-lo.
 <script language='JavaScript' type='text/javascript'>
 <!--
 document.write( '</' );
 document.write( 'span>' );
 //-->
 </script>)<br />Centro.<br />Tel: (41) 9673-6680</li>
</ul>
<h4 style="visibility: visible;">Ponta Grossa</h4>
<ul class="bullet-a">
<li><strong>Saana aka Guilherme Conforto dos Santos </strong>(
 <script language='JavaScript' type='text/javascript'>
 <!--
 var prefix = 'm&#97;&#105;lt&#111;:';
 var suffix = '';
 var attribs = '';
 var path = 'hr' + 'ef' + '=';
 var addy52246 = 't&#105;&#111;b&#111;z&#111;x' + '&#64;';
 addy52246 = addy52246 + 'h&#111;tm&#97;&#105;l' + '&#46;' + 'c&#111;m';
 document.write( '<a ' + path + '\'' + prefix + addy52246 + suffix + '\'' + attribs + '>' );
 document.write( addy52246 );
 document.write( '<\/a>' );
 //-->
 </script><script language='JavaScript' type='text/javascript'>
 <!--
 document.write( '<span style=\'display: none;\'>' );
 //-->
 </script>Este endereço de e-mail está protegido contra spambots. Você deve habilitar o JavaScript para visualizá-lo.
 <script language='JavaScript' type='text/javascript'>
 <!--
 document.write( '</' );
 document.write( 'span>' );
 //-->
 </script>)<br />Links: <a target="_blank" href="http://www.orkut.com.br/Main#Profile?uid=15226248948939348043">Orkut</a> - <a target="_blank" href="http://www.myspace.com/djsaana">MySpace</a></li>
<br />
<li><strong>Soha aka Rômulo Andrade</strong> (
 <script language='JavaScript' type='text/javascript'>
 <!--
 var prefix = 'm&#97;&#105;lt&#111;:';
 var suffix = '';
 var attribs = '';
 var path = 'hr' + 'ef' + '=';
 var addy42196 = 'djs&#111;h&#97;' + '&#64;';
 addy42196 = addy42196 + 'gm&#97;&#105;l' + '&#46;' + 'c&#111;m';
 document.write( '<a ' + path + '\'' + prefix + addy42196 + suffix + '\'' + attribs + '>' );
 document.write( addy42196 );
 document.write( '<\/a>' );
 //-->
 </script><script language='JavaScript' type='text/javascript'>
 <!--
 document.write( '<span style=\'display: none;\'>' );
 //-->
 </script>Este endereço de e-mail está protegido contra spambots. Você deve habilitar o JavaScript para visualizá-lo.
 <script language='JavaScript' type='text/javascript'>
 <!--
 document.write( '</' );
 document.write( 'span>' );
 //-->
 </script>)<br />Tel: (42) 9936-5639 / (42) 3226-2885<br />Links: <a target="_blank" href="http://www.orkut.com.br/Main#Profile?rl=mp&amp;uid=11570403255916802060">Orkut</a> - <a target="_blank" href="http://www.myspace.com/djsoha">MySpace</a></li>
</ul>
<h4 style="visibility: visible;">São Mateus do Sul</h4>
<ul class="bullet-a">
<li><strong>Dymi </strong>(
 <script language='JavaScript' type='text/javascript'>
 <!--
 var prefix = 'm&#97;&#105;lt&#111;:';
 var suffix = '';
 var attribs = '';
 var path = 'hr' + 'ef' + '=';
 var addy9813 = 'dym&#105;_c&#111;rd&#101;&#105;r&#111;' + '&#64;';
 addy9813 = addy9813 + 'h&#111;tm&#97;&#105;l' + '&#46;' + 'c&#111;m';
 document.write( '<a ' + path + '\'' + prefix + addy9813 + suffix + '\'' + attribs + '>' );
 document.write( addy9813 );
 document.write( '<\/a>' );
 //-->
 </script><script language='JavaScript' type='text/javascript'>
 <!--
 document.write( '<span style=\'display: none;\'>' );
 //-->
 </script>Este endereço de e-mail está protegido contra spambots. Você deve habilitar o JavaScript para visualizá-lo.
 <script language='JavaScript' type='text/javascript'>
 <!--
 document.write( '</' );
 document.write( 'span>' );
 //-->
 </script>)<br />Tel: (42) 8816-7060 / (42) 8816-7060 / (41) 3257-2100<br />Links: <a target="_blank" href="http://www.orkut.com.br/Main#Profile?rl=mp&amp;uid=2595876933433285207">Orkut</a> - <a target="_blank" href="http://WWW.MYSPACE.COM/MUSICDYMI">MySpace</a> - <a target="_blank" href="http://WWW.TWITTER.COM/MUSICDYMI">Twitter</a><br /><br /></li>
<li><strong>Cibele </strong>(
 <script language='JavaScript' type='text/javascript'>
 <!--
 var prefix = 'm&#97;&#105;lt&#111;:';
 var suffix = '';
 var attribs = '';
 var path = 'hr' + 'ef' + '=';
 var addy70836 = 'b&#101;ll_z&#105;nh&#97;3' + '&#64;';
 addy70836 = addy70836 + 'h&#111;tm&#97;&#105;l' + '&#46;' + 'c&#111;m';
 document.write( '<a ' + path + '\'' + prefix + addy70836 + suffix + '\'' + attribs + '>' );
 document.write( addy70836 );
 document.write( '<\/a>' );
 //-->
 </script><script language='JavaScript' type='text/javascript'>
 <!--
 document.write( '<span style=\'display: none;\'>' );
 //-->
 </script>Este endereço de e-mail está protegido contra spambots. Você deve habilitar o JavaScript para visualizá-lo.
 <script language='JavaScript' type='text/javascript'>
 <!--
 document.write( '</' );
 document.write( 'span>' );
 //-->
 </script>)<br />Tel: (42) 8812-2010<br />Links: <a target="_blank" href="http://www.myspace.com/ciidubiel">MySpace</a></li>
</ul>
<h4 style="visibility: visible;">União da Vitória</h4>
<ul class="bullet-a">
<li><strong>Felipe Sicuro Sturmer </strong>(
 <script language='JavaScript' type='text/javascript'>
 <!--
 var prefix = 'm&#97;&#105;lt&#111;:';
 var suffix = '';
 var attribs = '';
 var path = 'hr' + 'ef' + '=';
 var addy44479 = 'fssp&#105;r&#97;s&#105;' + '&#64;';
 addy44479 = addy44479 + 'h&#111;tm&#97;&#105;l' + '&#46;' + 'c&#111;m';
 document.write( '<a ' + path + '\'' + prefix + addy44479 + suffix + '\'' + attribs + '>' );
 document.write( addy44479 );
 document.write( '<\/a>' );
 //-->
 </script><script language='JavaScript' type='text/javascript'>
 <!--
 document.write( '<span style=\'display: none;\'>' );
 //-->
 </script>Este endereço de e-mail está protegido contra spambots. Você deve habilitar o JavaScript para visualizá-lo.
 <script language='JavaScript' type='text/javascript'>
 <!--
 document.write( '</' );
 document.write( 'span>' );
 //-->
 </script>)<br />Tel: (42) 3523-2992<br />Links: <a target="_blank" href="http://www.orkut.com.br/Main#Profile?uid=1193840043773786793">Orkut</a></li>
</ul>
<h2 style="visibility: visible;">Santa Catarina</h2>
<h4 style="visibility: visible;">Florianópolis</h4>
<ul class="bullet-a">
<li><strong>Felpe Telles Poeta </strong>(
 <script language='JavaScript' type='text/javascript'>
 <!--
 var prefix = 'm&#97;&#105;lt&#111;:';
 var suffix = '';
 var attribs = '';
 var path = 'hr' + 'ef' + '=';
 var addy37266 = 'ftp&#111;&#101;t&#97;' + '&#64;';
 addy37266 = addy37266 + 'h&#111;tm&#97;&#105;l' + '&#46;' + 'c&#111;m';
 document.write( '<a ' + path + '\'' + prefix + addy37266 + suffix + '\'' + attribs + '>' );
 document.write( addy37266 );
 document.write( '<\/a>' );
 //-->
 </script><script language='JavaScript' type='text/javascript'>
 <!--
 document.write( '<span style=\'display: none;\'>' );
 //-->
 </script>Este endereço de e-mail está protegido contra spambots. Você deve habilitar o JavaScript para visualizá-lo.
 <script language='JavaScript' type='text/javascript'>
 <!--
 document.write( '</' );
 document.write( 'span>' );
 //-->
 </script>)<br />Tel: (48) 8464-6796<strong></strong><br /><br /></li>
<li><strong>Alana </strong>(
 <script language='JavaScript' type='text/javascript'>
 <!--
 var prefix = 'm&#97;&#105;lt&#111;:';
 var suffix = '';
 var attribs = '';
 var path = 'hr' + 'ef' + '=';
 var addy39358 = 'm&#117;sc4r&#105;4' + '&#64;';
 addy39358 = addy39358 + 'gm&#97;&#105;l' + '&#46;' + 'c&#111;m';
 document.write( '<a ' + path + '\'' + prefix + addy39358 + suffix + '\'' + attribs + '>' );
 document.write( addy39358 );
 document.write( '<\/a>' );
 //-->
 </script><script language='JavaScript' type='text/javascript'>
 <!--
 document.write( '<span style=\'display: none;\'>' );
 //-->
 </script>Este endereço de e-mail está protegido contra spambots. Você deve habilitar o JavaScript para visualizá-lo.
 <script language='JavaScript' type='text/javascript'>
 <!--
 document.write( '</' );
 document.write( 'span>' );
 //-->
 </script>)<br />Tel: (48) 3237-9663 / (48) 8836-9372<br />Links: <a target="_blank" href="http://www.orkut.com.br/Main#Profile?uid=8939579281051093172">Orkut</a> - <a target="_blank" href="http://www.soundcloud.com/djalanita">Soundcloud</a> - <a target="_blank" href="http://www.facebook.com/alana.curi">Facebook</a></li>
</ul>
<h4 style="visibility: visible;">Balneário Camboriú / Itajaí</h4>
<ul class="bullet-a">
<li><strong>Flavio Fayet </strong>(
 <script language='JavaScript' type='text/javascript'>
 <!--
 var prefix = 'm&#97;&#105;lt&#111;:';
 var suffix = '';
 var attribs = '';
 var path = 'hr' + 'ef' + '=';
 var addy57038 = 'ff&#97;y&#101;t007' + '&#64;';
 addy57038 = addy57038 + 'h&#111;tm&#97;&#105;l' + '&#46;' + 'c&#111;m';
 document.write( '<a ' + path + '\'' + prefix + addy57038 + suffix + '\'' + attribs + '>' );
 document.write( addy57038 );
 document.write( '<\/a>' );
 //-->
 </script><script language='JavaScript' type='text/javascript'>
 <!--
 document.write( '<span style=\'display: none;\'>' );
 //-->
 </script>Este endereço de e-mail está protegido contra spambots. Você deve habilitar o JavaScript para visualizá-lo.
 <script language='JavaScript' type='text/javascript'>
 <!--
 document.write( '</' );
 document.write( 'span>' );
 //-->
 </script>)<br />Tel: (47) 3361-6334<br />Links: <a target="_blank" href="http://www.orkut.com.br/Main#Profile?rl=ls&amp;uid=17643359035068202650">Orkut</a> - <a target="_blank" href="http://WWW.myspace.com/sednainc">MySpace</a> - <a target="_blank" href="http://twitter.com/ffayet007">Twitter</a></li>
</ul>
<h4 style="visibility: visible;">Joinville</h4>
<ul class="bullet-a">
<li><strong>Roger Thiago Oliveira </strong>(
 <script language='JavaScript' type='text/javascript'>
 <!--
 var prefix = 'm&#97;&#105;lt&#111;:';
 var suffix = '';
 var attribs = '';
 var path = 'hr' + 'ef' + '=';
 var addy26156 = 'r&#111;g&#101;rth&#105;&#97;g&#111;&#111;l&#105;v&#101;&#105;r&#97;' + '&#64;';
 addy26156 = addy26156 + 'gm&#97;&#105;l' + '&#46;' + 'c&#111;m';
 document.write( '<a ' + path + '\'' + prefix + addy26156 + suffix + '\'' + attribs + '>' );
 document.write( addy26156 );
 document.write( '<\/a>' );
 //-->
 </script><script language='JavaScript' type='text/javascript'>
 <!--
 document.write( '<span style=\'display: none;\'>' );
 //-->
 </script>Este endereço de e-mail está protegido contra spambots. Você deve habilitar o JavaScript para visualizá-lo.
 <script language='JavaScript' type='text/javascript'>
 <!--
 document.write( '</' );
 document.write( 'span>' );
 //-->
 </script>)<br />Tel: (47) 9951-3697<br />Links: <a target="_blank" href="http://www.facebook.com/rogerthiago">Facebook</a></li>
</ul>
<h4 style="visibility: visible;">Blumenau</h4>
<ul class="bullet-a">
<li><strong>Tiago Rafael Azul </strong>(
 <script language='JavaScript' type='text/javascript'>
 <!--
 var prefix = 'm&#97;&#105;lt&#111;:';
 var suffix = '';
 var attribs = '';
 var path = 'hr' + 'ef' + '=';
 var addy49757 = 'g&#101;r&#101;nc&#105;&#97;l' + '&#64;';
 addy49757 = addy49757 + 'sp&#97;c&#111;v&#97;g&#117;n' + '&#46;' + 'c&#111;m' + '&#46;' + 'br';
 document.write( '<a ' + path + '\'' + prefix + addy49757 + suffix + '\'' + attribs + '>' );
 document.write( addy49757 );
 document.write( '<\/a>' );
 //-->
 </script><script language='JavaScript' type='text/javascript'>
 <!--
 document.write( '<span style=\'display: none;\'>' );
 //-->
 </script>Este endereço de e-mail está protegido contra spambots. Você deve habilitar o JavaScript para visualizá-lo.
 <script language='JavaScript' type='text/javascript'>
 <!--
 document.write( '</' );
 document.write( 'span>' );
 //-->
 </script>)<br />Tel: (41) 9660-4544 / MSN: 
 <script language='JavaScript' type='text/javascript'>
 <!--
 var prefix = 'm&#97;&#105;lt&#111;:';
 var suffix = '';
 var attribs = '';
 var path = 'hr' + 'ef' + '=';
 var addy41836 = 'l&#105;st&#101;n.&#97;rt' + '&#64;';
 addy41836 = addy41836 + 'h&#111;tm&#97;&#105;l' + '&#46;' + 'c&#111;m';
 document.write( '<a ' + path + '\'' + prefix + addy41836 + suffix + '\'' + attribs + '>' );
 document.write( addy41836 );
 document.write( '<\/a>' );
 //-->
 </script><script language='JavaScript' type='text/javascript'>
 <!--
 document.write( '<span style=\'display: none;\'>' );
 //-->
 </script>Este endereço de e-mail está protegido contra spambots. Você deve habilitar o JavaScript para visualizá-lo.
 <script language='JavaScript' type='text/javascript'>
 <!--
 document.write( '</' );
 document.write( 'span>' );
 //-->
 </script><a target="_blank" href="http://www.facebook.com/rogerthiago"></a></li>
</ul>
<h4 style="visibility: visible;">Videira / Lages</h4>
<ul class="bullet-a">
<li><strong>Marcos Paulo Santini </strong>(
 <script language='JavaScript' type='text/javascript'>
 <!--
 var prefix = 'm&#97;&#105;lt&#111;:';
 var suffix = '';
 var attribs = '';
 var path = 'hr' + 'ef' + '=';
 var addy25528 = 'm&#97;rc&#111;sp&#97;&#117;l&#111;s&#97;nt&#105;n&#105;' + '&#64;';
 addy25528 = addy25528 + 'b&#111;l' + '&#46;' + 'c&#111;m' + '&#46;' + 'br';
 document.write( '<a ' + path + '\'' + prefix + addy25528 + suffix + '\'' + attribs + '>' );
 document.write( addy25528 );
 document.write( '<\/a>' );
 //-->
 </script><script language='JavaScript' type='text/javascript'>
 <!--
 document.write( '<span style=\'display: none;\'>' );
 //-->
 </script>Este endereço de e-mail está protegido contra spambots. Você deve habilitar o JavaScript para visualizá-lo.
 <script language='JavaScript' type='text/javascript'>
 <!--
 document.write( '</' );
 document.write( 'span>' );
 //-->
 </script>)<br />Tel: (49) 9985-5162</li>
</ul>
<h2 style="visibility: visible;">Rio Grande do Sul</h2>
<h4>Capital</h4>
<ul class="bullet-a">
<li><strong>Thiago Pires Gonçalves </strong>(
 <script language='JavaScript' type='text/javascript'>
 <!--
 var prefix = 'm&#97;&#105;lt&#111;:';
 var suffix = '';
 var attribs = '';
 var path = 'hr' + 'ef' + '=';
 var addy94132 = 'b&#105;j&#97;r&#105;' + '&#64;';
 addy94132 = addy94132 + 'msn' + '&#46;' + 'c&#111;m';
 document.write( '<a ' + path + '\'' + prefix + addy94132 + suffix + '\'' + attribs + '>' );
 document.write( addy94132 );
 document.write( '<\/a>' );
 //-->
 </script><script language='JavaScript' type='text/javascript'>
 <!--
 document.write( '<span style=\'display: none;\'>' );
 //-->
 </script>Este endereço de e-mail está protegido contra spambots. Você deve habilitar o JavaScript para visualizá-lo.
 <script language='JavaScript' type='text/javascript'>
 <!--
 document.write( '</' );
 document.write( 'span>' );
 //-->
 </script>)<br />Links: <a target="_blank" href="http://www.orkut.com.br/Main#Profile?uid=213587247634284605&amp;rl=t">Orkut</a> - <a target="_blank" href="http://www.soundcloud.com/djthiagobrazil">Soundcloud</a><a target="_blank" href="http://www.twitter.com/djcaroles"></a></li>
</ul>
<h2 style="visibility: visible;">Rio de Janeiro</h2>
<h4>Capital</h4>
<ul class="bullet-a">
<li><strong>Caroline </strong>(
 <script language='JavaScript' type='text/javascript'>
 <!--
 var prefix = 'm&#97;&#105;lt&#111;:';
 var suffix = '';
 var attribs = '';
 var path = 'hr' + 'ef' + '=';
 var addy2461 = 'c&#97;r&#111;l&#101;sr&#97;m&#111;s' + '&#64;';
 addy2461 = addy2461 + 'y&#97;h&#111;&#111;' + '&#46;' + 'c&#111;m' + '&#46;' + 'br';
 document.write( '<a ' + path + '\'' + prefix + addy2461 + suffix + '\'' + attribs + '>' );
 document.write( addy2461 );
 document.write( '<\/a>' );
 //-->
 </script><script language='JavaScript' type='text/javascript'>
 <!--
 document.write( '<span style=\'display: none;\'>' );
 //-->
 </script>Este endereço de e-mail está protegido contra spambots. Você deve habilitar o JavaScript para visualizá-lo.
 <script language='JavaScript' type='text/javascript'>
 <!--
 document.write( '</' );
 document.write( 'span>' );
 //-->
 </script>)<br />Tel: (21) 9119-5145 / (21) 2494-4534<br />Links: <a target="_blank" href="http://www.orkut.com.br/Main#Profile?uid=3086396642885006389&amp;rl=t">Orkut</a> - <a target="_blank" href="http://www.myspace.com/djcaroles">MySpace</a> - <a target="_blank" href="http://www.facebook.com/djcaroles">Facebook</a> - <a target="_blank" href="http://www.twitter.com/djcaroles">Twitter</a></li>
</ul>
<h2 style="visibility: visible;">São Paulo</h2>
<h4>Capital</h4>
<ul class="bullet-a">
<li><strong>João Carlos Sutemi </strong>(
 <script language='JavaScript' type='text/javascript'>
 <!--
 var prefix = 'm&#97;&#105;lt&#111;:';
 var suffix = '';
 var attribs = '';
 var path = 'hr' + 'ef' + '=';
 var addy72390 = 'djs&#117;t&#101;m&#105;' + '&#64;';
 addy72390 = addy72390 + 'gm&#97;&#105;l' + '&#46;' + 'c&#111;m';
 document.write( '<a ' + path + '\'' + prefix + addy72390 + suffix + '\'' + attribs + '>' );
 document.write( addy72390 );
 document.write( '<\/a>' );
 //-->
 </script><script language='JavaScript' type='text/javascript'>
 <!--
 document.write( '<span style=\'display: none;\'>' );
 //-->
 </script>Este endereço de e-mail está protegido contra spambots. Você deve habilitar o JavaScript para visualizá-lo.
 <script language='JavaScript' type='text/javascript'>
 <!--
 document.write( '</' );
 document.write( 'span>' );
 //-->
 </script>)<br />Tel: (11) 8092-5380<br />Links: <a target="_blank" href="http://WWW.myspace.com/sutemidj">MySpace</a> - <a target="_blank" href="http://www.facebook.com/sutemi">Facebook</a> - <a target="_blank" href="http://www.twitter.com/sutemi">Twitter</a></li>
</ul>
<div class="notice">
<div class="typo-icon">Quer se tornar um Ponto de Venda de ingressos para o Festival <strong>Tandava  Gathering</strong> na sua cidade? <a href="index.php?option=com_contact&amp;view=contact&amp;id=1&amp;Itemid=49">Entre  em contato</a> conosco.</div>
</div>
</div><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://elijah.com.br/tandava/2010/ingressos/86-onde-comprar.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/ingressos/86-onde-comprar.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/ingressos/86-onde-comprar.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL2VsaWphaC5jb20uYnIvdGFuZGF2YS8yMDEwL2luZ3Jlc3Nvcy84Ni1vbmRlLWNvbXByYXIuaHRtbA%3D%3D" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Quarta, 16 de Junho de 2010 22:02				</span>
					
								<span class="rt-date-modified">
					Última atualização em Quinta, 23 de Setembro de 2010 18:18				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>									
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/ingressos/87-venda-online.html">Ingressos Online</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<p>Os ingressos também estão disponíveis para todo país para compra <em>online</em> através do website parceiro <strong>Hot Ticket</strong>.</p>
<h4>Acesse: <a target="_blank" href="http://www.hotticket.biz/">www.hotticket.biz</a></h4><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://elijah.com.br/tandava/2010/ingressos/87-venda-online.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/ingressos/87-venda-online.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/ingressos/87-venda-online.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL2VsaWphaC5jb20uYnIvdGFuZGF2YS8yMDEwL2luZ3Jlc3Nvcy84Ny12ZW5kYS1vbmxpbmUuaHRtbA%3D%3D" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Quarta, 16 de Junho de 2010 21:14				</span>
					
								<span class="rt-date-modified">
					Última atualização em Terça, 17 de Agosto de 2010 19:45				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>									
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/ingressos/88-excursoes.html">Excursões para o Tandava</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<p>Se você mora em outra cidade ou estado, entre em contato com o parceiro da sua região e venha para o festival. Você encontra uma lista de parceiros<strong></strong> na página <a target="_self" href="index.php?option=com_content&view=article&id=86:onde-comprar&catid=43&Itemid=168">Onde Comprar</a>.</p>
<p>Faça parte do <strong>Tandava Gathering 2010</strong>.</p>
<div class="notice">
<div class="typo-icon">Sua cidade não tem um parceiro próximo e você quer organizar uma excursão exclusiva para o Festival <strong>Tandava Gathering</strong> na sua cidade? <a href="index.php?option=com_contact&view=contact&id=1&Itemid=49">Entre em contato</a> conosco.</div>
</div><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://elijah.com.br/tandava/2010/ingressos/88-excursoes.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/ingressos/88-excursoes.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/ingressos/88-excursoes.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL2VsaWphaC5jb20uYnIvdGFuZGF2YS8yMDEwL2luZ3Jlc3Nvcy84OC1leGN1cnNvZXMuaHRtbA%3D%3D" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Quarta, 16 de Junho de 2010 20:15				</span>
					
								<span class="rt-date-modified">
					Última atualização em Sábado, 04 de Setembro de 2010 11:47				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>							</div>
		
				<div class="clear"></div>
		
				
	</div>
</div>";s:4:"head";a:10:{s:5:"title";s:34:"Ingressos para o Tandava Gathering";s:11:"description";s:275:"Não surgiu um novo conceito. As coisas não vão mudar e nada será resgatado.  Não  é nada melhor ou maior de tudo que você já viu. Não  é inédito e nem ao menos é um evento. O respeito existe, mas o publico não, existe um encontro entre amigos e amigos de amigos.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:24:"text/html; charset=utf-8";}s:8:"standard";a:4:{s:6:"robots";s:13:"index, follow";s:8:"keywords";s:54:"tandava, gathering, festival, 2010, curitiba, novembro";s:8:"og:title";s:25:"Excursões para o Tandava";s:12:"og:site_name";s:17:"Tandava Gathering";}}s:5:"links";a:2:{i:0;s:109:"<link href="/tandava/2010/ingressos.feed?type=rss" rel="alternate" type="application/rss+xml" title="RSS 2.0"";i:1;s:112:"<link href="/tandava/2010/ingressos.feed?type=atom" rel="alternate" type="application/atom+xml" title="Atom 1.0"";}s:11:"styleSheets";a:0:{}s:5:"style";a:0:{}s:7:"scripts";a:2:{s:41:"/tandava/2010/media/system/js/mootools.js";s:15:"text/javascript";s:40:"/tandava/2010/media/system/js/caption.js";s:15:"text/javascript";}s:6:"script";a:0:{}s:6:"custom";a:1:{i:0;s:278:"<script type='text/javascript'>
/*<![CDATA[*/
	var jax_live_site = 'http://elijah.com.br/tandava/2010/index.php';
	var jax_site_type = '1.5';
/*]]>*/
</script><script type="text/javascript" src="http://elijah.com.br/tandava/2010/plugins/system/pc_includes/ajax_1.3.js"></script>";}}s:7:"pathway";a:1:{i:0;O:8:"stdClass":2:{s:4:"name";s:9:"Ingressos";s:4:"link";s:20:"index.php?Itemid=168";}}s:6:"module";a:0:{}}