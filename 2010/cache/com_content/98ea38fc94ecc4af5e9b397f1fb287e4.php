<?php die("Access Denied"); ?>
a:4:{s:4:"body";s:32477:"
<div class="rt-joomla ">
	<div class="rt-blog">

				<h1 class="rt-pagetitle">Atrações do Tandava Gathering</h1>
		
				<div class="rt-description">
										<p style="text-align: justify;">A edição 2010 do Tandava teve mais de 80 horas de música em 4 dias de festival multiplicados por 2 pistas. Os artistas foram escolhidos cuidadosamente pela oganização e a diversidade sonora é o que se ouve durante o encontro.&nbsp;</p>
<p style="text-align: justify;">A pista principal contou com grandes nomes internacionais e nacionais, como <a href="index.php?option=com_content&amp;view=article&amp;id=107:zaraus-live-portugual&amp;catid=45&amp;Itemid=195"><strong>Zaraus LIVE (Portugal)</strong></a>, <a href="index.php?option=com_content&amp;view=article&amp;id=104:rex-africa-do-sul&amp;catid=45&amp;Itemid=192"><strong>Rex (África do Sul)</strong></a>, <strong><a href="index.php?option=com_content&amp;view=article&amp;id=103:minimal-criminal-live-rj&amp;catid=45&amp;Itemid=193" target="_blank">Minimal Criminal LIVE (RJ)</a></strong>&nbsp;e&nbsp;<a href="index.php?option=com_content&amp;view=article&amp;id=117:sallun-pedra-branca-sp&amp;catid=45&amp;Itemid=172"><strong>Sallun (Pedra Branca - SP)</strong></a>, recebendo no último dia uma festa especial da gravadora <strong><a href="index.php?option=com_content&amp;view=article&amp;id=116:synk-recs-day-party&amp;catid=45&amp;Itemid=172">SYNK Records</a> </strong>para o encerramento do festival com destaque para a apresentação de <a href="index.php?option=com_content&amp;view=article&amp;id=115:truati-live-sp&amp;catid=45&amp;Itemid=172"><strong>Truati LIVE</strong></a>, <a href="index.php?option=com_content&amp;view=article&amp;id=114:rodrigo-carreira-pr&amp;catid=45&amp;Itemid=172">Rodrigo Carreira</a>, <a href="index.php?option=com_content&amp;view=article&amp;id=123:gabriel-boni-pr&amp;catid=45&amp;Itemid=172">Gabriel Boni</a>, <a href="index.php?option=com_content&amp;view=article&amp;id=113:gabi-lima-pr&amp;catid=45&amp;Itemid=172">Gabi Lima</a>, Nav e <a href="index.php?option=com_content&amp;view=article&amp;id=122:rodrigo-nickel-pr&amp;catid=45&amp;Itemid=172">Rodrigo Nickel</a>.</p>
<p style="text-align: justify;">A segunda pista, o Chill Out, assume o lugar para degustar sons alternativos à noite com festas que são referência em qualidade.</p>
<p style="text-align: justify;">Na sexta-feira, comandada pelos DJs Murillo e Schasko, aconteceu a festa <a href="index.php?option=com_content&amp;view=article&amp;id=93:funk-you&amp;catid=45&amp;Itemid=176"><strong>Funk You</strong></a>, referência do Funk Old School ao New School. Na segunda noite, Sábado, hospedou a festa <a href="index.php?option=com_content&amp;view=article&amp;id=106:vive-la-musique&amp;catid=45&amp;Itemid=196"><strong>Vive La Musique</strong></a>, famosa no circuito Electro Indie "<em>New Rave" </em>em Curitiba que além dos residentes conntará com<strong></strong><strong></strong> vários convidados especiais. E, no encerramento, a festa onde são proibidas músicais originais: <strong><a target="_blank" href="index.php?option=com_content&amp;view=article&amp;id=132:reboot&amp;catid=45&amp;Itemid=204">Reboot</a></strong> com os DJs Feiges, RenanF e Murillo e performances de <strong>Yamballoon Circus Arts</strong>.</p>
<p style="text-align: justify;">Conheça melhor as atrações da edição 2010 do festival abaixo.</p>
<blockquote>
<p>"As coisas não vão mudar e nada  será&nbsp;resgatado."</p>
</blockquote>
<p><cite><a href="../">Tandava Gathering</a></cite>, 2010.</p><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/atracoes-tandava.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>					</div>
		
				<div class="rt-leading-articles">
										
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/atracoes-tandava/funk-you.html">Funk You</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<p><img style="margin: 0px 10px 10px 0px; float: left;" alt="funk-you_thumb" src="images/stories/atracoes/funk-you_thumb.jpg" height="225" width="225" />Abrindo a primeira noite do Chill Out a festa <strong>Funk You</strong>, projeto que apresenta a&nbsp; música "negra", batidas quentes e extremamente dançantes! Do <em>Funk Old School</em> ao <em>New School</em>, <em>NuFunk</em>, <em>Breaks</em>, <em>Big Beat</em>, <em>Disco</em> e <em>Hip Hop</em>.</p>
<p>Com pouco mais de dois anos, a festa já virou referencia de Funk e breaks em Curitiba comandada pelos DJs Murillo e Schasko, sempre recebendo convidados da cena curitibana e nacional.</p>
<blockquote>
<p><strong>+ INFO: </strong><a target="_blank" href="http://www.myspace.com/funkyoucwb">www.myspace.com/funkyoucwb</a></p>
</blockquote><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/atracoes-tandava/funk-you.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/atracoes-tandava/93-funk-you.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/atracoes-tandava/93-funk-you.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL3d3dy5lbGlqYWguY29tLmJyL3RhbmRhdmEvMjAxMC9hdHJhY29lcy10YW5kYXZhL2Z1bmsteW91Lmh0bWw%3D" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Quarta, 16 de Junho de 2010 22:43				</span>
					
								<span class="rt-date-modified">
					Última atualização em Terça, 13 de Julho de 2010 20:54				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>									
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/atracoes-tandava/vive-la-musique.html">Vive La Musique</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<p style="text-align: justify;"><img style="margin: 0px 10px 25px 0px; float: left;" alt="vivelamusique" src="images/stories/atracoes/vivelamusique.png" height="150" width="225" /><strong></strong></p>
<p style="text-align: justify;">Durante a noite de Sábado, o Chill Out se transforma com o som dançante da "Vive" e o espírito da cena Electro Indie curitibana.</p>
<p style="text-align: justify;">A  festa Vive La Musique é produzida pelos DJs <a href="index.php?option=com_content&amp;view=article&amp;id=110:andre-nego-pr&amp;catid=45&amp;Itemid=172">André Nego</a> e <a href="index.php?option=com_content&amp;view=article&amp;id=111:joel-guglielmini-pr&amp;catid=45&amp;Itemid=172">Joel  Guglielmini</a> e procura unir música e arte, com diversas exposições,  performances e trabalhos de novos artistas, como Estúdio Rasputines,  criadores da logo da festa.</p>
<p style="text-align: justify;">Em setembro de 2009 a Vive completou um ano de vida com 10 Edições de muito sucesso. Pela festa já passaram grandes DJs, Daniel Peixoto (ex-Montage), Raul  Aguilera, Jô Mistinguett, Barbarela, All Starz (Posh – SP), Tchiello K  (Crew – SP), Killer on The Dancefloor (Crew – SP), Boss In Drama, Dirty  Noise (RS), Elijah Hatem, dentre outros DJs da cena local de Curitiba como  Gee X, Bogus, Renan Mendes, Sandra Carraro, além do ex-residente Cacá  Azevedo.</p>
<p style="text-align: justify;">O projeto teve início no clube Soho Underground e passou pelo Espaço  Roxy, Mahatma Electro Lounge, Circus Bar,&nbsp; Kitinete, Camden Club, Era Só e atualmente segue em formato itinerante por diversos clubs do circuito alternativo curitibano.<br /> <br /> Electro, electro house, disco punk, fidget house, new rave, techno, rock  e maximal são os estilos que predominam na pista da Vive La Musique. Aguardem uma noite cheia de atrações especiais e DJs convidados!</p>
<blockquote>
<p><strong>+ INFO: </strong><a target="_blank" href="http://www.vivelamusique.com.br">www.vivelamusique.com.br</a></p>
</blockquote><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/atracoes-tandava/vive-la-musique.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/atracoes-tandava/106-vive-la-musique.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/atracoes-tandava/106-vive-la-musique.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL3d3dy5lbGlqYWguY29tLmJyL3RhbmRhdmEvMjAxMC9hdHJhY29lcy10YW5kYXZhL3ZpdmUtbGEtbXVzaXF1ZS5odG1s" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Segunda, 02 de Agosto de 2010 18:37				</span>
					
								<span class="rt-date-modified">
					Última atualização em Segunda, 02 de Agosto de 2010 21:09				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>									
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/atracoes-tandava/reboot.html">Reboot</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<p style="text-align: justify;"><img style="margin: 0px 10px 10px 0px; float: left;" alt="reboot" src="images/stories/atracoes/reboot.png" width="225" height="225" /><strong>REBOOT</strong> [ri:'bu:t] : reiniciar o computador ou sistema.</p>
<p style="text-align: justify;">Que é isso afinal? REBOOT é um projeto de releituras musicais, cujo nome é a contração de dois termos musicais (REMIXES+BOOTLEGS=RE.BOOT).</p>
<p style="text-align: justify;">REMIX: é reconstrução de uma faixa musical com base em elementos do original adicionados de novos elementos.</p>
<p style="text-align: justify;">BOOTLEG, MASHUP: ambos os termos referem-se a recriações musicais a partir da fusão de canções e instrumentais de diferentes artistas, também mesclando, em geral, gêneros musicais diversos.</p>
<p style="text-align: justify;">A autenticidade das recriações produzidas em home-studios reside exatamente na reorganização de diferentes elementos de cada original utilizado, compondo obras artísticas criativas a partir de recortes, colagens e processamento de efeitos.</p>
<p style="text-align: justify;">O produto desta técnica também ficou conhecido como BASTARD POP, por misturar estilos como Rock, Funk, Hip-Hop e Pop, sendo então considerado como filho ilegítimo deste último.</p>
<p style="text-align: justify;">O conceito não se esgota aí... já se podem ver releituras de artes visuais, remixes e mashups de filmes e video-clips musicais, até mesmo misturas de trailers, teasers, animações, fotos, cartazes e flyers. O próprio projeto também não se resume à abordagem musical e também apresenta performances cênicas e circenses.</p>
<p style="text-align: justify;">A proposta cultural do projeto REBOOT pode ser resumida no lema/desafio: PROIBIDO ORIGINAIS !!!</p>
<h4 style="text-align: justify;">Artistas:</h4>
<p style="text-align: justify;">- DJs Feiges, RenanF e Murillo</p>
<h4>- Performances:</h4>
<p style="text-align: justify;">Yamballoon Circus Arts.</p>
<p><a href="images/stories/reboot/feiges_02.jpg" rel="rokbox[400 600](reboot)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//feiges_02_thumb.jpg" alt="" /></a> <a href="images/stories/reboot/feiges_03.jpg" rel="rokbox[460 400](reboot)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//feiges_03_thumb.jpg" alt="" /></a> <a href="images/stories/reboot/feiges_05.jpg" rel="rokbox[528 400](reboot)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//feiges_05_thumb.jpg" alt="" /></a> <a href="images/stories/reboot/murillo_01.jpg" rel="rokbox[599 400](reboot)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//murillo_01_thumb.jpg" alt="" /></a> <a href="images/stories/reboot/murillo_05.jpg" rel="rokbox[400 600](reboot)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//murillo_05_thumb.jpg" alt="" /></a> <a href="images/stories/reboot/renan_02.jpg" rel="rokbox[414 400](reboot)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//renan_02_thumb.jpg" alt="" /></a> <a href="images/stories/reboot/renan_05.jpg" rel="rokbox[445 400](reboot)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//renan_05_thumb.jpg" alt="" /></a> <a href="images/stories/reboot/yamba_01.jpg" rel="rokbox[600 400](reboot)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//yamba_01_thumb.jpg" alt="" /></a> <a href="images/stories/reboot/yamba_03.jpg" rel="rokbox[400 600](reboot)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//yamba_03_thumb.jpg" alt="" /></a> </p>
<blockquote>
<p><strong>+ INFO: </strong><a target="_blank" href="http://reboot.blog.com">reboot.blog.com</a></p>
</blockquote><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/atracoes-tandava/reboot.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/atracoes-tandava/132-reboot.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/atracoes-tandava/132-reboot.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL3d3dy5lbGlqYWguY29tLmJyL3RhbmRhdmEvMjAxMC9hdHJhY29lcy10YW5kYXZhL3JlYm9vdC5odG1s" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Sexta, 15 de Outubro de 2010 19:23				</span>
					
								<span class="rt-date-modified">
					Última atualização em Segunda, 18 de Outubro de 2010 12:31				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>									
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/atracoes-tandava/synk-recs-day-party.html">Synk Recs Day Party</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<div class="rt-article-content">
<p style="text-align: justify;"><img style="margin: 0px 10px 25px 0px; float: left;" alt="synk-recs" src="images/stories/atracoes/synk-recs.png" width="225" height="150" /><strong></strong></p>
<p style="text-align: justify;">No último dia do festival – feriado de Segunda-Feira, 15 de Novembro – o <strong>Tandava Gathering</strong> recebe uma festa especial da gravadora <strong>SYNK Records </strong>para o encerramento da edição 2010.</p>
<p style="text-align: justify;">A festa inicia pela manhã e se prolonga pelo último dia com destaque para a apresentação de <a href="index.php?option=com_content&amp;view=article&amp;id=115:truati-live-sp&amp;catid=45&amp;Itemid=172"><strong>Truati LIVE</strong></a>, <a href="index.php?option=com_content&amp;view=article&amp;id=114:rodrigo-carreira-pr&amp;catid=45&amp;Itemid=172">Rodrigo Carreira</a>, <a target="_self" href="index.php?option=com_content&amp;view=article&amp;id=123:gabriel-boni-pr&amp;catid=45&amp;Itemid=172">Gabriel Boni</a>, <a href="index.php?option=com_content&amp;view=article&amp;id=113:gabi-lima-pr&amp;catid=45&amp;Itemid=172">Gabi Lima</a> e <a href="index.php?option=com_content&amp;view=article&amp;id=122:rodrigo-nickel-pr&amp;catid=45&amp;Itemid=172">Rodrigo Nickel</a>.</p>
<p style="text-align: justify;">Um encerramento com qualidade e sonoridade sem igual. Aguardem pelo Line Up completo!</p>
<p style="text-align: justify;"><strong>+ SOBRE A SYNK</strong></p>
<p>Pensando e distribuindo música digital desde 2007, a Synk Records  oferece inovação, lançando composições musicais que estimulam o público à  novas percepções criativas.</p>
<p>Transcendendo a representação de gêneros e estilos pré-definidos, a  Synk Records posiciona-se no mercado através da seleção de artistas  antenados, que misturam o uso de tecnologia de ponta, musicalidade e  ritmo para as pistas de dança.</p>
<p style="text-align: justify;">A marca Synk foi idealizada e fundada pelos artistas Rodrigo  Carreira, Leandro Pacceli e Gabi em parceria com o selo Presslab Records  (Itália), e seus integrantes: Omar Neri e Luigi Gori (Presslaboys).</p>
<blockquote>
<p><strong>+ INFO: </strong><a target="_blank" href="http://www.synk.com.br/">www.synk.com.br</a></p>
</blockquote>
</div><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/atracoes-tandava/synk-recs-day-party.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/atracoes-tandava/116-synk-recs-day-party.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/atracoes-tandava/116-synk-recs-day-party.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL3d3dy5lbGlqYWguY29tLmJyL3RhbmRhdmEvMjAxMC9hdHJhY29lcy10YW5kYXZhL3N5bmstcmVjcy1kYXktcGFydHkuaHRtbA%3D%3D" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Terça, 03 de Agosto de 2010 22:10				</span>
					
								<span class="rt-date-modified">
					Última atualização em Quinta, 09 de Setembro de 2010 12:40				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>									
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/atracoes-tandava/zaraus-live-portugal.html">Zaraus LIVE (Portugal)</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<p style="text-align: justify;"><img style="margin: 0px 10px 10px 0px; float: left;" alt="zaraus" src="images/stories/atracoes/zaraus.jpg" height="343" width="225" /><strong>Pedro Oliveira</strong> aka <strong>Zaraus</strong>, nascido em 81. Estudou Engenharia de Áudio por 3 anos, Midi e Síntese na Dance Planet em Lisboa. Escuta Trance desde 1997. Viveu no Reino Unido 2001 a 2003, na Holanda em 2003, no Brasil de 2004 a 2006 e agora mora em Lisboa.</p>
<p style="text-align: justify;">Como a maioria dos artistas de Trance de Portugal, Zaraus foi altamente influenciado pelas festas Trance "old school" de Portugal quando as festas de Psy não eram infectadas pela música regressiva, e quando apenas se tocava o verdadeiro Goa e música para frente.</p>
<p style="text-align: justify;">Algumas influências em nomes: Phyx, Clock Wise, Shift, Menog, Matt V, Man With No Name, Kox Box, Green Ohms, Cydonia, Paul Taylor, Dick Trevor, Dino Psaras, Green Nuns Of The Revolution, Disckster, Eat Static, Infected Mushroom antigo, Skazi antigo, GMS antigo, Astrix antigo e muito muitos outros...</p>
<blockquote>
<p><strong>+ INFO: </strong><a target="_blank" href="http://www.myspace.com/zaraus">www.myspace.com/zaraus</a></p>
</blockquote>
<p><strong>+ MÚSICAS:</strong></p>
<object codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=10,0,32,18" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" type="application/x-shockwave-flash" height="225" width="100%">
<param name="allowscriptaccess" value="always" />
<param name="src" value="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fsoundcloud.com%2Fzaraus&amp;secret_url=false" /><embed height="225" width="100%" src="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fsoundcloud.com%2Fzaraus&amp;secret_url=false" allowscriptaccess="always" type="application/x-shockwave-flash"></embed>
</object>
<span><a href="http://soundcloud.com/zaraus">Latest tracks by Zaraus</a></span><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/atracoes-tandava/zaraus-live-portugal.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/atracoes-tandava/107-zaraus-live-portugual.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/atracoes-tandava/107-zaraus-live-portugual.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL3d3dy5lbGlqYWguY29tLmJyL3RhbmRhdmEvMjAxMC9hdHJhY29lcy10YW5kYXZhL3phcmF1cy1saXZlLXBvcnR1Z2FsLmh0bWw%3D" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Segunda, 02 de Agosto de 2010 18:47				</span>
					
								<span class="rt-date-modified">
					Última atualização em Segunda, 02 de Agosto de 2010 20:38				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>									
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/atracoes-tandava/rex-africa-do-sul.html">Rex (África do Sul)</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<p style="text-align: justify;"><img style="margin: 0px 10px 10px 0px; float: left;" alt="REX" src="images/stories/atracoes/REX.jpg" width="225" height="169" />REX é de Cape Town, África do Sul e representa a NANO Records desde 2004.</p>
<p style="text-align: justify;">Sua carreira de DJ iniciou em 1996 quando Rex começou na cena underground como DAT Jockey, tocando GOA e evoluindo para o Psy-trance com o passar dos anos. Ao mesmo tempo em que tocava, viajava e organizava festas pelo mundo. Viveu na Europa por 6 anos, onde foi responsável pelas festas underground com o nome de Indigo Children - uma das maiores e mais notórias festas indoor e outdoor em Londres em 2003/04.</p>
<p style="text-align: justify;">Isso possibilitou com que ele entrasse em contato com diferentes artistas que o inspiraram, além de pessoas de todas as partes do mundo e com culturas muito diferentes. Tudo isso contribuiu na construção de um tipo único de música o que é ser dj e finalmente produzir música.</p>
<p style="text-align: justify;">No momento, Rex está no Brasil cuidando dos negócios da NANO na América do Sul, além, é claro, de atrair multidões com músicas da NANO de todas as partes do Mundo. Rex já se apresentou nos festivais mais importantes do Brasil como Universo Paralello, Trancendence e TranceFormation; além de festas pelos quatro cantos do pais.</p>
<blockquote>
<p><strong>+ INFO: </strong><a target="_blank" href="http://www.myspace.com/djrexnano">www.myspace.com/djrexnano</a></p>
</blockquote><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/atracoes-tandava/rex-africa-do-sul.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/atracoes-tandava/104-rex-africa-do-sul.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/atracoes-tandava/104-rex-africa-do-sul.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL3d3dy5lbGlqYWguY29tLmJyL3RhbmRhdmEvMjAxMC9hdHJhY29lcy10YW5kYXZhL3JleC1hZnJpY2EtZG8tc3VsLmh0bWw%3D" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Segunda, 02 de Agosto de 2010 12:55				</span>
					
								<span class="rt-date-modified">
					Última atualização em Segunda, 02 de Agosto de 2010 17:39				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>				</div>
		
				<div class="clear"></div>
		
				<div class="rt-pagination">
						<p class="rt-results">
				Página 1 de 7			</p>
						
<div class="tab">
<div class="tab2"><div class="page-active">Início</div></div></div>
<div class="tab">
<div class="tab2"><div class="page-active">Anterior</div></div></div>
<div class="page-block"><div class="page-active">1</div></div>
<div class="page-block"><div class="page-inactive"><a href="/tandava/2010/atracoes-tandava.html?start=6" title="2">2</a></div></div>
<div class="page-block"><div class="page-inactive"><a href="/tandava/2010/atracoes-tandava.html?start=12" title="3">3</a></div></div>
<div class="page-block"><div class="page-inactive"><a href="/tandava/2010/atracoes-tandava.html?start=18" title="4">4</a></div></div>
<div class="page-block"><div class="page-inactive"><a href="/tandava/2010/atracoes-tandava.html?start=24" title="5">5</a></div></div>
<div class="page-block"><div class="page-inactive"><a href="/tandava/2010/atracoes-tandava.html?start=30" title="6">6</a></div></div>
<div class="page-block"><div class="page-inactive"><a href="/tandava/2010/atracoes-tandava.html?start=36" title="7">7</a></div></div>
<div class="tab">
<div class="tab2"><div class="page-inactive"><a href="/tandava/2010/atracoes-tandava.html?start=6" title="Próximo">Próximo</a></div></div></div>
<div class="tab">
<div class="tab2"><div class="page-inactive"><a href="/tandava/2010/atracoes-tandava.html?start=36" title="Fim">Fim</a></div></div></div>		</div>
				
	</div>
</div>";s:4:"head";a:10:{s:5:"title";s:31:"Atrações do Tandava Gathering";s:11:"description";s:275:"Não surgiu um novo conceito. As coisas não vão mudar e nada será resgatado.  Não  é nada melhor ou maior de tudo que você já viu. Não  é inédito e nem ao menos é um evento. O respeito existe, mas o publico não, existe um encontro entre amigos e amigos de amigos.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:24:"text/html; charset=utf-8";}s:8:"standard";a:5:{s:6:"robots";s:13:"index, follow";s:8:"keywords";s:54:"tandava, gathering, festival, 2010, curitiba, novembro";s:8:"og:title";s:20:"Rex (África do Sul)";s:12:"og:site_name";s:17:"Tandava Gathering";s:8:"og:image";s:69:"http://www.elijah.com.br/tandava/2010/images/stories/atracoes/REX.jpg";}}s:5:"links";a:2:{i:0;s:116:"<link href="/tandava/2010/atracoes-tandava.feed?type=rss" rel="alternate" type="application/rss+xml" title="RSS 2.0"";i:1;s:119:"<link href="/tandava/2010/atracoes-tandava.feed?type=atom" rel="alternate" type="application/atom+xml" title="Atom 1.0"";}s:11:"styleSheets";a:0:{}s:5:"style";a:0:{}s:7:"scripts";a:2:{s:41:"/tandava/2010/media/system/js/mootools.js";s:15:"text/javascript";s:40:"/tandava/2010/media/system/js/caption.js";s:15:"text/javascript";}s:6:"script";a:0:{}s:6:"custom";a:1:{i:0;s:286:"<script type='text/javascript'>
/*<![CDATA[*/
	var jax_live_site = 'http://www.elijah.com.br/tandava/2010/index.php';
	var jax_site_type = '1.5';
/*]]>*/
</script><script type="text/javascript" src="http://www.elijah.com.br/tandava/2010/plugins/system/pc_includes/ajax_1.3.js"></script>";}}s:7:"pathway";a:1:{i:0;O:8:"stdClass":2:{s:4:"name";s:10:"Atrações";s:4:"link";s:20:"index.php?Itemid=172";}}s:6:"module";a:0:{}}