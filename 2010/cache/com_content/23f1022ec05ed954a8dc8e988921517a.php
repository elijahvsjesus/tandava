<?php die("Access Denied"); ?>
a:4:{s:4:"body";s:16710:"
<div class="rt-joomla ">
	<div class="rt-blog">

				<h1 class="rt-pagetitle">Local do Tandava Gathering</h1>
		
				<div class="rt-description">
										<p style="text-align: justify;">O festival <strong>Tandava Gathering</strong> acontece anualmente no Paraná nos arredores de Curitiba. A localização e&nbsp; o mapa do local somente são divulgados na semana do encontro para todos os participantes cadastrados.</p>
<p style="text-align: justify; display: none;">Essa é uma das características que diferencia o Tandava de um evento, não existe venda de ingressos para o festival na portaria. Aliás, não existe portaria, existe uma recepção aos participantes previamente inscritos.</p>
<blockquote>
<p>"Não&nbsp; é&nbsp;inédito e nem ao menos é um evento."</p>
</blockquote>
<p><cite><a href="index.php?option=com_content&amp;view=section&amp;id=6&amp;Itemid=164">Tandava Gathering</a></cite>, Edição 2010.</p><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/local.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>					</div>
		
				<div class="rt-leading-articles">
										
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/local/142-sobre-o-local.html">Sobre o Local</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<p style="text-align: justify;">A edição 2010 do <strong>Tandava Gathering</strong> aconteceu em uma propriedade com 120 alqueires totalmente integrados a natureza, situados em local privilegiado com vista para a Serra do Mar e cercados por rios de águas claras.</p>
<p style="text-align: justify;">Esse espaço é mata nativa e área de preservação ambiental, TODO CUIDADO COM O LIXO É NECESSÁRIO!</p>
<p style="text-align: justify;">Entre as paisagens, uma cachoeira liberada para banho dentro e durante todo o festival.  Trilhas levam a grutas, lagos, 2 diferentes rios e uma infinidade de belezas naturais. Passeios a cavalo e trekking estão entre as atividades desenvolvidas com acompanhamento de instrutores/guias.</p>
<h4 style="text-align: justify;">Fotos do Local</h4>
<p><a href="images/stories/fotos/2010/local/DSC000795.JPG" rel="rokbox[640 480](tandava2010local)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC000795_thumb.JPG" alt="" /></a> <a href="images/stories/fotos/2010/local/DSC00715.JPG" rel="rokbox[640 480](tandava2010local)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC00715_thumb.JPG" alt="" /></a> <a href="images/stories/fotos/2010/local/DSC00716.JPG" rel="rokbox[640 480](tandava2010local)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC00716_thumb.JPG" alt="" /></a> <a href="images/stories/fotos/2010/local/DSC00729.JPG" rel="rokbox[640 480](tandava2010local)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC00729_thumb.JPG" alt="" /></a> <a href="images/stories/fotos/2010/local/DSC00747.JPG" rel="rokbox[640 480](tandava2010local)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC00747_thumb.JPG" alt="" /></a> <a href="images/stories/fotos/2010/local/DSC00750.JPG" rel="rokbox[640 480](tandava2010local)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC00750_thumb.JPG" alt="" /></a> <a href="images/stories/fotos/2010/local/DSC00752.JPG" rel="rokbox[480 640](tandava2010local)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC00752_thumb.JPG" alt="" /></a> <a href="images/stories/fotos/2010/local/DSC00755.JPG" rel="rokbox[384 512](tandava2010local)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC00755_thumb.JPG" alt="" /></a> <a href="images/stories/fotos/2010/local/DSC00757.JPG" rel="rokbox[384 512](tandava2010local)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC00757_thumb.JPG" alt="" /></a> <a href="images/stories/fotos/2010/local/DSC00776.JPG" rel="rokbox[640 480](tandava2010local)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC00776_thumb.JPG" alt="" /></a> <a href="images/stories/fotos/2010/local/DSC00778.JPG" rel="rokbox[640 480](tandava2010local)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC00778_thumb.JPG" alt="" /></a> <a href="images/stories/fotos/2010/local/DSC00809.JPG" rel="rokbox[640 480](tandava2010local)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC00809_thumb.JPG" alt="" /></a> </p><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/local/142-sobre-o-local.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/local/142-sobre-o-local.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/local/142-sobre-o-local.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL3d3dy5lbGlqYWguY29tLmJyL3RhbmRhdmEvMjAxMC9sb2NhbC8xNDItc29icmUtby1sb2NhbC5odG1s" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Terça, 26 de Outubro de 2010 17:21				</span>
					
								<span class="rt-date-modified">
					Última atualização em Quarta, 17 de Novembro de 2010 18:10				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>									
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/local/91-estrutura.html">Estrutura</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<p>Toda estrutura necessária para uma estadia agradável e segura dos participantes do <strong>Tandava Gathering </strong>é planejada.</p>
<p>O local do encontro possui estrutura completa como:</p>
<ul class="bullet-c">
<li>Área para Camping.</li>
<li>Área para refeições e alimentação.</li>
<li>Banheiros femininos e masculinos.</li>
<li>Chuveiros quentes.</li>
<li>Bar e barracas com opções de alimentação.</li>
<li>Pontos de energia elétrica.</li>
<li>Será permitido fogueiras.</li>
</ul>
<h4>Refeições e Alimentação</h4>
<p>É permitida a entrada com comida no evento e também será disponibilizado área para alimentação.</p>
<p>Será servido almoço e café da manhã todos os dias (SÁB / DOM / SEG).</p>
<p>O café da manhã será servido das 07:00 as 10:30 e o almoço será servido das 12:00 as 15:30.</p>
<p>É altamente recomendado ficar atento aos horários das refeições para poder aproveitar todo o encontro com energia suficiente! Não será servido jantar em nenhum dia, porém paralelo as refeições existirão, funcionado em tempo integral, barracas com opções de alimentação (variando do fast food à alimentação vegetariana).</p>
<p><em>Obs.: O valor das refeições <strong>não</strong> está incluído no valor dos ingressos.</em></p>
<h4>Bebidas e Bar</h4>
<p>É permitida a entrada com bebidas no evento e também será disponibilizado bar no local com estrutura completa e a intenção de suprir os menos organizados que esquecerem seu kit bebidas.</p>
<p>Os preços praticados pelo bar são divulgados com antecedência, conforme tabela abaixo:</p>
<pre>Cerveja (Lata 355 ml) - R$3,00<br />Refrigerante - R$3,00<br />Água - R$2,00<br />Dose de Bacardi Apple / Vodka Smirnoff - R$5,00<br />Copo de Catuaba - R$5,00 <br />Dose de Whisky Natu Nobilis - R$5,00<br />Dose de Whisky J. Walker Red - R$8,00<br />Energético - R$8,00 <br />Copo de Gelo – R$ 1,00<br /></pre>
<ul class="bullet-c">
</ul><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/local/91-estrutura.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/local/91-estrutura.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/local/91-estrutura.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL3d3dy5lbGlqYWguY29tLmJyL3RhbmRhdmEvMjAxMC9sb2NhbC85MS1lc3RydXR1cmEuaHRtbA%3D%3D" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Quarta, 16 de Junho de 2010 22:16				</span>
					
								<span class="rt-date-modified">
					Última atualização em Quarta, 27 de Outubro de 2010 14:02				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>									
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/local/mapa-como-chegar-tandava-2010.html">Mapa Tandava 2010</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<p style="text-align: justify;">A edição 2010 do <strong>Tandava Gathering</strong> aconteceu em uma  propriedade com 120 alqueires totalmente integrados a natureza, situados  em local privilegiado com vista para a Serra do Mar e cercados por rios  de águas claras.</p>
<p style="text-align: justify;">A localização e o mapa do local são divulgados apenas para todos os participantes cadastrados. Confira abaixo:</p>
<h2>Como Chegar ao Tandava 2010</h2>
<p style="text-align: justify;">O local se chama <strong>Haras Cartel </strong>e fica a cerca de 40km da Grande Curitiba. Confira o mapa abaixo, faça o <a target="_blank" href="images/stories/mapa/tandava2010-mapa-como-chegar.pdf">download</a> da <a target="_blank" href="images/stories/mapa/tandava2010-mapa-como-chegar.pdf">versão para impressão</a> e <a href="#instrucoes">leia as instruções</a>!</p>
<p>Get ready!</p>
<h4>Visualizar Mapa</h4>
<p style="text-align: center;"><a href="images/stories/mapa/tandava2010-mapa-como-chegar.jpg" rel="rokbox[900 590]" title="Mapa. Como Chegar ao Tandava 2010"><img class="album" src="http://www.tandava.com.br/images/stories/mapa/tandava2010-mapa-como-chegar_thumb.jpg" alt="Mapa. Como Chegar ao Tandava 2010" /></a> </p>
<h4>Download Mapa - Versão para Impressão (PDF)</h4>
<div class="download">
<div class="typo-icon"><a target="_blank" href="images/stories/mapa/tandava2010-mapa-como-chegar.pdf">Clique Aqui e baixe o Mapa e Instruções de Como Chegar ao Tandava 2010.</a></div>
</div>
<h4><a name="instrucoes"></a>Instruções</h4>
<ul class="bullet-c">
<li><strong>De Curitiba:</strong></li>
</ul>
<ul class="bullet-3">
<li>Pegar a Av. Das Torres e, após Portal de São José dos Pinhais, seguir pela BR 376 sentido Joinville.</li>
<li>Seguir por aproximadamente 40 km, passando pelo Contorno Sul, Pedágio e Posto Receita Estadual até o Posto Monte Carlo II.</li>
<li>1.4km após o Posto Monte Carlo II, entrar a direita no KM 648 próximo ao retorno.</li>
<li>Após 100m virar a direita e seguir placas indicativas para Haras Cartel de acordo com o mapa.</li>
</ul>
<ul class="bullet-c">
<li><strong>De Santa Catarina:</strong></li>
</ul>
<ul class="bullet-3">
<li>Pegar a BR 376 Joinville e seguir sentido Curitiba.</li>
<li>Após Posto Monte Carlo no KM 652, seguir por aproximadamente 5km e fazer o retorno.</li>
<li>Entrar a direita na entrada no KM 648.</li>
<li>Após 100m virar a direita e seguir placas indicativas para Haras Cartel de acordo com o mapa.</li>
</ul>
<h4>Fotos e Estrutura do Tandava 2010</h4>
<div class="camera">
<div class="typo-icon"><a href="local.html">Saiba Mais sobre o local, suas belezas naturais e a estrutura do Tandava 2010.</a></div>
</div>
<h4>IMPORTANTE!</h4>
<ul class="bullet-c">
<li>Lembre-se que à noite a visibilidade é menor, então vá devagar para conseguir visualizar todas as placas.</li>
<li>Se beber, NÃO dirija!</li>
</ul><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/local/mapa-como-chegar-tandava-2010.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/local/89-mapa-como-chegar.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/local/89-mapa-como-chegar.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL3d3dy5lbGlqYWguY29tLmJyL3RhbmRhdmEvMjAxMC9sb2NhbC9tYXBhLWNvbW8tY2hlZ2FyLXRhbmRhdmEtMjAxMC5odG1s" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Quarta, 16 de Junho de 2010 22:09				</span>
					
								<span class="rt-date-modified">
					Última atualização em Quarta, 17 de Novembro de 2010 18:11				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>							</div>
		
				<div class="clear"></div>
		
				
	</div>
</div>";s:4:"head";a:10:{s:5:"title";s:26:"Local do Tandava Gathering";s:11:"description";s:275:"Não surgiu um novo conceito. As coisas não vão mudar e nada será resgatado.  Não  é nada melhor ou maior de tudo que você já viu. Não  é inédito e nem ao menos é um evento. O respeito existe, mas o publico não, existe um encontro entre amigos e amigos de amigos.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:24:"text/html; charset=utf-8";}s:8:"standard";a:5:{s:6:"robots";s:13:"index, follow";s:8:"keywords";s:54:"tandava, gathering, festival, 2010, curitiba, novembro";s:8:"og:title";s:17:"Mapa Tandava 2010";s:12:"og:site_name";s:17:"Tandava Gathering";s:8:"og:image";s:122:"http://www.elijah.com.br/tandava/2010/http://www.tandava.com.br/images/stories/mapa/tandava2010-mapa-como-chegar_thumb.jpg";}}s:5:"links";a:2:{i:0;s:105:"<link href="/tandava/2010/local.feed?type=rss" rel="alternate" type="application/rss+xml" title="RSS 2.0"";i:1;s:108:"<link href="/tandava/2010/local.feed?type=atom" rel="alternate" type="application/atom+xml" title="Atom 1.0"";}s:11:"styleSheets";a:0:{}s:5:"style";a:0:{}s:7:"scripts";a:2:{s:41:"/tandava/2010/media/system/js/mootools.js";s:15:"text/javascript";s:40:"/tandava/2010/media/system/js/caption.js";s:15:"text/javascript";}s:6:"script";a:0:{}s:6:"custom";a:1:{i:0;s:286:"<script type='text/javascript'>
/*<![CDATA[*/
	var jax_live_site = 'http://www.elijah.com.br/tandava/2010/index.php';
	var jax_site_type = '1.5';
/*]]>*/
</script><script type="text/javascript" src="http://www.elijah.com.br/tandava/2010/plugins/system/pc_includes/ajax_1.3.js"></script>";}}s:7:"pathway";a:1:{i:0;O:8:"stdClass":2:{s:4:"name";s:5:"Local";s:4:"link";s:20:"index.php?Itemid=174";}}s:6:"module";a:0:{}}