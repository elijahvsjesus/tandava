<?php die("Access Denied"); ?>
a:4:{s:4:"body";s:31936:"
<div class="rt-joomla ">
	<div class="rt-blog">

				<h1 class="rt-pagetitle">Programação do Tandava Gathering</h1>
		
				<div class="rt-description">
										<p style="text-align: justify;">O <strong>Tandava</strong> é para todos que querem experimentar, para aqueles que  respeitam a si mesmo e aos outros. Na verdade é mais do que isso, é  feito para quem apenas deseja alguns dias fora da maçante realidade do  cotidiano.</p>
<p style="text-align: justify;">E tudo isso é materializado através da música e da arte.</p>
<p style="text-align: justify;">A música por  sua vez assume o papel de sincronizar, não só o a pista de dança, mas o  ritmo de tudo que acontece no festival. E a arte visa elevar, ainda que  por 3 dias, a percepção de você para com o mundo ao seu redor e,  principalmente, com você mesmo.</p>
<div class="note">
<div class="typo-icon"><span style="font-size: 8pt;">A programação e o cronograma das atividades e atrações poderão sofrer alterações devido a imprevistos, conflitos de agenda e deslocamento dos artistas.</span></div>
</div>
<blockquote>
<p>"É uma fuga para onde tempo e espaço estão aquém de afetar o relacionamento interpessoal."</p>
</blockquote>
<p><cite><a href="../">Tandava Gathering</a></cite>, 2010.</p><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://elijah.com.br/tandava/2010/programacao-tandava.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>					</div>
		
				<div class="rt-leading-articles">
										
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/programacao-tandava/133-line-up-completo.html">Line Up Completo</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<p>A edição 2010 do <strong>Tandava </strong>terá mais de 80 horas de música em 4 dias de  festival multiplicados por 2 pistas. Os artistas foram escolhidos  cuidadosamente pela oganização e a diversidade sonora é o que se ouve  durante o encontro.</p>
<p>Confira abaixo o <strong>LINE UP</strong> completo desta edição:</p>
<hr />
<h3>SEXTA.12.NOVEMBRO</h3>
<div id="2columns" style="display: inline-block;">
<div style="width: 300px; float: left;" id="mainstage">
<h4>Main Stage</h4>
<p>20:00 - Suissa vs Azahel (PR)</p>
<ul class="bullet-c">
<li><strong>Psy-Trance Full On</strong></li>
</ul>
<p>21:00 - Titicow (PR)<br /> 22:00 - Fernanda (PR)<br /> 23:00 - Dulio (PR)</p>
</div>
<div style="width: 300px; float: right;" id="chillout">
<h4>Chill Out Stage</h4>
<p>18:00 às 23:59 - FECHADO</p>
</div>
</div>
<div style="clear: both;"></div>
<hr />
<h3>SÁBADO.13.NOVEMBRO</h3>
<div id="2columns" style="display: inline-block;">
<div style="width: 300px; float: left;" id="mainstage">
<h4>Main Stage</h4>
<ul class="bullet-c">
<li><strong>Psy-Trance Full On</strong></li>
</ul>
<p>00:00 - <a href="index.php?option=com_content&amp;view=article&amp;id=137:anginha-rodrigues-pr&amp;catid=45&amp;Itemid=172">Anginha</a> vs Luana Cherry (PR)<br /> 01:00 - <strong><span style="font-size: 12pt;"><a href="index.php?option=com_content&amp;view=article&amp;id=107:zaraus-live-portugual&amp;catid=45&amp;Itemid=195">Zaraus LIVE (Portugal)</a></span></strong></p>
<ul class="bullet-c">
<li><strong>Dark Psychedelic</strong></li>
</ul>
<p>03:00 - <strong><span style="font-size: 12pt;"><a href="index.php?option=com_content&amp;view=article&amp;id=118:demonizz-live-sp&amp;catid=45&amp;Itemid=198">Demonizz LIVE (SP)</a></span></strong><br /> 04:00 - <strong><span style="font-size: 12pt;">Rodrigo CPU (PR)</span></strong></p>
<ul class="bullet-c">
<li><strong>Psy-Trance Full On</strong></li>
</ul>
<p>06:00 - <strong>Starting Up LIVE (PR)</strong><br />07:00 - <span style="font-size: 12pt;"><strong>SweDagon LIVE (PR)</strong></span><br /> 08:00 - <a href="index.php?option=com_content&amp;view=article&amp;id=138:clara-pr&amp;catid=45&amp;Itemid=172">Clara<span style="font-size: 8pt;"> (Psy Adonai - Bola de Neve PR)</span></a></p>
<ul class="bullet-c">
<li><strong>Progressive</strong></li>
</ul>
<p>09:00 - <span style="font-size: 12pt;"><strong>Beat Gate LIVE (PR)</strong></span><br />10:00 - <a href="index.php?option=com_content&amp;view=article&amp;id=141:chucky-pr&amp;catid=45&amp;Itemid=172">Chucky (PR)</a><br />11:00 - Saana (PR)<br /> 12:30 - Soha (PR)<br /> 14:00 - <strong>Thiago Alves (SP)</strong><br /> 15:30 - Magoo (PR)<br /> 17:00 - <strong>Fábio Leal (SP)</strong><br /> 18:30 - <span style="font-size: 12pt;"><strong><a target="_blank" href="index.php?option=com_content&amp;view=article&amp;id=124:ahlan-droid-pr&amp;catid=45&amp;Itemid=172">Ahlan Droid (PR)</a></strong></span></p>
<ul class="bullet-c">
<li><strong>Techno / House / Electro</strong></li>
</ul>
<p>20:30 - <a href="index.php?option=com_content&amp;view=article&amp;id=140:urbit-live-pr&amp;catid=45&amp;Itemid=172"><strong>Urbit LIVE (PR)</strong></a><br /> 21:30 - <span style="font-size: 12pt;"><strong><a href="index.php?option=com_content&amp;view=article&amp;id=112:elijah-hatem-pr&amp;catid=45&amp;Itemid=172">Elijah Hatem (PR)</a></strong></span><br /> 23:30 - <span style="font-size: 12pt;"><strong><strong>Kriminal Groove LIVE</strong></strong><strong>&nbsp;</strong></span></p>
</div>
<div style="width: 300px; float: right;" id="chillout">
<h4>Chill Out Stage</h4>
<ul class="bullet-c">
<li><span style="font-size: 12pt;"><strong><a href="index.php?option=com_content&amp;view=article&amp;id=93:funk-you&amp;catid=45&amp;Itemid=176">Funk You<span style="font-size: 8pt;"> (00h às 07h)</span></a></strong></span></li>
</ul>
<p>Abrindo a primeira noite do Chill Out a festa <a href="index.php?option=com_content&amp;view=article&amp;id=93:funk-you&amp;catid=45&amp;Itemid=176"><strong>Funk You</strong></a>, comandada pelos DJs <strong>Murillo</strong>, <strong><a href="index.php?option=com_content&amp;view=article&amp;id=94:schasko&amp;catid=45&amp;Itemid=172">Schasko</a></strong> e convidados.</p>
<ul class="bullet-c">
<li><strong><span style="font-size: 12pt;">Nova Delhi</span><span style="font-size: 8pt;"> (07h às 23h)</span></strong></li>
</ul>
<p><span style="font-size: 8pt;">Psychill, progbient, spacechill, organicchill são as trilhas sonoras que embalam da tarde de sábado no Tandava com o grupo curitibano <strong>Nova Delhi</strong>.</span></p>
<p>07:00 - Marcio Scheffler (PR)<br />09:00 - <em>A confirmar</em><br />11:00 - Azul (PR)<br />13:00 - Roninho (PR)<br />15:00 - Andrey Vinicius (PR)<br />17:00 - <em>A confirmar</em><br />19:00 - Patrick Rosa vs Azul (PR)<br />21:00 - <span style="font-size: 12pt;"><strong>Virtual Chakra LIVE (PR)</strong></span><br />22:00 - Xrú (PR)</p>
<ul class="bullet-c">
<li><strong><strong>Electro / Indie</strong></strong></li>
<p>23:00 -&nbsp; <span style="font-size: 12pt;"></span><strong><strong><span style="font-size: 12pt;">Lexosset LIVE (PR)</span><br /></strong></strong></p>
</ul>
</div>
</div>
<div style="clear: both;"></div>
<hr />
<h3><strong>DOMINGO.14.NOVEMBRO</strong></h3>
<div id="2columns" style="display: inline-block;">
<div style="width: 300px; float: left;" id="mainstage">
<h4><strong>Main Stage</strong></h4>
<ul class="bullet-c">
<li><strong><strong>Techno / House / Electro</strong></strong></li>
</ul>
<p><span style="font-size: 12pt;">&nbsp;</span><strong><span style="font-size: 12pt;"><strong>&nbsp;</strong></span></strong>00:30 - <strong><a href="index.php?option=com_content&amp;view=article&amp;id=126:roger-thiago-sc&amp;catid=45&amp;Itemid=172"><strong>Roger Thiago (SC)</strong></a><br /> </strong>01:30 - <strong><span style="font-size: 12pt;"><strong><a href="index.php?option=com_content&amp;view=article&amp;id=109:felipe-kantek-pr&amp;catid=45&amp;Itemid=172">Felipe Kantek (PR)</a></strong></span> <br /> </strong>03:30 - <strong><span style="font-size: 12pt;"><strong><a href="index.php?option=com_content&amp;view=article&amp;id=105:dj-afahell&amp;catid=45&amp;Itemid=172">AfaHell (PR)</a></strong></span><br /> </strong>05:30 - <a href="index.php?option=com_content&amp;view=article&amp;id=125:kazz-pr&amp;catid=45&amp;Itemid=172">Kazz (PR)</a><br /> 06:30 - Correção<strong><br /> </strong>08:30 - <a href="index.php?option=com_content&amp;view=article&amp;id=139:gabriel-bibi-pr&amp;catid=45&amp;Itemid=172">Gabriel Bibi (PR)</a><br /> 09:30 - <a href="index.php?option=com_content&amp;view=article&amp;id=128:dymi-pr&amp;catid=45&amp;Itemid=172">Dymi (PR)</a></p>
<ul class="bullet-c">
<li><strong><strong>Progressive Dark</strong></strong></li>
</ul>
<p>10:30 - <strong><a href="index.php?option=com_content&amp;view=article&amp;id=127:tiptronic-live-pr&amp;catid=45&amp;Itemid=172"><strong>Tip_Tronic LIVE (PR)</strong></a><br /> </strong>11:30 - <strong><span style="font-size: 12pt;"><strong><a href="index.php?option=com_content&amp;view=article&amp;id=103:minimal-criminal-live-rj&amp;catid=45&amp;Itemid=193">Minimal Criminal LIVE (RJ)</a></strong></span> <br /> </strong></p>
<ul class="bullet-c">
<li><strong><strong>Psy-Trance Full On</strong></strong></li>
</ul>
<p>12:30 - <strong><span style="font-size: 12pt;"><strong><a href="index.php?option=com_content&amp;view=article&amp;id=108:psapo-pr&amp;catid=45&amp;Itemid=172">Psapo (PR)</a></strong></span><br /> </strong>14:30 - <strong><span style="font-size: 12pt;"><strong><a href="index.php?option=com_content&amp;view=article&amp;id=104:rex-africa-do-sul&amp;catid=45&amp;Itemid=192">Rex (África do Sul)</a></strong></span><br /> </strong>16:30 - <strong><strong><a href="index.php?option=com_content&amp;view=article&amp;id=95:ahal-rama&amp;catid=45&amp;Itemid=172">Ahal Rãma (SC)<br /></a></strong></strong>17:30 - Correção<strong><strong><a href="index.php?option=com_content&amp;view=article&amp;id=95:ahal-rama&amp;catid=45&amp;Itemid=172"></a></strong></strong></p>
<ul class="bullet-c">
<li><strong><strong>Dark Psychedelic</strong></strong></li>
</ul>
<p>18:30 - <strong><strong>Thiago Pelotas (RS)</strong><br /> </strong>19:30 - <strong><strong>Sutemi (SP)</strong><br /> </strong>21:00 - <strong><a href="index.php?option=com_content&amp;view=article&amp;id=130:urucubaca-live-pr&amp;catid=45&amp;Itemid=172"><span style="font-size: 12pt;"><strong>Urucubaca LIVE (PR)</strong></span></a><br /></strong>22:00 - Correção</p>
</div>
<div style="width: 300px; float: right;" id="chillout">
<h4><strong><strong>Chill Out Stage</strong></strong></h4>
<ul class="bullet-c">
<li><span style="font-size: 12pt;"><strong><strong><strong><a href="index.php?option=com_content&amp;view=article&amp;id=106:vive-la-musique&amp;catid=45&amp;Itemid=196">Vive La Musique<span style="font-size: 8pt;"> (00h às 09h)</span></a></strong></strong></strong></span></li>
</ul>
<p style="text-align: justify;">Durante a noite de Sábado, o Chill Out se transforma com o som dançante da "Vive" e o espírito da cena Electro Indie curitibana.</p>
<p>00:00 - Débora Mello (Galeria Lúdica)<br />01:00 - Sandra Carraro (PR)<br />02:00 - LeduxCWB (PR)<strong>&nbsp;</strong><br />03:00 - Sérgio CAOS (PR)<br />04:00 - <strong><strong> <a href="index.php?option=com_content&amp;view=article&amp;id=111:joel-guglielmini-pr&amp;catid=45&amp;Itemid=172">Joel Guglielmini (PR)</a></strong></strong><strong>&nbsp;</strong><strong>&nbsp;</strong><br />05:30 - Manolo Neto (PR)<br />06:30 -<strong> <a href="index.php?option=com_content&amp;view=article&amp;id=110:andre-nego-pr&amp;catid=45&amp;Itemid=172">André Nego (PR)</a></strong><strong><br /></strong>08:00 - Raggamonk (PR)</p>
<ul class="bullet-c">
<li><strong>Chill Out</strong></li>
</ul>
<p>09:00 - Anderoath (PR)<br />10:30 - <strong><strong><span style="font-size: 12pt;"><a href="index.php?option=com_content&amp;view=article&amp;id=135:live-constroi-pr&amp;catid=45&amp;Itemid=172"><strong>Live Constrói (PR)</strong></a></span><br /></strong></strong>12:00 - <strong><strong><a href="index.php?option=com_content&amp;view=article&amp;id=112:elijah-hatem-pr&amp;catid=45&amp;Itemid=172"><strong>DJ Elijah Hatem (Chill Out Set)</strong></a></strong></strong></p>
<p>13:00 - <strong><strong><strong><span style="font-size: 12pt;"><a href="index.php?option=com_content&amp;view=article&amp;id=117:sallun-pedra-branca-sp&amp;catid=45&amp;Itemid=199">Sallun (Pedra Branca - SP)</a></span></strong></strong></strong></p>
<p>16:00 - <em>A confirmar</em><br />17:00 - Correção<br />18:00 - <a href="index.php?option=com_content&amp;view=article&amp;id=129:felipe-kojake-pr&amp;catid=45&amp;Itemid=172">Felipe Kojake (PR)</a><br />21:00 - <a href="index.php?option=com_content&amp;view=article&amp;id=96:friq-trip&amp;catid=45&amp;Itemid=172"></a><strong><strong><a href="index.php?option=com_content&amp;view=article&amp;id=96:friq-trip&amp;catid=45&amp;Itemid=172">Friq Trip (SC)</a></strong></strong></p>
</div>
</div>
<div style="clear: both;"></div>
<hr />
<h3><strong><strong>SEGUNDA.15.NOVEMBRO</strong></strong></h3>
<div id="2columns" style="display: inline-block;">
<div style="width: 300px; float: left;" id="mainstage">
<h4><strong><strong>Main Stage</strong></strong></h4>
<ul class="bullet-c">
<li><strong><strong><strong>Progressive Dark</strong></strong></strong></li>
</ul>
<p>00:30 - Correção<br />01:30 - <strong><strong><strong><a href="index.php?option=com_content&amp;view=article&amp;id=121:alanita-sc&amp;catid=45&amp;Itemid=172"><strong>Alanita (SC)</strong></a><br /></strong></strong></strong>03:30 - DeeJoker LIVE (PR)<br />04:30 - <strong><strong><strong><span style="font-size: 12pt;"><strong><a href="index.php?option=com_content&amp;view=article&amp;id=100:slow-control-live-sc&amp;catid=45&amp;Itemid=172">Slow Control LIVE (SC)</a></strong></span></strong></strong></strong></p>
<ul class="bullet-c">
<li><strong><strong><strong>Techno / House / Electro</strong></strong></strong></li>
</ul>
<p>05:30 - <strong><strong><strong><a href="index.php?option=com_content&amp;view=article&amp;id=119:caroles-rj&amp;catid=45&amp;Itemid=172"><strong>Caroles (RJ)</strong></a></strong></strong></strong></p>
<ul class="bullet-c">
<li><span style="font-size: 12pt;"><strong><strong><strong><a href="index.php?option=com_content&amp;view=article&amp;id=116:synk-recs-day-party&amp;catid=45&amp;Itemid=197">Synk Day Party<span style="font-size: 8pt;"> (08h às 20h)</span></a></strong></strong></strong></span><strong> </strong></li>
</ul>
<p>08:00 - Nav (PR)<br />09:30 - <a href="index.php?option=com_content&amp;view=article&amp;id=120:gromma-pr&amp;catid=45&amp;Itemid=172">Gromma (PR)</a><br />11:00 - <strong><a href="index.php?option=com_content&amp;view=article&amp;id=122:rodrigo-nickel-pr&amp;catid=45&amp;Itemid=172">Rodrigo Nickel (PR)</a></strong><br />12:30 - <a href="index.php?option=com_content&amp;view=article&amp;id=123:gabriel-boni-pr&amp;catid=45&amp;Itemid=172">Gabriel Boni (PR)</a><br />14:00 - <strong><strong><strong><a href="index.php?option=com_content&amp;view=article&amp;id=114:rodrigo-carreira-pr&amp;catid=45&amp;Itemid=172">Rodrigo Carreira (PR)</a><br /></strong></strong></strong>15:30 - <strong><strong><strong><span style="font-size: 12pt;"><strong><a href="index.php?option=com_content&amp;view=article&amp;id=115:truati-live-sp&amp;catid=45&amp;Itemid=200">Truati LIVE (SP)</a></strong></span><br /></strong></strong></strong>16:30 - <strong><strong><strong><a href="index.php?option=com_content&amp;view=article&amp;id=113:gabi-lima-pr&amp;catid=45&amp;Itemid=172">Gabi Lima (PR)</a><br /></strong></strong></strong>18:00 - <a href="index.php?option=com_content&amp;view=article&amp;id=131:crash-pr&amp;catid=45&amp;Itemid=172">Crash (PR)</a><br />19:30 - Jam Sessions Synk</p>
</div>
<div style="width: 300px; float: right;" id="chillout">
<h4><strong><strong><strong>Chill Out Stage</strong></strong></strong></h4>
<ul class="bullet-c">
<li><span style="font-size: 12pt;"><span style="font-size: 12pt;"><strong><strong><strong><a href="index.php?option=com_content&amp;view=article&amp;id=132:reboot&amp;catid=45&amp;Itemid=204">REBOOT<span style="font-size: 8pt;"> (23h às 08h)</span></a></strong></strong></strong></span></span><strong> </strong></li>
</ul>
<p>No encerramento do Chill Out, a festa onde são proibidas músicais originais: <a target="_blank" href="atracoes-tandava/reboot.html">Reboot</a><a target="_blank" href="atracoes-tandava/reboot.html"></a>.</p>
<p>DJs Feiges, RenanF e Murillo e performances de Yamballoon Circus Arts.</p>
<p>08:00 às 20:00 - FECHADO</p>
</div>
</div>
<div style="clear: both;"></div><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://elijah.com.br/tandava/2010/programacao-tandava/133-line-up-completo.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/programacao-tandava/133-line-up-completo.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/programacao-tandava/133-line-up-completo.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL2VsaWphaC5jb20uYnIvdGFuZGF2YS8yMDEwL3Byb2dyYW1hY2FvLXRhbmRhdmEvMTMzLWxpbmUtdXAtY29tcGxldG8uaHRtbA%3D%3D" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Terça, 19 de Outubro de 2010 11:35				</span>
					
								<span class="rt-date-modified">
					Última atualização em Terça, 09 de Novembro de 2010 16:28				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>									
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/programacao-tandava/134-arte-cultura-e-oficinas.html">Arte, Cultura e Oficinas</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<p style="text-align: justify;">A edição 2010 do Tandava terá inúmeras oficinas, exposições e outras atividades culturais durante todos os dias do evento. Participe!</p>
<p>Confira abaixo a Programação Cultural do <strong>Tandava Gathering</strong>:</p>
<h4>Programações Diárias</h4>
<p>Durante o Sábado, Domingo e Segunda.</p>
<ul class="bullet-c">
<li><strong>Roda de Malabares<br /></strong>Vanderlei de Mattos Biscaia fará pirofagia com Devil Fire e rodas de malabares, além da confecção e venda de peças.<strong><br /></strong> </li>
<br />
<li><strong>Exposição Fotográfica by MushPics<br /></strong>Conheça o MushPics: <a target="_blank" href="http://www.mushpics.com">www.mushpics.com</a><br /><br /></li>
<li><strong>Cobertura Fotográfica by Luna Cris</strong><br />Conheça Luna Cris:&nbsp;<a target="_blank" href="http://www.clikssnabalada.com/">www.clikssnabalada.com</a><br /><br /></li>
<li><strong>Cobertura Fotográfica by MushPics</strong><br />Conheça o MushPics: <a target="_blank" href="http://www.mushpics.com/">www.mushpics.com<br /></a><br /><strong></strong></li>
<li><strong>Cobertura Fotográfica by LeduxCWB</strong><br />Conheça o LeduxCWB: <a target="_blank" href="http://leduxcwb.wordpress.com/">leduxcwb.wordpress.com</a></li>
</ul>
<h4>Programação Sábado</h4>
<ul class="bullet-c">
<li><strong>Oficina de Objetos Tridimensionais (08:00 às 18:00)<br /></strong>Ministrantes: Marcio Scheffler e Azul.<br /><br />Em breve mais informações.<strong><br /><br /></strong><strong></strong></li>
<li><strong>Oficina de Modelagem em Argila (13:00 às 15:00)<br /></strong>Ministrante: Felipe S. (Ano Novo Maia / União da Vitória)<br /><br />A prática da modelagem em argila terá como objetivo principal estabelecer uma inter-relação entre o ser humano e os elementos naturais que dele fazem parte.<br /><br />Dentre eles estão a terra, meio de renovação e origem de vida, a base de tudo; a água, que purifica, deixa maleável; o ar que nutre, leva e traz e fornece leveza.<br /><br /> Elementos que compõe tanto o ser humano quanto o material a ser utilizado. Também se trata do momento de criar, dar forma ao pensamento integrando-o a materialidade da argila, respeitando suas propriedades e compreendendo as suas técnicas de manuseio. <br /><br /></li>
<li><strong>Oficina de Tie Die (15:00 às 17:00)<br /></strong>Ministrante: Andrey Torvalski.<br /><br />Em breve mais informações.<br /><br /><em>Obs.: O participante deverá levar uma camiseta (preferencialmente sem estampa) para participar.</em></li>
</ul>
<h4>Programação Domingo</h4>
<ul class="bullet-c">
<li style="text-align: justify;"><strong>Live Paint (00:00)<br /></strong>Artista: Orlando Muzca - artista plástico<br /> <br /> Uma laicra será pintada durante o festival. Em um processo de arqueologia do subconsciente, o artista trás à superfície os símbolos que estão presentes em cada momento de sua experiência visual.<br /><br />Mais Informações: <a target="_blank" href="http://www.mzc-lab.com/">www.mzc-lab.com</a><br /><strong><br /></strong> </li>
<strong></strong>
<li style="text-align: justify;"><strong>Oficina de Estampa de Tecido - Estêncil (15:00)<br /></strong>Ministrante: Felipe S. (Ano Novo Maia / União da Vitória)<br /> <br /> Conjunto de processos artesanais e técnicas decorativas aplicadas sobre tecido de algodão utilizando motivos e desenhos variados. Tem como objetivo difundir a técnica de estampa e a construção do molde vazado (estêncil). Consiste numa técnica exata, simples, a qual proporciona um excelente resultado visual.<br /><br />O fundamento da oficina permeia a ideia de que cada um pode criar sua própria estampa e utilizá-las em camisetas e outros tipos de pano (cangas, decoração, etc.) em vez de comprar em uma loja por um valor mais elevado.<br /> <br /> <em>Obs.: Cada participante deverá levar uma camiseta branca de algodão ou pano branco de algodão. Camisetas e panos de outras cores claras também poderão ser utilizados.<br /><br /></em></li>
<li><strong>Oficina de Micologia (17:00)<br /></strong>Ministrante: Matusael (Ano Novo Maia - União da Vitória)<br /><br />O objetivo desta oficina será abordar sobre os fungos em nosso meio ambiente, sua origem e sua relação com a consciência humana, e detalhar técnicas básicas de cultivo de cogumelos que desde a antiguidade são conhecidos pelos seus poderes mágicos..<br /><br /></li>
<li style="text-align: justify;"><strong>Vivência Circular de Pintura Abstrata (15:00 às 19:00)<br /></strong>Ministrante: Orlando Muzca aka Muzg, artista plástico e membro do projeto Urbit.<br /><br />A Vivência Circular de Pintura Abstrata é um mandala coletivo com o objetivo de unir seus praticantes numa psicosfera mântrica e interativa, proporcionando a abstração da mente através de um ritual circular, artístico e contemplativo.<br /><br />O processo acontece de forma lúdica no qual cada pessoa interage com a outra através do suporte onde estará sendo pintado o mandala.<br /><br />Cada rodada dura em média 30 minutos, com o número mínimo de 2 e máximo de 8 participantes. As cores são distribuídas aleatoriamente e passam de mão em mão.<br /><br />Em seguida outra rodada se inicia para complementar a pintura anterior ou até mesmo reciclar totalmente o tecido, compondo um novo mandala sobre o que foi formado anteriormente.<br /><br /><em>Obs.: Os inscritos deverão levar roupas velhas para participar, pois a tinta mancha os tecidos.<br /></em></li>
</ul>
<h4>Programação Segunda</h4>
<ul class="bullet-c">
<li style="text-align: justify;"><strong>Oficina de Hatha Yoga (08:00)<br /></strong>Ministrantes: Stefany Oliveira.<br /><br />Hatha Yoga, pré-clássico, significa sol (ha), lua (tha), positivo e negativo, homem e mulher, ou seja representa energias opostas. Procura o equilíbrio do corpo e mente através de exercícios físicos (asanas), respiração controlada (pranayama) e quietude da mente através do relaxamento e meditação.<br /><br />"<em>O Yoga é como música. O ritmo do corpo, a melodia da mente e a harmonia da alma criam a sinfonia da vida.</em>" - B. K. S. Iyengar<br /><strong><br /></strong> </li>
<strong> </strong>
<li style="text-align: justify;"><strong>Vivência Circular de Pintura Abstrata (15:00 às 19:00)<br /></strong>Ministrante: Orlando Muzca aka Muzg, artista plástico e membro do projeto Urbit.<br /><br />A  Vivência Circular de Pintura Abstrata é um mandala coletivo com o  objetivo de unir seus praticantes numa psicosfera mântrica e interativa,  proporcionando a abstração da mente através de um ritual circular,  artístico e contemplativo.<br /><br />O processo acontece de forma lúdica no  qual cada pessoa interage com a outra através do suporte onde estará  sendo pintado o mandala.<br /><br />Cada rodada dura em média 30 minutos,  com o número mínimo de 2 e máximo de 8 participantes. As cores são  distribuídas aleatoriamente e passam de mão em mão.<br /><br />Em seguida  outra rodada se inicia para complementar a pintura anterior ou até mesmo  reciclar totalmente o tecido, compondo um novo mandala sobre o que foi  formado anteriormente.<br /><br /><em>Obs.: Os inscritos deverão levar roupas velhas para participar, pois a tinta mancha os tecidos.</em><br /><br /><em></em></li>
</ul>
<h4>Stand S.O.S. Trance Dancers por Natural Flux Institute</h4>
<p>Natural Flux Institute nasceu da necessidade dos Naturoterapêutas e Yogaterapêutas formados nas Faculdades Integradas “Espírita” em Integrar as diversas Terapias Naturais e Práticas Transcendentais, no intuito de proporcionar Qualidade Vida e Longevidade através da prevenção de doenças e manutenção da saúde física e mental.</p>
<p>Esperamos contribuir, desta forma, ao desenvolvimento de indivíduos saudáveis, satisfeitos e mantenedores conscientes de sí próprios e do Planeta.</p>
<p><em>Obs.: Estas atividades terão custos individuais. Em breve serão publicados os custos de cada uma das terapias</em>.</p>
<h5>Massagens Terapeuticas - S.O.S. Trance Dancers</h5>
<ul class="bullet-c">
<li><strong>Don't Stop! - N.1 (R$ 15,00)<br /></strong>Com óleo: Reflexo, Ayurveda, Zen Shiatsu, Tuina ou  Relaxante - de  acordo com as possibilidades. <br />Pés, Panturrilhas e Coxas  Laterais (Zen  Shiatsu).<br /><br /><strong></strong></li>
<li><strong>Don't Stop! - N.2 (R$ 20,00)<br /></strong>Com óleo: Reflexo, Ayurveda, Zen Shiatsu, Tuina  ou Relaxante - de  acordo com as possibilidade.<br /> Ombros, trapézios,  pescoço,  Interescapular e canal da bexiga nas costas.<br /><br /><strong></strong></li>
<li><strong>Don't Stop! - N.3 (R$ 15,00)<br /></strong>Com óleo: Reflexo ou Ayurveda ou Zen Shiatsu ou  Tuina ou Relaxante -  de acordo com as possibilidades.<br />Face, Braços,  Mãos, Reiki.<br /><br /><strong></strong></li>
<li><strong>Don't Stop! - N.4 (R$ 25,00)<br /></strong>Com óleo: Reflexo ou Ayurveda ou Zen Shiatsu ou  Tuina ou Relaxante -  de acordo com as possibilidades.<br /> Alongamentos específicos com Nuad Bo Harn.<br /><br /><strong></strong></li>
<li><strong>Don't Stop! - N.5 (R$ 15,00)<br /></strong>Shiatsu Expresso.<br /><br /><strong></strong></li>
<li><strong>Don't Stop! - Combo Keep On Dancing (R$ 40,00)<br /></strong>Combo com Don't Stop! N.1 + N.2 + N.3.<br /><br /><strong></strong></li>
<li><strong>Don't Stop! - Combo Ressureição (R$ 50,00)<br /></strong>Combo com Don't Stop! N.1 + N.2 + N.3. + N.4</li>
</ul>
<h5>Atendimentos Naturoterápicos - S.O.S. Trance Dancers</h5>
<p>Custo: R$ 20,00.</p>
<p><em>* Inclui prescrição em Fitoterapia e Florais de Bach.</em></p>
<h5>Acupuntura Abdominal - S.O.S. Trance Dancers</h5>
<p>Custo: R$ 30,00.</p>
<h5>Auriculoterapia - S.O.S. Trance Dancers</h5>
<p>Custo: R$ 15,00.</p>
<h5>Reiki</h5>
<p>Custo: R$ 10,00.</p>
<h5>Demais Atendimentos e Práticas no Instituto ou a Domicílio</h5>
<p>Naturoterapia: Acupuntura Sistemica e Abdominal, Massoterapia Indiana, Chinesa, Japonesa e Ocidental, Fitoterapia, Florais, Geoterapia, Trofoterapia, Auriculoterapia, Reiki.</p>
<p>Práticas transcendentais: Hatha Yoga, Taijiquan (Taichichuan familia Yang), Kung fu Shaolin, Qigong, Alquimia Interna Taoista (breve), Muaythai, Defesa pessoal.</p><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://elijah.com.br/tandava/2010/programacao-tandava/134-arte-cultura-e-oficinas.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/programacao-tandava/134-arte-cultura-e-oficinas.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/programacao-tandava/134-arte-cultura-e-oficinas.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL2VsaWphaC5jb20uYnIvdGFuZGF2YS8yMDEwL3Byb2dyYW1hY2FvLXRhbmRhdmEvMTM0LWFydGUtY3VsdHVyYS1lLW9maWNpbmFzLmh0bWw%3D" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Terça, 19 de Outubro de 2010 16:08				</span>
					
								<span class="rt-date-modified">
					Última atualização em Quarta, 03 de Novembro de 2010 17:13				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>							</div>
		
				<div class="clear"></div>
		
				
	</div>
</div>";s:4:"head";a:10:{s:5:"title";s:34:"Programação do Tandava Gathering";s:11:"description";s:275:"Não surgiu um novo conceito. As coisas não vão mudar e nada será resgatado.  Não  é nada melhor ou maior de tudo que você já viu. Não  é inédito e nem ao menos é um evento. O respeito existe, mas o publico não, existe um encontro entre amigos e amigos de amigos.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:24:"text/html; charset=utf-8";}s:8:"standard";a:4:{s:6:"robots";s:13:"index, follow";s:8:"keywords";s:54:"tandava, gathering, festival, 2010, curitiba, novembro";s:8:"og:title";s:24:"Arte, Cultura e Oficinas";s:12:"og:site_name";s:17:"Tandava Gathering";}}s:5:"links";a:2:{i:0;s:119:"<link href="/tandava/2010/programacao-tandava.feed?type=rss" rel="alternate" type="application/rss+xml" title="RSS 2.0"";i:1;s:122:"<link href="/tandava/2010/programacao-tandava.feed?type=atom" rel="alternate" type="application/atom+xml" title="Atom 1.0"";}s:11:"styleSheets";a:0:{}s:5:"style";a:0:{}s:7:"scripts";a:2:{s:41:"/tandava/2010/media/system/js/mootools.js";s:15:"text/javascript";s:40:"/tandava/2010/media/system/js/caption.js";s:15:"text/javascript";}s:6:"script";a:0:{}s:6:"custom";a:1:{i:0;s:278:"<script type='text/javascript'>
/*<![CDATA[*/
	var jax_live_site = 'http://elijah.com.br/tandava/2010/index.php';
	var jax_site_type = '1.5';
/*]]>*/
</script><script type="text/javascript" src="http://elijah.com.br/tandava/2010/plugins/system/pc_includes/ajax_1.3.js"></script>";}}s:7:"pathway";a:1:{i:0;O:8:"stdClass":2:{s:4:"name";s:13:"Programação";s:4:"link";s:20:"index.php?Itemid=206";}}s:6:"module";a:0:{}}