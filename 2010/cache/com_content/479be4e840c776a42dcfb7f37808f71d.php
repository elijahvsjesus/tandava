<?php die("Access Denied"); ?>
a:4:{s:4:"body";s:7031:"
<div class="rt-joomla ">
	<div class="rt-article">
		
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/o-festival.html">Tandava Gathering</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
		
		
		
		
		<img style="margin: 0px 10px 10px 0px; float: left;" alt="tandava-home-1" src="images/stories/home/tandava-home-1.jpg" width="150" height="219" />
<blockquote>
<p style="text-align: justify;">"Não surgiu um novo conceito. As coisas não vão mudar e nada  será&nbsp;resgatado. Não&nbsp; é&nbsp;nada melhor ou maior de tudo que você&nbsp;já&nbsp;viu. Não&nbsp; é&nbsp;inédito e nem ao menos é um evento. O respeito existe, mas  o  publico não, existe um encontro entre amigos e amigos de amigos."</p>
</blockquote>
<p style="text-align: justify;">O <strong>Tandava Gathering</strong> é um festival, ou um encontro como alguns podem preferir, sem fins lucrativos, feito por pessoas que possuem outra atividade profissional e desenvolvem essa idéia por simples e convicta crença na cena <em>underground</em>.</p>
<p style="text-align: justify;"><img style="margin: 20px; float: right;" alt="tandava-home-2" src="images/stories/home/tandava-home-2.jpg" width="200" height="151" />O Tandava é para todos que querem experimentar, para aqueles que respeitam a si mesmo e aos outros. Na verdade é mais do que isso, é feito para quem apenas deseja alguns dias fora da maçante realidade do cotidiano.</p>
<p style="text-align: justify;">É uma fuga para onde tempo e espaço estão aquém de afetar o relacionamento interpessoal.</p>
<h4>Música e Arte</h4>
<p style="text-align: justify;">E tudo isso é materializado através da música e da arte. A música por sua vez assume o papel de sincronizar, não só o a pista de dança, mas o ritmo de tudo que acontece no festival. E a arte visa elevar, ainda que por 3 dias, a percepção de você para com o mundo ao seu redor e, principalmente, com você mesmo.</p>
<p style="text-align: justify;">Fugindo do tom lúdico e emblemático que os mais céticos podem preferir evitar, o principal motivo para qual o Tandava foi criado é a diversão, o sorriso, o bem-estar. Sinta-se convidado a passar um feriado em um local agradável, ao som de boa musica e em convivência com pessoas dispostas a se divertir. Joint It!</p>
<p><img style="vertical-align: middle; display: block; margin-left: auto; margin-right: auto;" alt="tandava-home-3" src="images/stories/home/tandava-home-3.jpg" width="480" height="310" /></p>

<h4>O Espírito do Tandava</h4>
<p style="text-align: justify;">Costumava-se pensar que a palavra festival era forte demais para o Tandava, que em sua essência é um encontro de amigos.</p>
<p style="text-align: justify;">Seja como for, algumas coisas são pensadas para que o espírito familiar seja mantido. A entrada com bebida e comida é permitida, assim como ausentar-se do local e retornar também é permitido. Fogueiras são permitidas e incentivadas, desde que mantido o bom senso em relação à segurança.</p>
<p style="text-align: justify;">A força motriz é evitar regras e rótulos, a fim de que quem quer que esteja presente sinta-se no direito de mudar o rumo de tudo a qualquer momento.</p>
<p style="text-align: justify;">Não se deve taxar o Tandava como um evento de música eletrônica, afinal como algo que aspira ser a forma física da vontade dos presentes, é impossível de ser preciso, como é natural das vontades.&nbsp;&nbsp; &nbsp;</p>
<p style="text-align: justify;">Outra peculiaridade é a área restrita do site, com mural e outras formas de comunicação entre os usuários, esse recurso permite que os participantes possam se conhecer nos meses que antecedem o festival. Essa característica interativa do site auxilia o coletivismo e gera ações como central de caronas e campeonatos dos mais engraçados esportes (de disputas de bolinha de gude a torneios de bets).</p>
<p style="text-align: justify;">Uma breve retrospectiva a cerca das duas primeiras edições do Tandava é útil para contextualizar os recém chegados, uma vez que essa terceira edição almeja atingir números mais expressivos em se tratando de participantes do encontro.</p>
<h4>Últimas Edições</h4>
<ul>
<li><a href="index.php?option=com_content&amp;view=article&amp;id=98:tandava-gathering-2008-primeira-edicao&amp;catid=41&amp;Itemid=164">Tandava Gathering 2008 - Primeira Edição</a><br />Veja as <a href="index.php?option=com_content&amp;view=article&amp;id=97:fotos-tandava-2008&amp;catid=44&amp;Itemid=173">Fotos</a></li>
<br />
<li><a href="index.php?option=com_content&amp;view=article&amp;id=99:tandava-gathering-2009-segunda-edicao&amp;catid=41&amp;Itemid=164">Tandava Gathering 2009 - Segunda Edição</a><br />Veja as <a href="index.php?option=com_content&amp;view=article&amp;id=85:fotos-tandava-2009&amp;catid=44&amp;Itemid=173">Fotos</a></li>
<br />
<li><a href="index.php?option=com_content&amp;view=frontpage&amp;Itemid=1">Tandava Gathering 2010 - Terceira Edição</a><br />12 a 15 de Novembro de 2010. Em breve Fotos!</li>
<br /> 
</ul><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://elijah.com.br/tandava/2010/o-festival.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
						<div class="rt-article-icons">
								<a href="/tandava/2010/o-festival.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/o-festival.html?tmpl=component&amp;print=1&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL2VsaWphaC5jb20uYnIvdGFuZGF2YS8yMDEwL28tZmVzdGl2YWwuaHRtbA%3D%3D" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>																</div>
			
						<span class="rt-date-posted">
				Domingo, 25 de Abril de 2010 14:18			</span>
			
						<span class="rt-date-modified">
				Última atualização em Quinta, 18 de Novembro de 2010 16:58			</span>
			
						<span class="rt-author">
				Postado por de elijah hatem			</span>
				
					</div></div></div>
		
					</div>
</div>";s:4:"head";a:10:{s:5:"title";s:17:"Tandava Gathering";s:11:"description";s:275:"Não surgiu um novo conceito. As coisas não vão mudar e nada será resgatado.  Não  é nada melhor ou maior de tudo que você já viu. Não  é inédito e nem ao menos é um evento. O respeito existe, mas o publico não, existe um encontro entre amigos e amigos de amigos.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:24:"text/html; charset=utf-8";}s:8:"standard";a:7:{s:6:"robots";s:13:"index, follow";s:8:"keywords";s:54:"tandava, gathering, festival, 2010, curitiba, novembro";s:8:"og:title";s:17:"Tandava Gathering";s:12:"og:site_name";s:17:"Tandava Gathering";s:8:"og:image";s:72:"http://elijah.com.br/tandava/2010/images/stories/home/tandava-home-1.jpg";s:5:"title";s:17:"Tandava Gathering";s:6:"author";s:15:"de elijah hatem";}}s:5:"links";a:0:{}s:11:"styleSheets";a:0:{}s:5:"style";a:0:{}s:7:"scripts";a:2:{s:41:"/tandava/2010/media/system/js/mootools.js";s:15:"text/javascript";s:40:"/tandava/2010/media/system/js/caption.js";s:15:"text/javascript";}s:6:"script";a:0:{}s:6:"custom";a:1:{i:0;s:278:"<script type='text/javascript'>
/*<![CDATA[*/
	var jax_live_site = 'http://elijah.com.br/tandava/2010/index.php';
	var jax_site_type = '1.5';
/*]]>*/
</script><script type="text/javascript" src="http://elijah.com.br/tandava/2010/plugins/system/pc_includes/ajax_1.3.js"></script>";}}s:7:"pathway";a:1:{i:0;O:8:"stdClass":2:{s:4:"name";s:8:"Festival";s:4:"link";s:20:"index.php?Itemid=164";}}s:6:"module";a:0:{}}