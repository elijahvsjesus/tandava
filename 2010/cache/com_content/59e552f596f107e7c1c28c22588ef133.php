<?php die("Access Denied"); ?>
a:4:{s:4:"body";s:204460:"
<div class="rt-joomla ">
	<div class="rt-blog">

				<h1 class="rt-pagetitle">Atrações do Tandava Gathering</h1>
		
				<div class="rt-description">
										<p style="text-align: justify;">A edição 2010 do Tandava teve mais de 80 horas de música em 4 dias de festival multiplicados por 2 pistas. Os artistas foram escolhidos cuidadosamente pela oganização e a diversidade sonora é o que se ouve durante o encontro.&nbsp;</p>
<p style="text-align: justify;">A pista principal contou com grandes nomes internacionais e nacionais, como <a href="index.php?option=com_content&amp;view=article&amp;id=107:zaraus-live-portugual&amp;catid=45&amp;Itemid=195"><strong>Zaraus LIVE (Portugal)</strong></a>, <a href="index.php?option=com_content&amp;view=article&amp;id=104:rex-africa-do-sul&amp;catid=45&amp;Itemid=192"><strong>Rex (África do Sul)</strong></a>, <strong><a href="index.php?option=com_content&amp;view=article&amp;id=103:minimal-criminal-live-rj&amp;catid=45&amp;Itemid=193" target="_blank">Minimal Criminal LIVE (RJ)</a></strong>&nbsp;e&nbsp;<a href="index.php?option=com_content&amp;view=article&amp;id=117:sallun-pedra-branca-sp&amp;catid=45&amp;Itemid=172"><strong>Sallun (Pedra Branca - SP)</strong></a>, recebendo no último dia uma festa especial da gravadora <strong><a href="index.php?option=com_content&amp;view=article&amp;id=116:synk-recs-day-party&amp;catid=45&amp;Itemid=172">SYNK Records</a> </strong>para o encerramento do festival com destaque para a apresentação de <a href="index.php?option=com_content&amp;view=article&amp;id=115:truati-live-sp&amp;catid=45&amp;Itemid=172"><strong>Truati LIVE</strong></a>, <a href="index.php?option=com_content&amp;view=article&amp;id=114:rodrigo-carreira-pr&amp;catid=45&amp;Itemid=172">Rodrigo Carreira</a>, <a href="index.php?option=com_content&amp;view=article&amp;id=123:gabriel-boni-pr&amp;catid=45&amp;Itemid=172">Gabriel Boni</a>, <a href="index.php?option=com_content&amp;view=article&amp;id=113:gabi-lima-pr&amp;catid=45&amp;Itemid=172">Gabi Lima</a>, Nav e <a href="index.php?option=com_content&amp;view=article&amp;id=122:rodrigo-nickel-pr&amp;catid=45&amp;Itemid=172">Rodrigo Nickel</a>.</p>
<p style="text-align: justify;">A segunda pista, o Chill Out, assume o lugar para degustar sons alternativos à noite com festas que são referência em qualidade.</p>
<p style="text-align: justify;">Na sexta-feira, comandada pelos DJs Murillo e Schasko, aconteceu a festa <a href="index.php?option=com_content&amp;view=article&amp;id=93:funk-you&amp;catid=45&amp;Itemid=176"><strong>Funk You</strong></a>, referência do Funk Old School ao New School. Na segunda noite, Sábado, hospedou a festa <a href="index.php?option=com_content&amp;view=article&amp;id=106:vive-la-musique&amp;catid=45&amp;Itemid=196"><strong>Vive La Musique</strong></a>, famosa no circuito Electro Indie "<em>New Rave" </em>em Curitiba que além dos residentes conntará com<strong></strong><strong></strong> vários convidados especiais. E, no encerramento, a festa onde são proibidas músicais originais: <strong><a target="_blank" href="index.php?option=com_content&amp;view=article&amp;id=132:reboot&amp;catid=45&amp;Itemid=204">Reboot</a></strong> com os DJs Feiges, RenanF e Murillo e performances de <strong>Yamballoon Circus Arts</strong>.</p>
<p style="text-align: justify;">Conheça melhor as atrações da edição 2010 do festival abaixo.</p>
<blockquote>
<p>"As coisas não vão mudar e nada  será&nbsp;resgatado."</p>
</blockquote>
<p><cite><a href="../">Tandava Gathering</a></cite>, 2010.</p><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/atracoes-tandava/ver-todas.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>					</div>
		
				<div class="rt-leading-articles">
										
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/atracoes-tandava/105-dj-afahell.html">AfaHell (PR)</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<p style="text-align: justify;"><img style="margin: 0px 10px 10px 0px; float: left;" alt="afahell" src="images/stories/atracoes/afahell.jpg" width="225" height="335" /><strong>Rafael Henrique</strong>, 26 anos, aka <strong>Afahell</strong> começou como DJ em 2003 e já se apresentou em diversas cidades no Paraná, Santa Catarina, São Paulo e Minas Gerais. Paralelo a carreira como DJ esta envolvido na produção de diversos eventos (Cosmoon, Makunba, Tandava, Earthdance)</p>
<p style="text-align: justify;">Optou pelas vertentes mais undergrounds começando com o Dark Psychedelic até aflorar o interesse pelo Techno e Tech House, FAST OR LOW procura inovar e sempre trazendo tracks atuais conduz o dancefloor com prazer e técnica admiráveis.</p>
<p style="text-align: justify;">Dividiu o palco com artistas como D-nox, Alex Bau, Adam k, D.a.v.e the Drummer, Midrodizko, Tegma, Sara Gali, Felguk, Minimal Criminal, Trieb, Kriminal Groove, Liquid Soul, Andromeda, Aerospace, Bizzare Contact, Paranormal Atack, DNA, Protoculture, Rinkadink, Burn in Noise, Rica Amaral, ZIK, Baphomet Engine, Para Halu, Kerosense Club, Xenomorph entre diversos outros.</p>
<blockquote>
<p><strong>+ INFO: </strong><a target="_blank" href="http://www.myspace.com/afahell">www.myspace.com/afahell</a></p>
</blockquote><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/atracoes-tandava/105-dj-afahell.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/atracoes-tandava/ver-todas/105-dj-afahell.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/atracoes-tandava/ver-todas/105-dj-afahell.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL3d3dy5lbGlqYWguY29tLmJyL3RhbmRhdmEvMjAxMC9hdHJhY29lcy10YW5kYXZhLzEwNS1kai1hZmFoZWxsLmh0bWw%3D" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Segunda, 02 de Agosto de 2010 18:15				</span>
					
								<span class="rt-date-modified">
					Última atualização em Segunda, 02 de Agosto de 2010 18:26				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>									
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/atracoes-tandava/95-ahal-rama.html">Ahal Rãma (SC)</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<p><img style="margin: 0px 10px 10px 0px; float: left;" alt="ahalrama" src="images/stories/atracoes/ahalrama.jpg" width="113" height="480" /><strong>Fabio Lima</strong> a.k.a. <strong>Ahal Rãma</strong> teve contato com a música eletrônica no início dos anos 90 através da profissão que exercia de técnico de som profissional da empresa do seu pai. Desde então vem aprimorando seu conhecimento a respeito de engenharia de áudio e mixagens para ampliar o nível de suas apresentações e produções.<br /><br />Em 2001 partiu de sua cidade natal em Minas Gerais para a ilha de Florianópolis onde conheceu o trance. Um forte movimento se formou na região dando origem ao núcleo Digital Roots onde Fábio foi um dos idealizadores, promovendo e dando apoio para vários eventos e festivais como Earthdance, Encontro Fora do Tempo, Festival Forta do Tempo, Grito de Gaia, etc.<br /><br />Fábio considera ser bem eclético, permeou por muitas linhas tais como: Electrohouse, Progressive Trance, Trance, Techhouse, Techno, Minimal, Chill Out. Hoje trabalha em suas produções próprias e remix de músicas de artistas consagrados, construindo assim um set único com efeitos em real time e loops, sempre inovando a cada momento!<br /><br />Desenvolve sets de Minimal, Techno, Trance, Techhouse e Chill Out (músicas xamânicas e Reggae raiz).</p>
<blockquote>
<p><strong>+ INFO: </strong><a target="_blank" href="http://www.myspace.com/ahalrama">www.myspace.com/ahalrama</a></p>
</blockquote><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/atracoes-tandava/95-ahal-rama.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/atracoes-tandava/ver-todas/95-ahal-rama.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/atracoes-tandava/ver-todas/95-ahal-rama.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL3d3dy5lbGlqYWguY29tLmJyL3RhbmRhdmEvMjAxMC9hdHJhY29lcy10YW5kYXZhLzk1LWFoYWwtcmFtYS5odG1s" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Quarta, 16 de Junho de 2010 22:43				</span>
					
								<span class="rt-date-modified">
					Última atualização em Segunda, 02 de Agosto de 2010 12:24				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>									
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/atracoes-tandava/124-ahlan-droid-pr.html">Ahlan Droid (PR)</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<p style="text-align: justify;"><img style="margin: 0px 10px 10px 0px; float: left;" alt="ahlan-droid" src="images/stories/atracoes/ahlan-droid.jpg" width="225" height="288" /></p>
<p align="justify"><strong>Ahlan Droid</strong> é conhecido na cena curitibana pela qualidade em seus sets e pela adaptabilidade nos mais variados contextos.</p>
<p align="justify">Acredita que somente através da perfeita interação entre ambiente, público e DJ, é que é possível alcançar patamares cada vez mais elevados de consciência coletiva, o que busca a todo momento.</p>
<p align="justify">Sócio fundador do núcleo psicodélico Revolucion-e, é também colaborador de eventos como o festival Tandava e a festa Selectro, além do movimento de Ctrl.Cultura.</p>
<p align="justify">Interage com o Dance Floor utilizando influências progressivas entre os diversos estilos do house, trance e techno, podendo incorporar construções minimalistas ou fazendo até mesmo transições ao full on. A cada ocasião um set diferente, uma viagem distinta..</p>
<blockquote>
<p><strong>+ INFO: </strong><a target="_blank" href="http://www.myspace.com/djahlandroid">www.myspace.com/djAhlanDroid</a></p>
</blockquote>
<p><strong>+ DJ SETS / TRACKS</strong></p>
<object codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=10,0,32,18" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" type="application/x-shockwave-flash" height="225" width="100%">
<param name="allowscriptaccess" value="always" />
<param name="src" value="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fsoundcloud.com%2Fahlandroid&amp;secret_url=false" /><embed height="225" width="100%" src="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fsoundcloud.com%2Fahlandroid&amp;secret_url=false" allowscriptaccess="always" type="application/x-shockwave-flash"></embed>
</object>
<span><a href="http://soundcloud.com/ahlandroid">Latest tracks by AhlanDroid</a></span><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/atracoes-tandava/124-ahlan-droid-pr.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/atracoes-tandava/ver-todas/124-ahlan-droid-pr.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/atracoes-tandava/ver-todas/124-ahlan-droid-pr.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL3d3dy5lbGlqYWguY29tLmJyL3RhbmRhdmEvMjAxMC9hdHJhY29lcy10YW5kYXZhLzEyNC1haGxhbi1kcm9pZC1wci5odG1s" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Terça, 31 de Agosto de 2010 22:14				</span>
					
								<span class="rt-date-modified">
					Última atualização em Terça, 31 de Agosto de 2010 22:20				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>									
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/atracoes-tandava/121-alanita-sc.html">Alanita (SC)</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<p style="text-align: justify;"><img style="margin: 0px 10px 10px 0px; float: left;" alt="alanita" src="images/stories/atracoes/alanita.jpg" width="225" height="321" /></p>
<p align="justify">Alana Curi é Alanita, DJ de 21 anos que começou na vida psicodelica em 2003, aos 14 anos.</p>
<p align="justify">Depois de ir mais afundo nas vertentes psicodélicas, ela descobriu o primeiro estilo musical que tocaria alguns anos depois: o dark psychedelic, com atmosferas escuras, bassline reto e synths rasgados.</p>
<p align="justify">Desde 2006 Alanita vem tocando música underground, fazendo sets profundos, obscuros e viajantes. Em 2007 começou a produzir com Marcelo (Cannibal Barbecue) e juntos criaram o live Bezoar, uma mistura de influências de ambos os lados.</p>
<p align="justify">Apos amadurecer ainda mais sua musicalidade, Alanita adicionou a seu set o estilo progressivo, porém mantendo a linha underground, buscando sempre as tracks mais pesadas e intensas.</p>
<p align="justify">Ela já participou dos lines dos festivais Universo Paralello e Festival Fora do Tempo e também já tocou nos estados do Rio Grande do Sul, Santa Catarina, Parana, Sao Paulo, Minas Gerais e Bahia.</p>
<p align="justify">Hoje em dia Alanita faz parte das gravadoras Mind Tweakers Rec de Brasilia e Glitchy Tonic Rec da Alemanha.</p>
<blockquote>
<p><strong>+ INFO: </strong><a target="_blank" href="http://www.facebook.com/alana.curi">www.facebook.com/alana.curi</a></p>
</blockquote>
<p><strong>+ DJ SETS / MIXTAPES</strong></p>
<object codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=10,0,32,18" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" type="application/x-shockwave-flash" height="81" width="100%">
<param name="allowscriptaccess" value="always" />
<param name="src" value="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fsoundcloud.com%2Fdjalanita%2Fset-prog-dark-junho-10&amp;secret_url=false" /><embed height="81" width="100%" src="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fsoundcloud.com%2Fdjalanita%2Fset-prog-dark-junho-10&amp;secret_url=false" allowscriptaccess="always" type="application/x-shockwave-flash"></embed>
</object>
<span><a href="http://soundcloud.com/djalanita/set-prog-dark-junho-10">Set Prog Dark - Junho/10</a> by <a href="http://soundcloud.com/djalanita">Dj Alanita</a></span><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/atracoes-tandava/121-alanita-sc.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/atracoes-tandava/ver-todas/121-alanita-sc.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/atracoes-tandava/ver-todas/121-alanita-sc.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL3d3dy5lbGlqYWguY29tLmJyL3RhbmRhdmEvMjAxMC9hdHJhY29lcy10YW5kYXZhLzEyMS1hbGFuaXRhLXNjLmh0bWw%3D" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Sexta, 20 de Agosto de 2010 01:26				</span>
					
								<span class="rt-date-modified">
					Última atualização em Sexta, 20 de Agosto de 2010 01:33				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>									
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/atracoes-tandava/110-andre-nego-pr.html">André Nego (PR)</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<p style="text-align: justify;"><img style="margin: 0px 10px 10px 0px; float: left;" alt="André Nego" src="images/stories/atracoes/andre-nego.jpg" height="350" width="225" /></p>
<p align="justify">Formado na Escola de DJs Christ Academy, <strong>André Nego</strong> já tocou em diversas  casas noturnas e eventos de Curitiba (Soho, La Lupe, Wonka Bar, Jean  Pierre Lobo Café, Espaço Roxy e Mega Bazar Lúdica).</p>
<p align="justify">Seu estilo é variado dentro da Indietronica e inclui  Electro house, Electro, Disco Punk, New Rave e Maximal.</p>
<p align="justify">No seu set você  vai ouvir Digitalism, Spank Rock, Simian Mobile Disco, Headman, The  Teenagers, Foals, Daft Punk, The Twelves, Dick4Dick, Soulwax, The Toxic  Avenger, Erol Alkan, MSTRKRFT, Crystal Castle.</p>
<p align="justify">Além de DJ, promove junto com o DJ Joel Guglielmini, a festa Vive la Musique, que une música e exposições de arte.</p>
<blockquote>
<p><strong>+ INFO: </strong><a target="_blank" href="http://www.vivelamusique.com.br">www.vivelamusique.com.br</a></p>
</blockquote><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/atracoes-tandava/110-andre-nego-pr.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/atracoes-tandava/ver-todas/110-andre-nego-pr.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/atracoes-tandava/ver-todas/110-andre-nego-pr.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL3d3dy5lbGlqYWguY29tLmJyL3RhbmRhdmEvMjAxMC9hdHJhY29lcy10YW5kYXZhLzExMC1hbmRyZS1uZWdvLXByLmh0bWw%3D" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Segunda, 02 de Agosto de 2010 20:49				</span>
					
								<span class="rt-date-modified">
					Última atualização em Segunda, 02 de Agosto de 2010 20:58				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>									
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/atracoes-tandava/137-anginha-rodrigues-pr.html">Anginha Rodrigues (PR)</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<p style="text-align: justify;"><img style="margin: 0px 10px 10px 0px; float: left;" alt="anginha-3" src="images/stories/atracoes/anginha-3.jpg" width="200" height="300" /></p>
<p align="justify">Anginha Rodrigues convive com a música desde a infância. Criada em uma família de músicos, começou sua carreira como DJ em 2006, desenvolvendo sempre a linha noturna do Psy-Trance, a DJ vem se destacando com excelentes mixagens, ótimo gosto musical e ativa atuação na cena.<br /><br />Seus DJ Sets mesclam o Psytrance Full On Night à vertentes de Dark Psychedelic envolvendo o dancefloor em uma atmosfera noturna e grooveada, sempre com muito carisma e contato com o público.</p>
<blockquote>
<p><strong>+ INFO: </strong><a target="_blank" href="http://www.myspace.com/anginharodrigues"><strong></strong>myspace.com/anginharodrigues</a></p>
</blockquote><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/atracoes-tandava/137-anginha-rodrigues-pr.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/atracoes-tandava/ver-todas/137-anginha-rodrigues-pr.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/atracoes-tandava/ver-todas/137-anginha-rodrigues-pr.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL3d3dy5lbGlqYWguY29tLmJyL3RhbmRhdmEvMjAxMC9hdHJhY29lcy10YW5kYXZhLzEzNy1hbmdpbmhhLXJvZHJpZ3Vlcy1wci5odG1s" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Sexta, 22 de Outubro de 2010 10:47				</span>
					
								<span class="rt-date-modified">
					Última atualização em Sexta, 22 de Outubro de 2010 11:04				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>									
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/atracoes-tandava/119-caroles-rj.html">Caroles (RJ)</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<p style="text-align: justify;"><img style="margin: 0px 10px 10px 0px; float: left;" alt="caroles" src="images/stories/atracoes/caroles.jpg" width="225" height="337" /></p>
<p align="justify">Seus DJ Sets sempre convidam o público para uma viagem inovadora. Vêm surpreendendo as pistas com musicas envolventes e dançantes seja qual for o estilo adotado entre todas as vertentes do electro, podendo passear pelo techno, prog house e até mesmo pelos inusitados electroclash e breakbeat, mas sempre com base no verdadeiro electro-house. <br /><br />Com mixagens suaves, limpas e momentos bem femininos que muito agradam também aos homens, seu som sempre levanta a pista com muita personalidade e vem conquistando cada vez mais fãs por onde passa. <br /><br />Recentemente integrou o line up do Festival Universo Paralello, conhecido como ser o melhor festival de ano novo do mundo, além de comandar as pick ups das duas pistas da Sunset Day Party em Minas Gerais, considerada por muitos a melhor <em>day party </em>do país.</p>
<blockquote>
<p><strong>+ INFO: </strong><a href="http://www.myspace.com/djcaroles" target="_blank">www.myspace.com/djcaroles</a></p>
</blockquote>
<p><strong>+ DJ SETS / MIXTAPES</strong></p>
<object codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=10,0,32,18" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" type="application/x-shockwave-flash" height="225" width="100%">
<param name="allowscriptaccess" value="always" />
<param name="src" value="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fsoundcloud.com%2Fdjcaroles&amp;secret_url=false" /><embed height="225" width="100%" src="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fsoundcloud.com%2Fdjcaroles&amp;secret_url=false" allowscriptaccess="always" type="application/x-shockwave-flash"></embed>
</object>
<span><a href="http://soundcloud.com/djcaroles">Latest tracks by djcaroles</a></span><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/atracoes-tandava/119-caroles-rj.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/atracoes-tandava/ver-todas/119-caroles-rj.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/atracoes-tandava/ver-todas/119-caroles-rj.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL3d3dy5lbGlqYWguY29tLmJyL3RhbmRhdmEvMjAxMC9hdHJhY29lcy10YW5kYXZhLzExOS1jYXJvbGVzLXJqLmh0bWw%3D" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Quarta, 04 de Agosto de 2010 13:12				</span>
					
								<span class="rt-date-modified">
					Última atualização em Quarta, 04 de Agosto de 2010 13:24				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>									
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/atracoes-tandava/141-chucky-pr.html">Chucky (PR)</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<p style="text-align: justify;"><img style="margin: 0px 10px 10px 0px; float: left;" alt="chucky" src="images/stories/atracoes/chucky.jpg" width="225" height="329" /></p>
<p style="text-align: justify;">Luiz Gustavo Kochanny aka DJ Chucky, nascido em 1985, começou a freqüentar festas em 2000 e em seguida participou na produção de dois eventos de peso em Curitiba ( Cosmoon e Earthdance).</p>
<p style="text-align: justify;">Em 2005, com o incentivo de diversos amigos produtores de festas e DJs de Curitiba, iniciou sua carreira como DJ, onde destacou-se nos set’s de progressive house, apresentando-se em diversas cidades do Paraná e Santa Catarina.</p>
<p style="text-align: justify;">Atualmente Gustavo é sócio fundador do projeto MAKANÃ, que foi um sucesso na sua primeira edição, pela qualidade na organização do evento que aconteceu em São José dos Pinhais. Chucky vem aprimorando seus conhecimentos e mostrando qualidade em seus set’s de Techno, tech house e minimal, pela sua técnica de discotecar, dedicação e pesquisas em sempre levar tracks atuais e dançantes ao publico.<strong> <br /></strong></p>
<blockquote><strong>+ INFO:</strong> <a target="_blank" href="http://soundcloud.com/chucky-lgk">soundcloud.com/chucky-lgk</a></blockquote>
<br />
<p><strong>+ DJ SET / PROMO</strong></p>
<p>
<object codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=10,0,32,18" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" type="application/x-shockwave-flash" height="81" width="100%">
<param name="allowscriptaccess" value="always" />
<param name="src" value="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fapi.soundcloud.com%2Ftracks%2F6319521&amp;secret_url=false" /><embed height="81" width="100%" src="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fapi.soundcloud.com%2Ftracks%2F6319521&amp;secret_url=false" allowscriptaccess="always" type="application/x-shockwave-flash"></embed>
</object>
<span><a href="http://soundcloud.com/chucky-lgk/chucky-dj-set-october-2010">Chucky DJ Set - October 2010</a> by <a href="http://soundcloud.com/chucky-lgk">DJchucky-LGK</a></span></p><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/atracoes-tandava/141-chucky-pr.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/atracoes-tandava/ver-todas/141-chucky-pr.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/atracoes-tandava/ver-todas/141-chucky-pr.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL3d3dy5lbGlqYWguY29tLmJyL3RhbmRhdmEvMjAxMC9hdHJhY29lcy10YW5kYXZhLzE0MS1jaHVja3ktcHIuaHRtbA%3D%3D" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Terça, 26 de Outubro de 2010 13:19				</span>
					
								<span class="rt-date-modified">
					Última atualização em Terça, 26 de Outubro de 2010 13:26				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>									
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/atracoes-tandava/138-clara-pr.html">Clara (PR)</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<p style="text-align: justify;"><img style="margin: 0px 10px 10px 0px; float: left;" alt="clara" src="images/stories/atracoes/clara.jpg" width="225" height="339" /></p>
<p>Dj'ane desde 2005 , atuou ao lado de nomes como: BURN IN NOISE, E-JEKT, TRISTAN, MAGNETICA, MEGALOPSY, POLARIS, entre outros.</p>
<p>Tocava <em>Psy-Trance Full On Groove</em>, onde se destacou na cena de e-music... Afastada para ser mãe, Dj'ane Clara volta as pistas com um som totalmente inovador, através do Psytrance Gospel , ela mostrará um novo caminho aberto à cena Curitibana.</p>
<blockquote>"Deus escolheu as coisas loucas deste mundo para confundir as sábias" 1 COR.1:27</blockquote><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/atracoes-tandava/138-clara-pr.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/atracoes-tandava/ver-todas/138-clara-pr.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/atracoes-tandava/ver-todas/138-clara-pr.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL3d3dy5lbGlqYWguY29tLmJyL3RhbmRhdmEvMjAxMC9hdHJhY29lcy10YW5kYXZhLzEzOC1jbGFyYS1wci5odG1s" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Sexta, 22 de Outubro de 2010 10:56				</span>
					
								<span class="rt-date-modified">
					Última atualização em Segunda, 25 de Outubro de 2010 09:57				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>									
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/atracoes-tandava/131-crash-pr.html">Crash (PR)</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<p style="text-align: justify;"><img style="margin: 0px 10px 10px 0px; float: left;" alt="crash" src="images/stories/atracoes/crash.jpg" width="225" height="300" /></p>
<blockquote>Making people dance since 2002.<br /></blockquote><br />
<p style="text-align: justify;"><strong>Thiago Guimarães Toniatti</strong>, aka <strong>DJ Crash</strong>, conheceu o universo da dance music no ano de 2002 influenciado pela onda techno que dominava a cidade de curitiba.</p>
<p style="text-align: justify;">Hoje, integrante do seleto casting do núcleo movingstone, e com 7 anos de experiência, se apresentou ao lado de grandes nomes como stephan bodzin (herzbult), boris brejcha (harthouse), holgi star (kiddaz) , oblivion (autist), gaz james (toxic records), peter gun ( ger ), dnox (sprout ), king roc (bruk), tim healey (uk); materializou materias e resenhas para o www.movingstone.com.br; idealizou o portal www.lisergicdjs.com; conduziu mais de 30 noites no saudoso stereo pub, club em que era residente semanal; e, ainda, é idealizador, e responsável, pelo projeto technologic grooves, este voltado para os beats mais contemporaneos, como o tech house e o techno de vanguarda.</p>
<p style="text-align: justify;">Toda essa atividade fez com que o dj adquirisse grande bagagem cultural e, essencialmente, musical, refletindo-se em sets que passeiam entre os grooves do tech house e a energia do techno.<strong>&nbsp;</strong></p>
<blockquote>
<p><strong>+ INFO:</strong> <a target="_blank" href="http://www.orkut.com/community.aspx?cmm=30576503">Comunidade DJ Crash no Orkut</a></p>
</blockquote>
<p><strong>+ DJ SET / PROMO</strong></p>
<p>
<object codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=10,0,32,18" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" type="application/x-shockwave-flash" height="81" width="100%">
<param name="allowscriptaccess" value="always" />
<param name="src" value="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fapi.soundcloud.com%2Ftracks%2F486600%3Fsecret_token%3Ds-cd61d&amp;secret_url=false" /><embed height="81" width="100%" src="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fapi.soundcloud.com%2Ftracks%2F486600%3Fsecret_token%3Ds-cd61d&amp;secret_url=false" allowscriptaccess="always" type="application/x-shockwave-flash"></embed>
</object>
<span><a href="http://soundcloud.com/dj_crash/djcrash-confort-gruvs-august2009">DJCRASH_CONFORT GRUVS_AUGUST2009</a> by <a href="http://soundcloud.com/dj_crash">dj_crash</a></span>
</p><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/atracoes-tandava/131-crash-pr.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/atracoes-tandava/ver-todas/131-crash-pr.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/atracoes-tandava/ver-todas/131-crash-pr.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL3d3dy5lbGlqYWguY29tLmJyL3RhbmRhdmEvMjAxMC9hdHJhY29lcy10YW5kYXZhLzEzMS1jcmFzaC1wci5odG1s" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Quarta, 13 de Outubro de 2010 12:26				</span>
					
								<span class="rt-date-modified">
					Última atualização em Quarta, 13 de Outubro de 2010 12:40				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>									
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/atracoes-tandava/demonizz-live-sp.html">Demonizz LIVE (SP)</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<p style="text-align: justify;"><img style="margin: 0px 10px 10px 0px; float: left;" alt="demonizz" src="images/stories/atracoes/demonizz.jpg" width="225" height="289" /></p>
<p align="justify">Os DJs paulistanos <strong>Leandro Morales</strong> e <strong>Thiago Pasetchny</strong> uniram-se no ano de 2005 para dar inicio as suas proprias produções e atualmente, encontram-se ao lado dos mais importantes projetos da cena “Dark Trance” brasileira.<br /><br />Após percorrer um longo caminho, o projeto <strong>Demonizz </strong>alcança um dos principais objetivos: seu mais recente trabalho em parceria com os projetos Bash e Necropsycho no album ‘Extreme Deformities’, que logo após o lançamento obteve resultados extremamente satisfatórios, conquistando espaço no respeitado chart de Goa Gil.<br /><br />Apresentando-se nos principais festivais do Brasil, tais como, Universo Paralello (2008 e 2009), Festival Fora do Tempo (2008), Soulvision Festival (2009) e Indepen.dance Festival no Rio Grande do Sul (2007), Leandro e Thiago além de residentes dos núcleos Makunba em Curitiba e Virada Cultural em São Paulo, tornam-se no inicio do ano de 2009 parte integrante do casting da agência 4ideas, responsável pelos eventos mais conceituais da cena psicodélica no Brasil.<br /><br />As músicas do Demonizz prometem uma experiência única em sua composição, cuja característica mais evidente é o ritmo hipnótico de suas baterias, basslines pesados e sintetizadores ácidos e confusos.<br /><br />Sem deixar o dance floor morrer, Demonizz faz uma fusão de dark trance com diversas influências da musica eletronica, criando uma atmosfera alucinante nas pistas.</p>
<blockquote>
<p><strong>+ INFO: </strong><a target="_blank" href="http://www.demonizz.com">www.demonizz.com</a></p>
</blockquote><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/atracoes-tandava/demonizz-live-sp.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/atracoes-tandava/ver-todas/118-demonizz-live-sp.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/atracoes-tandava/ver-todas/118-demonizz-live-sp.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL3d3dy5lbGlqYWguY29tLmJyL3RhbmRhdmEvMjAxMC9hdHJhY29lcy10YW5kYXZhL2RlbW9uaXp6LWxpdmUtc3AuaHRtbA%3D%3D" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Terça, 03 de Agosto de 2010 22:30				</span>
					
								<span class="rt-date-modified">
					Última atualização em Terça, 03 de Agosto de 2010 22:35				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>									
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/atracoes-tandava/128-dymi-pr.html">Dymi (PR)</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<p style="text-align: justify;"><img style="margin: 0px 10px 10px 0px; float: left;" alt="dimy" src="images/stories/atracoes/dimy.jpg" width="225" height="322" />Sócio fundador da festa Selectro, Dymi se interessa pela música eletrônica desde 2002 onde teve a oportunidade de acompanhar de perto o crescimento de vários redutos e projetos undergrounds da cena Curitibana.</p>
<p style="text-align: justify;">Começou como DJ em 2007 e está em constante aprimoramento com seus Sets diversificados que variam entre o Progressive House / Tech House sempre tendo como destaque elementos minimalistas procurando fazer a fusão do embalo e basslines do House e vertentes, com as pitadas viajantes do Minimal.</p>
<p style="text-align: justify;">Continua sua vasta procura sobre outras vertentes e se interessa muito pelas vertentes:&nbsp; Deep House, Lounge Music, Trip Hop, Chill Out, Dub e outras, planejando um novo projeto no futuro.</p>
<p style="text-align: justify;">Acredita que ser eclético tem suas vantagens, mas tudo que se ouve deve ter um grande cuidado, e respeitar ordens básicas de bom gosto e seleção. Acima de qualquer profissão ou de qualquer valor monetário empregado dentro de suas apresentações, preza pelo amor a música e pelo respeito a quem está o ouvindo.</p><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/atracoes-tandava/128-dymi-pr.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/atracoes-tandava/ver-todas/128-dymi-pr.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/atracoes-tandava/ver-todas/128-dymi-pr.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL3d3dy5lbGlqYWguY29tLmJyL3RhbmRhdmEvMjAxMC9hdHJhY29lcy10YW5kYXZhLzEyOC1keW1pLXByLmh0bWw%3D" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Segunda, 13 de Setembro de 2010 10:56				</span>
					
								<span class="rt-date-modified">
					Última atualização em Segunda, 13 de Setembro de 2010 11:01				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>									
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/atracoes-tandava/112-elijah-hatem-pr.html">Elijah Hatem (PR)</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<p style="text-align: justify;"><img style="margin: 0px 10px 10px 0px; float: left;" alt="de elijah hatem" src="images/stories/atracoes/elijah.jpg" width="225" height="338" /></p>
<p align="justify">Fundador do projeto cultural e selo digital <a href="http://www.metropolis.art.br/undefined/" target="_blank"><strong>Metropolis Project</strong></a>, o <strong><a href="http://www.metropolis.art.br/elijah" target="_blank">DJ Elijah Hatem</a> </strong>participa ativamente da cena eletrônica e vem se destacando cada vez mais em clubs e festas em todo o Brasil.</p>
<p align="justify">Criado no cenário underground do eixo Curitiba - São  Paulo, o DJ possui uma percepção musical única e um extenso gosto  musical. Sempre misturando diferentes vertentes  e influências, traz ao  seu público DJ Sets intensos, do <em>House</em> ao <em>Electro</em>, passeando pelo <em>Breaks</em>, <strong><a href="http://www.metropolis.art.br/elijah" target="_blank">Elijah</a> </strong>esbanja energia, originalidade e o <em>lifestyle</em> das grandes metrópoles.</p>
<p align="justify">Além de atuar como DJ e agitador cultural, <strong><a href="http://www.metropolis.art.br/elijah" target="_blank">Elijah</a></strong> também faz a produção musical e a apresentação do programa <a target="_blank" href="http://www.91rock.com.br/electroontherocks"><strong>Electro On The Rocks</strong></a> ao lado do <strong>DJ Mauricião Singer</strong>. Voltado ao cenário eletrônico local, o programa é transmitido toda Sexta-Feira às 23:59 pela rádio curitibana <a target="_blank" href="http://www.91rock.com.br/"><strong>91 Rock (91,3 FM)</strong></a>.</p>
Em constante mutação, a experiência e compromisso do <strong><a href="http://www.metropolis.art.br/elijah" target="_blank">DJ Elijah Hatem</a></strong><strong> </strong>com a<em> </em>música eletrônica<em> </em>nacional  é inconfundível, sempre presente no cenário eletrônico, pode-se definir  sua carreira como algo além da música em si, e sim, em prol de toda a  cena cultural brasileira.
<p>"<em>It's all about music</em>" - de elijah hatem.</p>
<blockquote>
<p><strong>+ INFO: </strong><a target="_blank" href="http://www.metropolis.art.br/elijah">www.metropolis.art.br/elijah</a></p>
</blockquote>
<p><strong>+ DJ SETS / MIXTAPES</strong></p>
<h5>"Low BPM" . Electro-Clash / Micro-Techno</h5>
<object height="81" width="100%"> <param name="movie" value="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fsoundcloud.com%2Felijahhatemlow%2Fmy-own-peaceful-path-a-tribute-to-trentemoller&secret_url=false"></param> <param name="allowscriptaccess" value="always"></param> <embed allowscriptaccess="always" height="81" src="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fsoundcloud.com%2Felijahhatemlow%2Fmy-own-peaceful-path-a-tribute-to-trentemoller&secret_url=false" type="application/x-shockwave-flash" width="100%"></embed> </object>  <span><a href="http://soundcloud.com/elijahhatemlow/my-own-peaceful-path-a-tribute-to-trentemoller">My Own Peaceful Path (A Tribute To Trentemøller)</a> by <a href="http://soundcloud.com/elijahhatemlow">elijahhatemlow</a></span> 
<h5>Electro-Rock . Maximal Promo Set</h5>
<object height="225" width="100%"> <param name="movie" value="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fsoundcloud.com%2Felijahhatem&secret_url=false"></param> <param name="allowscriptaccess" value="always"></param> <embed allowscriptaccess="always" height="225" src="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fsoundcloud.com%2Felijahhatem&secret_url=false" type="application/x-shockwave-flash" width="100%"></embed> </object>  <span><a href="http://soundcloud.com/elijahhatem">Latest tracks by elijahhatem</a></span> 
<h5>Tandava Madness em 2009</h5>
<object height="81" width="100%"> <param name="movie" value="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fsoundcloud.com%2Felijahhatemalternative%2Fde-elijah-hatem-tandava-madness-2009&secret_url=false"></param> <param name="allowscriptaccess" value="always"></param> <embed allowscriptaccess="always" height="81" src="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fsoundcloud.com%2Felijahhatemalternative%2Fde-elijah-hatem-tandava-madness-2009&secret_url=false" type="application/x-shockwave-flash" width="100%"></embed> </object>  <span><a href="http://soundcloud.com/elijahhatemalternative/de-elijah-hatem-tandava-madness-2009">de elijah hatem Tandava Madness 2009</a> by <a href="http://soundcloud.com/elijahhatemalternative">elijahhatemalternative</a></span> 
<p><strong>+ LIVE VÍDEOS / PLAYLIST</strong></p>
<h5>Electro-Rock / Maximal Vinyl Set @ In New Music We Trust Party</h5>
<div align="center"><object width="480" height="385"><param name="movie" value="http://www.youtube.com/p/C94969210FED1B88?hl=pt_BR&fs=1"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/p/C94969210FED1B88?hl=pt_BR&fs=1" type="application/x-shockwave-flash" width="480" height="385" allowscriptaccess="always" allowfullscreen="true"></embed></object></div><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/atracoes-tandava/112-elijah-hatem-pr.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/atracoes-tandava/ver-todas/112-elijah-hatem-pr.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/atracoes-tandava/ver-todas/112-elijah-hatem-pr.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL3d3dy5lbGlqYWguY29tLmJyL3RhbmRhdmEvMjAxMC9hdHJhY29lcy10YW5kYXZhLzExMi1lbGlqYWgtaGF0ZW0tcHIuaHRtbA%3D%3D" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Segunda, 02 de Agosto de 2010 21:02				</span>
					
								<span class="rt-date-modified">
					Última atualização em Terça, 31 de Agosto de 2010 17:53				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>									
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/atracoes-tandava/109-felipe-kantek-pr.html">Felipe Kantek (PR)</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<p style="text-align: justify;"><img style="margin: 0px 10px 10px 0px; float: left;" alt="kantek" src="images/stories/atracoes/kantek.jpg" height="338" width="225" /></p>
<p align="justify">O <strong>DJ Felipe Kantek</strong> tem se destacado  na cena do sul do país pelo seu estilo arrojado de tocar Electro House.  Sempre com muito carisma e mantendo a pista animada, fundador do  Metropolis Project e ativo produtor cultural na cena eletrônica, mantém  seus sets sempre atualizados com as últimas tendências nacionais e  internacionais.<br /><br /><strong>Felipe Kantek</strong> começou sua  carreira como DJ em 2005, onde se destacou rapidamente na cena  underground de Curitiba. A qualidade de som e vasta diversidade sonora  fizeram do seu DJ Set uma marca na cidade, pautada pelo início da cena  Electro House.</p>
<p align="justify">Seu som atravessa inúmeras vertentes da música  eletrônica, influenciado pelo Electro, Tech House,&nbsp;Funky House,&nbsp;Electro  House,&nbsp;Groove&nbsp;e&nbsp;Techno, sempre atualizado com as últimas tendências  musicais mundiais garantindo um DJ Set sempre original e pista animada.<br /><br />Um  dos fundadores do projeto cultural Metropolis Project, desde 2007  Felipe Kantek atua ativamente na cena musical como produtor cultural  promovendo eventos e ações voltadas à cultura eletrônica.</p>
<p align="justify">Atualmente, também é Label Manager de Electro, House e  Tech-House da Metropolis Project Records, sendo responsável pelo  lançamento e exportação de álbuns e compilações de inúmeros produtores  nacionais para o mercado internacional.</p>
<blockquote>
<p><strong>+ INFO: </strong><a target="_blank" href="http://www.metropolis.art.br/kantek">www.metropolis.art.br/kantek</a></p>
</blockquote>
<p style="text-align: justify;"><strong>+ DJ SETs / MIXTAPES:</strong></p>
<object codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=10,0,32,18" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" type="application/x-shockwave-flash" height="81" width="100%">
<param name="allowscriptaccess" value="always" />
<param name="src" value="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fsoundcloud.com%2Fkantek%2Fset-felipe-kantek-maio-2010&amp;secret_url=false" /><embed height="81" width="100%" src="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fsoundcloud.com%2Fkantek%2Fset-felipe-kantek-maio-2010&amp;secret_url=false" allowscriptaccess="always" type="application/x-shockwave-flash"></embed>
</object>
<span><a href="http://soundcloud.com/kantek/set-felipe-kantek-maio-2010">Set Felipe Kantek maio 2010</a> by <a href="http://soundcloud.com/kantek">kantek</a></span><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/atracoes-tandava/109-felipe-kantek-pr.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/atracoes-tandava/ver-todas/109-felipe-kantek-pr.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/atracoes-tandava/ver-todas/109-felipe-kantek-pr.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL3d3dy5lbGlqYWguY29tLmJyL3RhbmRhdmEvMjAxMC9hdHJhY29lcy10YW5kYXZhLzEwOS1mZWxpcGUta2FudGVrLXByLmh0bWw%3D" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Segunda, 02 de Agosto de 2010 20:46				</span>
					
								<span class="rt-date-modified">
					Última atualização em Segunda, 02 de Agosto de 2010 20:49				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>									
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/atracoes-tandava/129-felipe-kojake-pr.html">Felipe Kojake (PR)</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<p style="text-align: justify;"><img style="margin: 0px 10px 10px 0px; float: left;" alt="kojake" src="images/stories/atracoes/kojake.jpg" width="225" height="259" /></p>
<p align="justify">Felipe Kojake é um artista curitibano. Dj, produtor e baixista envolvido com música desde 1997. As linhas de som que influenciam tanto os sets como as suas produções, variam desde o downbeat, nu jazz , grooves , breaks e funk, passando tb pelo deep e tech house.</p>
<p align="justify">Em novembro de 2007 lançou uma track em vinyl ao lado do Dj Soneca, pela gravadora Royal Soul Records. Atualmente Kojake é dj residente as quintas feiras no Taj bar, foi o autor e produtor da compilação “Song..s of Taj”, cd lançado com tracks de grandes nomes da cena nacional de musica eletrônica.</p>
<p align="justify">Com o projeto Groovefall de LIVE P.A., ao lado do DJ Delatorre já tocou em diversas festas, festivais e chill outs brasileiros como: Tribe, Xxxperience, Earthdance, Cosmoon, Kabalah, Universo Parallelo, Cachoeira Alta, Tranceformation, Smirnoff private party, Tríade rec. party, Red Bull 3Style e em muitos outros clubes como Danghai, Wyn, Es vedra, Taj Camboriu, Grand Cassino Royale (Pgy) entre outras festas privates.</p>
<p align="justify">Suas produções estão a venda em sites como juno download e beatport, assinada pelos selos Royal Soul Rec. e Tríade Rec.</p>
<p align="justify">“<em>Sempre buscando novas ferramentas de produção, utilizando instrumentos orgânicos e shynts inovadores, buscando a evolução nos timbres, melodias e idéias, sempre em busca do groove perfeito e da melhor mixagem para o ouvido de todos.</em>"</p>
<blockquote>
<p><strong>+ INFO: </strong><a target="_blank" href="http://www.myspace.com/felipekojake">www.myspace.com/felipekojake</a></p>
</blockquote>
<p><strong>+ DJ SETS / TRACKS</strong></p>
<p>
<object codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=10,0,32,18" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" type="application/x-shockwave-flash" height="225" width="100%">
<param name="allowscriptaccess" value="always" />
<param name="src" value="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fapi.soundcloud.com%2Fusers%2F528289&amp;secret_url=false" /><embed height="225" width="100%" src="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fapi.soundcloud.com%2Fusers%2F528289&amp;secret_url=false" allowscriptaccess="always" type="application/x-shockwave-flash"></embed>
</object>
<span><a href="http://soundcloud.com/felipekojake">Latest tracks by Felipe Kojake</a></span></p><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/atracoes-tandava/129-felipe-kojake-pr.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/atracoes-tandava/ver-todas/129-felipe-kojake-pr.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/atracoes-tandava/ver-todas/129-felipe-kojake-pr.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL3d3dy5lbGlqYWguY29tLmJyL3RhbmRhdmEvMjAxMC9hdHJhY29lcy10YW5kYXZhLzEyOS1mZWxpcGUta29qYWtlLXByLmh0bWw%3D" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Quarta, 22 de Setembro de 2010 09:37				</span>
					
								<span class="rt-date-modified">
					Última atualização em Quarta, 22 de Setembro de 2010 09:43				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>									
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/atracoes-tandava/96-friq-trip.html">Friq Trip (SC)</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<p><img style="margin: 0px 10px 25px 0px; float: left;" alt="friq-trip-2" src="images/stories/atracoes/friq-trip-2.jpg" height="246" width="225" /><strong>Felipe Silveira</strong> aka <strong>Friq Trip</strong> desde 2002 tem o prazer de pesquisa música downtempo e suas vertentes. Apaixonou-se pelo gênero frequentando os chillouts das festas open-air da cidade de Campinas-SP.<br /><br />O estilo de música mais lenta sai dos chillouts e passa a assumir grande espaço em sua vida pessoal, tornando-se a trilha sonora do seu dia a dia.<br /><br />Tocou por um bom tempo música de pista (Minimal / Electro) e no começo de 2007 sai da capital de São Paulo e se muda para Florianópolis.<br /><br />O contato intenso com a natureza ajuda Felipe a restabelecer seu ritmo interno e, a partir daí, tendo também estudado musicoterapia, concentra suas energias e criatividade nos sets de música downtempo e suas vertentes. Felipe acredita que de forma bem trabalhada e consciente um DJ pode atuar de forma terapêutica.<br /><br />Através da música, transporta as pessoas para outros planos, conduzindo estados de relaxamento, tranquilidade e paz. Seus sets apresentam uma característica envolvente, alegre e relaxante, sem perder o balanço de um bom groove.<br /><br />Um som que te faz dançar de maneira fluida e permite que você esteja consciente de seu corpo e movimento. Passeia pelo nu-jazz, instrumental, IDM e turntablism.<br /><br />Seus sets são apresentados e formulados como uma história, desenvolvem-se interagindo e respeitando sempre o "momento", o contexto em que está inserido.</p>
<blockquote>
<p><strong>+ INFO: </strong><a target="_blank" href="http://www.myspace.com/djfriqtrip">www.myspace.com/djfriqtrip</a></p>
</blockquote><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/atracoes-tandava/96-friq-trip.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/atracoes-tandava/ver-todas/96-friq-trip.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/atracoes-tandava/ver-todas/96-friq-trip.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL3d3dy5lbGlqYWguY29tLmJyL3RhbmRhdmEvMjAxMC9hdHJhY29lcy10YW5kYXZhLzk2LWZyaXEtdHJpcC5odG1s" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Quarta, 16 de Junho de 2010 22:43				</span>
					
								<span class="rt-date-modified">
					Última atualização em Terça, 03 de Agosto de 2010 21:36				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>									
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/atracoes-tandava/funk-you.html">Funk You</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<p><img style="margin: 0px 10px 10px 0px; float: left;" alt="funk-you_thumb" src="images/stories/atracoes/funk-you_thumb.jpg" height="225" width="225" />Abrindo a primeira noite do Chill Out a festa <strong>Funk You</strong>, projeto que apresenta a&nbsp; música "negra", batidas quentes e extremamente dançantes! Do <em>Funk Old School</em> ao <em>New School</em>, <em>NuFunk</em>, <em>Breaks</em>, <em>Big Beat</em>, <em>Disco</em> e <em>Hip Hop</em>.</p>
<p>Com pouco mais de dois anos, a festa já virou referencia de Funk e breaks em Curitiba comandada pelos DJs Murillo e Schasko, sempre recebendo convidados da cena curitibana e nacional.</p>
<blockquote>
<p><strong>+ INFO: </strong><a target="_blank" href="http://www.myspace.com/funkyoucwb">www.myspace.com/funkyoucwb</a></p>
</blockquote><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/atracoes-tandava/funk-you.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/atracoes-tandava/ver-todas/93-funk-you.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/atracoes-tandava/ver-todas/93-funk-you.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL3d3dy5lbGlqYWguY29tLmJyL3RhbmRhdmEvMjAxMC9hdHJhY29lcy10YW5kYXZhL2Z1bmsteW91Lmh0bWw%3D" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Quarta, 16 de Junho de 2010 22:43				</span>
					
								<span class="rt-date-modified">
					Última atualização em Terça, 13 de Julho de 2010 20:54				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>									
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/atracoes-tandava/113-gabi-lima-pr.html">Gabi Lima (PR)</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<p style="text-align: justify;"><img style="margin: 0px 10px 10px 0px; float: left;" alt="gabi-lima" src="images/stories/atracoes/gabi-lima.jpg" width="225" height="339" /></p>
<p align="justify">Com 7 anos de carreira, a DJ <strong>Gabi Lima</strong> esbanja um longo trajeto dentro do cenário da música eletrônica.</p>
<p align="justify">Sua paixão com a arte de discotecar e com o mercado da música eletrônica surgiu a 14 anos atrás, pesquisando e ouvindo programas de radio, ela se indentificou com o estilo e com a arte e logo começou a freqüentar eventos direcionados a esse mundo em São Paulo.</p>
<p align="justify">Porém, foi trabalhando na loja Doscor Disco,&nbsp; referência para os DJs paranaenses, que Gabi pode ter uma visão mais ampla e aguçada do mundo da música eletrônica, além de ter contato com os equipamentos, DJs, lançamentos e toda a história é trajetória desse universo, ela estava diariamente em contato com as novas influências do mercardo.</p>
<p align="justify">E desde então a DJ nunca mais parou.</p>
<p align="justify">Além disso em 2001, Gabi junto a outros parceiros trabalhou em prol de divulgar e informar o público do sul do Brasil, com projetos semanais ao qual tinha o nome de Shift e Plug, e traziam grandes atrações do cenário nacional e internacional, em uma época que não axistiam eventos open air, e pouquissimos clubs pelo Brasil trabalhavam com esse segmento.</p>
<p align="justify"><strong>+ APRESENTAÇÃO</strong></p>
<p align="justify">Suas apresentações trazem influências das vertentes do house e do Techno. Gabi busca trazer para seus sets dinamismo é versatilidade,&nbsp; com mixagens longas e bem trabalhadas ela abusa de sua experiência e carisma nas pick-ups, garantindo diversão é boa música para os mais diferenciados e exigentes públicos, de acordo com cada momento da noite.</p>
<p align="justify"><strong>+ PROJETOS E RESIDÊNCIAS</strong></p>
<p align="justify">Gabi é residente mensal do Danghai Club em Curitiba, onde se apresenta freqüentemente ao lado de algumas das melhores atrações do cenário nacional e internacional. Outra frente de trabalho e criação da DJ é o projeto ‘Girls on the Decks’, apresentado ao lado da dj Ale Albieri, no formato show Girls On The Decks ainda conta com o percussionista Fernando Schaefer, ex baterista de bandas como Sepultura, Rodox e Pavilhão 9, a vocalista Julia Koch e a Vj Anny Mello. Além&nbsp; do live percussion e VJ(vídeo arts), garantindo um show de diversão, emoção, arte e cultura.</p>
<p align="justify">Como empresária, Gabi Lima está a frente da agência curitibana Synk, juntamente com o dj e produtor Rodrigo Carreira. Além da agência, a marca inclui a Synk Records, Synk Management e a Synk Enterteinment.</p>
<p align="justify">Pela Synk entertainment os eventos produzidos por Gabi Lima e Rodrigo Carreira são, a Disco.nnect e Synk label party, onde trazem todas as influências musicais da agência e seus DJs para as noites. O evento synk label party aconteceu durante 1 ano e meio no&nbsp; club Vibe em Curitiba, um dos clubs mais importante da cena do sul, e está retornando agora após sua reforma, a synk label Party retoma com novo formato e com uma fluência maior de noites, já a Disco.nnect foi lançada em março desse ano com uma edição no Danghai club, e segue em uma tour por alguns clubs do sul do Brasil.</p>
<p align="justify">Ao longo de sua carreira Gabi Lima já dividiu o palco com artistas mundialmente conhecidos, tais como: Tiesto, Trentemoller, Manuel de la Mare,&nbsp; Super Flu, Alex Kenji, Martin Eyerer, John Acquaviva, Leif Hatfield, Fabrício Peçanha, Gabe Live, Leozinho, Gui Borato, Felguk entre outros.</p>
<p align="justify">Suas apresentações já passaram por todos os estados do sul, e também por estados como São Paulo, Rio de janeiro, Goias, Mato Grosso do sul, Minas e Pernambuco.</p>
<blockquote>
<p><strong>+ INFO: </strong><a target="_blank" href="http://www.synk.com.br/gabilima">www.synk.com.br/gabilima</a></p>
</blockquote>
<p style="text-align: justify;"><strong>+ DJ SETs / MIXTAPES:</strong></p>
<object codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=10,0,32,18" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" type="application/x-shockwave-flash" height="225" width="100%">
<param name="allowscriptaccess" value="always" />
<param name="src" value="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fsoundcloud.com%2Fgabilima&amp;secret_url=false" /><embed height="225" width="100%" src="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fsoundcloud.com%2Fgabilima&amp;secret_url=false" allowscriptaccess="always" type="application/x-shockwave-flash"></embed>
</object>
<span><a href="http://soundcloud.com/gabilima">Latest tracks by Gabi Lima</a></span><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/atracoes-tandava/113-gabi-lima-pr.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/atracoes-tandava/ver-todas/113-gabi-lima-pr.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/atracoes-tandava/ver-todas/113-gabi-lima-pr.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL3d3dy5lbGlqYWguY29tLmJyL3RhbmRhdmEvMjAxMC9hdHJhY29lcy10YW5kYXZhLzExMy1nYWJpLWxpbWEtcHIuaHRtbA%3D%3D" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Terça, 03 de Agosto de 2010 21:54				</span>
					
								<span class="rt-date-modified">
					Última atualização em Terça, 03 de Agosto de 2010 22:09				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>									
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/atracoes-tandava/139-gabriel-bibi-pr.html">Gabriel Bibi (PR)</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<p style="text-align: justify;"><img style="margin: 0px 10px 10px 0px; float: left;" alt="gabriel-bibi" src="images/stories/atracoes/gabriel-bibi.jpg" width="225" height="312" /></p>
<p style="text-align: justify;">Influenciado pelo Techno, vertente consagrada da e-music, Gabriel Bibi agita as pistas ao som do Electro Progressivo. Incorporando sempre muita seriedade em suas mixagens, Gabriel Bibi proporciona ao público um som pesado e preenchido.</p>
<p style="text-align: justify;">Foi tocando em cidades do Paraná e Santa Catarina que teve a oportunidade de trabalhar com artistas profissionais de peso até então, tais como: Sandro Perez (Uruguay), Noir (Dinamarca), Eric Entrena (Espanha), Rodrigo Carreira, Paulo Antonio, entre outros. Atualmente Gabriel é residente do London Pub, na cidade em que vive.<strong> <br /></strong></p>
<blockquote><strong>+ INFO:</strong> <a target="_blank" href="http://www.soundcloud.com/djgabrielbibi"><span style="color: #2a5db0;">soundcloud.com/d</span>jgabrielbi</a><wbr></wbr><a target="_blank" href="http://www.soundcloud.com/djgabrielbibi">bi</a><a target="_blank" href="http://www.orkut.com/community.aspx?cmm=30576503"></a></blockquote>
<p><strong>+ DJ SET / PROMO</strong></p>
<p>
<object codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=10,0,32,18" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" type="application/x-shockwave-flash" height="81" width="100%">
<param name="allowscriptaccess" value="always" />
<param name="src" value="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fapi.soundcloud.com%2Ftracks%2F6005758&amp;secret_url=false" /><embed height="81" width="100%" src="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fapi.soundcloud.com%2Ftracks%2F6005758&amp;secret_url=false" allowscriptaccess="always" type="application/x-shockwave-flash"></embed>
</object>
<span><a href="http://soundcloud.com/djgabrielbibi/batstaca-outubro-2010">Batstaca @ Outubro 2010</a> by <a href="http://soundcloud.com/djgabrielbibi">Gabriel Bibi [London Pub]</a></span></p><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/atracoes-tandava/139-gabriel-bibi-pr.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/atracoes-tandava/ver-todas/139-gabriel-bibi-pr.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/atracoes-tandava/ver-todas/139-gabriel-bibi-pr.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL3d3dy5lbGlqYWguY29tLmJyL3RhbmRhdmEvMjAxMC9hdHJhY29lcy10YW5kYXZhLzEzOS1nYWJyaWVsLWJpYmktcHIuaHRtbA%3D%3D" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Sexta, 22 de Outubro de 2010 14:44				</span>
					
								<span class="rt-date-modified">
					Última atualização em Sexta, 22 de Outubro de 2010 15:03				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>									
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/atracoes-tandava/123-gabriel-boni-pr.html">Gabriel Boni (PR)</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<p style="text-align: justify;"><img style="margin: 0px 10px 10px 0px; float: left;" alt="gabriel_boni" src="images/stories/atracoes/gabriel_boni.jpg" width="225" height="250" /></p>
<p align="justify">Natural de Brasília, Gabriel Boni é revelaçao no cenário eletrônico nacional e internacional. Nos últimos 4 anos dividiu o&nbsp; palco com nomes como: King Roc, Anthony Collins, Tom Clark, Tim Healey, Phase, Boris Brechja, Magal, Paco Osuna, Elli Iwasa, D-nox, Niko Schwind, Marshall, Alex Kenji, Ahmet Sendi, John Acquaviva, Phase entre outros.</p>
<p align="justify">É fundador do selo iNminimax Records, que produz festas em Curitiba, Brasilia e Porto Alegre, além da Espanha, Italia e&nbsp; Alemanha. É um dos selos brasileiros mais vendidos no mercado mundial através de sites como Beatport.e muitos outros. Conta com a colaboraçao de grandes nomes como&nbsp; Minicoolboyz (MINUS), Alex Costa (Noir Music), Franck Valat (Polar Noise), Niko Schwind (Stil Vor Talent/Autist), Autistic, Dualism (Autist/ Archipel) e outros artistas.</p>
<p align="justify">No Brasil&nbsp; ja passou pelos mais variados clubs e festas, tocando para&nbsp; até 15.000 pessoas em alguns evevtos, alem de ser atracao em festivais como Universo Paralello, e redisente do clube Roxy em Curitiba.</p>
<p align="justify">Foi organizador do Club Stage Soulvision Festival 2010, eleito melhor DJ set de Cuiabá pelo site Factóide e vencedor da Final Jagermaister DJs Contest Brasil 2009/2010.</p>
<blockquote>
<p><strong>+ INFO: </strong><a target="_blank" href="http://www.myspace.com/rodrigonickel"></a><a href="http://smartbiz.uol.com.br/djdetalhe.php?id=169">http://smartbiz.uol.com.br/djdetalhe.php?id=169</a></p>
</blockquote>
<p><strong>+ DJ SETS / TRACKS</strong></p>
<object codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=10,0,32,18" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" type="application/x-shockwave-flash" height="225" width="100%">
<param name="allowscriptaccess" value="always" />
<param name="src" value="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fsoundcloud.com%2Fgabrielboni&amp;secret_url=false" /><embed type="application/x-shockwave-flash" height="225" width="100%" src="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fsoundcloud.com%2Fgabrielboni&amp;secret_url=false" allowscriptaccess="always"></embed>
</object>
<span><a href="http://soundcloud.com/gabrielboni">Latest tracks by Gabriel Boni</a></span><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/atracoes-tandava/123-gabriel-boni-pr.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/atracoes-tandava/ver-todas/123-gabriel-boni-pr.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/atracoes-tandava/ver-todas/123-gabriel-boni-pr.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL3d3dy5lbGlqYWguY29tLmJyL3RhbmRhdmEvMjAxMC9hdHJhY29lcy10YW5kYXZhLzEyMy1nYWJyaWVsLWJvbmktcHIuaHRtbA%3D%3D" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Segunda, 30 de Agosto de 2010 17:31				</span>
					
								<span class="rt-date-modified">
					Última atualização em Segunda, 30 de Agosto de 2010 17:35				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>									
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/atracoes-tandava/120-gromma-pr.html">Gromma (PR)</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<p style="text-align: justify;"><img style="margin: 0px 10px 10px 0px; float: left;" alt="gromma" src="images/stories/atracoes/gromma.jpg" width="225" height="319" /></p>
<p align="justify">Considerado como uma revelação no cenário  eletrônico curitibano, <strong>João Paulo</strong>, mais conhecido como <strong>Gromma</strong>, afirma  que a fórmula para isso é dedicação, gosto musical extremamente refinado  e uma excelente bagagem adquirida antes de mesmo de começar a  discotecar, desde 2001 como expectador por dancefloors de clubs e  festivais Brasil à fora, como exemplo as boas e velhas edições da LeTone  que aconteciam em Curitiba.</p>
<p align="justify">Começou a discotecar em 2006 com incentivo  de amigos que já tocavam e também por gostar muito da música e o que os  clubs e festivais proporcionavam naquela época. Sempre gostou das  sonoridades conceituais, "diferentes" e ao mesmo tempo inteligentes,  hoje em dia sua linha de som se baseia em House com bastante Groove e  influencias de Deep-House e Techno, seguindo as tendencias Européias  onde ultimamente percebemos que as "sonoridades" estão cada vez mais  "orgânicas" e ao mesmo tempo menos "robóticas", "digitais", o que muitos  na Europa já chamam de levada "Tropical".</p>
<p align="justify">Recentemente vem se dedicando  à alguns projetos de festas em Curitiba, Urban Concept e Tech  Grooves,ambos visando uma extrema qualidade sonora e o conceito  underground, juntando fãs a cada edição realizada.</p>
<p align="justify">Gromma já passou por  praticamente todos os clubs do circuito underground curitibano como  Circus, Soho, Roxy, Wonka, LaLupe, JPL, Sioux entre outros; nos melhores  clubs de Curitiba como a Vibe Red Concept (um dos melhores clubs do  Brasil), Lique e clubs de conceito um pouco mais comercial como Inside  Music Bar, Danghai e Mahatma, e também em grandes festivais nacionais  como o Soulvision no inteiror de São Paulo,presença constante no  interior do Paraná e litoral de SC.</p>
<p align="justify">Dividiu line-ups com artistas como:  Edu Imbernon (Espanha), Alex Kenji (Italia), Alex Bau (Alemanha),D-Nox  (Alemanha), Gaz James (UK), Gustavo Bravetti (Uruguai), Dimitri Nakov  (França), Jerome Isma-Ae (Alemanha), Dave The Drummer (UK), Gabe LIVE  (Brasil), Oblivion (Brasil), Mara Bruiser (SP-Brasil), Elton D -  Maxximal LIVE (3Plus), Aninha (3Plus), Paulo Jardim (BH-Brasil), Allan  Villar (DF-Brasil), DXTR-LAB (SP-Brasil), Rodrigo Carreira (PR-Brasil),  Gabi Lima (PR-Brasil), Rolldabeetz (PR-Brasil), Rodrigo Nickel  (PR-Brasil) entre outros.</p>
<p align="justify">Recentemente foi convidado à partipar do  casting da Moove Bookings &amp; Artist Managent, agência curitibana em  constante crescimento, contando com artistas como Pirupa (Itália), Butch  (Alemanha), Jay Lumen (Hungria) entre outros.</p>
<p align="justify">Em Junho de 2009, foi  selecionado entre os 7 finalistas na categoria underground do Concurso  Multicultural TribalTech (concurso realizado para selecionar novos  talentos por um dos maiores festivais de música eletronica do Brasil, a  TribalTech - <a href="http://www.tribaltech.art.br/" target="_blank">www.tribaltech.art.br</a>)  onde concorreu com quase 200 artistas do Brasil inteiro. Logo mais  tarde em Outubro de 2009 foi finalista da etapa paranaense do  Jagermeister Djs Contest (Maior Concurso de Djs da América Latina),  fazendo uma excelente apresentação na Lique em Curitiba.</p>
<blockquote>
<p><strong>+ INFO: </strong><a href="http://www.myspace.com/grommamusic" target="_blank">www.myspace.com/grommamusic</a></p>
</blockquote>
<p><strong>+ DJ SETS / MIXTAPES</strong></p>
<object codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=10,0,32,18" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" type="application/x-shockwave-flash" height="225" width="100%">
<param name="allowscriptaccess" value="always" />
<param name="src" value="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fsoundcloud.com%2Fgromma&amp;secret_url=false" /><embed height="225" width="100%" src="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fsoundcloud.com%2Fgromma&amp;secret_url=false" allowscriptaccess="always" type="application/x-shockwave-flash"></embed>
</object>
<span><a href="http://soundcloud.com/gromma">Latest tracks by Gromma</a></span><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/atracoes-tandava/120-gromma-pr.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/atracoes-tandava/ver-todas/120-gromma-pr.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/atracoes-tandava/ver-todas/120-gromma-pr.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL3d3dy5lbGlqYWguY29tLmJyL3RhbmRhdmEvMjAxMC9hdHJhY29lcy10YW5kYXZhLzEyMC1ncm9tbWEtcHIuaHRtbA%3D%3D" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Quinta, 05 de Agosto de 2010 00:45				</span>
					
								<span class="rt-date-modified">
					Última atualização em Quinta, 05 de Agosto de 2010 00:53				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>									
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/atracoes-tandava/111-joel-guglielmini-pr.html">Joel Guglielmini (PR)</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<p style="text-align: justify;"><img style="margin: 0px 10px 10px 0px; float: left;" alt="joel-guglielmini" src="images/stories/atracoes/joel-guglielmini.jpg" height="338" width="225" /></p>
<p align="justify">Formado na Academia Internacional de Música Eletrônica de Curitiba (<strong>AIMEC</strong>)  em 2007 e, sempre em busca de novas tecnologias e sonoridades,  procurando formas de surpreender o público. Vem se destacando na cena  eletrônica curitibana e nacional, tendo recebido inclusive indicação no <strong>AIMEC AWARDS 2008</strong> na categoria de "<strong>MELHOR DJ</strong>".</p>
<p align="justify">Seus sets são sinestésicos e viciantes, passeando por diversos estilos  musicais (electro, techno, rock, maximal...). Tudo é música e música é  tudo!<br /><br />Sua carreira nasceu de uma paixão, e sua paixão está  alavancando sua carreira, permitindo mostrar seu talento nas pistas e  deixando seu público apaixonado pelo seu estilo inusitado de tocar.<br /><br />Já  tocou em diversos clubes de Curitiba, como Wonka, Vibe, Roxy Club, Soho  Underground, Mahatma Electro Lounge, Bartho, Cats Club, Circus Bar e  também na <strong>ADIDAS HOUSE PARTY CWB</strong>. Tendo tocado ao lado  de grandes DJs como Raul Aguilera, Gromma, Renan Mendes, Boss in Drama,  Sandra Carraro, Márcio Vermelho (SP), Tchiello K (CREW/SP), André Nego,  Cacá Azevedo, Mark Costello (UK), Killer on The Dancefloor (CREW/SP),  Rodrigo Lopes, Rodrigo Nickel, Madame Mim (MTV), Jô Mistinguett,  Barbarella, Daniel Peixoto, All Starz e Rolldabeetz.<br /><br />Fez um podcast para o site <strong>SKOL BEATS</strong> (Beatscast nº 35) e atualmente é um dos DJs colaboradores do programa "<strong>OI FM NA PISTA</strong>".<br /><br />Produz em parceria com o DJ André Nego a festa "<strong>VIVE LA MUSIQUE</strong>",  que une música e exposições de diversos gêneros de arte, como por  exemplo do Estúdio Rasputines. Além disso, é residente do núcleo de  festas Momotech.</p>
<p align="justify">Dele, apenas uma palavra: "<strong>LOADING</strong>".</p>
<blockquote>
<p><strong>+ INFO: </strong><a target="_blank" href="http://www.myspace.com/joelguglielmini">www.myspace.com/joelguglielmini</a><a target="_blank" href="http://www.vivelamusique.com.br"></a></p>
</blockquote>
<p><strong>+ DJ SETS / MIXTAPES</strong></p>
<object codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=10,0,32,18" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" type="application/x-shockwave-flash" height="225" width="100%">
<param name="allowscriptaccess" value="always" />
<param name="src" value="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fsoundcloud.com%2Fjoelguglielmini&amp;secret_url=false" /><embed height="225" width="100%" src="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fsoundcloud.com%2Fjoelguglielmini&amp;secret_url=false" allowscriptaccess="always" type="application/x-shockwave-flash"></embed>
</object>
<span><a href="http://soundcloud.com/joelguglielmini">Latest tracks by Joel Guglielmini</a></span><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/atracoes-tandava/111-joel-guglielmini-pr.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/atracoes-tandava/ver-todas/111-joel-guglielmini-pr.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/atracoes-tandava/ver-todas/111-joel-guglielmini-pr.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL3d3dy5lbGlqYWguY29tLmJyL3RhbmRhdmEvMjAxMC9hdHJhY29lcy10YW5kYXZhLzExMS1qb2VsLWd1Z2xpZWxtaW5pLXByLmh0bWw%3D" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Segunda, 02 de Agosto de 2010 20:58				</span>
					
								<span class="rt-date-modified">
					Última atualização em Segunda, 02 de Agosto de 2010 21:01				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>									
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/atracoes-tandava/125-kazz-pr.html">Kazz (PR)</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<p style="text-align: justify;"><img style="margin: 0px 10px 10px 0px; float: left;" alt="kazz" src="images/stories/atracoes/kazz.jpg" width="225" height="288" /></p>
<p align="justify">DJ KAZZ, 23 anos, nascido em Curitiba - PR. O nome KazZ surgiu da pronúncia do nome: Cezar de Araujo Santos... iniciais "CAS".</p>
<p align="justify">Com um vasto repertório, a linha Techno, Minimal-Tech cria um ambiente único e muito dançante.</p>
<p align="justify">Buscando sempre a interação com o público, influenciado por grandes nomes da musica eletrônica como Victor Ruiz, Glitter, Oliver Giacomotto, Piatto; DJ KAZZ surpreende em suas mixagens, utilizando de uma técnica diferenciada, fazendo de seu SET único.</p>
<blockquote>
<p><strong>+ INFO: </strong><a target="_blank" href="http://www.soundcloud.com/djkazz">www.soundcloud.com/djkazz</a></p>
</blockquote>
<p><strong>+ DJ SETS / MIXTAPES</strong></p>
<object codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=10,0,32,18" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" type="application/x-shockwave-flash" height="225" width="100%">
<param name="allowscriptaccess" value="always" />
<param name="src" value="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fapi.soundcloud.com%2Fusers%2F995669&amp;secret_url=false" /><embed height="225" width="100%" src="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fapi.soundcloud.com%2Fusers%2F995669&amp;secret_url=false" allowscriptaccess="always" type="application/x-shockwave-flash"></embed>
</object>
<span><a href="http://soundcloud.com/djkazz">Latest tracks by djkazz</a></span><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/atracoes-tandava/125-kazz-pr.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/atracoes-tandava/ver-todas/125-kazz-pr.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/atracoes-tandava/ver-todas/125-kazz-pr.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL3d3dy5lbGlqYWguY29tLmJyL3RhbmRhdmEvMjAxMC9hdHJhY29lcy10YW5kYXZhLzEyNS1rYXp6LXByLmh0bWw%3D" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Quinta, 09 de Setembro de 2010 12:52				</span>
					
								<span class="rt-date-modified">
					Última atualização em Quinta, 09 de Setembro de 2010 12:56				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>									
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/atracoes-tandava/135-live-constroi-pr.html">Live Constrói (PR)</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<p style="text-align: justify;"><img style="margin: 0px 10px 10px 0px; float: left;" alt="liveconstroi" src="images/stories/atracoes/liveconstroi.jpg" width="225" height="169" /></p>
<p align="justify">LIVE Constrói é um projeto de música eletrônica ao vivo, formado pelos produtores Anderson Almeida e Rodrigo Azevedo.</p>
<p align="justify">Através de software e controladores MIDI, o duo constrói todas as suas tracks dentro da sua apresentação e com o auxilio de alguns dispositivos os produtores criam vários timbres que posteriormente são gravados e processados ao vivo.</p>
<blockquote>
<p><strong>+ INFO: </strong><a target="_blank" href="http://www.myspace.com/liveconstroi">www.myspace.com/liveconstroi</a></p>
</blockquote>
<p><strong>+ TRACKS / PROMO</strong></p>
<p>
<object codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=10,0,32,18" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" type="application/x-shockwave-flash" height="225" width="100%">
<param name="allowscriptaccess" value="always" />
<param name="src" value="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fapi.soundcloud.com%2Fusers%2F1955156&amp;secret_url=false" /><embed height="225" width="100%" src="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fapi.soundcloud.com%2Fusers%2F1955156&amp;secret_url=false" allowscriptaccess="always" type="application/x-shockwave-flash"></embed>
</object>
<span><a href="http://soundcloud.com/liveconstroi">Latest tracks by liveconstroi</a></span></p><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/atracoes-tandava/135-live-constroi-pr.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/atracoes-tandava/ver-todas/135-live-constroi-pr.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/atracoes-tandava/ver-todas/135-live-constroi-pr.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL3d3dy5lbGlqYWguY29tLmJyL3RhbmRhdmEvMjAxMC9hdHJhY29lcy10YW5kYXZhLzEzNS1saXZlLWNvbnN0cm9pLXByLmh0bWw%3D" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Quarta, 20 de Outubro de 2010 11:08				</span>
					
								<span class="rt-date-modified">
					Última atualização em Quarta, 20 de Outubro de 2010 11:13				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>									
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/atracoes-tandava/minimal-criminal-live.html">Minimal Criminal LIVE (RJ)</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<p><img style="margin: 0px 10px 10px 0px; float: left;" alt="Minimal Criminal" src="images/stories/atracoes/minimalcriminal.jpg" width="225" height="169" /><strong>Minimal Criminal</strong> é o projeto iniciado em 2003 por Valério Zhyin e Bruno Echoes, mistura minimal techno/house, progressive trance and electro.</p>
<p>Produz musicas variando entre 125 e 140 BPM que apresentam atmosferas darks, profundas e hipnóticas.<strong>&nbsp;</strong></p>
<blockquote>
<p><strong>+ INFO: </strong><a target="_blank" href="http://www.minimal-criminal.com">www.minimal-criminal.com</a></p>
</blockquote>
<p><strong>+ VÍDEOS</strong></p>
<p style="text-align: center;">
<object codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=10,0,32,18" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" type="application/x-shockwave-flash" height="385" width="600">
<param name="allowFullScreen" value="true" />
<param name="allowScriptAccess" value="always" />
<param name="src" value="http://www.youtube.com/v/-gQu-5qn89o&amp;color1=0xb1b1b1&amp;color2=0xd0d0d0&amp;hl=en_US&amp;feature=player_embedded&amp;fs=1" /><embed height="385" width="600" src="http://www.youtube.com/v/-gQu-5qn89o&amp;color1=0xb1b1b1&amp;color2=0xd0d0d0&amp;hl=en_US&amp;feature=player_embedded&amp;fs=1" allowscriptaccess="always" allowfullscreen="true" type="application/x-shockwave-flash"></embed>
</object>
</p>
<p style="text-align: center;">
<object codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=10,0,32,18" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" type="application/x-shockwave-flash" height="385" width="600">
<param name="allowFullScreen" value="true" />
<param name="allowScriptAccess" value="always" />
<param name="src" value="http://www.youtube.com/v/LEmceo-ZSkI&amp;color1=0xb1b1b1&amp;color2=0xd0d0d0&amp;hl=en_US&amp;feature=player_embedded&amp;fs=1" /><embed height="385" width="600" src="http://www.youtube.com/v/LEmceo-ZSkI&amp;color1=0xb1b1b1&amp;color2=0xd0d0d0&amp;hl=en_US&amp;feature=player_embedded&amp;fs=1" allowscriptaccess="always" allowfullscreen="true" type="application/x-shockwave-flash"></embed>
</object>
</p>
<p style="text-align: center;">
<object codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=10,0,32,18" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" type="application/x-shockwave-flash" height="385" width="600">
<param name="allowFullScreen" value="true" />
<param name="allowScriptAccess" value="always" />
<param name="src" value="http://www.youtube.com/v/wXGggYSq8Ks&amp;color1=0x3a3a3a&amp;color2=0x999999&amp;hl=en_US&amp;feature=player_embedded&amp;fs=1" /><embed height="385" width="600" src="http://www.youtube.com/v/wXGggYSq8Ks&amp;color1=0x3a3a3a&amp;color2=0x999999&amp;hl=en_US&amp;feature=player_embedded&amp;fs=1" allowscriptaccess="always" allowfullscreen="true" type="application/x-shockwave-flash"></embed>
</object>
</p><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/atracoes-tandava/minimal-criminal-live.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/atracoes-tandava/ver-todas/103-minimal-criminal-live-rj.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/atracoes-tandava/ver-todas/103-minimal-criminal-live-rj.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL3d3dy5lbGlqYWguY29tLmJyL3RhbmRhdmEvMjAxMC9hdHJhY29lcy10YW5kYXZhL21pbmltYWwtY3JpbWluYWwtbGl2ZS5odG1s" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Segunda, 02 de Agosto de 2010 12:43				</span>
					
								<span class="rt-date-modified">
					Última atualização em Segunda, 02 de Agosto de 2010 12:52				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>									
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/atracoes-tandava/108-psapo-pr.html">Psapo (PR)</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<p style="text-align: justify;"><img style="margin: 0px 10px 10px 0px; float: left;" alt="psapo" src="images/stories/atracoes/psapo.jpg" height="338" width="225" /></p>
<p style="text-align: justify;">O <strong>DJ Psapo </strong>começou a se interessar por música  eletrônica em 2000, mas somente em 2003 que teve a oportunidade de  aprender e aprimorar seus conhecimentos para seu ingresso neste mundo  fascinante.</p>
<p style="text-align: justify;">Em 2004 cursou em uma das principais Academias de Música de  Curitiba(AIMEC) e depois disso teve sua carreira alavanca com o Projeto  Oxydus, sendo o seu idealizador principal e atuante até a data de hoje.  Onde era realizado toda sexta-feira em Curitiba, no Bambuz Bar, onde fez  diversas amizades e muitos contatos para poder chegar onde chegou. Hoje  em dia é realizado Secrets Party´s e eventos menores onde é tentado  resgatar a origem da psicodélia. Também é um dos Organizadores do  Festival que acontece no feriado de 15 de Novembro, Tandava Gathering.</p>
<p style="text-align: justify;">Sua principal tendência hoje é o FULL ON, principalmente a linha  High-Tech Groove. Com graves fortes e melodias transcendentais, levando o  seu público ao êxtase máximo de alegria, mas também com uma linha forte  no Electro Underground.</p>
<p style="text-align: justify;">Sua tendência atual segue os seguintes artistas: AMD, Aphid Moon,  Dickster, Burn In Noise, Headroon, E-Jeckt, Insomnia, The First Stone,  Lógica, Mental Broadcast, Ultravoice, Xpiral, Loud, Xerox &amp;  Illumination, 28, Earthling, entre outros. Caminhou com a cena do  Psy-trance em Curitiba se tornando um dos principais DJS a cena local.</p>
<p style="text-align: justify;">Com isso conquistou diversos amigos e também tornou-se um dos  moderadores do site Eletrogralha (www.egralha.com), site de informações  sobre noticias da música eletrônica, baladas, festas. Integrante do  núcleo Metropolis Project (www.metropolis.art.br/psapo).</p>
<p style="text-align: justify;">Em 2008 recebeu um prêmio, onde foi eleito por votação no site do Egralha de Melhor DJ pela escolha do Publico.</p>
<p style="text-align: justify;">Hoje com alguns anos de experiência teve oportunidade de tocar nos  principais eventos locais mostrando sua habilidade nas mixagens, esforço  e desempenho com músicas e estilos sempre inovadores. Se apresentou nos  principais eventos Opens Airs e também nas principais boates de sua  cidade (Curitiba), além de tocar com grandes nomes do Psy-Trance  Mundial, sendo alguns como: The Commercial Hippies, Xpiral, Polaris,  Akasha, Ultravoice, Tatic Mind, Heterogenesys, Zaraus, Magnética, Rica  Amaral, Dimitri Nakov, Freakulizer, Rodrigo Carreira, Sesto Sento,  Gataka, Ferby Boys, DJ Rodrigo CPU, Canibal Barbecue, Oxyd, Swe Dagon,  Beat Gate, Rex, Underverse entre outros.</p>
<blockquote>
<p><strong>+ INFO: </strong><a target="_blank" href="http://www.soundcloud.com/djpsapo">www.soundcloud.com/djpsapo</a></p>
</blockquote>
<p style="text-align: justify;"><strong>+ DJ SETs / MIXTAPES:</strong></p>
<object codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=10,0,32,18" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" type="application/x-shockwave-flash" height="225" width="100%">
<param name="allowscriptaccess" value="always" />
<param name="src" value="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fsoundcloud.com%2Fdjpsapo&amp;secret_url=false" /><embed height="225" width="100%" src="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fsoundcloud.com%2Fdjpsapo&amp;secret_url=false" allowscriptaccess="always" type="application/x-shockwave-flash"></embed>
</object>
<span><a href="http://soundcloud.com/djpsapo">Latest tracks by djpsapo</a></span><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/atracoes-tandava/108-psapo-pr.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/atracoes-tandava/ver-todas/108-psapo-pr.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/atracoes-tandava/ver-todas/108-psapo-pr.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL3d3dy5lbGlqYWguY29tLmJyL3RhbmRhdmEvMjAxMC9hdHJhY29lcy10YW5kYXZhLzEwOC1wc2Fwby1wci5odG1s" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Segunda, 02 de Agosto de 2010 20:38				</span>
					
								<span class="rt-date-modified">
					Última atualização em Segunda, 02 de Agosto de 2010 20:46				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>									
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/atracoes-tandava/reboot.html">Reboot</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<p style="text-align: justify;"><img style="margin: 0px 10px 10px 0px; float: left;" alt="reboot" src="images/stories/atracoes/reboot.png" width="225" height="225" /><strong>REBOOT</strong> [ri:'bu:t] : reiniciar o computador ou sistema.</p>
<p style="text-align: justify;">Que é isso afinal? REBOOT é um projeto de releituras musicais, cujo nome é a contração de dois termos musicais (REMIXES+BOOTLEGS=RE.BOOT).</p>
<p style="text-align: justify;">REMIX: é reconstrução de uma faixa musical com base em elementos do original adicionados de novos elementos.</p>
<p style="text-align: justify;">BOOTLEG, MASHUP: ambos os termos referem-se a recriações musicais a partir da fusão de canções e instrumentais de diferentes artistas, também mesclando, em geral, gêneros musicais diversos.</p>
<p style="text-align: justify;">A autenticidade das recriações produzidas em home-studios reside exatamente na reorganização de diferentes elementos de cada original utilizado, compondo obras artísticas criativas a partir de recortes, colagens e processamento de efeitos.</p>
<p style="text-align: justify;">O produto desta técnica também ficou conhecido como BASTARD POP, por misturar estilos como Rock, Funk, Hip-Hop e Pop, sendo então considerado como filho ilegítimo deste último.</p>
<p style="text-align: justify;">O conceito não se esgota aí... já se podem ver releituras de artes visuais, remixes e mashups de filmes e video-clips musicais, até mesmo misturas de trailers, teasers, animações, fotos, cartazes e flyers. O próprio projeto também não se resume à abordagem musical e também apresenta performances cênicas e circenses.</p>
<p style="text-align: justify;">A proposta cultural do projeto REBOOT pode ser resumida no lema/desafio: PROIBIDO ORIGINAIS !!!</p>
<h4 style="text-align: justify;">Artistas:</h4>
<p style="text-align: justify;">- DJs Feiges, RenanF e Murillo</p>
<h4>- Performances:</h4>
<p style="text-align: justify;">Yamballoon Circus Arts.</p>
<p><a href="images/stories/reboot/feiges_02.jpg" rel="rokbox[400 600](reboot)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//feiges_02_thumb.jpg" alt="" /></a> <a href="images/stories/reboot/feiges_03.jpg" rel="rokbox[460 400](reboot)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//feiges_03_thumb.jpg" alt="" /></a> <a href="images/stories/reboot/feiges_05.jpg" rel="rokbox[528 400](reboot)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//feiges_05_thumb.jpg" alt="" /></a> <a href="images/stories/reboot/murillo_01.jpg" rel="rokbox[599 400](reboot)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//murillo_01_thumb.jpg" alt="" /></a> <a href="images/stories/reboot/murillo_05.jpg" rel="rokbox[400 600](reboot)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//murillo_05_thumb.jpg" alt="" /></a> <a href="images/stories/reboot/renan_02.jpg" rel="rokbox[414 400](reboot)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//renan_02_thumb.jpg" alt="" /></a> <a href="images/stories/reboot/renan_05.jpg" rel="rokbox[445 400](reboot)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//renan_05_thumb.jpg" alt="" /></a> <a href="images/stories/reboot/yamba_01.jpg" rel="rokbox[600 400](reboot)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//yamba_01_thumb.jpg" alt="" /></a> <a href="images/stories/reboot/yamba_03.jpg" rel="rokbox[400 600](reboot)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//yamba_03_thumb.jpg" alt="" /></a> </p>
<blockquote>
<p><strong>+ INFO: </strong><a target="_blank" href="http://reboot.blog.com">reboot.blog.com</a></p>
</blockquote><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/atracoes-tandava/reboot.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/atracoes-tandava/ver-todas/132-reboot.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/atracoes-tandava/ver-todas/132-reboot.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL3d3dy5lbGlqYWguY29tLmJyL3RhbmRhdmEvMjAxMC9hdHJhY29lcy10YW5kYXZhL3JlYm9vdC5odG1s" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Sexta, 15 de Outubro de 2010 19:23				</span>
					
								<span class="rt-date-modified">
					Última atualização em Segunda, 18 de Outubro de 2010 12:31				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>									
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/atracoes-tandava/rex-africa-do-sul.html">Rex (África do Sul)</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<p style="text-align: justify;"><img style="margin: 0px 10px 10px 0px; float: left;" alt="REX" src="images/stories/atracoes/REX.jpg" width="225" height="169" />REX é de Cape Town, África do Sul e representa a NANO Records desde 2004.</p>
<p style="text-align: justify;">Sua carreira de DJ iniciou em 1996 quando Rex começou na cena underground como DAT Jockey, tocando GOA e evoluindo para o Psy-trance com o passar dos anos. Ao mesmo tempo em que tocava, viajava e organizava festas pelo mundo. Viveu na Europa por 6 anos, onde foi responsável pelas festas underground com o nome de Indigo Children - uma das maiores e mais notórias festas indoor e outdoor em Londres em 2003/04.</p>
<p style="text-align: justify;">Isso possibilitou com que ele entrasse em contato com diferentes artistas que o inspiraram, além de pessoas de todas as partes do mundo e com culturas muito diferentes. Tudo isso contribuiu na construção de um tipo único de música o que é ser dj e finalmente produzir música.</p>
<p style="text-align: justify;">No momento, Rex está no Brasil cuidando dos negócios da NANO na América do Sul, além, é claro, de atrair multidões com músicas da NANO de todas as partes do Mundo. Rex já se apresentou nos festivais mais importantes do Brasil como Universo Paralello, Trancendence e TranceFormation; além de festas pelos quatro cantos do pais.</p>
<blockquote>
<p><strong>+ INFO: </strong><a target="_blank" href="http://www.myspace.com/djrexnano">www.myspace.com/djrexnano</a></p>
</blockquote><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/atracoes-tandava/rex-africa-do-sul.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/atracoes-tandava/ver-todas/104-rex-africa-do-sul.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/atracoes-tandava/ver-todas/104-rex-africa-do-sul.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL3d3dy5lbGlqYWguY29tLmJyL3RhbmRhdmEvMjAxMC9hdHJhY29lcy10YW5kYXZhL3JleC1hZnJpY2EtZG8tc3VsLmh0bWw%3D" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Segunda, 02 de Agosto de 2010 12:55				</span>
					
								<span class="rt-date-modified">
					Última atualização em Segunda, 02 de Agosto de 2010 17:39				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>									
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/atracoes-tandava/114-rodrigo-carreira-pr.html">Rodrigo Carreira (PR)</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<p style="text-align: justify;"><img style="margin: 0px 10px 10px 0px; float: left;" alt="rodrigo-carreira" src="images/stories/atracoes/rodrigo-carreira.jpg" width="225" height="339" /></p>
<p align="justify"><strong>Rodrigo Carreira</strong> é sem dúvida um dos grandes nomes da música eletrônica no Brasil, trabalha profissionalmente como DJ desde 1993 e participou ativamente do crescimento da cena no sul do país, especialmente na cidade de Curitiba.</p>
<p align="justify">Como produtor tem lançamentos em selos como: 303 Lovers, Presslab Records, Intoxikate, Lo Kik, Tropical Beats, Soul Man Music e no seu próprio selo “Synk Records”, figurando frequentemente nas listas de mais vendidos dos principais sites de venda de música digital no mundo.</p>
<p align="justify">A qualidade de seu trabalho faz com que tenha o apoio de importantes DJ's e produtores como: Noir, Catz and Dogz, Manuel de la Mare, Alex Kenji, D-Nox &amp; Beckers, Patrick la Funk entre muitos outros.</p>
<p align="justify">Já se apresentou em países como Alemanha, Inglaterra e Japão, além de estar no line up de alguns dos melhores clubs, festas e festivais do Brasil como: Tribal Tech, XXXperience, Planeta Atlântida, Soul Vision, Universo Paralelo, Chemical Music, Fiction, Clash, Zucker, Deputa Madre, Bielle, além de suas residencias nos clubs "Vibe" e "Danghai", onde promove as noites "Disco.nnect" e "Synk label party”.</p>
<p align="justify">Rodrigo é socio fundador da marca “Synk” juntamente com Gabi Lima. Sendo um dos responsáveis pela “Synk Bookings”, “Synk Enterteinment” e “Synk Records” onde cuida também da direção artistica do selo.</p>
<blockquote>
<p><strong>+ INFO: </strong><a target="_blank" href="http://www.synk.com.br/rodrigocarreira">www.synk.com.br/rodrigocarreira</a></p>
</blockquote>
<p style="text-align: justify;"><strong>+ MÚSICAS / DJ SETs / MIXTAPES:</strong></p>
<object codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=10,0,32,18" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" type="application/x-shockwave-flash" height="225" width="100%">
<param name="allowscriptaccess" value="always" />
<param name="src" value="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fsoundcloud.com%2Frodrigo-carreira&amp;secret_url=false" /><embed height="225" width="100%" src="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fsoundcloud.com%2Frodrigo-carreira&amp;secret_url=false" allowscriptaccess="always" type="application/x-shockwave-flash"></embed>
</object>
<span><a href="http://soundcloud.com/rodrigo-carreira">Latest tracks by Rodrigo Carreira</a></span><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/atracoes-tandava/114-rodrigo-carreira-pr.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/atracoes-tandava/ver-todas/114-rodrigo-carreira-pr.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/atracoes-tandava/ver-todas/114-rodrigo-carreira-pr.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL3d3dy5lbGlqYWguY29tLmJyL3RhbmRhdmEvMjAxMC9hdHJhY29lcy10YW5kYXZhLzExNC1yb2RyaWdvLWNhcnJlaXJhLXByLmh0bWw%3D" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Terça, 03 de Agosto de 2010 22:00				</span>
					
								<span class="rt-date-modified">
					Última atualização em Terça, 03 de Agosto de 2010 22:09				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>									
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/atracoes-tandava/122-rodrigo-nickel-pr.html">Rodrigo Nickel (PR)</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<p style="text-align: justify;"><img style="margin: 0px 10px 10px 0px; float: left;" alt="rodrigo-nickel" src="images/stories/atracoes/rodrigo-nickel.jpg" height="300" width="225" /></p>
<p align="justify">Citado pela revista inglesa Mixmag logo na sua primeira edição brasileira como um dos artistas nacionais a ficar de olho, atualmente figurando na 59ª posição do TOP 100 da House Mag e também campeão do primeiro concurso de DJs da marca alemã Jagermeister no Brasil, o curitibano Rodrigo Nickel começou sua carreira de DJ em Portugal, para onde se mudou em 1992 e ficou durante quase 15 anos e onde também organizava e residia no mítico after hours Break-Fast no clube Europa em Lisboa, por onde passaram artistas como Chris Lake, D-Nox &amp; Beckers ou Kasey Taylor e o qual também foi nomeado como o melhor after hours daquele país em 2005 para o Dance Club Awards, da publicação homônima, a Dance Club.</p>
<p align="justify">De volta ao Brasil desde janeiro de 2006, é presença contante nos line ups de clubes ao redor do país como o D-edge, Vegas Club, SPKZ em São Paulo, Deputamadre, Label Club e Chalezinho em Belo Horizonte, Vibe, Eon, Café de La Musique e Danghai em Curitiba, Bielle e Felts em Cascavel, Circuit Club em Florianópolis, Glam e Deseo em Camboriú, Maze em Joinville, Quartier Latin em Porto Alegre entre muitos outros, ao lado de Martin Eyerer, King Roc, Presslaboys, Phatjak, Khainz, Alex Kenji, Hauswerks, Niko Schwind, Xavi Beat , David Amo &amp; Julio Navas, Tim Healey, Deep Mariano ou Gui Boratto para citar alguns, ou ainda em grandes festivais como o Universo Paralello, Tribe ou Tribaltech.<br /><br />Desde 2007 divide seu tempo entre a organização da Decibel, projeto que atualmente tem residência fixa no club Vibe e que em 2 anos já conta com mais de 50 edições em cidades como Curitiba, Cascavel (Bielle), Maringá (17 Lounge Disco), Florianópolis (Circuit) ou Belo Horizonte (Deputamadre) e já apresentou artistas como Kanio, Gaz James, Sam Fraser, Kore, Tommy Jacobbs ou Gustavo Bravetti e onde também é um dos residentes ao lado dos DJs Hugo Miyamura &amp; Daniel Costa.</p>
<p align="justify">Atualmente também organiza outra festa, a Lick My Sunday em parceria com a DJ Aninha , além de estar começando com sua própria gravadora, a Origami onde irá lançar músicas de nomes como King Roc, Rick Nicholls &amp; Asher Jones, Chris Llopis, entre muitos outros e acaba de voltar de uma tour pelo Chile, país<br />de grandes nomes como Luciano e Ricardo Vilallobos, onde se apresentou nos clubs Piso 33 em Santiago e Puerto Ibiza em Rancágua onde conquistou uma residência trimestral.</p>
<blockquote>
<p><strong>+ INFO: </strong><a target="_blank" href="http://www.myspace.com/rodrigonickel">www.myspace.com/rodrigonickel</a></p>
</blockquote>
<p><strong>+ DJ SETS / TRACKS</strong></p>
<object codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=10,0,32,18" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" type="application/x-shockwave-flash" height="225" width="100%">
<param name="allowscriptaccess" value="always" />
<param name="src" value="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fsoundcloud.com%2Frodrigo-nickel&amp;secret_url=false" /><embed height="225" width="100%" src="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fsoundcloud.com%2Frodrigo-nickel&amp;secret_url=false" allowscriptaccess="always" type="application/x-shockwave-flash"></embed>
</object>
<span><a href="http://soundcloud.com/rodrigo-nickel">Latest tracks by Rodrigo Nickel</a></span><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/atracoes-tandava/122-rodrigo-nickel-pr.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/atracoes-tandava/ver-todas/122-rodrigo-nickel-pr.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/atracoes-tandava/ver-todas/122-rodrigo-nickel-pr.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL3d3dy5lbGlqYWguY29tLmJyL3RhbmRhdmEvMjAxMC9hdHJhY29lcy10YW5kYXZhLzEyMi1yb2RyaWdvLW5pY2tlbC1wci5odG1s" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Terça, 24 de Agosto de 2010 09:06				</span>
					
								<span class="rt-date-modified">
					Última atualização em Terça, 24 de Agosto de 2010 09:14				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>									
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/atracoes-tandava/126-roger-thiago-sc.html">Roger Thiago (SC)</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<p style="text-align: justify;"><img style="margin: 0px 10px 10px 0px; float: left;" alt="roger-thiago" src="images/stories/atracoes/roger-thiago.jpg" width="225" height="232" /></p>
<p align="justify">Roger Thiago (Simova / Mood) está envolvido com a e-music desde 2002.</p>
<p align="justify">Em 2004 fez sua primeira festa com o núcleo SIMOVA CREW que já tem 6 anos e 16 edições, ajudando a revelar vários artistas regionais por todo o estado, sendo considerada uma das festas open air mais respeitadas da cena underground do estado.</p>
<p align="justify">Seus sets vão do Minimal ao Tech House com grooves bem marcantes.</p>
<p><strong>+ DJ SETS / MIXTAPES</strong></p>
<p>
<object codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=10,0,32,18" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" type="application/x-shockwave-flash" height="81" width="100%">
<param name="allowscriptaccess" value="always" />
<param name="src" value="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fapi.soundcloud.com%2Ftracks%2F5842279%3Fsecret_token%3Ds-0z8Dd&amp;secret_url=false" /><embed type="application/x-shockwave-flash" height="81" width="100%" src="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fapi.soundcloud.com%2Ftracks%2F5842279%3Fsecret_token%3Ds-0z8Dd&amp;secret_url=false" allowscriptaccess="always"></embed>
</object>
<span><a href="http://soundcloud.com/rogerthiago/roger-thiago-music-therapy">Roger Thiago - Music Therapy</a> by <a href="http://soundcloud.com/rogerthiago">ROGERTHIAGO</a></span></p><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/atracoes-tandava/126-roger-thiago-sc.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/atracoes-tandava/ver-todas/126-roger-thiago-sc.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/atracoes-tandava/ver-todas/126-roger-thiago-sc.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL3d3dy5lbGlqYWguY29tLmJyL3RhbmRhdmEvMjAxMC9hdHJhY29lcy10YW5kYXZhLzEyNi1yb2dlci10aGlhZ28tc2MuaHRtbA%3D%3D" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Quinta, 09 de Setembro de 2010 12:56				</span>
					
								<span class="rt-date-modified">
					Última atualização em Quinta, 14 de Outubro de 2010 12:28				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>									
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/atracoes-tandava/sallun-pedra-branca.html">Sallun (Pedra Branca - SP)</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<p style="text-align: justify;"><img style="margin: 0px 10px 10px 0px; float: left;" alt="sallun" src="images/stories/atracoes/sallun.jpg" width="225" height="320" /></p>
<p align="justify"><strong>Luciano Sallun</strong>, 30, vem explorando em sua música o a união entre o acústico e eletrônico no world music, e na musica experimental contemporânea.</p>
<p align="justify">O músico, musicoterapeuta, compositor, produtor e dj pesquisa musicas e instrumentos de diversas partes do mundo, como como sitar, alaúde e samissen. Esses elementos estão presentes na cultura indiana, árabe e japonesa, respectivamente.</p>
<p align="justify">Sallun cursou faculdade de musicoterapia, na Faculdade Paulista de Artes, bem como estuda instrumentos como os ditos a cima, estando por um período na Índia estudando sitar com o mestre Sanjeeb Sircar.</p>
<p align="justify">Desenvolve assim com seus projetos e principalmente no seu projeto solo uma pesquisa étnicomusicologia com musicas do mundo, buscando a fusão entre as diversas culturas juntas. Onde está gravando seu primeiro álbum solo, com a união do jazz e world music.</p>
<p align="justify">Além disso, o músico paulista agrega às suas performances instrumentos musicais de criação própria, como meio de buscar timbres inexplorados. Entre as invenções está o armesk, feito com uma lata de biscoitos e com um cabo de vassoura, a viola oriental, com duas cabaças ligadas por um bambu e cordas de viola e as flautas feitas de canos plásticos e bexigas de borracha. Desenvolve construção experimental de instrumentos de junto ao musico e luthier alternativo Fernando Sardo 2003 fazendo uma instalação sonora chamada de Dessintetizador. Juntos eles fazem parte do grupo GEM (Grupo Experimental de Música), que realiza apresentações musicais somente com os instrumentos criados.</p>
<p align="justify">Na parte de produção eletrônica, ele produz os beats e efeitos das faixas do grupo <strong>Pedra Branca</strong>, no qual também realiza a concepção artística. O Pedra Branca já lançou dois álbuns e se apresentou em diversos locais pelo Brasil e Portugal. Trabalho com publico abrangente e reconhecido mundialmente.</p>
<p align="justify">No seu dj set ele mistura a música eletrônica (particularmente trip hop e downbeat) com world music (musicas tradicionais de diferentes lugares e culturas, o que inclui o uso de instrumentos locais) e elabora a fusão que transpassa o elemento unicamente musical e atinge a mistura de culturas em si. “Como mixar um tambor africano com uma voz da asiática e com um berimbal”, explica.</p>
<p align="justify">O dj diz que no fundo há uma ligação entre as variantes musicais. Seu estilo se encaixa entre chill out, ambient stage, ou world music stage.</p>
<p align="justify">O outro projeto do qual Sallun participa é o Liquidus Ambiento, que começou em 2003. E vem desenvolvendo seu primeiro álbum. O instrumentista também já desenvolveu atividades com inúmeros artistas. Trabalhou com a percussionista Simone Soul e o produtor e baixista Alfredo Bello (Projeto Cru), o produtor Rodrigo Soneca (Trotter) onde tem juntos o projeto chamado Pindorama. Desenvolveu trilhas sonoras para espetáculos de dança, como o Monomitos, e para performances multimídia, como a Útero. Entre outras, dança indiana e dança do ventre em algumas apresentações.</p>
<p align="justify">Atualmente está fazendo a direção musical e produção musical do espetáculo Crenças e crendices, quem disse? Da Cia de Dança de Diadema, dirigido por Ana Bottosso.</p>
<blockquote>
<p><strong>+ INFO: </strong><a target="_blank" href="http://www.myspace.com/lucianosallun">www.myspace.com/lucianosallun</a></p>
</blockquote>
<a href="http://soundcloud.com/rodrigo-carreira"></a><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/atracoes-tandava/sallun-pedra-branca.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/atracoes-tandava/ver-todas/117-sallun-pedra-branca-sp.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/atracoes-tandava/ver-todas/117-sallun-pedra-branca-sp.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL3d3dy5lbGlqYWguY29tLmJyL3RhbmRhdmEvMjAxMC9hdHJhY29lcy10YW5kYXZhL3NhbGx1bi1wZWRyYS1icmFuY2EuaHRtbA%3D%3D" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Terça, 03 de Agosto de 2010 22:23				</span>
					
								<span class="rt-date-modified">
					Última atualização em Terça, 03 de Agosto de 2010 22:29				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>									
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/atracoes-tandava/94-schasko.html">Schasko (PR)</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<p><img style="margin: 0px 10px 10px 0px; float: left;" alt="schasko-2" src="images/stories/atracoes/schasko-2.jpg" width="225" height="150" />A cada novo dia a música Downbeat conquista novos e ardilosos fãs em todo mundo. E não poderia ser diferente com este estilo democrático que passeia livremente nos mais diferentes espaços com as mais variadas personalidades, e acolhe em sua sonoridade diversas influências.</p>
<p>Seguindo o fluxo está o Dj Schasko, velho conhecido da cena eletrônica brasileira, e presença confirmada nos principais festivais do país. Técnica, versatilidade, bom gosto e extensa bagagem musical, são algumas das principais características que um Dj precisa; e Schasko com certeza tem de sobra.</p>
<p>Com uma longa estrada dentro das vertentes do Downbeat, como trip-hop, dub, lounge, nujazz, funk, broken beats, R &amp; B, hip-hop, mash up´s, sempre atualizado em novos sons e tendências, procura apresentar algo novo e com personalidade. Dj Schasko é um dos principais nomes da cena nacional. Já tocou em dois dos maiores festivais do mundo (Skoll Beats – SP 2005 e Boom Festival - Portugal 2008).</p>
<p>Em 2008 foi convidado pelo Produtor e Dj Soneca–SP (Trotter), para representar o selo musical: Royal Soul Records, em seguida junto com o Dj Murillo, deram inicio ao Projeto Funk You, que com dois anos de existência já é referencia de&nbsp; Nufunk e Breaks em Curitiba.</p>
<p>Em 2009 foi convidado para mixar a compilação “Songs of Taj” do famoso Taj Bar, uma compilação 100% nacional. Ainda em 2009 foi convidado para ser residente do projeto semanal Cambalacho, com pouco mais de dois anos é um dos principais núcleos da cena Curitibana.</p>
<blockquote>
<p><strong>+ INFO: </strong><a target="_blank" href="http://www.myspace.com/funkyoucwb">www.myspace.com/funkyoucwb</a></p>
</blockquote>
<p><strong>+ DJ SET / MIXTAPE:</strong></p>
<object codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=10,0,32,18" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" type="application/x-shockwave-flash" height="81" width="100%">
<param name="allowscriptaccess" value="always" />
<param name="src" value="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fsoundcloud.com%2Fdj-schasko%2Fdjschasko-smokingtime-sessions-trusty-radio&amp;secret_url=false" /><embed height="81" width="100%" src="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fsoundcloud.com%2Fdj-schasko%2Fdjschasko-smokingtime-sessions-trusty-radio&amp;secret_url=false" allowscriptaccess="always" type="application/x-shockwave-flash"></embed>
</object>
<span><a href="http://soundcloud.com/dj-schasko/djschasko-smokingtime-sessions-trusty-radio">DjSchasko@ SmokingTime sessions - Trusty radio</a> by <a href="http://soundcloud.com/dj-schasko">Dj Schasko</a></span><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/atracoes-tandava/94-schasko.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/atracoes-tandava/ver-todas/94-schasko.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/atracoes-tandava/ver-todas/94-schasko.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL3d3dy5lbGlqYWguY29tLmJyL3RhbmRhdmEvMjAxMC9hdHJhY29lcy10YW5kYXZhLzk0LXNjaGFza28uaHRtbA%3D%3D" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Quarta, 16 de Junho de 2010 22:43				</span>
					
								<span class="rt-date-modified">
					Última atualização em Sexta, 10 de Setembro de 2010 12:31				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>									
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/atracoes-tandava/100-slow-control-live-sc.html">Slow Control LIVE (SC)</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<p><img style="margin: 0px 10px 10px 0px; float: left;" alt="Slow Control" src="images/stories/atracoes/slowcontrol.jpg" width="225" height="168" /><strong></strong>Projeto de Taicir S. Elias aka <strong>Tarta</strong>, iniciando como DJ nos meados de 2004, sempre curtiu a linha de Full On Groove tendendo para o Progressive Trance. Após fazer faculdade de Publicidade e Propaganda, resolveu entrar de vez na música eletrônica.<br /><br />Então resolveu fazer curso de produção musical entregando-se de vez ao Progressive, Tarta traz em suas produções bass grooves pesados e ao mesmo tempo sutis seguindo a linha do verdadeiro Progressive Trance e utilizando tendências e timbres de Electro, Minimal e Tech-House.<br /><br /><strong>Slow Control</strong> traz em suas músicas melodias, ritmos marcantes e bass lines acentuados, criando assim um estilo único e muita psicodelia musical.</p>
<blockquote>
<p><strong>+ INFO: </strong><a target="_blank" href="http://www.myspace.com/slowcontrol">www.myspace.com/slowcontrol</a></p>
</blockquote>
<p><strong>+ MÚSICAS:</strong></p>
<object codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=10,0,32,18" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" type="application/x-shockwave-flash" height="225" width="100%">
<param name="allowscriptaccess" value="always" />
<param name="src" value="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fsoundcloud.com%2Fslowcontrol&amp;secret_url=false" /><embed height="225" width="100%" src="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fsoundcloud.com%2Fslowcontrol&amp;secret_url=false" allowscriptaccess="always" type="application/x-shockwave-flash"></embed>
</object>
<span><a href="http://soundcloud.com/slowcontrol">Latest tracks by • Slow Control •</a></span><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/atracoes-tandava/100-slow-control-live-sc.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/atracoes-tandava/ver-todas/100-slow-control-live-sc.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/atracoes-tandava/ver-todas/100-slow-control-live-sc.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL3d3dy5lbGlqYWguY29tLmJyL3RhbmRhdmEvMjAxMC9hdHJhY29lcy10YW5kYXZhLzEwMC1zbG93LWNvbnRyb2wtbGl2ZS1zYy5odG1s" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Segunda, 02 de Agosto de 2010 12:27				</span>
					
								<span class="rt-date-modified">
					Última atualização em Segunda, 02 de Agosto de 2010 12:31				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>									
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/atracoes-tandava/synk-recs-day-party.html">Synk Recs Day Party</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<div class="rt-article-content">
<p style="text-align: justify;"><img style="margin: 0px 10px 25px 0px; float: left;" alt="synk-recs" src="images/stories/atracoes/synk-recs.png" width="225" height="150" /><strong></strong></p>
<p style="text-align: justify;">No último dia do festival – feriado de Segunda-Feira, 15 de Novembro – o <strong>Tandava Gathering</strong> recebe uma festa especial da gravadora <strong>SYNK Records </strong>para o encerramento da edição 2010.</p>
<p style="text-align: justify;">A festa inicia pela manhã e se prolonga pelo último dia com destaque para a apresentação de <a href="index.php?option=com_content&amp;view=article&amp;id=115:truati-live-sp&amp;catid=45&amp;Itemid=172"><strong>Truati LIVE</strong></a>, <a href="index.php?option=com_content&amp;view=article&amp;id=114:rodrigo-carreira-pr&amp;catid=45&amp;Itemid=172">Rodrigo Carreira</a>, <a target="_self" href="index.php?option=com_content&amp;view=article&amp;id=123:gabriel-boni-pr&amp;catid=45&amp;Itemid=172">Gabriel Boni</a>, <a href="index.php?option=com_content&amp;view=article&amp;id=113:gabi-lima-pr&amp;catid=45&amp;Itemid=172">Gabi Lima</a> e <a href="index.php?option=com_content&amp;view=article&amp;id=122:rodrigo-nickel-pr&amp;catid=45&amp;Itemid=172">Rodrigo Nickel</a>.</p>
<p style="text-align: justify;">Um encerramento com qualidade e sonoridade sem igual. Aguardem pelo Line Up completo!</p>
<p style="text-align: justify;"><strong>+ SOBRE A SYNK</strong></p>
<p>Pensando e distribuindo música digital desde 2007, a Synk Records  oferece inovação, lançando composições musicais que estimulam o público à  novas percepções criativas.</p>
<p>Transcendendo a representação de gêneros e estilos pré-definidos, a  Synk Records posiciona-se no mercado através da seleção de artistas  antenados, que misturam o uso de tecnologia de ponta, musicalidade e  ritmo para as pistas de dança.</p>
<p style="text-align: justify;">A marca Synk foi idealizada e fundada pelos artistas Rodrigo  Carreira, Leandro Pacceli e Gabi em parceria com o selo Presslab Records  (Itália), e seus integrantes: Omar Neri e Luigi Gori (Presslaboys).</p>
<blockquote>
<p><strong>+ INFO: </strong><a target="_blank" href="http://www.synk.com.br/">www.synk.com.br</a></p>
</blockquote>
</div><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/atracoes-tandava/synk-recs-day-party.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/atracoes-tandava/ver-todas/116-synk-recs-day-party.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/atracoes-tandava/ver-todas/116-synk-recs-day-party.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL3d3dy5lbGlqYWguY29tLmJyL3RhbmRhdmEvMjAxMC9hdHJhY29lcy10YW5kYXZhL3N5bmstcmVjcy1kYXktcGFydHkuaHRtbA%3D%3D" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Terça, 03 de Agosto de 2010 22:10				</span>
					
								<span class="rt-date-modified">
					Última atualização em Quinta, 09 de Setembro de 2010 12:40				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>									
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/atracoes-tandava/127-tiptronic-live-pr.html">Tip_Tronic LIVE (PR)</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<p style="text-align: justify;"><img style="margin: 0px 10px 10px 0px; float: left;" alt="tip_tronic" src="images/stories/atracoes/tip_tronic.jpg" width="225" height="238" /></p>
<p style="text-align: justify;">Alejandro Bargueño Machado De Curitiba-PR, em 2004 começou a fazer suas primeiras mixagens, após um tempo pegou gosto e jeito para ser DJ e quando percebeu já estava tocando nos clubes de sua cidade. Assim que dominou os cdj’s e mixers ele parte para mais uma etapa: fazer remix de suas bandas preferidas como Queens of the Stone Age, Pantera, Ozzy entre outras. A partir daí foi criado o projeto Tip_Tronic em 2007.</p>
<p style="text-align: justify;">Atualmente com seu live pronto, Tip_Tronic, classifica-se em um estilo único (MIX OF ALL). Uma mistura de prog, electro, minimal, Techno e fixo no seus 130 BPM e ainda com suas inspirações em bandas e filmes preferidos, mais uma de suas excentricidades. Com o SS(sound system) sobre seu controle, ele consegue fazer com quem ninguém deixe a pista.</p>
<p style="text-align: justify;">Seu estilo descontraído no palco consegue contagiar a galera e deixar todos num ótimo astral e na mesma freqüência.</p>
<p style="text-align: justify;">Já tocou ao lado de Ganjasonic, Yage, Dj Mack, Marc’s, Gabriel Mazza, Beat Gate, Heterogenesis, Pragmatix, Swe Dagon, Megalopsy, Frantic Noise, Dimitri Nacov, Pedrão, Kamadon, Alkaline e Made in Space. Mas não parou por ai, em 2008/2009, Alejandro saiu em uma volta por San Diego C.A onde permaneceu por 3 meses e tocou vários set’s de rock and roll, e ai percebeu que nunca poderia esquecer onde tudo começou!</p>
<p style="text-align: justify;">Depois dessa trip, pegou novamente seu passaporte e decolou para Londres UK, onde ouviu de tudo e alimentou ainda mais o seu estilo. Passados 7 meses fora, voltou para sua cidade natal. Com novas influências e muito gás, continuou a produzir novas músicas, e logo chegou o momento onde participou e venceu o concurso Multicultural Tribaltech na categoria Psytrance e subgêneros, tendo o prazer e a oportunidade de dividir o palco com os artistas:Feio, Gataka, Vibe Tribe, X-Noize, Sesto Sento, Liquid Soul, FelGuk, Tim Healey, David Amo &amp; Julio Navas, Gustavo Bravetti, Chris Lake, Format B, Thomas Schumasher, D.Ramirez e Dub Taylor aka Tigerskin .</p>
<p style="text-align: justify;">Para quem espera que ele finalize no full on, Prog ou Eletro, se prepare para mais volta na pista fora do circuito!!! Já que a desaceleração é praticamente impossível de se evitar, então que ela venha carregada de qualidade, peso diferencial e muita viagem, que por sinal será longa e sem passaporte!</p>
<blockquote>
<p><strong>+ INFO: </strong><a target="_blank" href="http://www.myspace.com/tiptroniclive">www.myspace.com/tiptroniclive</a></p>
</blockquote>
<p><strong>+ TRACKS / PROMOS</strong></p>
<object codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=10,0,32,18" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" type="application/x-shockwave-flash" height="225" width="100%">
<param name="allowscriptaccess" value="always" />
<param name="src" value="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fapi.soundcloud.com%2Fusers%2F239345&amp;secret_url=false" /><embed height="225" width="100%" src="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fapi.soundcloud.com%2Fusers%2F239345&amp;secret_url=false" allowscriptaccess="always" type="application/x-shockwave-flash"></embed>
</object>
<span><a href="http://soundcloud.com/tip_tronic">Latest tracks by Tip_Tronic</a></span><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/atracoes-tandava/127-tiptronic-live-pr.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/atracoes-tandava/ver-todas/127-tiptronic-live-pr.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/atracoes-tandava/ver-todas/127-tiptronic-live-pr.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL3d3dy5lbGlqYWguY29tLmJyL3RhbmRhdmEvMjAxMC9hdHJhY29lcy10YW5kYXZhLzEyNy10aXB0cm9uaWMtbGl2ZS1wci5odG1s" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Sexta, 10 de Setembro de 2010 12:23				</span>
					
								<span class="rt-date-modified">
					Última atualização em Sexta, 10 de Setembro de 2010 12:30				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>									
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/atracoes-tandava/truati-live-sp.html">Truati LIVE (SP)</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<p style="text-align: justify;"><img style="margin: 0px 10px 10px 0px; float: left;" alt="truati-live" src="images/stories/atracoes/truati-live.jpg" width="225" height="290" /></p>
<p style="text-align: justify;"><strong>Estevão  Melchior</strong> é brasileiro, nascido em Sorocaba, interior de São Paulo. Em  meados de 2002 iniciou suas produções musicais com seu projeto <strong>Truati</strong>.  De seu home-studio saíram músicas influenciadas pelo som do trance e do  house progressivo.</p>
<p style="text-align: justify;">Hoje  Truati trilha um caminho próprio. Usa com sabedoria suas influências na  criação de um tech-house com baixos marcantes e elementos de house. Já  coleciona belas posições nos charts do Beatport e diversos outros sites  de venda de musica digital.</p>
<p style="text-align: justify;">Foi  recentemente o vencedor da categoria Underground do concurso de novos  talentos da TRIBALTECH, um dos mais importantes festivais de musica  eletrônica do Brasil, onde se apresentou ao lado de grandes nomes como:  Format B, Tim Healey, Chris Lake, Marc Romboy, David Amo &amp; Julio  Navas, Beckers, D.Ramirez, Southmen, Gui Boratto e Maetrik.</p>
<p style="text-align: justify;"><strong>TRUATI</strong> integra o seleto time de produtores da Synk Records e Lo Kik Recods,  que já lançou hits assinados por Gui Boratto, Anderson Noise, Paolo  Mojo, Wehbba, Gabe, Marcelo V.O.R, D-Nox&amp;Beckers, Gleen Morrinson,  Mora&amp;Naccarati, Rafael Noronha, entre outros. Lançou faixas por  importantes selos nacionais e internacionais e ficou conhecido com o  remix da musica “Terraza” dos DJs e produtores Mora&amp;Naccarati.</p>
<p style="text-align: justify;">Após  alguns anos em estúdio aprimorando sua técnica e experimentando novas  sonoridades, Truati vem com uma apresentação ao vivo repleta de  novidades e improvisos, trazendo em seu repertório lançamentos  quentíssimos, além de músicas próprias que fazem parte do case de muitos  DJs pelo mundo.</p>
<blockquote>
<p><strong>+ INFO: </strong><a target="_blank" href="http://www.myspace.com/truatimusic">www.myspace.com/truatimusic</a></p>
</blockquote>
<p style="text-align: justify;"><strong>+ MÚSICAS / DJ SETs / MIXTAPES:</strong></p>
<object codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=10,0,32,18" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" type="application/x-shockwave-flash" height="225" width="100%">
<param name="allowscriptaccess" value="always" />
<param name="src" value="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fsoundcloud.com%2Ftruati&amp;secret_url=false" /><embed height="225" width="100%" src="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fsoundcloud.com%2Ftruati&amp;secret_url=false" allowscriptaccess="always" type="application/x-shockwave-flash"></embed>
</object>
<span><a href="http://soundcloud.com/truati">Latest tracks by Truati</a></span><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/atracoes-tandava/truati-live-sp.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/atracoes-tandava/ver-todas/115-truati-live-sp.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/atracoes-tandava/ver-todas/115-truati-live-sp.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL3d3dy5lbGlqYWguY29tLmJyL3RhbmRhdmEvMjAxMC9hdHJhY29lcy10YW5kYXZhL3RydWF0aS1saXZlLXNwLmh0bWw%3D" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Terça, 03 de Agosto de 2010 22:04				</span>
					
								<span class="rt-date-modified">
					Última atualização em Terça, 03 de Agosto de 2010 22:09				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>									
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/atracoes-tandava/140-urbit-live-pr.html">Urbit LIVE (PR)</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<p style="text-align: justify;"><img style="margin: 0px 10px 10px 0px; float: left;" alt="urbit" src="images/stories/atracoes/urbit.jpg" width="225" height="169" /></p>
<p style="text-align: justify;">O nome Urbit é uma referência à urbe ou cidade, e Bit traz uma referência à tecnologia digital, aos games e a música eletrônica. Pode-se dizer que Urbit é um som eletrônico produzido por jovens que habitam um grande centro urbano. Para àqueles que gostam de viajar um pouco mais, há também outra interpretação: U = you R = are Bit = numbers ou urbit = You Are Bit, uma reflexão (crítica) ao nosso sistema sócio-econômico que transforma TUDO em números e cifras!</p>
<p style="text-align: justify;">O projeto Urbit é formado pelos músicos Victor Ayres, Orlando Muzg e Ronaldo Miers. Após alguns anos de experiência na produção em softwares os três resolveram se unir para montar um projeto voltado à performance live.</p>
<p style="text-align: justify;">Suas apresentações são baseadas no conceito de livre improvisação e as músicas são criadas em tempo real. Essas características permitem que o som do projeto se adapte a qualquer ambiente, e consequentemente as apresentações do Urbit são únicas e nunca se repetem.</p>
<p style="text-align: justify;">Para isso utilizam hardwares, sintetizadores analógicos e digitais. O quebra-cabeça vai sendo montado em frente ao público e todos podem acompanhar passo a passo a criação musical. Todo este processo foi denominado Live Creation (ou criação ao vivo) para se diferenciar dos modelos de Live utilizados atualmente.</p>
<p style="text-align: justify;">O projeto tem alguns materiais que foram gravados ao vivo e podem ser baixados no myspace: <a target="_blank" href="http://www.myspace.com/urbit">www.myspace.com/urbit</a> ou no blog: <a target="_blank" href="http://www.ctrlcultura.blogspot.com">www.ctrlcultura.blogspot.com</a>. Há também alguns vídeos no youtube. Confira o trabalho deste novo projeto de Live Electronic e supreenda-se.<strong> <br /></strong></p>
<blockquote><strong>+ INFO:</strong> <a target="_blank" href="http://www.myspace.com/urbit">www.myspace.com/urbit</a><a target="_blank" href="http://www.soundcloud.com/djgabrielbibi"><span style="color: #2a5db0;"></span></a></blockquote>
<p><strong>+ DJ SET / PROMO</strong></p>
<p>
<object codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=10,0,32,18" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" type="application/x-shockwave-flash" height="225" width="100%">
<param name="allowscriptaccess" value="always" />
<param name="src" value="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fapi.soundcloud.com%2Fusers%2F433740&amp;secret_url=false" /><embed height="225" width="100%" src="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fapi.soundcloud.com%2Fusers%2F433740&amp;secret_url=false" allowscriptaccess="always" type="application/x-shockwave-flash"></embed>
</object>
<span><a href="http://soundcloud.com/urbit">Latest tracks by urbit</a></span></p><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/atracoes-tandava/140-urbit-live-pr.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/atracoes-tandava/ver-todas/140-urbit-live-pr.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/atracoes-tandava/ver-todas/140-urbit-live-pr.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL3d3dy5lbGlqYWguY29tLmJyL3RhbmRhdmEvMjAxMC9hdHJhY29lcy10YW5kYXZhLzE0MC11cmJpdC1saXZlLXByLmh0bWw%3D" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Sexta, 22 de Outubro de 2010 16:17				</span>
					
								<span class="rt-date-modified">
					Última atualização em Sexta, 22 de Outubro de 2010 16:22				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>									
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/atracoes-tandava/130-urucubaca-live-pr.html">Urucubaca LIVE (PR)</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<p><img style="margin: 0px 10px 10px 0px; float: left;" alt="Urucubaca_Live" src="images/stories/atracoes/Urucubaca_Live.jpg" width="225" height="266" /><strong>Urucubaca</strong> é o novo projeto de música eletrônica do músico e produtor Mar Schiavon.</p>
<p style="text-align: justify;">Surgiu em 2008, ano da Tomenta Elétrica, com uma proposta diferente dos projetos anteriores. Essa nova fase é mais eclética, flertando entre vertentes do Dark como EBM, Progressive e Dark Psychedelic entre outras, sem apego a BPM ou outras limitações.</p>
<p style="text-align: justify;">Os timbres geralmente são soturnos e introspectivos, com alguns kicks distorcidos e basslines "gordos"&nbsp; com muito groove. Já para as melodias há preferência por escalas exóticas, por vezes dissonantes, para obter uma sonoridade peculiar, todas essas texturas amparadas por ambiencias que levam os ouvintes a diferentes estágios de percepção.</p>
<p style="text-align: justify;">Tendo amizade com produtores do país inteiro, o projeto faz parte da gravadora PsySeeD Records, que vem lançando excelentes discos de psytrance brasileiro, fruto dessa amizade e parceria..<strong>&nbsp;</strong></p>
<blockquote>
<p><strong>+ INFO:</strong> <a target="_blank" href="http://www.myspace.com/urucubacadark">www.myspace.com/urucubacadark</a></p>
</blockquote>
<p><strong>+ TRACKS / PROMO</strong></p>
<p>
<object codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=10,0,32,18" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" type="application/x-shockwave-flash" height="225" width="100%">
<param name="allowscriptaccess" value="always" />
<param name="src" value="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fapi.soundcloud.com%2Fusers%2F809383&amp;secret_url=false" /><embed height="225" width="100%" src="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fapi.soundcloud.com%2Fusers%2F809383&amp;secret_url=false" allowscriptaccess="always" type="application/x-shockwave-flash"></embed>
</object>
<span><a href="http://soundcloud.com/urucubaca">Latest tracks by Urucubaca</a></span></p>
<p><strong>+ VÍDEOS</strong></p>
<p style="text-align: center;">
<object codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=10,0,32,18" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" type="application/x-shockwave-flash" height="385" width="600">
<param name="allowFullScreen" value="true" />
<param name="allowScriptAccess" value="always" />
<param name="src" value="http://www.youtube.com/v/VD116N7WFQg&amp;hl=en_US&amp;feature=player_embedded&amp;version=3" /><embed height="385" width="600" src="http://www.youtube.com/v/VD116N7WFQg&amp;hl=en_US&amp;feature=player_embedded&amp;version=3" allowscriptaccess="always" allowfullscreen="true" type="application/x-shockwave-flash"></embed>
</object>
</p>
<p style="text-align: center;">
<object codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=10,0,32,18" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" type="application/x-shockwave-flash" height="385" width="600">
<param name="allowFullScreen" value="true" />
<param name="allowScriptAccess" value="always" />
<param name="src" value="http://www.youtube.com/v/oV9zHsYZuVU&amp;hl=en_US&amp;feature=player_embedded&amp;version=3" /><embed height="385" width="600" src="http://www.youtube.com/v/oV9zHsYZuVU&amp;hl=en_US&amp;feature=player_embedded&amp;version=3" allowscriptaccess="always" allowfullscreen="true" type="application/x-shockwave-flash"></embed>
</object>
</p><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/atracoes-tandava/130-urucubaca-live-pr.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/atracoes-tandava/ver-todas/130-urucubaca-live-pr.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/atracoes-tandava/ver-todas/130-urucubaca-live-pr.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL3d3dy5lbGlqYWguY29tLmJyL3RhbmRhdmEvMjAxMC9hdHJhY29lcy10YW5kYXZhLzEzMC11cnVjdWJhY2EtbGl2ZS1wci5odG1s" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Quinta, 23 de Setembro de 2010 14:17				</span>
					
								<span class="rt-date-modified">
					Última atualização em Quinta, 23 de Setembro de 2010 14:27				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>									
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/atracoes-tandava/vive-la-musique.html">Vive La Musique</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<p style="text-align: justify;"><img style="margin: 0px 10px 25px 0px; float: left;" alt="vivelamusique" src="images/stories/atracoes/vivelamusique.png" height="150" width="225" /><strong></strong></p>
<p style="text-align: justify;">Durante a noite de Sábado, o Chill Out se transforma com o som dançante da "Vive" e o espírito da cena Electro Indie curitibana.</p>
<p style="text-align: justify;">A  festa Vive La Musique é produzida pelos DJs <a href="index.php?option=com_content&amp;view=article&amp;id=110:andre-nego-pr&amp;catid=45&amp;Itemid=172">André Nego</a> e <a href="index.php?option=com_content&amp;view=article&amp;id=111:joel-guglielmini-pr&amp;catid=45&amp;Itemid=172">Joel  Guglielmini</a> e procura unir música e arte, com diversas exposições,  performances e trabalhos de novos artistas, como Estúdio Rasputines,  criadores da logo da festa.</p>
<p style="text-align: justify;">Em setembro de 2009 a Vive completou um ano de vida com 10 Edições de muito sucesso. Pela festa já passaram grandes DJs, Daniel Peixoto (ex-Montage), Raul  Aguilera, Jô Mistinguett, Barbarela, All Starz (Posh – SP), Tchiello K  (Crew – SP), Killer on The Dancefloor (Crew – SP), Boss In Drama, Dirty  Noise (RS), Elijah Hatem, dentre outros DJs da cena local de Curitiba como  Gee X, Bogus, Renan Mendes, Sandra Carraro, além do ex-residente Cacá  Azevedo.</p>
<p style="text-align: justify;">O projeto teve início no clube Soho Underground e passou pelo Espaço  Roxy, Mahatma Electro Lounge, Circus Bar,&nbsp; Kitinete, Camden Club, Era Só e atualmente segue em formato itinerante por diversos clubs do circuito alternativo curitibano.<br /> <br /> Electro, electro house, disco punk, fidget house, new rave, techno, rock  e maximal são os estilos que predominam na pista da Vive La Musique. Aguardem uma noite cheia de atrações especiais e DJs convidados!</p>
<blockquote>
<p><strong>+ INFO: </strong><a target="_blank" href="http://www.vivelamusique.com.br">www.vivelamusique.com.br</a></p>
</blockquote><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/atracoes-tandava/vive-la-musique.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/atracoes-tandava/ver-todas/106-vive-la-musique.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/atracoes-tandava/ver-todas/106-vive-la-musique.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL3d3dy5lbGlqYWguY29tLmJyL3RhbmRhdmEvMjAxMC9hdHJhY29lcy10YW5kYXZhL3ZpdmUtbGEtbXVzaXF1ZS5odG1s" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Segunda, 02 de Agosto de 2010 18:37				</span>
					
								<span class="rt-date-modified">
					Última atualização em Segunda, 02 de Agosto de 2010 21:09				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>									
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/atracoes-tandava/zaraus-live-portugal.html">Zaraus LIVE (Portugal)</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<p style="text-align: justify;"><img style="margin: 0px 10px 10px 0px; float: left;" alt="zaraus" src="images/stories/atracoes/zaraus.jpg" height="343" width="225" /><strong>Pedro Oliveira</strong> aka <strong>Zaraus</strong>, nascido em 81. Estudou Engenharia de Áudio por 3 anos, Midi e Síntese na Dance Planet em Lisboa. Escuta Trance desde 1997. Viveu no Reino Unido 2001 a 2003, na Holanda em 2003, no Brasil de 2004 a 2006 e agora mora em Lisboa.</p>
<p style="text-align: justify;">Como a maioria dos artistas de Trance de Portugal, Zaraus foi altamente influenciado pelas festas Trance "old school" de Portugal quando as festas de Psy não eram infectadas pela música regressiva, e quando apenas se tocava o verdadeiro Goa e música para frente.</p>
<p style="text-align: justify;">Algumas influências em nomes: Phyx, Clock Wise, Shift, Menog, Matt V, Man With No Name, Kox Box, Green Ohms, Cydonia, Paul Taylor, Dick Trevor, Dino Psaras, Green Nuns Of The Revolution, Disckster, Eat Static, Infected Mushroom antigo, Skazi antigo, GMS antigo, Astrix antigo e muito muitos outros...</p>
<blockquote>
<p><strong>+ INFO: </strong><a target="_blank" href="http://www.myspace.com/zaraus">www.myspace.com/zaraus</a></p>
</blockquote>
<p><strong>+ MÚSICAS:</strong></p>
<object codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=10,0,32,18" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" type="application/x-shockwave-flash" height="225" width="100%">
<param name="allowscriptaccess" value="always" />
<param name="src" value="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fsoundcloud.com%2Fzaraus&amp;secret_url=false" /><embed height="225" width="100%" src="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fsoundcloud.com%2Fzaraus&amp;secret_url=false" allowscriptaccess="always" type="application/x-shockwave-flash"></embed>
</object>
<span><a href="http://soundcloud.com/zaraus">Latest tracks by Zaraus</a></span><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/atracoes-tandava/zaraus-live-portugal.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/atracoes-tandava/ver-todas/107-zaraus-live-portugual.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/atracoes-tandava/ver-todas/107-zaraus-live-portugual.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL3d3dy5lbGlqYWguY29tLmJyL3RhbmRhdmEvMjAxMC9hdHJhY29lcy10YW5kYXZhL3phcmF1cy1saXZlLXBvcnR1Z2FsLmh0bWw%3D" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Segunda, 02 de Agosto de 2010 18:47				</span>
					
								<span class="rt-date-modified">
					Última atualização em Segunda, 02 de Agosto de 2010 20:38				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>							</div>
		
				<div class="clear"></div>
		
				
	</div>
</div>";s:4:"head";a:10:{s:5:"title";s:31:"Atrações do Tandava Gathering";s:11:"description";s:275:"Não surgiu um novo conceito. As coisas não vão mudar e nada será resgatado.  Não  é nada melhor ou maior de tudo que você já viu. Não  é inédito e nem ao menos é um evento. O respeito existe, mas o publico não, existe um encontro entre amigos e amigos de amigos.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:24:"text/html; charset=utf-8";}s:8:"standard";a:5:{s:6:"robots";s:13:"index, follow";s:8:"keywords";s:54:"tandava, gathering, festival, 2010, curitiba, novembro";s:8:"og:title";s:22:"Zaraus LIVE (Portugal)";s:12:"og:site_name";s:17:"Tandava Gathering";s:8:"og:image";s:72:"http://www.elijah.com.br/tandava/2010/images/stories/atracoes/zaraus.jpg";}}s:5:"links";a:2:{i:0;s:126:"<link href="/tandava/2010/atracoes-tandava/ver-todas.feed?type=rss" rel="alternate" type="application/rss+xml" title="RSS 2.0"";i:1;s:129:"<link href="/tandava/2010/atracoes-tandava/ver-todas.feed?type=atom" rel="alternate" type="application/atom+xml" title="Atom 1.0"";}s:11:"styleSheets";a:0:{}s:5:"style";a:0:{}s:7:"scripts";a:2:{s:41:"/tandava/2010/media/system/js/mootools.js";s:15:"text/javascript";s:40:"/tandava/2010/media/system/js/caption.js";s:15:"text/javascript";}s:6:"script";a:0:{}s:6:"custom";a:1:{i:0;s:286:"<script type='text/javascript'>
/*<![CDATA[*/
	var jax_live_site = 'http://www.elijah.com.br/tandava/2010/index.php';
	var jax_site_type = '1.5';
/*]]>*/
</script><script type="text/javascript" src="http://www.elijah.com.br/tandava/2010/plugins/system/pc_includes/ajax_1.3.js"></script>";}}s:7:"pathway";a:2:{i:0;O:8:"stdClass":2:{s:4:"name";s:10:"Atrações";s:4:"link";s:20:"index.php?Itemid=172";}i:1;O:8:"stdClass":2:{s:4:"name";s:12:"Ver Todas...";s:4:"link";s:20:"index.php?Itemid=191";}}s:6:"module";a:0:{}}