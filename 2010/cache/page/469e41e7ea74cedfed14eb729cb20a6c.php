<?php die("Access Denied"); ?>
<?xml version="1.0" encoding="utf-8"?>
<!-- generator="Joomla! 1.5 - Open Source Content Management" -->
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
	<channel>
		<title>Tandava Gathering - 4 Dias de Festival / Camping / Chill Out / Oficinas</title>
		<description>Não surgiu um novo conceito. As coisas não vão mudar e nada será resgatado.  Não  é nada melhor ou maior de tudo que você já viu. Não  é inédito e nem ao menos é um evento. O respeito existe, mas o publico não, existe um encontro entre amigos e amigos de amigos.</description>
		<link>http://elijah.com.br/tandava/2010/component/content/frontpage.html</link>
		<lastBuildDate>Sat, 17 Dec 2011 22:59:34 +0000</lastBuildDate>
		<generator>Joomla! 1.5 - Open Source Content Management</generator>
		<language>pt-br</language>
		<item>
			<title>O Tandava 2010</title>
			<link>http://elijah.com.br/tandava/2010/component/content/article/48-terceira-edicao/144-o-tandava-2010.html</link>
			<guid>http://elijah.com.br/tandava/2010/component/content/article/48-terceira-edicao/144-o-tandava-2010.html</guid>
			<description><![CDATA[<h4>Pela simples e convicta crença na cena underground...</h4>
<p style="text-align: justify;">O <strong>Tandava 2010</strong> foi prova da existência de harmonia entre a diversidade.</p>
<p style="text-align: justify;">Mostrou o bom senso de uma geração, ou mais de uma, capaz de assumir identidade própria <img style="margin: 10px 0px 10px 20px; float: right;" alt="tandava2010-mamao-2" src="http://elijah.com.br/tandava/2010/images/stories/home/tandava2010-mamao-2.jpg" width="200" height="133" />e se auto-organizar, indiferentes a tabus e estereotipos. Esse mérito pertence a todos os presentes,  que de alguma maneira foram para lá dispostos a interagir, conhecer pessoas novas, e lutar pela manutenção dessa sensação familiar de um encontro.</p>
<p style="text-align: justify;">Não existem planos para tornar o <strong>Tandava</strong> um grande festival ou um evento, a intenção será sempre a mesma: Boa música em um local agradavel e na presença de Amigos.</p>
<p style="text-align: justify;">Com tantos <em>feedbacks</em> positivos e novas amizades dispostas a ajudar na construção do tandava, com grande prazer já começa a tomar forma a próxima aventura em 2011.</p>
<p style="text-align: justify;">Contando com 1 dia a mais de duração, está programado para entre os dias 11/11/11 e 15/11/11 a 4ª edição do Tandava, contamos com todos os envolvidos nessa edição e  todos aqueles dispostos a se dedicar a evolução sadia dessa idéia.</p>
<p style="text-align: justify;">A satisfação é inegável, <strong>OBRIGADO</strong>.</p>
<p><img style="margin: 10px auto; vertical-align: middle; display: block;" alt="tandava2010-mamao-3" src="http://elijah.com.br/tandava/2010/images/stories/home/tandava2010-mamao-3.jpg" width="480" height="270" /></p>
<div class="camera">
<div class="typo-icon">Em breve, mais fotos e cobertura completa da terceira edição do Tandava.</div>
</div>
<blockquote>
<p style="text-align: justify;">In Lak'Ech. Somos Todos Um.</p>
</blockquote>
<p><cite><a href="http://elijah.com.br/tandava/2010/../">Tandava Gathering</a></cite>, 2010</p>]]></description>
			<author>djahlandroid@hotmail.com (Ahlan Droid)</author>
			<category>frontpage</category>
			<pubDate>Thu, 18 Nov 2010 19:01:25 +0000</pubDate>
		</item>
	</channel>
</rss>
