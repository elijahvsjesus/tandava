<?php die("Access Denied"); ?>
<?xml version="1.0" encoding="utf-8"?>
<!-- generator="Joomla! 1.5 - Open Source Content Management" -->
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
	<channel>
		<title>Fotos do Tandava Gathering</title>
		<description>Não surgiu um novo conceito. As coisas não vão mudar e nada será resgatado.  Não  é nada melhor ou maior de tudo que você já viu. Não  é inédito e nem ao menos é um evento. O respeito existe, mas o publico não, existe um encontro entre amigos e amigos de amigos.</description>
		<link>http://www.elijah.com.br/tandava/2010/fotos.html</link>
		<lastBuildDate>Tue, 02 Aug 2011 17:04:41 +0000</lastBuildDate>
		<generator>Joomla! 1.5 - Open Source Content Management</generator>
		<language>pt-br</language>
		<item>
			<title>Fotos do Tandava 2010</title>
			<link>http://www.elijah.com.br/tandava/2010/fotos/143-fotos-do-tandava-2010.html</link>
			<guid>http://www.elijah.com.br/tandava/2010/fotos/143-fotos-do-tandava-2010.html</guid>
			<description><![CDATA[<p>A terceira edição do <strong>Tandava Gathering </strong>aconteceu no <strong>Haras Cartel</strong>, um local fantástico há 40km de Curitiba, entre os dias 12 e 15 de Novembro de 2010.</p>
<p>Abaixo, você encontra as fotos do encontro a partir de inúmeros pontos de vista de diferentes fotógrafos que estiveram presentes no evento. Confira::</p>
<h3>Fotos by MushPics.com</h3>
<p>Acesse também através do <a target="_blank" href="http://mushpics.com/tandava2010.html">website</a> e <a target="_blank" href="http://www.flickr.com/photos/mushpics">Flickr</a> do Mushpics.</p>
<p>{rokbox album=|tandava2010mushpics|}images/stories/fotos/2010/mushpics/*{/rokbox}</p>
<h3>Fotos by Pinduca</h3>
<p>{rokbox album=|tandava2010pinduca|}images/stories/fotos/2010/pinduca/*{/rokbox}</p>
<h3>Fotos by Mamão</h3>
<p>{rokbox album=|tandava2010mamao|}images/stories/fotos/2010/mamao/*{/rokbox}</p>
<h3>Fotos by leduxCWB</h3>
<p>Acesse também através do <a target="_blank" href="http://leduxcwb.wordpress.com/2010/11/23/tandava-vive-la-musique-13-e-14nov2010/">blog do leduxCWB</a>.</p>
<p>Em breve mais fotos aqui nessa página.</p>
<h3>Fotos da Galera</h3>
Em breve! <br /><br />
<div class="camera">
<div class="typo-icon">
<p><strong>Envie suas fotos!</strong></p>
<p>Se você participou do Tandava Gathering 2010, colabore! Envie suas fotos e faça parte desta história.</p>
<p><a href="http://www.elijah.com.br/tandava/2010/index.php?option=com_contact&amp;view=contact&amp;id=1&amp;Itemid=49">Clique Aqui</a> e entre em contato conosco.</p>
</div>
</div>]]></description>
			<author>elijah@metropolis.art.br (de elijah hatem)</author>
			<category>Fotos</category>
			<pubDate>Wed, 17 Nov 2010 21:01:48 +0000</pubDate>
		</item>
		<item>
			<title>Foto Panorâmica 360º do Tandava 2009</title>
			<link>http://www.elijah.com.br/tandava/2010/fotos/92-tandava-panoramica-360-tandava-2009.html</link>
			<guid>http://www.elijah.com.br/tandava/2010/fotos/92-tandava-panoramica-360-tandava-2009.html</guid>
			<description><![CDATA[<p>Os fotógrafos da <a target="_blank" href="http://www.mushpics.com/"><strong>MushPics.com</strong></a> estiveram na edição 2009 do <strong>Tandava Gathering </strong>e registraram momentos únicos. Clique na imagem abaixo e confira uma foto panorâmica em 360º da segunda noite do Festival.</p>
<p style="text-align: center;">{rokbox size=|fullscreen| thumb=|images/stories/tandava360_thumb.jpg|}http://www.mushpics.com/panoramicas/tandava2009/index.html{/rokbox}</p>]]></description>
			<author>elijah@metropolis.art.br (de elijah hatem)</author>
			<category>Fotos</category>
			<pubDate>Wed, 16 Jun 2010 03:00:00 +0000</pubDate>
		</item>
		<item>
			<title>Fotos do Tandava 2009</title>
			<link>http://www.elijah.com.br/tandava/2010/fotos/85-fotos-tandava-2009.html</link>
			<guid>http://www.elijah.com.br/tandava/2010/fotos/85-fotos-tandava-2009.html</guid>
			<description><![CDATA[<p>A segunda edição do <strong>Tandava Gathering </strong>aconteceu em um ensolarado final de semana nos dias 13, 14 e 15 de Novembro de 2009 na Chácara Santa Rita nos arredores de Curitiba.</p>
<p>Uma experiência sem igual e um encontro de amigos e amigos de amigos. Confira as fotos:</p>
<h3>Fotos by MushPics.com</h3>
<p>{rokbox album=|tandava2009|}images/stories/fotos/2009/mushpics/*{/rokbox}</p>
<h3>Fotos da Galera</h3>
<h5>by TaePsy</h5>
<p>{rokbox album=|tandava2009-taepsy|}images/stories/fotos/2009/taepsy/*{/rokbox}</p>
<div class="camera">
<div class="typo-icon">
<p><strong>Envie suas fotos!</strong></p>
<p>Se você participou do Tandava Gathering 2009, colabore! Envie suas fotos e faça parte desta história.</p>
<p><a href="http://www.elijah.com.br/tandava/2010/index.php?option=com_contact&amp;view=contact&amp;id=1&amp;Itemid=49">Clique Aqui</a> e entre em contato conosco.</p>
</div>
</div>]]></description>
			<author>elijah@metropolis.art.br (de elijah hatem)</author>
			<category>Fotos</category>
			<pubDate>Thu, 10 Jun 2010 03:09:12 +0000</pubDate>
		</item>
		<item>
			<title>Fotos do Tandava 2008</title>
			<link>http://www.elijah.com.br/tandava/2010/fotos/97-fotos-tandava-2008.html</link>
			<guid>http://www.elijah.com.br/tandava/2010/fotos/97-fotos-tandava-2008.html</guid>
			<description><![CDATA[<p>A primeira edição do <strong>Tandava Gathering </strong>aconteceu em um ensolarado final de semana de 2008 nos arredores de Curitiba.</p>
<p>Uma experiência sem igual e o começo de um encontro de amigos e amigos de amigos. Confira as fotos:</p>
<h3>Fotos by MushPics.com</h3>
<p>{rokbox album=|tandava2008|}images/stories/fotos/2008/mushpics/*{/rokbox}</p>
<div class="camera">
<div class="typo-icon">
<p><strong>Envie suas fotos!</strong></p>
<p>Se você participou do Tandava Gathering 2008, colabore! Envie suas fotos e faça parte desta história.</p>
<p><a href="http://www.elijah.com.br/tandava/2010/index.php?option=com_contact&amp;view=contact&amp;id=1&amp;Itemid=49">Clique Aqui</a> e entre em contato conosco.</p>
</div>
</div>]]></description>
			<author>elijah@metropolis.art.br (de elijah hatem)</author>
			<category>Fotos</category>
			<pubDate>Wed, 09 Jun 2010 03:09:12 +0000</pubDate>
		</item>
	</channel>
</rss>
