<?php die("Access Denied"); ?>
<?xml version="1.0" encoding="utf-8"?>
<!-- generator="Joomla! 1.5 - Open Source Content Management" -->
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
	<channel>
		<title>Atrações do Tandava Gathering</title>
		<description>Não surgiu um novo conceito. As coisas não vão mudar e nada será resgatado.  Não  é nada melhor ou maior de tudo que você já viu. Não  é inédito e nem ao menos é um evento. O respeito existe, mas o publico não, existe um encontro entre amigos e amigos de amigos.</description>
		<link>http://www.elijah.com.br/tandava/2010/atracoes-tandava.html</link>
		<lastBuildDate>Mon, 18 Jul 2011 17:40:59 +0000</lastBuildDate>
		<generator>Joomla! 1.5 - Open Source Content Management</generator>
		<language>pt-br</language>
		<item>
			<title>Funk You</title>
			<link>http://www.elijah.com.br/tandava/2010/atracoes-tandava/funk-you.html</link>
			<guid>http://www.elijah.com.br/tandava/2010/atracoes-tandava/funk-you.html</guid>
			<description><![CDATA[<p><img style="margin: 0px 10px 10px 0px; float: left;" alt="funk-you_thumb" src="http://www.elijah.com.br/tandava/2010/images/stories/atracoes/funk-you_thumb.jpg" height="225" width="225" />Abrindo a primeira noite do Chill Out a festa <strong>Funk You</strong>, projeto que apresenta a&nbsp; música "negra", batidas quentes e extremamente dançantes! Do <em>Funk Old School</em> ao <em>New School</em>, <em>NuFunk</em>, <em>Breaks</em>, <em>Big Beat</em>, <em>Disco</em> e <em>Hip Hop</em>.</p>
<p>Com pouco mais de dois anos, a festa já virou referencia de Funk e breaks em Curitiba comandada pelos DJs Murillo e Schasko, sempre recebendo convidados da cena curitibana e nacional.</p>
<blockquote>
<p><strong>+ INFO: </strong><a target="_blank" href="http://www.myspace.com/funkyoucwb">www.myspace.com/funkyoucwb</a></p>
</blockquote>]]></description>
			<author>elijah@metropolis.art.br (de elijah hatem)</author>
			<category>Atrações</category>
			<pubDate>Thu, 17 Jun 2010 01:43:42 +0000</pubDate>
		</item>
		<item>
			<title>Vive La Musique</title>
			<link>http://www.elijah.com.br/tandava/2010/atracoes-tandava/vive-la-musique.html</link>
			<guid>http://www.elijah.com.br/tandava/2010/atracoes-tandava/vive-la-musique.html</guid>
			<description><![CDATA[<p style="text-align: justify;"><img style="margin: 0px 10px 25px 0px; float: left;" alt="vivelamusique" src="http://www.elijah.com.br/tandava/2010/images/stories/atracoes/vivelamusique.png" height="150" width="225" /><strong></strong></p>
<p style="text-align: justify;">Durante a noite de Sábado, o Chill Out se transforma com o som dançante da "Vive" e o espírito da cena Electro Indie curitibana.</p>
<p style="text-align: justify;">A  festa Vive La Musique é produzida pelos DJs <a href="http://www.elijah.com.br/tandava/2010/index.php?option=com_content&amp;view=article&amp;id=110:andre-nego-pr&amp;catid=45&amp;Itemid=172">André Nego</a> e <a href="http://www.elijah.com.br/tandava/2010/index.php?option=com_content&amp;view=article&amp;id=111:joel-guglielmini-pr&amp;catid=45&amp;Itemid=172">Joel  Guglielmini</a> e procura unir música e arte, com diversas exposições,  performances e trabalhos de novos artistas, como Estúdio Rasputines,  criadores da logo da festa.</p>
<p style="text-align: justify;">Em setembro de 2009 a Vive completou um ano de vida com 10 Edições de muito sucesso. Pela festa já passaram grandes DJs, Daniel Peixoto (ex-Montage), Raul  Aguilera, Jô Mistinguett, Barbarela, All Starz (Posh – SP), Tchiello K  (Crew – SP), Killer on The Dancefloor (Crew – SP), Boss In Drama, Dirty  Noise (RS), Elijah Hatem, dentre outros DJs da cena local de Curitiba como  Gee X, Bogus, Renan Mendes, Sandra Carraro, além do ex-residente Cacá  Azevedo.</p>
<p style="text-align: justify;">O projeto teve início no clube Soho Underground e passou pelo Espaço  Roxy, Mahatma Electro Lounge, Circus Bar,&nbsp; Kitinete, Camden Club, Era Só e atualmente segue em formato itinerante por diversos clubs do circuito alternativo curitibano.<br /> <br /> Electro, electro house, disco punk, fidget house, new rave, techno, rock  e maximal são os estilos que predominam na pista da Vive La Musique. Aguardem uma noite cheia de atrações especiais e DJs convidados!</p>
<blockquote>
<p><strong>+ INFO: </strong><a target="_blank" href="http://www.vivelamusique.com.br">www.vivelamusique.com.br</a></p>
</blockquote>]]></description>
			<author>elijah@metropolis.art.br (de elijah hatem)</author>
			<category>Atrações</category>
			<pubDate>Mon, 02 Aug 2010 21:37:00 +0000</pubDate>
		</item>
		<item>
			<title>Reboot</title>
			<link>http://www.elijah.com.br/tandava/2010/atracoes-tandava/reboot.html</link>
			<guid>http://www.elijah.com.br/tandava/2010/atracoes-tandava/reboot.html</guid>
			<description><![CDATA[<p style="text-align: justify;"><img style="margin: 0px 10px 10px 0px; float: left;" alt="reboot" src="http://www.elijah.com.br/tandava/2010/images/stories/atracoes/reboot.png" width="225" height="225" /><strong>REBOOT</strong> [ri:'bu:t] : reiniciar o computador ou sistema.</p>
<p style="text-align: justify;">Que é isso afinal? REBOOT é um projeto de releituras musicais, cujo nome é a contração de dois termos musicais (REMIXES+BOOTLEGS=RE.BOOT).</p>
<p style="text-align: justify;">REMIX: é reconstrução de uma faixa musical com base em elementos do original adicionados de novos elementos.</p>
<p style="text-align: justify;">BOOTLEG, MASHUP: ambos os termos referem-se a recriações musicais a partir da fusão de canções e instrumentais de diferentes artistas, também mesclando, em geral, gêneros musicais diversos.</p>
<p style="text-align: justify;">A autenticidade das recriações produzidas em home-studios reside exatamente na reorganização de diferentes elementos de cada original utilizado, compondo obras artísticas criativas a partir de recortes, colagens e processamento de efeitos.</p>
<p style="text-align: justify;">O produto desta técnica também ficou conhecido como BASTARD POP, por misturar estilos como Rock, Funk, Hip-Hop e Pop, sendo então considerado como filho ilegítimo deste último.</p>
<p style="text-align: justify;">O conceito não se esgota aí... já se podem ver releituras de artes visuais, remixes e mashups de filmes e video-clips musicais, até mesmo misturas de trailers, teasers, animações, fotos, cartazes e flyers. O próprio projeto também não se resume à abordagem musical e também apresenta performances cênicas e circenses.</p>
<p style="text-align: justify;">A proposta cultural do projeto REBOOT pode ser resumida no lema/desafio: PROIBIDO ORIGINAIS !!!</p>
<h4 style="text-align: justify;">Artistas:</h4>
<p style="text-align: justify;">- DJs Feiges, RenanF e Murillo</p>
<h4>- Performances:</h4>
<p style="text-align: justify;">Yamballoon Circus Arts.</p>
<p>{rokbox album=|reboot|}images/stories/reboot/*{/rokbox}</p>
<blockquote>
<p><strong>+ INFO: </strong><a target="_blank" href="http://reboot.blog.com">reboot.blog.com</a></p>
</blockquote>]]></description>
			<author>elijah@metropolis.art.br (de elijah hatem)</author>
			<category>Atrações</category>
			<pubDate>Fri, 15 Oct 2010 22:23:20 +0000</pubDate>
		</item>
		<item>
			<title>Synk Recs Day Party</title>
			<link>http://www.elijah.com.br/tandava/2010/atracoes-tandava/synk-recs-day-party.html</link>
			<guid>http://www.elijah.com.br/tandava/2010/atracoes-tandava/synk-recs-day-party.html</guid>
			<description><![CDATA[<div class="rt-article-content">
<p style="text-align: justify;"><img style="margin: 0px 10px 25px 0px; float: left;" alt="synk-recs" src="http://www.elijah.com.br/tandava/2010/images/stories/atracoes/synk-recs.png" width="225" height="150" /><strong></strong></p>
<p style="text-align: justify;">No último dia do festival – feriado de Segunda-Feira, 15 de Novembro – o <strong>Tandava Gathering</strong> recebe uma festa especial da gravadora <strong>SYNK Records </strong>para o encerramento da edição 2010.</p>
<p style="text-align: justify;">A festa inicia pela manhã e se prolonga pelo último dia com destaque para a apresentação de <a href="http://www.elijah.com.br/tandava/2010/index.php?option=com_content&amp;view=article&amp;id=115:truati-live-sp&amp;catid=45&amp;Itemid=172"><strong>Truati LIVE</strong></a>, <a href="http://www.elijah.com.br/tandava/2010/index.php?option=com_content&amp;view=article&amp;id=114:rodrigo-carreira-pr&amp;catid=45&amp;Itemid=172">Rodrigo Carreira</a>, <a target="_self" href="http://www.elijah.com.br/tandava/2010/index.php?option=com_content&amp;view=article&amp;id=123:gabriel-boni-pr&amp;catid=45&amp;Itemid=172">Gabriel Boni</a>, <a href="http://www.elijah.com.br/tandava/2010/index.php?option=com_content&amp;view=article&amp;id=113:gabi-lima-pr&amp;catid=45&amp;Itemid=172">Gabi Lima</a> e <a href="http://www.elijah.com.br/tandava/2010/index.php?option=com_content&amp;view=article&amp;id=122:rodrigo-nickel-pr&amp;catid=45&amp;Itemid=172">Rodrigo Nickel</a>.</p>
<p style="text-align: justify;">Um encerramento com qualidade e sonoridade sem igual. Aguardem pelo Line Up completo!</p>
<p style="text-align: justify;"><strong>+ SOBRE A SYNK</strong></p>
<p>Pensando e distribuindo música digital desde 2007, a Synk Records  oferece inovação, lançando composições musicais que estimulam o público à  novas percepções criativas.</p>
<p>Transcendendo a representação de gêneros e estilos pré-definidos, a  Synk Records posiciona-se no mercado através da seleção de artistas  antenados, que misturam o uso de tecnologia de ponta, musicalidade e  ritmo para as pistas de dança.</p>
<p style="text-align: justify;">A marca Synk foi idealizada e fundada pelos artistas Rodrigo  Carreira, Leandro Pacceli e Gabi em parceria com o selo Presslab Records  (Itália), e seus integrantes: Omar Neri e Luigi Gori (Presslaboys).</p>
<blockquote>
<p><strong>+ INFO: </strong><a target="_blank" href="http://www.synk.com.br/">www.synk.com.br</a></p>
</blockquote>
</div>]]></description>
			<author>elijah@metropolis.art.br (de elijah hatem)</author>
			<category>Atrações</category>
			<pubDate>Wed, 04 Aug 2010 01:10:25 +0000</pubDate>
		</item>
		<item>
			<title>Zaraus LIVE (Portugal)</title>
			<link>http://www.elijah.com.br/tandava/2010/atracoes-tandava/zaraus-live-portugal.html</link>
			<guid>http://www.elijah.com.br/tandava/2010/atracoes-tandava/zaraus-live-portugal.html</guid>
			<description><![CDATA[<p style="text-align: justify;"><img style="margin: 0px 10px 10px 0px; float: left;" alt="zaraus" src="http://www.elijah.com.br/tandava/2010/images/stories/atracoes/zaraus.jpg" height="343" width="225" /><strong>Pedro Oliveira</strong> aka <strong>Zaraus</strong>, nascido em 81. Estudou Engenharia de Áudio por 3 anos, Midi e Síntese na Dance Planet em Lisboa. Escuta Trance desde 1997. Viveu no Reino Unido 2001 a 2003, na Holanda em 2003, no Brasil de 2004 a 2006 e agora mora em Lisboa.</p>
<p style="text-align: justify;">Como a maioria dos artistas de Trance de Portugal, Zaraus foi altamente influenciado pelas festas Trance "old school" de Portugal quando as festas de Psy não eram infectadas pela música regressiva, e quando apenas se tocava o verdadeiro Goa e música para frente.</p>
<p style="text-align: justify;">Algumas influências em nomes: Phyx, Clock Wise, Shift, Menog, Matt V, Man With No Name, Kox Box, Green Ohms, Cydonia, Paul Taylor, Dick Trevor, Dino Psaras, Green Nuns Of The Revolution, Disckster, Eat Static, Infected Mushroom antigo, Skazi antigo, GMS antigo, Astrix antigo e muito muitos outros...</p>
<blockquote>
<p><strong>+ INFO: </strong><a target="_blank" href="http://www.myspace.com/zaraus">www.myspace.com/zaraus</a></p>
</blockquote>
<p><strong>+ MÚSICAS:</strong></p>
<object codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=10,0,32,18" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" type="application/x-shockwave-flash" height="225" width="100%">
<param name="allowscriptaccess" value="always" />
<param name="src" value="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fsoundcloud.com%2Fzaraus&amp;secret_url=false" /><embed height="225" width="100%" src="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fsoundcloud.com%2Fzaraus&amp;secret_url=false" allowscriptaccess="always" type="application/x-shockwave-flash"></embed>
</object>
<span><a href="http://soundcloud.com/zaraus">Latest tracks by Zaraus</a></span>]]></description>
			<author>elijah@metropolis.art.br (de elijah hatem)</author>
			<category>Atrações</category>
			<pubDate>Mon, 02 Aug 2010 21:47:33 +0000</pubDate>
		</item>
		<item>
			<title>Rex (África do Sul)</title>
			<link>http://www.elijah.com.br/tandava/2010/atracoes-tandava/rex-africa-do-sul.html</link>
			<guid>http://www.elijah.com.br/tandava/2010/atracoes-tandava/rex-africa-do-sul.html</guid>
			<description><![CDATA[<p style="text-align: justify;"><img style="margin: 0px 10px 10px 0px; float: left;" alt="REX" src="http://www.elijah.com.br/tandava/2010/images/stories/atracoes/REX.jpg" width="225" height="169" />REX é de Cape Town, África do Sul e representa a NANO Records desde 2004.</p>
<p style="text-align: justify;">Sua carreira de DJ iniciou em 1996 quando Rex começou na cena underground como DAT Jockey, tocando GOA e evoluindo para o Psy-trance com o passar dos anos. Ao mesmo tempo em que tocava, viajava e organizava festas pelo mundo. Viveu na Europa por 6 anos, onde foi responsável pelas festas underground com o nome de Indigo Children - uma das maiores e mais notórias festas indoor e outdoor em Londres em 2003/04.</p>
<p style="text-align: justify;">Isso possibilitou com que ele entrasse em contato com diferentes artistas que o inspiraram, além de pessoas de todas as partes do mundo e com culturas muito diferentes. Tudo isso contribuiu na construção de um tipo único de música o que é ser dj e finalmente produzir música.</p>
<p style="text-align: justify;">No momento, Rex está no Brasil cuidando dos negócios da NANO na América do Sul, além, é claro, de atrair multidões com músicas da NANO de todas as partes do Mundo. Rex já se apresentou nos festivais mais importantes do Brasil como Universo Paralello, Trancendence e TranceFormation; além de festas pelos quatro cantos do pais.</p>
<blockquote>
<p><strong>+ INFO: </strong><a target="_blank" href="http://www.myspace.com/djrexnano">www.myspace.com/djrexnano</a></p>
</blockquote>]]></description>
			<author>elijah@metropolis.art.br (de elijah hatem)</author>
			<category>Atrações</category>
			<pubDate>Mon, 02 Aug 2010 15:55:07 +0000</pubDate>
		</item>
		<item>
			<title>Sallun (Pedra Branca - SP)</title>
			<link>http://www.elijah.com.br/tandava/2010/atracoes-tandava/sallun-pedra-branca.html</link>
			<guid>http://www.elijah.com.br/tandava/2010/atracoes-tandava/sallun-pedra-branca.html</guid>
			<description><![CDATA[<p style="text-align: justify;"><img style="margin: 0px 10px 10px 0px; float: left;" alt="sallun" src="http://www.elijah.com.br/tandava/2010/images/stories/atracoes/sallun.jpg" width="225" height="320" /></p>
<p align="justify"><strong>Luciano Sallun</strong>, 30, vem explorando em sua música o a união entre o acústico e eletrônico no world music, e na musica experimental contemporânea.</p>
<p align="justify">O músico, musicoterapeuta, compositor, produtor e dj pesquisa musicas e instrumentos de diversas partes do mundo, como como sitar, alaúde e samissen. Esses elementos estão presentes na cultura indiana, árabe e japonesa, respectivamente.</p>
<p align="justify">Sallun cursou faculdade de musicoterapia, na Faculdade Paulista de Artes, bem como estuda instrumentos como os ditos a cima, estando por um período na Índia estudando sitar com o mestre Sanjeeb Sircar.</p>
<p align="justify">Desenvolve assim com seus projetos e principalmente no seu projeto solo uma pesquisa étnicomusicologia com musicas do mundo, buscando a fusão entre as diversas culturas juntas. Onde está gravando seu primeiro álbum solo, com a união do jazz e world music.</p>
<p align="justify">Além disso, o músico paulista agrega às suas performances instrumentos musicais de criação própria, como meio de buscar timbres inexplorados. Entre as invenções está o armesk, feito com uma lata de biscoitos e com um cabo de vassoura, a viola oriental, com duas cabaças ligadas por um bambu e cordas de viola e as flautas feitas de canos plásticos e bexigas de borracha. Desenvolve construção experimental de instrumentos de junto ao musico e luthier alternativo Fernando Sardo 2003 fazendo uma instalação sonora chamada de Dessintetizador. Juntos eles fazem parte do grupo GEM (Grupo Experimental de Música), que realiza apresentações musicais somente com os instrumentos criados.</p>
<p align="justify">Na parte de produção eletrônica, ele produz os beats e efeitos das faixas do grupo <strong>Pedra Branca</strong>, no qual também realiza a concepção artística. O Pedra Branca já lançou dois álbuns e se apresentou em diversos locais pelo Brasil e Portugal. Trabalho com publico abrangente e reconhecido mundialmente.</p>
<p align="justify">No seu dj set ele mistura a música eletrônica (particularmente trip hop e downbeat) com world music (musicas tradicionais de diferentes lugares e culturas, o que inclui o uso de instrumentos locais) e elabora a fusão que transpassa o elemento unicamente musical e atinge a mistura de culturas em si. “Como mixar um tambor africano com uma voz da asiática e com um berimbal”, explica.</p>
<p align="justify">O dj diz que no fundo há uma ligação entre as variantes musicais. Seu estilo se encaixa entre chill out, ambient stage, ou world music stage.</p>
<p align="justify">O outro projeto do qual Sallun participa é o Liquidus Ambiento, que começou em 2003. E vem desenvolvendo seu primeiro álbum. O instrumentista também já desenvolveu atividades com inúmeros artistas. Trabalhou com a percussionista Simone Soul e o produtor e baixista Alfredo Bello (Projeto Cru), o produtor Rodrigo Soneca (Trotter) onde tem juntos o projeto chamado Pindorama. Desenvolveu trilhas sonoras para espetáculos de dança, como o Monomitos, e para performances multimídia, como a Útero. Entre outras, dança indiana e dança do ventre em algumas apresentações.</p>
<p align="justify">Atualmente está fazendo a direção musical e produção musical do espetáculo Crenças e crendices, quem disse? Da Cia de Dança de Diadema, dirigido por Ana Bottosso.</p>
<blockquote>
<p><strong>+ INFO: </strong><a target="_blank" href="http://www.myspace.com/lucianosallun">www.myspace.com/lucianosallun</a></p>
</blockquote>
<a href="http://soundcloud.com/rodrigo-carreira"></a>]]></description>
			<author>elijah@metropolis.art.br (de elijah hatem)</author>
			<category>Atrações</category>
			<pubDate>Wed, 04 Aug 2010 01:23:23 +0000</pubDate>
		</item>
		<item>
			<title>Minimal Criminal LIVE (RJ)</title>
			<link>http://www.elijah.com.br/tandava/2010/atracoes-tandava/minimal-criminal-live.html</link>
			<guid>http://www.elijah.com.br/tandava/2010/atracoes-tandava/minimal-criminal-live.html</guid>
			<description><![CDATA[<p><img style="margin: 0px 10px 10px 0px; float: left;" alt="Minimal Criminal" src="http://www.elijah.com.br/tandava/2010/images/stories/atracoes/minimalcriminal.jpg" width="225" height="169" /><strong>Minimal Criminal</strong> é o projeto iniciado em 2003 por Valério Zhyin e Bruno Echoes, mistura minimal techno/house, progressive trance and electro.</p>
<p>Produz musicas variando entre 125 e 140 BPM que apresentam atmosferas darks, profundas e hipnóticas.<strong>&nbsp;</strong></p>
<blockquote>
<p><strong>+ INFO: </strong><a target="_blank" href="http://www.minimal-criminal.com">www.minimal-criminal.com</a></p>
</blockquote>
<p><strong>+ VÍDEOS</strong></p>
<p style="text-align: center;">
<object codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=10,0,32,18" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" type="application/x-shockwave-flash" height="385" width="600">
<param name="allowFullScreen" value="true" />
<param name="allowScriptAccess" value="always" />
<param name="src" value="http://www.youtube.com/v/-gQu-5qn89o&amp;color1=0xb1b1b1&amp;color2=0xd0d0d0&amp;hl=en_US&amp;feature=player_embedded&amp;fs=1" /><embed height="385" width="600" src="http://www.youtube.com/v/-gQu-5qn89o&amp;color1=0xb1b1b1&amp;color2=0xd0d0d0&amp;hl=en_US&amp;feature=player_embedded&amp;fs=1" allowscriptaccess="always" allowfullscreen="true" type="application/x-shockwave-flash"></embed>
</object>
</p>
<p style="text-align: center;">
<object codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=10,0,32,18" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" type="application/x-shockwave-flash" height="385" width="600">
<param name="allowFullScreen" value="true" />
<param name="allowScriptAccess" value="always" />
<param name="src" value="http://www.youtube.com/v/LEmceo-ZSkI&amp;color1=0xb1b1b1&amp;color2=0xd0d0d0&amp;hl=en_US&amp;feature=player_embedded&amp;fs=1" /><embed height="385" width="600" src="http://www.youtube.com/v/LEmceo-ZSkI&amp;color1=0xb1b1b1&amp;color2=0xd0d0d0&amp;hl=en_US&amp;feature=player_embedded&amp;fs=1" allowscriptaccess="always" allowfullscreen="true" type="application/x-shockwave-flash"></embed>
</object>
</p>
<p style="text-align: center;">
<object codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=10,0,32,18" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" type="application/x-shockwave-flash" height="385" width="600">
<param name="allowFullScreen" value="true" />
<param name="allowScriptAccess" value="always" />
<param name="src" value="http://www.youtube.com/v/wXGggYSq8Ks&amp;color1=0x3a3a3a&amp;color2=0x999999&amp;hl=en_US&amp;feature=player_embedded&amp;fs=1" /><embed height="385" width="600" src="http://www.youtube.com/v/wXGggYSq8Ks&amp;color1=0x3a3a3a&amp;color2=0x999999&amp;hl=en_US&amp;feature=player_embedded&amp;fs=1" allowscriptaccess="always" allowfullscreen="true" type="application/x-shockwave-flash"></embed>
</object>
</p>]]></description>
			<author>elijah@metropolis.art.br (de elijah hatem)</author>
			<category>Atrações</category>
			<pubDate>Mon, 02 Aug 2010 15:43:33 +0000</pubDate>
		</item>
		<item>
			<title>Demonizz LIVE (SP)</title>
			<link>http://www.elijah.com.br/tandava/2010/atracoes-tandava/demonizz-live-sp.html</link>
			<guid>http://www.elijah.com.br/tandava/2010/atracoes-tandava/demonizz-live-sp.html</guid>
			<description><![CDATA[<p style="text-align: justify;"><img style="margin: 0px 10px 10px 0px; float: left;" alt="demonizz" src="http://www.elijah.com.br/tandava/2010/images/stories/atracoes/demonizz.jpg" width="225" height="289" /></p>
<p align="justify">Os DJs paulistanos <strong>Leandro Morales</strong> e <strong>Thiago Pasetchny</strong> uniram-se no ano de 2005 para dar inicio as suas proprias produções e atualmente, encontram-se ao lado dos mais importantes projetos da cena “Dark Trance” brasileira.<br /><br />Após percorrer um longo caminho, o projeto <strong>Demonizz </strong>alcança um dos principais objetivos: seu mais recente trabalho em parceria com os projetos Bash e Necropsycho no album ‘Extreme Deformities’, que logo após o lançamento obteve resultados extremamente satisfatórios, conquistando espaço no respeitado chart de Goa Gil.<br /><br />Apresentando-se nos principais festivais do Brasil, tais como, Universo Paralello (2008 e 2009), Festival Fora do Tempo (2008), Soulvision Festival (2009) e Indepen.dance Festival no Rio Grande do Sul (2007), Leandro e Thiago além de residentes dos núcleos Makunba em Curitiba e Virada Cultural em São Paulo, tornam-se no inicio do ano de 2009 parte integrante do casting da agência 4ideas, responsável pelos eventos mais conceituais da cena psicodélica no Brasil.<br /><br />As músicas do Demonizz prometem uma experiência única em sua composição, cuja característica mais evidente é o ritmo hipnótico de suas baterias, basslines pesados e sintetizadores ácidos e confusos.<br /><br />Sem deixar o dance floor morrer, Demonizz faz uma fusão de dark trance com diversas influências da musica eletronica, criando uma atmosfera alucinante nas pistas.</p>
<blockquote>
<p><strong>+ INFO: </strong><a target="_blank" href="http://www.demonizz.com">www.demonizz.com</a></p>
</blockquote>]]></description>
			<author>elijah@metropolis.art.br (de elijah hatem)</author>
			<category>Atrações</category>
			<pubDate>Wed, 04 Aug 2010 01:30:11 +0000</pubDate>
		</item>
		<item>
			<title>Truati LIVE (SP)</title>
			<link>http://www.elijah.com.br/tandava/2010/atracoes-tandava/truati-live-sp.html</link>
			<guid>http://www.elijah.com.br/tandava/2010/atracoes-tandava/truati-live-sp.html</guid>
			<description><![CDATA[<p style="text-align: justify;"><img style="margin: 0px 10px 10px 0px; float: left;" alt="truati-live" src="http://www.elijah.com.br/tandava/2010/images/stories/atracoes/truati-live.jpg" width="225" height="290" /></p>
<p style="text-align: justify;"><strong>Estevão  Melchior</strong> é brasileiro, nascido em Sorocaba, interior de São Paulo. Em  meados de 2002 iniciou suas produções musicais com seu projeto <strong>Truati</strong>.  De seu home-studio saíram músicas influenciadas pelo som do trance e do  house progressivo.</p>
<p style="text-align: justify;">Hoje  Truati trilha um caminho próprio. Usa com sabedoria suas influências na  criação de um tech-house com baixos marcantes e elementos de house. Já  coleciona belas posições nos charts do Beatport e diversos outros sites  de venda de musica digital.</p>
<p style="text-align: justify;">Foi  recentemente o vencedor da categoria Underground do concurso de novos  talentos da TRIBALTECH, um dos mais importantes festivais de musica  eletrônica do Brasil, onde se apresentou ao lado de grandes nomes como:  Format B, Tim Healey, Chris Lake, Marc Romboy, David Amo &amp; Julio  Navas, Beckers, D.Ramirez, Southmen, Gui Boratto e Maetrik.</p>
<p style="text-align: justify;"><strong>TRUATI</strong> integra o seleto time de produtores da Synk Records e Lo Kik Recods,  que já lançou hits assinados por Gui Boratto, Anderson Noise, Paolo  Mojo, Wehbba, Gabe, Marcelo V.O.R, D-Nox&amp;Beckers, Gleen Morrinson,  Mora&amp;Naccarati, Rafael Noronha, entre outros. Lançou faixas por  importantes selos nacionais e internacionais e ficou conhecido com o  remix da musica “Terraza” dos DJs e produtores Mora&amp;Naccarati.</p>
<p style="text-align: justify;">Após  alguns anos em estúdio aprimorando sua técnica e experimentando novas  sonoridades, Truati vem com uma apresentação ao vivo repleta de  novidades e improvisos, trazendo em seu repertório lançamentos  quentíssimos, além de músicas próprias que fazem parte do case de muitos  DJs pelo mundo.</p>
<blockquote>
<p><strong>+ INFO: </strong><a target="_blank" href="http://www.myspace.com/truatimusic">www.myspace.com/truatimusic</a></p>
</blockquote>
<p style="text-align: justify;"><strong>+ MÚSICAS / DJ SETs / MIXTAPES:</strong></p>
<object codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=10,0,32,18" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" type="application/x-shockwave-flash" height="225" width="100%">
<param name="allowscriptaccess" value="always" />
<param name="src" value="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fsoundcloud.com%2Ftruati&amp;secret_url=false" /><embed height="225" width="100%" src="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fsoundcloud.com%2Ftruati&amp;secret_url=false" allowscriptaccess="always" type="application/x-shockwave-flash"></embed>
</object>
<span><a href="http://soundcloud.com/truati">Latest tracks by Truati</a></span>]]></description>
			<author>elijah@metropolis.art.br (de elijah hatem)</author>
			<category>Atrações</category>
			<pubDate>Wed, 04 Aug 2010 01:04:13 +0000</pubDate>
		</item>
	</channel>
</rss>
