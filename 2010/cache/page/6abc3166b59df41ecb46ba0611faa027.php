<?php die("Access Denied"); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br" >
<head>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-2048503-7']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
	  <base href="http://www.elijah.com.br/tandava/2010/atracoes-tandava.html" />
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="robots" content="index, follow" />
  <meta name="keywords" content="tandava, gathering, festival, 2010, curitiba, novembro" />
  <meta name="og:title" content="Rex (África do Sul)" />
  <meta name="og:site_name" content="Tandava Gathering" />
  <meta name="og:image" content="http://www.elijah.com.br/tandava/2010/images/stories/atracoes/REX.jpg" />
  <meta name="description" content="Não surgiu um novo conceito. As coisas não vão mudar e nada será resgatado.  Não  é nada melhor ou maior de tudo que você já viu. Não  é inédito e nem ao menos é um evento. O respeito existe, mas o publico não, existe um encontro entre amigos e amigos de amigos." />
  <meta name="generator" content="Joomla! 1.5 - Open Source Content Management" />
  <title>Atrações do Tandava Gathering</title>
  <link href="/tandava/2010/atracoes-tandava.feed?type=rss" rel="alternate" type="application/rss+xml" title="RSS 2.0" />
  <link href="/tandava/2010/atracoes-tandava.feed?type=atom" rel="alternate" type="application/atom+xml" title="Atom 1.0" />
  <link href="/tandava/2010/favicon.ico" rel="shortcut icon" type="image/x-icon" />
  <link rel="stylesheet" href="/tandava/2010/plugins/system/rokbox/themes/dark/rokbox-style.css" type="text/css" />
  <link rel="stylesheet" href="/tandava/2010/components/com_gantry/css/gantry.css" type="text/css" />
  <link rel="stylesheet" href="/tandava/2010/components/com_gantry/css/grid-12.css" type="text/css" />
  <link rel="stylesheet" href="/tandava/2010/components/com_gantry/css/joomla.css" type="text/css" />
  <link rel="stylesheet" href="/tandava/2010/templates/rt_juxta_j15/css/joomla.css" type="text/css" />
  <link rel="stylesheet" href="/tandava/2010/templates/rt_juxta_j15/css/style3.css" type="text/css" />
  <link rel="stylesheet" href="/tandava/2010/templates/rt_juxta_j15/css/demo-styles.css" type="text/css" />
  <link rel="stylesheet" href="/tandava/2010/templates/rt_juxta_j15/css/template.css" type="text/css" />
  <link rel="stylesheet" href="/tandava/2010/templates/rt_juxta_j15/css/template-chrome-win.css" type="text/css" />
  <link rel="stylesheet" href="/tandava/2010/templates/rt_juxta_j15/css/typography.css" type="text/css" />
  <link rel="stylesheet" href="/tandava/2010/templates/rt_juxta_j15/css/backgrounds.css" type="text/css" />
  <link rel="stylesheet" href="/tandava/2010/templates/rt_juxta_j15/css/fusionmenu.css" type="text/css" />
  <style type="text/css">
    <!--
#rt-main-surround ul.menu li.active > a, #rt-main-surround ul.menu li.active > .separator, #rt-main-surround ul.menu li.active > .item, #rt-main-surround .square4 ul.menu li:hover > a, #rt-main-surround .square4 ul.menu li:hover > .item, #rt-main-surround .square4 ul.menu li:hover > .separator, .roktabs-links ul li.active span, .menutop li:hover > .item, .menutop li.f-menuparent-itemfocus .item, .menutop li.active > .item {color:#701110;}
a, .button, #rt-main-surround ul.menu a:hover, #rt-main-surround ul.menu .separator:hover, #rt-main-surround ul.menu .item:hover, .title1 .module-title .title {color:#701110;}body #rt-logo {width:600px;height:200px;}
    -->
  </style>
  <script type="text/javascript" src="/tandava/2010/media/system/js/mootools.js"></script>
  <script type="text/javascript" src="/tandava/2010/media/system/js/caption.js"></script>
  <script type="text/javascript" src="/tandava/2010/plugins/system/rokbox/rokbox.js"></script>
  <script type="text/javascript" src="/tandava/2010/plugins/system/rokbox/themes/dark/rokbox-config.js"></script>
  <script type="text/javascript" src="/tandava/2010/components/com_gantry/js/gantry-buildspans.js"></script>
  <script type="text/javascript" src="/tandava/2010/components/com_gantry/js/gantry-inputs.js"></script>
  <script type="text/javascript" src="/tandava/2010/modules/mod_roknavmenu/themes/fusion/js/fusion.js"></script>
  <script type="text/javascript">
var rokboxPath = '/tandava/2010/plugins/system/rokbox/';
			window.addEvent('domready', function() {
				var modules = ['rt-block'];
				var header = ['h3','h2','h1'];
				GantryBuildSpans(modules, header);
			});
		InputsExclusion.push('.content_vote','#rt-popup','#vmMainPage')
		        window.addEvent('load', function() {
					new Fusion('ul.menutop', {
						pill: 0,
						effect: 'slide and fade',
						opacity: 1,
						hideDelay: 500,
						centered: 0,
						tweakInitial: {'x': 9, 'y': 6},
        				tweakSubsequent: {'x': 0, 'y': -14},
						menuFx: {duration: 200, transition: Fx.Transitions.Sine.easeOut},
						pillFx: {duration: 400, transition: Fx.Transitions.Back.easeOut}
					});
	            });
  </script>
  <script type='text/javascript'>
/*<![CDATA[*/
	var jax_live_site = 'http://www.elijah.com.br/tandava/2010/index.php';
	var jax_site_type = '1.5';
/*]]>*/
</script><script type="text/javascript" src="http://www.elijah.com.br/tandava/2010/plugins/system/pc_includes/ajax_1.3.js"></script>
</head>
	<body  class="backgroundlevel-high backgroundstyle-style3 bodylevel-high cssstyle-style3 font-family-optima font-size-is-large menu-type-fusionmenu col12 ">
		<div id="rt-mainbg-overlay">
			<div class="rt-surround-wrap"><div class="rt-surround"><div class="rt-surround2"><div class="rt-surround3">
				<div class="rt-container">
										<div id="rt-drawer">
												<div class="clear"></div>
					</div>
															<div id="rt-header-wrap"><div id="rt-header-wrap2">
												<div id="rt-header-graphic">
																				<div class="rt-header-padding">
																							<div id="rt-header">
									<div class="rt-grid-12 rt-alpha rt-omega">
    			<div class="rt-block">
    	    	<a href="/tandava/2010/" id="rt-logo"></a>
    		</div>
	    
</div>
									<div class="clear"></div>
								</div>
																								<div id="rt-navigation"><div id="rt-navigation2"><div id="rt-navigation3">
									
<div class="nopill">
	<ul class="menutop level1 " >
						<li class="item1 root" >
					<a class="orphan item bullet" href="http://www.elijah.com.br/tandava/2010/"  >
				<span>
			    				Home				   
				</span>
			</a>
			
			
	</li>	
							<li class="item164 root" >
					<a class="orphan item bullet" href="/tandava/2010/o-festival.html"  >
				<span>
			    				Festival				   
				</span>
			</a>
			
			
	</li>	
							<li class="item206 root" >
					<a class="orphan item bullet" href="/tandava/2010/programacao-tandava.html"  >
				<span>
			    				Programação				   
				</span>
			</a>
			
			
	</li>	
							<li class="item172 parent active root" >
					<a class="daddy item bullet" href="/tandava/2010/atracoes-tandava.html"  >
				<span>
			    				Atrações				   
				</span>
			</a>
			
					<div class="fusion-submenu-wrapper level2 columns2">
				<div class="drop-top"></div>
				<ul class="level2 columns2">
								
							<li class="item176" >
					<a class="orphan item image" href="/tandava/2010/atracoes-tandava/funk-you.html"  >
				<span>
			    			        <img src="/tandava/2010/templates/rt_juxta_j15/images/icons/funkyou_icon.png" alt="funkyou_icon.png" />
			    				Funk You				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item196" >
					<a class="orphan item image" href="/tandava/2010/atracoes-tandava/vive-la-musique.html"  >
				<span>
			    			        <img src="/tandava/2010/templates/rt_juxta_j15/images/icons/vivelamusique_icon.png" alt="vivelamusique_icon.png" />
			    				Vive La Musique				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item204" >
					<a class="orphan item image" href="/tandava/2010/atracoes-tandava/reboot.html"  >
				<span>
			    			        <img src="/tandava/2010/templates/rt_juxta_j15/images/icons/reboot_icon.png" alt="reboot_icon.png" />
			    				Reboot				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item197" >
					<a class="orphan item image" href="/tandava/2010/atracoes-tandava/synk-recs-day-party.html"  >
				<span>
			    			        <img src="/tandava/2010/templates/rt_juxta_j15/images/icons/synk_icon.png" alt="synk_icon.png" />
			    				Synk Recs Day Party				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item192" >
					<a class="orphan item bullet" href="/tandava/2010/atracoes-tandava/rex-africa-do-sul.html"  >
				<span>
			    				Rex (África do Sul)				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item195" >
					<a class="orphan item bullet" href="/tandava/2010/atracoes-tandava/zaraus-live-portugal.html"  >
				<span>
			    				Zaraus LIVE (Portugal)				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item199" >
					<a class="orphan item bullet" href="/tandava/2010/atracoes-tandava/sallun-pedra-branca.html"  >
				<span>
			    				Sallun (Pedra Branca)				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item193" >
					<a class="orphan item bullet" href="/tandava/2010/atracoes-tandava/minimal-criminal-live.html"  >
				<span>
			    				Minimal Criminal LIVE				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item200" >
					<a class="orphan item bullet" href="/tandava/2010/atracoes-tandava/truati-live-sp.html"  >
				<span>
			    				Truati LIVE (SP)				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item198" >
					<a class="orphan item bullet" href="/tandava/2010/atracoes-tandava/demonizz-live-sp.html"  >
				<span>
			    				Demonizz LIVE (SP)				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item191" >
					<a class="orphan item bullet" href="/tandava/2010/atracoes-tandava/ver-todas.html"  >
				<span>
			    				Ver Todas...				   
				</span>
			</a>
			
			
	</li>	
										</ul>
			</div>
			
	</li>	
							<li class="item168 root" >
					<a class="orphan item bullet" href="/tandava/2010/ingressos.html"  >
				<span>
			    				Ingressos				   
				</span>
			</a>
			
			
	</li>	
							<li class="item174 parent root" >
					<a class="daddy item bullet" href="/tandava/2010/local.html"  >
				<span>
			    				Local				   
				</span>
			</a>
			
					<div class="fusion-submenu-wrapper level2">
				<div class="drop-top"></div>
				<ul class="level2">
								
							<li class="item207" >
					<a class="orphan item bullet" href="/tandava/2010/local/mapa-como-chegar-tandava-2010.html"  >
				<span>
			    				Mapa Tandava 2010				   
				</span>
			</a>
			
			
	</li>	
										</ul>
			</div>
			
	</li>	
							<li class="item173 root" >
					<a class="orphan item bullet" href="/tandava/2010/fotos.html"  >
				<span>
			    				Fotos				   
				</span>
			</a>
			
			
	</li>	
							<li class="item175 root" >
					<span class="orphan item bullet nolink">
			    <span>
			        			    Área Restrita			    			    </span>
			</span>
			
			
	</li>	
							<li class="item49 root" >
					<a class="orphan item bullet" href="/tandava/2010/contato.html"  >
				<span>
			    				Contato				   
				</span>
			</a>
			
			
	</li>	
				</ul>
</div>

								    <div class="clear"></div>
								</div></div></div>
																						</div>
																			</div>
											</div></div>
															<div id="rt-main-surround">
												<div id="rt-breadcrumbs"><div id="rt-breadcrumbs2"><div id="rt-breadcrumbs3">
								<div class="rt-breadcrumb-surround">
		<a href="http://www.elijah.com.br/tandava/2010/" id="breadcrumbs-home"></a>
		<span class="breadcrumbs pathway">
<span class="no-link">Atrações</span></span>
	</div>
	
							<div class="clear"></div>
						</div></div></div>
																							              <div id="rt-main" class="mb8-sa4">
                <div class="rt-main-inner">
                    <div class="rt-grid-8 ">
                                                						<div class="rt-block">
							<div class="default">
	                            <div id="rt-mainbody">
	                                
<div class="rt-joomla ">
	<div class="rt-blog">

				<h1 class="rt-pagetitle">Atrações do Tandava Gathering</h1>
		
				<div class="rt-description">
										<p style="text-align: justify;">A edição 2010 do Tandava teve mais de 80 horas de música em 4 dias de festival multiplicados por 2 pistas. Os artistas foram escolhidos cuidadosamente pela oganização e a diversidade sonora é o que se ouve durante o encontro.&nbsp;</p>
<p style="text-align: justify;">A pista principal contou com grandes nomes internacionais e nacionais, como <a href="index.php?option=com_content&amp;view=article&amp;id=107:zaraus-live-portugual&amp;catid=45&amp;Itemid=195"><strong>Zaraus LIVE (Portugal)</strong></a>, <a href="index.php?option=com_content&amp;view=article&amp;id=104:rex-africa-do-sul&amp;catid=45&amp;Itemid=192"><strong>Rex (África do Sul)</strong></a>, <strong><a href="index.php?option=com_content&amp;view=article&amp;id=103:minimal-criminal-live-rj&amp;catid=45&amp;Itemid=193" target="_blank">Minimal Criminal LIVE (RJ)</a></strong>&nbsp;e&nbsp;<a href="index.php?option=com_content&amp;view=article&amp;id=117:sallun-pedra-branca-sp&amp;catid=45&amp;Itemid=172"><strong>Sallun (Pedra Branca - SP)</strong></a>, recebendo no último dia uma festa especial da gravadora <strong><a href="index.php?option=com_content&amp;view=article&amp;id=116:synk-recs-day-party&amp;catid=45&amp;Itemid=172">SYNK Records</a> </strong>para o encerramento do festival com destaque para a apresentação de <a href="index.php?option=com_content&amp;view=article&amp;id=115:truati-live-sp&amp;catid=45&amp;Itemid=172"><strong>Truati LIVE</strong></a>, <a href="index.php?option=com_content&amp;view=article&amp;id=114:rodrigo-carreira-pr&amp;catid=45&amp;Itemid=172">Rodrigo Carreira</a>, <a href="index.php?option=com_content&amp;view=article&amp;id=123:gabriel-boni-pr&amp;catid=45&amp;Itemid=172">Gabriel Boni</a>, <a href="index.php?option=com_content&amp;view=article&amp;id=113:gabi-lima-pr&amp;catid=45&amp;Itemid=172">Gabi Lima</a>, Nav e <a href="index.php?option=com_content&amp;view=article&amp;id=122:rodrigo-nickel-pr&amp;catid=45&amp;Itemid=172">Rodrigo Nickel</a>.</p>
<p style="text-align: justify;">A segunda pista, o Chill Out, assume o lugar para degustar sons alternativos à noite com festas que são referência em qualidade.</p>
<p style="text-align: justify;">Na sexta-feira, comandada pelos DJs Murillo e Schasko, aconteceu a festa <a href="index.php?option=com_content&amp;view=article&amp;id=93:funk-you&amp;catid=45&amp;Itemid=176"><strong>Funk You</strong></a>, referência do Funk Old School ao New School. Na segunda noite, Sábado, hospedou a festa <a href="index.php?option=com_content&amp;view=article&amp;id=106:vive-la-musique&amp;catid=45&amp;Itemid=196"><strong>Vive La Musique</strong></a>, famosa no circuito Electro Indie "<em>New Rave" </em>em Curitiba que além dos residentes conntará com<strong></strong><strong></strong> vários convidados especiais. E, no encerramento, a festa onde são proibidas músicais originais: <strong><a target="_blank" href="index.php?option=com_content&amp;view=article&amp;id=132:reboot&amp;catid=45&amp;Itemid=204">Reboot</a></strong> com os DJs Feiges, RenanF e Murillo e performances de <strong>Yamballoon Circus Arts</strong>.</p>
<p style="text-align: justify;">Conheça melhor as atrações da edição 2010 do festival abaixo.</p>
<blockquote>
<p>"As coisas não vão mudar e nada  será&nbsp;resgatado."</p>
</blockquote>
<p><cite><a href="../">Tandava Gathering</a></cite>, 2010.</p><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/atracoes-tandava.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>					</div>
		
				<div class="rt-leading-articles">
										
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/atracoes-tandava/funk-you.html">Funk You</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<p><img style="margin: 0px 10px 10px 0px; float: left;" alt="funk-you_thumb" src="images/stories/atracoes/funk-you_thumb.jpg" height="225" width="225" />Abrindo a primeira noite do Chill Out a festa <strong>Funk You</strong>, projeto que apresenta a&nbsp; música "negra", batidas quentes e extremamente dançantes! Do <em>Funk Old School</em> ao <em>New School</em>, <em>NuFunk</em>, <em>Breaks</em>, <em>Big Beat</em>, <em>Disco</em> e <em>Hip Hop</em>.</p>
<p>Com pouco mais de dois anos, a festa já virou referencia de Funk e breaks em Curitiba comandada pelos DJs Murillo e Schasko, sempre recebendo convidados da cena curitibana e nacional.</p>
<blockquote>
<p><strong>+ INFO: </strong><a target="_blank" href="http://www.myspace.com/funkyoucwb">www.myspace.com/funkyoucwb</a></p>
</blockquote><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/atracoes-tandava/funk-you.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/atracoes-tandava/93-funk-you.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/atracoes-tandava/93-funk-you.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL3d3dy5lbGlqYWguY29tLmJyL3RhbmRhdmEvMjAxMC9hdHJhY29lcy10YW5kYXZhL2Z1bmsteW91Lmh0bWw%3D" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Quarta, 16 de Junho de 2010 22:43				</span>
					
								<span class="rt-date-modified">
					Última atualização em Terça, 13 de Julho de 2010 20:54				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>									
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/atracoes-tandava/vive-la-musique.html">Vive La Musique</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<p style="text-align: justify;"><img style="margin: 0px 10px 25px 0px; float: left;" alt="vivelamusique" src="images/stories/atracoes/vivelamusique.png" height="150" width="225" /><strong></strong></p>
<p style="text-align: justify;">Durante a noite de Sábado, o Chill Out se transforma com o som dançante da "Vive" e o espírito da cena Electro Indie curitibana.</p>
<p style="text-align: justify;">A  festa Vive La Musique é produzida pelos DJs <a href="index.php?option=com_content&amp;view=article&amp;id=110:andre-nego-pr&amp;catid=45&amp;Itemid=172">André Nego</a> e <a href="index.php?option=com_content&amp;view=article&amp;id=111:joel-guglielmini-pr&amp;catid=45&amp;Itemid=172">Joel  Guglielmini</a> e procura unir música e arte, com diversas exposições,  performances e trabalhos de novos artistas, como Estúdio Rasputines,  criadores da logo da festa.</p>
<p style="text-align: justify;">Em setembro de 2009 a Vive completou um ano de vida com 10 Edições de muito sucesso. Pela festa já passaram grandes DJs, Daniel Peixoto (ex-Montage), Raul  Aguilera, Jô Mistinguett, Barbarela, All Starz (Posh – SP), Tchiello K  (Crew – SP), Killer on The Dancefloor (Crew – SP), Boss In Drama, Dirty  Noise (RS), Elijah Hatem, dentre outros DJs da cena local de Curitiba como  Gee X, Bogus, Renan Mendes, Sandra Carraro, além do ex-residente Cacá  Azevedo.</p>
<p style="text-align: justify;">O projeto teve início no clube Soho Underground e passou pelo Espaço  Roxy, Mahatma Electro Lounge, Circus Bar,&nbsp; Kitinete, Camden Club, Era Só e atualmente segue em formato itinerante por diversos clubs do circuito alternativo curitibano.<br /> <br /> Electro, electro house, disco punk, fidget house, new rave, techno, rock  e maximal são os estilos que predominam na pista da Vive La Musique. Aguardem uma noite cheia de atrações especiais e DJs convidados!</p>
<blockquote>
<p><strong>+ INFO: </strong><a target="_blank" href="http://www.vivelamusique.com.br">www.vivelamusique.com.br</a></p>
</blockquote><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/atracoes-tandava/vive-la-musique.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/atracoes-tandava/106-vive-la-musique.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/atracoes-tandava/106-vive-la-musique.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL3d3dy5lbGlqYWguY29tLmJyL3RhbmRhdmEvMjAxMC9hdHJhY29lcy10YW5kYXZhL3ZpdmUtbGEtbXVzaXF1ZS5odG1s" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Segunda, 02 de Agosto de 2010 18:37				</span>
					
								<span class="rt-date-modified">
					Última atualização em Segunda, 02 de Agosto de 2010 21:09				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>									
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/atracoes-tandava/reboot.html">Reboot</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<p style="text-align: justify;"><img style="margin: 0px 10px 10px 0px; float: left;" alt="reboot" src="images/stories/atracoes/reboot.png" width="225" height="225" /><strong>REBOOT</strong> [ri:'bu:t] : reiniciar o computador ou sistema.</p>
<p style="text-align: justify;">Que é isso afinal? REBOOT é um projeto de releituras musicais, cujo nome é a contração de dois termos musicais (REMIXES+BOOTLEGS=RE.BOOT).</p>
<p style="text-align: justify;">REMIX: é reconstrução de uma faixa musical com base em elementos do original adicionados de novos elementos.</p>
<p style="text-align: justify;">BOOTLEG, MASHUP: ambos os termos referem-se a recriações musicais a partir da fusão de canções e instrumentais de diferentes artistas, também mesclando, em geral, gêneros musicais diversos.</p>
<p style="text-align: justify;">A autenticidade das recriações produzidas em home-studios reside exatamente na reorganização de diferentes elementos de cada original utilizado, compondo obras artísticas criativas a partir de recortes, colagens e processamento de efeitos.</p>
<p style="text-align: justify;">O produto desta técnica também ficou conhecido como BASTARD POP, por misturar estilos como Rock, Funk, Hip-Hop e Pop, sendo então considerado como filho ilegítimo deste último.</p>
<p style="text-align: justify;">O conceito não se esgota aí... já se podem ver releituras de artes visuais, remixes e mashups de filmes e video-clips musicais, até mesmo misturas de trailers, teasers, animações, fotos, cartazes e flyers. O próprio projeto também não se resume à abordagem musical e também apresenta performances cênicas e circenses.</p>
<p style="text-align: justify;">A proposta cultural do projeto REBOOT pode ser resumida no lema/desafio: PROIBIDO ORIGINAIS !!!</p>
<h4 style="text-align: justify;">Artistas:</h4>
<p style="text-align: justify;">- DJs Feiges, RenanF e Murillo</p>
<h4>- Performances:</h4>
<p style="text-align: justify;">Yamballoon Circus Arts.</p>
<p><a href="images/stories/reboot/feiges_02.jpg" rel="rokbox[400 600](reboot)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//feiges_02_thumb.jpg" alt="" /></a> <a href="images/stories/reboot/feiges_03.jpg" rel="rokbox[460 400](reboot)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//feiges_03_thumb.jpg" alt="" /></a> <a href="images/stories/reboot/feiges_05.jpg" rel="rokbox[528 400](reboot)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//feiges_05_thumb.jpg" alt="" /></a> <a href="images/stories/reboot/murillo_01.jpg" rel="rokbox[599 400](reboot)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//murillo_01_thumb.jpg" alt="" /></a> <a href="images/stories/reboot/murillo_05.jpg" rel="rokbox[400 600](reboot)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//murillo_05_thumb.jpg" alt="" /></a> <a href="images/stories/reboot/renan_02.jpg" rel="rokbox[414 400](reboot)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//renan_02_thumb.jpg" alt="" /></a> <a href="images/stories/reboot/renan_05.jpg" rel="rokbox[445 400](reboot)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//renan_05_thumb.jpg" alt="" /></a> <a href="images/stories/reboot/yamba_01.jpg" rel="rokbox[600 400](reboot)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//yamba_01_thumb.jpg" alt="" /></a> <a href="images/stories/reboot/yamba_03.jpg" rel="rokbox[400 600](reboot)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//yamba_03_thumb.jpg" alt="" /></a> </p>
<blockquote>
<p><strong>+ INFO: </strong><a target="_blank" href="http://reboot.blog.com">reboot.blog.com</a></p>
</blockquote><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/atracoes-tandava/reboot.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/atracoes-tandava/132-reboot.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/atracoes-tandava/132-reboot.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL3d3dy5lbGlqYWguY29tLmJyL3RhbmRhdmEvMjAxMC9hdHJhY29lcy10YW5kYXZhL3JlYm9vdC5odG1s" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Sexta, 15 de Outubro de 2010 19:23				</span>
					
								<span class="rt-date-modified">
					Última atualização em Segunda, 18 de Outubro de 2010 12:31				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>									
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/atracoes-tandava/synk-recs-day-party.html">Synk Recs Day Party</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<div class="rt-article-content">
<p style="text-align: justify;"><img style="margin: 0px 10px 25px 0px; float: left;" alt="synk-recs" src="images/stories/atracoes/synk-recs.png" width="225" height="150" /><strong></strong></p>
<p style="text-align: justify;">No último dia do festival – feriado de Segunda-Feira, 15 de Novembro – o <strong>Tandava Gathering</strong> recebe uma festa especial da gravadora <strong>SYNK Records </strong>para o encerramento da edição 2010.</p>
<p style="text-align: justify;">A festa inicia pela manhã e se prolonga pelo último dia com destaque para a apresentação de <a href="index.php?option=com_content&amp;view=article&amp;id=115:truati-live-sp&amp;catid=45&amp;Itemid=172"><strong>Truati LIVE</strong></a>, <a href="index.php?option=com_content&amp;view=article&amp;id=114:rodrigo-carreira-pr&amp;catid=45&amp;Itemid=172">Rodrigo Carreira</a>, <a target="_self" href="index.php?option=com_content&amp;view=article&amp;id=123:gabriel-boni-pr&amp;catid=45&amp;Itemid=172">Gabriel Boni</a>, <a href="index.php?option=com_content&amp;view=article&amp;id=113:gabi-lima-pr&amp;catid=45&amp;Itemid=172">Gabi Lima</a> e <a href="index.php?option=com_content&amp;view=article&amp;id=122:rodrigo-nickel-pr&amp;catid=45&amp;Itemid=172">Rodrigo Nickel</a>.</p>
<p style="text-align: justify;">Um encerramento com qualidade e sonoridade sem igual. Aguardem pelo Line Up completo!</p>
<p style="text-align: justify;"><strong>+ SOBRE A SYNK</strong></p>
<p>Pensando e distribuindo música digital desde 2007, a Synk Records  oferece inovação, lançando composições musicais que estimulam o público à  novas percepções criativas.</p>
<p>Transcendendo a representação de gêneros e estilos pré-definidos, a  Synk Records posiciona-se no mercado através da seleção de artistas  antenados, que misturam o uso de tecnologia de ponta, musicalidade e  ritmo para as pistas de dança.</p>
<p style="text-align: justify;">A marca Synk foi idealizada e fundada pelos artistas Rodrigo  Carreira, Leandro Pacceli e Gabi em parceria com o selo Presslab Records  (Itália), e seus integrantes: Omar Neri e Luigi Gori (Presslaboys).</p>
<blockquote>
<p><strong>+ INFO: </strong><a target="_blank" href="http://www.synk.com.br/">www.synk.com.br</a></p>
</blockquote>
</div><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/atracoes-tandava/synk-recs-day-party.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/atracoes-tandava/116-synk-recs-day-party.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/atracoes-tandava/116-synk-recs-day-party.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL3d3dy5lbGlqYWguY29tLmJyL3RhbmRhdmEvMjAxMC9hdHJhY29lcy10YW5kYXZhL3N5bmstcmVjcy1kYXktcGFydHkuaHRtbA%3D%3D" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Terça, 03 de Agosto de 2010 22:10				</span>
					
								<span class="rt-date-modified">
					Última atualização em Quinta, 09 de Setembro de 2010 12:40				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>									
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/atracoes-tandava/zaraus-live-portugal.html">Zaraus LIVE (Portugal)</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<p style="text-align: justify;"><img style="margin: 0px 10px 10px 0px; float: left;" alt="zaraus" src="images/stories/atracoes/zaraus.jpg" height="343" width="225" /><strong>Pedro Oliveira</strong> aka <strong>Zaraus</strong>, nascido em 81. Estudou Engenharia de Áudio por 3 anos, Midi e Síntese na Dance Planet em Lisboa. Escuta Trance desde 1997. Viveu no Reino Unido 2001 a 2003, na Holanda em 2003, no Brasil de 2004 a 2006 e agora mora em Lisboa.</p>
<p style="text-align: justify;">Como a maioria dos artistas de Trance de Portugal, Zaraus foi altamente influenciado pelas festas Trance "old school" de Portugal quando as festas de Psy não eram infectadas pela música regressiva, e quando apenas se tocava o verdadeiro Goa e música para frente.</p>
<p style="text-align: justify;">Algumas influências em nomes: Phyx, Clock Wise, Shift, Menog, Matt V, Man With No Name, Kox Box, Green Ohms, Cydonia, Paul Taylor, Dick Trevor, Dino Psaras, Green Nuns Of The Revolution, Disckster, Eat Static, Infected Mushroom antigo, Skazi antigo, GMS antigo, Astrix antigo e muito muitos outros...</p>
<blockquote>
<p><strong>+ INFO: </strong><a target="_blank" href="http://www.myspace.com/zaraus">www.myspace.com/zaraus</a></p>
</blockquote>
<p><strong>+ MÚSICAS:</strong></p>
<object codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=10,0,32,18" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" type="application/x-shockwave-flash" height="225" width="100%">
<param name="allowscriptaccess" value="always" />
<param name="src" value="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fsoundcloud.com%2Fzaraus&amp;secret_url=false" /><embed height="225" width="100%" src="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fsoundcloud.com%2Fzaraus&amp;secret_url=false" allowscriptaccess="always" type="application/x-shockwave-flash"></embed>
</object>
<span><a href="http://soundcloud.com/zaraus">Latest tracks by Zaraus</a></span><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/atracoes-tandava/zaraus-live-portugal.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/atracoes-tandava/107-zaraus-live-portugual.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/atracoes-tandava/107-zaraus-live-portugual.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL3d3dy5lbGlqYWguY29tLmJyL3RhbmRhdmEvMjAxMC9hdHJhY29lcy10YW5kYXZhL3phcmF1cy1saXZlLXBvcnR1Z2FsLmh0bWw%3D" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Segunda, 02 de Agosto de 2010 18:47				</span>
					
								<span class="rt-date-modified">
					Última atualização em Segunda, 02 de Agosto de 2010 20:38				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>									
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/atracoes-tandava/rex-africa-do-sul.html">Rex (África do Sul)</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<p style="text-align: justify;"><img style="margin: 0px 10px 10px 0px; float: left;" alt="REX" src="images/stories/atracoes/REX.jpg" width="225" height="169" />REX é de Cape Town, África do Sul e representa a NANO Records desde 2004.</p>
<p style="text-align: justify;">Sua carreira de DJ iniciou em 1996 quando Rex começou na cena underground como DAT Jockey, tocando GOA e evoluindo para o Psy-trance com o passar dos anos. Ao mesmo tempo em que tocava, viajava e organizava festas pelo mundo. Viveu na Europa por 6 anos, onde foi responsável pelas festas underground com o nome de Indigo Children - uma das maiores e mais notórias festas indoor e outdoor em Londres em 2003/04.</p>
<p style="text-align: justify;">Isso possibilitou com que ele entrasse em contato com diferentes artistas que o inspiraram, além de pessoas de todas as partes do mundo e com culturas muito diferentes. Tudo isso contribuiu na construção de um tipo único de música o que é ser dj e finalmente produzir música.</p>
<p style="text-align: justify;">No momento, Rex está no Brasil cuidando dos negócios da NANO na América do Sul, além, é claro, de atrair multidões com músicas da NANO de todas as partes do Mundo. Rex já se apresentou nos festivais mais importantes do Brasil como Universo Paralello, Trancendence e TranceFormation; além de festas pelos quatro cantos do pais.</p>
<blockquote>
<p><strong>+ INFO: </strong><a target="_blank" href="http://www.myspace.com/djrexnano">www.myspace.com/djrexnano</a></p>
</blockquote><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/atracoes-tandava/rex-africa-do-sul.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/atracoes-tandava/104-rex-africa-do-sul.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/atracoes-tandava/104-rex-africa-do-sul.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL3d3dy5lbGlqYWguY29tLmJyL3RhbmRhdmEvMjAxMC9hdHJhY29lcy10YW5kYXZhL3JleC1hZnJpY2EtZG8tc3VsLmh0bWw%3D" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Segunda, 02 de Agosto de 2010 12:55				</span>
					
								<span class="rt-date-modified">
					Última atualização em Segunda, 02 de Agosto de 2010 17:39				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>				</div>
		
				<div class="clear"></div>
		
				<div class="rt-pagination">
						<p class="rt-results">
				Página 1 de 7			</p>
						
<div class="tab">
<div class="tab2"><div class="page-active">Início</div></div></div>
<div class="tab">
<div class="tab2"><div class="page-active">Anterior</div></div></div>
<div class="page-block"><div class="page-active">1</div></div>
<div class="page-block"><div class="page-inactive"><a href="/tandava/2010/atracoes-tandava.html?start=6" title="2">2</a></div></div>
<div class="page-block"><div class="page-inactive"><a href="/tandava/2010/atracoes-tandava.html?start=12" title="3">3</a></div></div>
<div class="page-block"><div class="page-inactive"><a href="/tandava/2010/atracoes-tandava.html?start=18" title="4">4</a></div></div>
<div class="page-block"><div class="page-inactive"><a href="/tandava/2010/atracoes-tandava.html?start=24" title="5">5</a></div></div>
<div class="page-block"><div class="page-inactive"><a href="/tandava/2010/atracoes-tandava.html?start=30" title="6">6</a></div></div>
<div class="page-block"><div class="page-inactive"><a href="/tandava/2010/atracoes-tandava.html?start=36" title="7">7</a></div></div>
<div class="tab">
<div class="tab2"><div class="page-inactive"><a href="/tandava/2010/atracoes-tandava.html?start=6" title="Próximo">Próximo</a></div></div></div>
<div class="tab">
<div class="tab2"><div class="page-inactive"><a href="/tandava/2010/atracoes-tandava.html?start=36" title="Fim">Fim</a></div></div></div>		</div>
				
	</div>
</div>
	                            </div>
								<div class="clear"></div>
							</div>
						</div>
                                                                    </div>
                                <div class="rt-grid-4 ">
                <div id="rt-sidebar-a">
                                        <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3"><h2 class="title">Próximo Tandava...</h2></div></div></div></div>
						<div class="clear"></div>
		                						<div class="module-content">
		                	<script src="modules/mod_countdown/scripts/swfobject_modified.js" type="text/javascript"></script>

			<object id="FlashID" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="260" height="80">

  <param name="movie" value="modules/mod_countdown/countdown.swf?dayText=Dias&texto=Contagem Regressiva&hoursText=Horas&ano=2011&mont=11&dia=12&minutesText=Minutos&secondsText=Segs&displayColor=0x7011100&textoColor=0x333333&dayColor=0x333333&hoursColor=0x333333&minutesColor=0x333333&secondsColor=0x333333&pozadinaColor=0xd6d6d4&linijaColor=0xd6d6d4&trans=" />

  <param name="quality" value="high" />

  <param name="wmode" value="transparent" />

  <param name="swfversion" value="6.0.65.0" />

  

  <!-- This param tag prompts users with Flash Player 6.0 r65 and higher to download the latest version of Flash Player. Delete it if you don’t want users to see the prompt. -->

  <param name="expressinstall" value="modules/mod_countdown/scripts/expressInstall.swf" />

  <!-- Next object tag is for non-IE browsers. So hide it from IE using IECC. -->

  <!--[if !IE]>-->

  <object type="application/x-shockwave-flash" data="modules/mod_countdown/countdown.swf?dayText=Dias&texto=Contagem Regressiva&hoursText=Horas&ano=2011&mont=11&dia=12&minutesText=Minutos&secondsText=Segs&displayColor=0x701110&textoColor=0x333333&dayColor=0x333333&hoursColor=0x333333&minutesColor=0x333333&secondsColor=0x333333&pozadinaColor=0xd6d6d4&linijaColor=0xd6d6d4&trans=" width="260" height="80">

    <!--<![endif]-->

    <param name="quality" value="high" />

    <param name="wmode" value="transparent" />

    <param name="swfversion" value="6.0.65.0" />

    <param name="expressinstall" value="modules/mod_countdown/scripts/expressInstall.swf" />

	

    <!-- The browser displays the following alternative content for users with Flash Player 6.0 and older. -->

    <div>

      <h4>Content on this page requires a newer version of Adobe Flash Player.</h4>

      <p><a href="http://www.adobe.com/go/getflashplayer"><img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" width="112" height="33" /></a></p>

    </div>

    <!--[if !IE]>-->

  </object>

  <!--<![endif]-->

</object>

<script type="text/javascript">

<!--

swfobject.registerObject("FlashID");

//-->

</script>

								</div>
					</div>
				</div>
            </div>
        	                    <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3"><h2 class="title">Main Menu</h2></div></div></div></div>
						<div class="clear"></div>
		                						<div class="module-content">
		                	<ul class="menu level1" >
				<li class="item1" >
					<a class="orphan item bullet" href="http://www.elijah.com.br/tandava/2010/"  >
				<span>
			    				Home				   
				</span>
			</a>
			
			
	</li>	
					<li class="item164" >
					<a class="orphan item bullet" href="/tandava/2010/o-festival.html"  >
				<span>
			    				Festival				   
				</span>
			</a>
			
			
	</li>	
					<li class="item206" >
					<a class="orphan item bullet" href="/tandava/2010/programacao-tandava.html"  >
				<span>
			    				Programação				   
				</span>
			</a>
			
			
	</li>	
					<li class="item172 parent active" >
					<a class="daddy item bullet" href="/tandava/2010/atracoes-tandava.html"  >
				<span>
			    				Atrações				   
				</span>
			</a>
			
					<div class="fusion-submenu-wrapper level2 columns2">
				<div class="drop-top"></div>
				<ul class="level2 columns2">
								
							<li class="item176" >
					<a class="orphan item image" href="/tandava/2010/atracoes-tandava/funk-you.html"  >
				<span>
			    			        <img src="/tandava/2010/templates/rt_juxta_j15/images/icons/funkyou_icon.png" alt="funkyou_icon.png" />
			    				Funk You				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item196" >
					<a class="orphan item image" href="/tandava/2010/atracoes-tandava/vive-la-musique.html"  >
				<span>
			    			        <img src="/tandava/2010/templates/rt_juxta_j15/images/icons/vivelamusique_icon.png" alt="vivelamusique_icon.png" />
			    				Vive La Musique				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item204" >
					<a class="orphan item image" href="/tandava/2010/atracoes-tandava/reboot.html"  >
				<span>
			    			        <img src="/tandava/2010/templates/rt_juxta_j15/images/icons/reboot_icon.png" alt="reboot_icon.png" />
			    				Reboot				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item197" >
					<a class="orphan item image" href="/tandava/2010/atracoes-tandava/synk-recs-day-party.html"  >
				<span>
			    			        <img src="/tandava/2010/templates/rt_juxta_j15/images/icons/synk_icon.png" alt="synk_icon.png" />
			    				Synk Recs Day Party				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item192" >
					<a class="orphan item bullet" href="/tandava/2010/atracoes-tandava/rex-africa-do-sul.html"  >
				<span>
			    				Rex (África do Sul)				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item195" >
					<a class="orphan item bullet" href="/tandava/2010/atracoes-tandava/zaraus-live-portugal.html"  >
				<span>
			    				Zaraus LIVE (Portugal)				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item199" >
					<a class="orphan item bullet" href="/tandava/2010/atracoes-tandava/sallun-pedra-branca.html"  >
				<span>
			    				Sallun (Pedra Branca)				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item193" >
					<a class="orphan item bullet" href="/tandava/2010/atracoes-tandava/minimal-criminal-live.html"  >
				<span>
			    				Minimal Criminal LIVE				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item200" >
					<a class="orphan item bullet" href="/tandava/2010/atracoes-tandava/truati-live-sp.html"  >
				<span>
			    				Truati LIVE (SP)				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item198" >
					<a class="orphan item bullet" href="/tandava/2010/atracoes-tandava/demonizz-live-sp.html"  >
				<span>
			    				Demonizz LIVE (SP)				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item191" >
					<a class="orphan item bullet" href="/tandava/2010/atracoes-tandava/ver-todas.html"  >
				<span>
			    				Ver Todas...				   
				</span>
			</a>
			
			
	</li>	
										</ul>
			</div>
			
	</li>	
					<li class="item168" >
					<a class="orphan item bullet" href="/tandava/2010/ingressos.html"  >
				<span>
			    				Ingressos				   
				</span>
			</a>
			
			
	</li>	
					<li class="item174 parent" >
					<a class="orphan item bullet" href="/tandava/2010/local.html"  >
				<span>
			    				Local				   
				</span>
			</a>
			
			
	</li>	
					<li class="item173" >
					<a class="orphan item bullet" href="/tandava/2010/fotos.html"  >
				<span>
			    				Fotos				   
				</span>
			</a>
			
			
	</li>	
					<li class="item175" >
					<span class="orphan item bullet nolink">
			    <span>
			        			    Área Restrita			    			    </span>
			</span>
			
			
	</li>	
					<li class="item49" >
					<a class="orphan item bullet" href="/tandava/2010/contato.html"  >
				<span>
			    				Contato				   
				</span>
			</a>
			
			
	</li>	
		</ul>
						</div>
					</div>
				</div>
            </div>
        	                    <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3"><h2 class="title">Usuários Online</h2></div></div></div></div>
						<div class="clear"></div>
		                						<div class="module-content">
		                	<div>
	<ul style="list-style:none; margin: 0 0 10px; padding: 0;">
		
	
	</ul>
	
	<div>
		0 participantes		
					e 1 visitante				
		online 
		<br /><br />
        <div>
		<a href="/tandava/2010/area-restrita/comunidade/search/browse.html?sort=online">Ver Todos Participantes</a>
		</div>
	</div>
</div>						</div>
					</div>
				</div>
            </div>
        	                    <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3"><h2 class="title">Sua Opinião</h2></div></div></div></div>
						<div class="clear"></div>
		                						<div class="module-content">
		                	
<h4 class="rt-polltitle">
	Qual é a principal qualidade do Tandava para você?</h4>
<form action="index.php" method="post" name="form2" class="rt-poll">
	<fieldset>
				<div class="rt-pollrow">
			<input type="radio" name="voteid" id="voteid13" value="13" alt="13" />
			<label for="voteid13">
				Um encontro sem fins lucrativos.			</label>
		</div>
				<div class="rt-pollrow">
			<input type="radio" name="voteid" id="voteid14" value="14" alt="14" />
			<label for="voteid14">
				Reúne vários tipos de música.			</label>
		</div>
				<div class="rt-pollrow">
			<input type="radio" name="voteid" id="voteid15" value="15" alt="15" />
			<label for="voteid15">
				Clima familiar dos participantes.			</label>
		</div>
				<div class="rt-pollrow">
			<input type="radio" name="voteid" id="voteid16" value="16" alt="16" />
			<label for="voteid16">
				Entrada com bebidas liberada.			</label>
		</div>
				<div class="rt-pollrow">
			<input type="radio" name="voteid" id="voteid17" value="17" alt="17" />
			<label for="voteid17">
				Adoro acampar.			</label>
		</div>
			</fieldset>
	<div class="rt-pollbuttons">
		<div class="readon">
			<input type="submit" name="task_button" class="button" value="Votar" />
		</div>
		<div class="readon">
			<input type="button" name="option" class="button" value="Resultados" onclick="document.location.href='/tandava/2010/component/poll/15-qual-e-a-principal-qualidade-do-tandava-para-voce.html'" />
		</div>
	</div>

	<input type="hidden" name="option" value="com_poll" />
	<input type="hidden" name="task" value="vote" />
	<input type="hidden" name="id" value="15" />
	<input type="hidden" name="db90dc7238060a9fdf14583382fb70fb" value="1" /></form>
						</div>
					</div>
				</div>
            </div>
        	                    <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3"><h2 class="title">Área Restrita</h2></div></div></div></div>
						<div class="clear"></div>
		                						<div class="module-content">
		                	<form action="/tandava/2010/atracoes-tandava.html" method="post" name="login" id="form-login" >
		<fieldset class="input">
	<p id="form-login-username">
		<label for="modlgn_username">Email</label><br />
		<input id="modlgn_username" type="text" name="username" class="inputbox" alt="username" size="18" />
	</p>
	<p id="form-login-password">
		<label for="modlgn_passwd">Senha</label><br />
		<input id="modlgn_passwd" type="password" name="passwd" class="inputbox" size="18" alt="password" />
	</p>
		<p id="form-login-remember">
		<input type="checkbox" name="remember" class="checkbox" value="yes" alt="Lembrar-me" />
		<label class="remember">
			Lembrar-me		</label>
	</p>
		<div class="readon"><input type="submit" name="Submit" class="button" value="Entrar" /></div>
	</fieldset>
	<ul>
		<li>
			<a href="/tandava/2010/component/user/reset.html">
			Esqueci minha senha</a>
		</li>
		<li>
			<a href="/tandava/2010/component/user/remind.html">
			Esqueci meu nome de usuário</a>
		</li>
			</ul>
	
	<input type="hidden" name="option" value="com_user" />
	<input type="hidden" name="task" value="login" />
	<input type="hidden" name="return" value="L3RhbmRhdmEvMjAxMC9hcmVhLXJlc3RyaXRhL2NvbXVuaWRhZGUuaHRtbA==" />
	<input type="hidden" name="db90dc7238060a9fdf14583382fb70fb" value="1" /></form>
						</div>
					</div>
				</div>
            </div>
        	
                </div>
            </div>

                    <div class="clear"></div>
                </div>
            </div>
																								<div id="rt-bottom">
							<div class="rt-grid-4 rt-alpha">
                        <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3"><h2 class="title">Últimas Atualizações</h2></div></div></div></div>
						<div class="clear"></div>
		                						<div class="module-content">
		                	<ul class="latestnews">
	<li class="latestnews">
		<a href="/tandava/2010/fotos/143-fotos-do-tandava-2010.html" class="latestnews">
			Fotos do Tandava 2010</a>
	</li>
	<li class="latestnews">
		<a href="/tandava/2010/atracoes-tandava/144-o-tandava-2010.html" class="latestnews">
			O Tandava 2010</a>
	</li>
	<li class="latestnews">
		<a href="/tandava/2010/o-festival.html" class="latestnews">
			Tandava Gathering</a>
	</li>
	<li class="latestnews">
		<a href="/tandava/2010/local/mapa-como-chegar-tandava-2010.html" class="latestnews">
			Mapa Tandava 2010</a>
	</li>
	<li class="latestnews">
		<a href="/tandava/2010/local/142-sobre-o-local.html" class="latestnews">
			Sobre o Local</a>
	</li>
</ul>						</div>
					</div>
				</div>
            </div>
        	
</div>
<div class="rt-grid-4">
                        <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3"><h2 class="title">Mais Lidos</h2></div></div></div></div>
						<div class="clear"></div>
		                						<div class="module-content">
		                	<ul class="mostread">
	<li class="mostread">
		<a href="/tandava/2010/component/content/article/48-terceira-edicao/144-o-tandava-2010.html" class="mostread">
			O Tandava 2010</a>
	</li>
	<li class="mostread">
		<a href="/tandava/2010/atracoes-tandava/funk-you.html" class="mostread">
			Funk You</a>
	</li>
	<li class="mostread">
		<a href="/tandava/2010/fotos/85-fotos-tandava-2009.html" class="mostread">
			Fotos do Tandava 2009</a>
	</li>
	<li class="mostread">
		<a href="/tandava/2010/local/mapa-como-chegar-tandava-2010.html" class="mostread">
			Mapa Tandava 2010</a>
	</li>
	<li class="mostread">
		<a href="/tandava/2010/atracoes-tandava/synk-recs-day-party.html" class="mostread">
			Synk Recs Day Party</a>
	</li>
</ul>						</div>
					</div>
				</div>
            </div>
        	
</div>
<div class="rt-grid-4 rt-omega">
                        <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3"><h2 class="title">Destaque</h2></div></div></div></div>
						<div class="clear"></div>
		                						<div class="module-content">
		                	<p><strong><a href="index.php?option=com_content&amp;view=category&amp;layout=blog&amp;id=44&amp;Itemid=173"><strong>Clique Aqui</strong></a></strong> e confira as <a href="index.php?option=com_content&amp;view=category&amp;layout=blog&amp;id=44&amp;Itemid=173"><strong>fotos</strong></a> tiradas pelo <strong>MushPics </strong>nas edições anteriores do festival <strong>Tandava Gathering</strong>.</p>
<p>Esteve lá? <a href="mailto:tandava@tandava.com.br?subject=Fotos">Envie suas fotos</a> e faça parte desta história.</p>						</div>
					</div>
				</div>
            </div>
        	
</div>
							<div class="clear"></div>
						</div>
																		<div id="rt-footer"><div id="rt-footer2"><div id="rt-footer3">
							<div class="rt-grid-12 rt-alpha rt-omega">
                    <div class="title1">
                    <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3"><h2 class="title">Apoio Cultural e Parceiros</h2></div></div></div></div>
						<div class="clear"></div>
		                						<div class="module-content">
		                	<table border="0" width="100%">
<tbody>
<tr>
<td><a target="_blank" href="http://www.metropolis.art.br"><img style="margin: 10px; vertical-align: middle;" alt="metropolis" src="images/stories/parceiros/metropolis.png" width="100" height="100" /></a></td>
<td><img style="margin: 10px; vertical-align: middle;" alt="sica" src="images/stories/parceiros/sica.png" width="100" height="100" /></td>
<td><a target="_blank" href="http://www.wasabiam.com.br/"><img style="margin: 10px; vertical-align: middle;" alt="wasabi" src="images/stories/parceiros/wasabi.png" width="100" height="100" /></a></td>
<td><a target="_blank" href="http://www.myspace.com/funkyoucwb"><img style="margin: 10px; vertical-align: middle;" alt="funkyou_parceiro" src="images/stories/parceiros/funkyou_parceiro.png" width="100" height="100" /></a></td>
<td><img style="margin: 10px; vertical-align: middle;" alt="revolucione" src="images/stories/parceiros/revolucione.png" width="100" height="100" /></td>
</tr>
<tr>
<td><a target="_blank" href="http://www.egralha.com.br/"><img style="margin: 5px 10px; vertical-align: middle;" alt="egralha" src="images/stories/parceiros/egralha.png" width="100" height="50" /></a></td>
<td><a target="_blank" href="http://www.vivelamusique.com.br"><img style="margin: 5px 10px; vertical-align: middle;" alt="vivelamusique_parceiro" src="images/stories/parceiros/vivelamusique_parceiro.png" width="100" height="50" /></a></td>
<td><img style="margin: 5px 10px; vertical-align: middle;" alt="makunba" src="images/stories/parceiros/makunba.png" width="100" height="40" /></td>
<td><a target="_blank" href="http://synk.com.br/"><img style="margin: 5px 10px; vertical-align: middle;" alt="synk_parceiro" src="images/stories/parceiros/synk_parceiro.png" width="100" height="50" /></a></td>
<td><a target="_blank" href="http://www.mushpics.com/"><img style="margin: 10px; vertical-align: middle;" alt="mushpics" src="images/stories/parceiros/mushpics.png" width="100" height="50" /></a></td>
</tr>
<tr>
<td><a target="_blank" href="http://leduxcwb.wordpress.com/"><img style="margin: 5px 10px; vertical-align: middle;" alt="leduxcwb" src="images/stories/parceiros/leduxcwb.png" width="100" height="40" /></a></td>
<td><img style="margin: 5px 10px; vertical-align: middle;" alt="makana" src="images/stories/parceiros/makana.png" width="100" height="40" /></td>
<td><img style="margin: 10px; vertical-align: middle;" alt="Oxydus" src="images/stories/parceiros/oxydus_parceiro.png" width="100" height="50" /></td>
<td><a target="_blank" href="http://reboot.blog.com"><img style="margin: 10px; vertical-align: middle;" alt="reboot_parceiro" src="images/stories/parceiros/reboot_parceiro.png" width="100" height="50" /></a></td>
<td><a target="_blank" href="http://www.mzc-lab.com/"><img style="margin: 10px; vertical-align: middle;" alt="mzc-lab" src="images/stories/parceiros/mzc-lab.png" width="100" height="50" /></a></td>
</tr>
</tbody>
</table>						</div>
					</div>
				</div>
            </div>
                </div>
		
</div>
							<div class="clear"></div>
						</div></div></div>
											</div>
										<div id="rt-copyright">
						<div class="rt-grid-12 rt-alpha rt-omega">
                        <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-content">
		                	<table width="100%" border="0" cellpadding="0" cellspacing="1"><tr><td nowrap="nowrap"><a href="/tandava/2010/home.html" class="mainlevel" id="active_menu">Tandava Gathering</a><span class="mainlevel">  - </span><a href="/tandava/2010/o-festival.html" class="mainlevel" >O Festival</a><span class="mainlevel">  - </span><a href="/tandava/2010/atracoes-tandava.html" class="mainlevel" >Atrações</a><span class="mainlevel">  - </span><a href="/tandava/2010/ingressos.html" class="mainlevel" >Ingressos</a><span class="mainlevel">  - </span><a href="/tandava/2010/local.html" class="mainlevel" >Local</a><span class="mainlevel">  - </span><a href="/tandava/2010/fotos.html" class="mainlevel" >Fotos</a><span class="mainlevel">  - </span><a href="/tandava/2010/contato.html" class="mainlevel" >Contato</a><span class="mainlevel">  - </span><a href="/tandava/2010/mapa-do-site.html" class="mainlevel" >Mapa do Site</a></td></tr></table>						</div>
					</div>
				</div>
            </div>
        	
</div>
						<div class="clear"></div>
					</div>
															<div id="rt-debug">
						<div class="rt-grid-12 rt-alpha rt-omega">
                        <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-content">
		                	<p style="text-align: center;"><a href="http://validator.w3.org/check?uri=referer"><img style="margin: 0px 5px; vertical-align: middle;" src="http://www.w3.org/Icons/valid-xhtml10" alt="Valid XHTML 1.0 Transitional" height="31" width="88" /></a> <a href="http://jigsaw.w3.org/css-validator/check/referer"><img style="border: 0px none; width: 88px; height: 31px; margin: 0px 5px; vertical-align: middle;" src="http://jigsaw.w3.org/css-validator/images/vcss" alt="Valid CSS!" height="31px" width="88px" /></a><a href="http://feed2.w3.org/check.cgi?url=http%3A//www.tandava.com.br/index.php%3Fformat%3Dfeed%26type%3Drss"> <img style="margin: 0px 5px; vertical-align: middle;" src="images/stories/valid-rss-rogers.png" alt="valid-rss-rogers" title="Validate my Atom 1.0 feed" height="31" width="88" /> </a></p>
<p><span style="font-size: 9pt;">Tandava Gathering website was created by <strong>de elijah hatem</strong>. Powered by Open Source CMS <a target="_blank" href="http://www.joomla.org">Joomla 1.5</a> and developed using <a target="_blank" href="http://www.gantry-framework.org"><img style="vertical-align: bottom;" alt="gantry" src="images/stories/gantry.png" height="34" width="99" /></a> framework.</span></p>						</div>
					</div>
				</div>
            </div>
        	
</div>
						<div class="clear"></div>
					</div>
									</div>
			</div></div></div></div>
		</div>
			</body>
</html>
