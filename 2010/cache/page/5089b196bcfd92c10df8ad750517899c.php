<?php die("Access Denied"); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br" >
<head>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-2048503-7']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
	  <base href="http://elijah.com.br/tandava/2010/" />
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="robots" content="index, follow" />
  <meta name="keywords" content="tandava, gathering, festival, 2010, curitiba, novembro" />
  <meta name="og:title" content="" />
  <meta name="og:site_name" content="Tandava Gathering" />
  <meta name="og:image" content="http://elijah.com.br/tandava/2010/images/stories/youtube_tandava2008.jpg" />
  <meta name="description" content="Não surgiu um novo conceito. As coisas não vão mudar e nada será resgatado.  Não  é nada melhor ou maior de tudo que você já viu. Não  é inédito e nem ao menos é um evento. O respeito existe, mas o publico não, existe um encontro entre amigos e amigos de amigos." />
  <meta name="generator" content="Joomla! 1.5 - Open Source Content Management" />
  <title>Tandava Gathering - 4 Dias de Festival / Camping / Chill Out / Oficinas</title>
  <link href="/tandava/2010/index.php?format=feed&amp;type=rss" rel="alternate" type="application/rss+xml" title="RSS 2.0" />
  <link href="/tandava/2010/index.php?format=feed&amp;type=atom" rel="alternate" type="application/atom+xml" title="Atom 1.0" />
  <link href="/tandava/2010/favicon.ico" rel="shortcut icon" type="image/x-icon" />
  <link rel="stylesheet" href="/tandava/2010/plugins/system/rokbox/themes/dark/rokbox-style.css" type="text/css" />
  <link rel="stylesheet" href="/tandava/2010/components/com_gantry/css/gantry.css" type="text/css" />
  <link rel="stylesheet" href="/tandava/2010/components/com_gantry/css/grid-12.css" type="text/css" />
  <link rel="stylesheet" href="/tandava/2010/components/com_gantry/css/joomla.css" type="text/css" />
  <link rel="stylesheet" href="/tandava/2010/templates/rt_juxta_j15/css/joomla.css" type="text/css" />
  <link rel="stylesheet" href="/tandava/2010/templates/rt_juxta_j15/css/style3.css" type="text/css" />
  <link rel="stylesheet" href="/tandava/2010/templates/rt_juxta_j15/css/demo-styles.css" type="text/css" />
  <link rel="stylesheet" href="/tandava/2010/templates/rt_juxta_j15/css/template.css" type="text/css" />
  <link rel="stylesheet" href="/tandava/2010/templates/rt_juxta_j15/css/typography.css" type="text/css" />
  <link rel="stylesheet" href="/tandava/2010/templates/rt_juxta_j15/css/backgrounds.css" type="text/css" />
  <link rel="stylesheet" href="/tandava/2010/templates/rt_juxta_j15/css/fusionmenu.css" type="text/css" />
  <style type="text/css">
    <!--
#rt-main-surround ul.menu li.active > a, #rt-main-surround ul.menu li.active > .separator, #rt-main-surround ul.menu li.active > .item, #rt-main-surround .square4 ul.menu li:hover > a, #rt-main-surround .square4 ul.menu li:hover > .item, #rt-main-surround .square4 ul.menu li:hover > .separator, .roktabs-links ul li.active span, .menutop li:hover > .item, .menutop li.f-menuparent-itemfocus .item, .menutop li.active > .item {color:#701110;}
a, .button, #rt-main-surround ul.menu a:hover, #rt-main-surround ul.menu .separator:hover, #rt-main-surround ul.menu .item:hover, .title1 .module-title .title {color:#701110;}body #rt-logo {width:600px;height:200px;}
    -->
  </style>
  <script type="text/javascript" src="/tandava/2010/media/system/js/mootools.js"></script>
  <script type="text/javascript" src="/tandava/2010/media/system/js/caption.js"></script>
  <script type="text/javascript" src="/tandava/2010/plugins/system/rokbox/rokbox.js"></script>
  <script type="text/javascript" src="/tandava/2010/plugins/system/rokbox/themes/dark/rokbox-config.js"></script>
  <script type="text/javascript" src="/tandava/2010/components/com_gantry/js/gantry-buildspans.js"></script>
  <script type="text/javascript" src="/tandava/2010/components/com_gantry/js/gantry-inputs.js"></script>
  <script type="text/javascript" src="/tandava/2010/modules/mod_roknavmenu/themes/fusion/js/fusion.js"></script>
  <script type="text/javascript" src="/tandava/2010/modules/mod_roktabs/tmpl/roktabs.js"></script>
  <script type="text/javascript">
var rokboxPath = '/tandava/2010/plugins/system/rokbox/';
			window.addEvent('domready', function() {
				var modules = ['rt-block'];
				var header = ['h3','h2','h1'];
				GantryBuildSpans(modules, header);
			});
		InputsExclusion.push('.content_vote','#rt-popup','#vmMainPage')
		        window.addEvent('load', function() {
					new Fusion('ul.menutop', {
						pill: 0,
						effect: 'slide and fade',
						opacity: 1,
						hideDelay: 500,
						centered: 0,
						tweakInitial: {'x': 9, 'y': 6},
        				tweakSubsequent: {'x': 0, 'y': -14},
						menuFx: {duration: 200, transition: Fx.Transitions.Sine.easeOut},
						pillFx: {duration: 400, transition: Fx.Transitions.Back.easeOut}
					});
	            });
  </script>
  <script type='text/javascript'>
/*<![CDATA[*/
	var jax_live_site = 'http://elijah.com.br/tandava/2010/index.php';
	var jax_site_type = '1.5';
/*]]>*/
</script><script type="text/javascript" src="http://elijah.com.br/tandava/2010/plugins/system/pc_includes/ajax_1.3.js"></script>
</head>
	<body  class="backgroundlevel-high backgroundstyle-style3 bodylevel-high cssstyle-style3 font-family-optima font-size-is-large menu-type-fusionmenu col12 ">
		<div id="rt-mainbg-overlay">
			<div class="rt-surround-wrap"><div class="rt-surround"><div class="rt-surround2"><div class="rt-surround3">
				<div class="rt-container">
										<div id="rt-drawer">
												<div class="clear"></div>
					</div>
															<div id="rt-header-wrap"><div id="rt-header-wrap2">
												<div id="rt-header-graphic">
																				<div class="rt-header-padding">
																							<div id="rt-header">
									<div class="rt-grid-12 rt-alpha rt-omega">
    			<div class="rt-block">
    	    	<a href="/tandava/2010/" id="rt-logo"></a>
    		</div>
	    
</div>
									<div class="clear"></div>
								</div>
																								<div id="rt-navigation"><div id="rt-navigation2"><div id="rt-navigation3">
									
<div class="nopill">
	<ul class="menutop level1 " >
						<li class="item1 active root" >
					<a class="orphan item bullet" href="http://elijah.com.br/tandava/2010/"  >
				<span>
			    				Home				   
				</span>
			</a>
			
			
	</li>	
							<li class="item164 root" >
					<a class="orphan item bullet" href="/tandava/2010/o-festival.html"  >
				<span>
			    				Festival				   
				</span>
			</a>
			
			
	</li>	
							<li class="item206 root" >
					<a class="orphan item bullet" href="/tandava/2010/programacao-tandava.html"  >
				<span>
			    				Programação				   
				</span>
			</a>
			
			
	</li>	
							<li class="item172 parent root" >
					<a class="daddy item bullet" href="/tandava/2010/atracoes-tandava.html"  >
				<span>
			    				Atrações				   
				</span>
			</a>
			
					<div class="fusion-submenu-wrapper level2 columns2">
				<div class="drop-top"></div>
				<ul class="level2 columns2">
								
							<li class="item176" >
					<a class="orphan item image" href="/tandava/2010/atracoes-tandava/funk-you.html"  >
				<span>
			    			        <img src="/tandava/2010/templates/rt_juxta_j15/images/icons/funkyou_icon.png" alt="funkyou_icon.png" />
			    				Funk You				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item196" >
					<a class="orphan item image" href="/tandava/2010/atracoes-tandava/vive-la-musique.html"  >
				<span>
			    			        <img src="/tandava/2010/templates/rt_juxta_j15/images/icons/vivelamusique_icon.png" alt="vivelamusique_icon.png" />
			    				Vive La Musique				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item204" >
					<a class="orphan item image" href="/tandava/2010/atracoes-tandava/reboot.html"  >
				<span>
			    			        <img src="/tandava/2010/templates/rt_juxta_j15/images/icons/reboot_icon.png" alt="reboot_icon.png" />
			    				Reboot				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item197" >
					<a class="orphan item image" href="/tandava/2010/atracoes-tandava/synk-recs-day-party.html"  >
				<span>
			    			        <img src="/tandava/2010/templates/rt_juxta_j15/images/icons/synk_icon.png" alt="synk_icon.png" />
			    				Synk Recs Day Party				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item192" >
					<a class="orphan item bullet" href="/tandava/2010/atracoes-tandava/rex-africa-do-sul.html"  >
				<span>
			    				Rex (África do Sul)				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item195" >
					<a class="orphan item bullet" href="/tandava/2010/atracoes-tandava/zaraus-live-portugal.html"  >
				<span>
			    				Zaraus LIVE (Portugal)				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item199" >
					<a class="orphan item bullet" href="/tandava/2010/atracoes-tandava/sallun-pedra-branca.html"  >
				<span>
			    				Sallun (Pedra Branca)				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item193" >
					<a class="orphan item bullet" href="/tandava/2010/atracoes-tandava/minimal-criminal-live.html"  >
				<span>
			    				Minimal Criminal LIVE				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item200" >
					<a class="orphan item bullet" href="/tandava/2010/atracoes-tandava/truati-live-sp.html"  >
				<span>
			    				Truati LIVE (SP)				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item198" >
					<a class="orphan item bullet" href="/tandava/2010/atracoes-tandava/demonizz-live-sp.html"  >
				<span>
			    				Demonizz LIVE (SP)				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item191" >
					<a class="orphan item bullet" href="/tandava/2010/atracoes-tandava/ver-todas.html"  >
				<span>
			    				Ver Todas...				   
				</span>
			</a>
			
			
	</li>	
										</ul>
			</div>
			
	</li>	
							<li class="item168 root" >
					<a class="orphan item bullet" href="/tandava/2010/ingressos.html"  >
				<span>
			    				Ingressos				   
				</span>
			</a>
			
			
	</li>	
							<li class="item174 parent root" >
					<a class="daddy item bullet" href="/tandava/2010/local.html"  >
				<span>
			    				Local				   
				</span>
			</a>
			
					<div class="fusion-submenu-wrapper level2">
				<div class="drop-top"></div>
				<ul class="level2">
								
							<li class="item207" >
					<a class="orphan item bullet" href="/tandava/2010/local/mapa-como-chegar-tandava-2010.html"  >
				<span>
			    				Mapa Tandava 2010				   
				</span>
			</a>
			
			
	</li>	
										</ul>
			</div>
			
	</li>	
							<li class="item173 root" >
					<a class="orphan item bullet" href="/tandava/2010/fotos.html"  >
				<span>
			    				Fotos				   
				</span>
			</a>
			
			
	</li>	
							<li class="item175 root" >
					<span class="orphan item bullet nolink">
			    <span>
			        			    Área Restrita			    			    </span>
			</span>
			
			
	</li>	
							<li class="item49 root" >
					<a class="orphan item bullet" href="/tandava/2010/contato.html"  >
				<span>
			    				Contato				   
				</span>
			</a>
			
			
	</li>	
				</ul>
</div>

								    <div class="clear"></div>
								</div></div></div>
																						</div>
																			</div>
											</div></div>
															<div id="rt-main-surround">
												<div id="rt-breadcrumbs"><div id="rt-breadcrumbs2"><div id="rt-breadcrumbs3">
								<div class="rt-breadcrumb-surround">
		<a href="http://elijah.com.br/tandava/2010/" id="breadcrumbs-home"></a>
		<span class="breadcrumbs pathway">
</span>
	</div>
	
							<div class="clear"></div>
						</div></div></div>
																							              <div id="rt-main" class="mb8-sa4">
                <div class="rt-main-inner">
                    <div class="rt-grid-8 ">
                                                						<div class="rt-block">
							<div class="default">
	                            <div id="rt-mainbody">
	                                
<div class="rt-joomla ">
	<div class="rt-blog">

		
				<div class="rt-leading-articles">
										
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			O Tandava 2010			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
			
				
			<h4>Pela simples e convicta crença na cena underground...</h4>
<p style="text-align: justify;">O <strong>Tandava 2010</strong> foi prova da existência de harmonia entre a diversidade.</p>
<p style="text-align: justify;">Mostrou o bom senso de uma geração, ou mais de uma, capaz de assumir identidade própria <img style="margin: 10px 0px 10px 20px; float: right;" alt="tandava2010-mamao-2" src="images/stories/home/tandava2010-mamao-2.jpg" width="200" height="133" />e se auto-organizar, indiferentes a tabus e estereotipos. Esse mérito pertence a todos os presentes,  que de alguma maneira foram para lá dispostos a interagir, conhecer pessoas novas, e lutar pela manutenção dessa sensação familiar de um encontro.</p>
<p style="text-align: justify;">Não existem planos para tornar o <strong>Tandava</strong> um grande festival ou um evento, a intenção será sempre a mesma: Boa música em um local agradavel e na presença de Amigos.</p>
<p style="text-align: justify;">Com tantos <em>feedbacks</em> positivos e novas amizades dispostas a ajudar na construção do tandava, com grande prazer já começa a tomar forma a próxima aventura em 2011.</p>
<p style="text-align: justify;">Contando com 1 dia a mais de duração, está programado para entre os dias 11/11/11 e 15/11/11 a 4ª edição do Tandava, contamos com todos os envolvidos nessa edição e  todos aqueles dispostos a se dedicar a evolução sadia dessa idéia.</p>
<p style="text-align: justify;">A satisfação é inegável, <strong>OBRIGADO</strong>.</p>
<p><img style="margin: 10px auto; vertical-align: middle; display: block;" alt="tandava2010-mamao-3" src="images/stories/home/tandava2010-mamao-3.jpg" width="480" height="270" /></p>
<div class="camera">
<div class="typo-icon">Em breve, mais fotos e cobertura completa da terceira edição do Tandava.</div>
</div>
<blockquote>
<p style="text-align: justify;">In Lak'Ech. Somos Todos Um.</p>
</blockquote>
<p><cite><a href="../">Tandava Gathering</a></cite>, 2010</p><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://elijah.com.br/tandava/2010/component/content/article/48-terceira-edicao/144-o-tandava-2010.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>			
			
						
									<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/index.php?view=article&amp;catid=48:terceira-edicao&amp;id=144:o-tandava-2010&amp;format=pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/index.php?view=article&amp;catid=48:terceira-edicao&amp;id=144:o-tandava-2010&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL2VsaWphaC5jb20uYnIvdGFuZGF2YS8yMDEwL2NvbXBvbmVudC9jb250ZW50L2FydGljbGUvNDgtdGVyY2VpcmEtZWRpY2FvLzE0NC1vLXRhbmRhdmEtMjAxMC5odG1s" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
					
								<span class="rt-date-posted">
					Quinta, 18 de Novembro de 2010 16:01				</span>
					
								<span class="rt-date-modified">
					Última atualização em Quinta, 18 de Novembro de 2010 17:06				</span>
						
								<span class="rt-author">
					Postado por Ahlan Droid				</span>
					
							</div></div></div>
					</div>
		
				<div class="clear"></div>
	</div>
</div>				</div>
		
				<div class="clear"></div>
		
		
	</div>
</div>
	                            </div>
								<div class="clear"></div>
							</div>
						</div>
                                                                    </div>
                                <div class="rt-grid-4 ">
                <div id="rt-sidebar-a">
                                        <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3"><h2 class="title">Próximo Tandava...</h2></div></div></div></div>
						<div class="clear"></div>
		                						<div class="module-content">
		                	<script src="modules/mod_countdown/scripts/swfobject_modified.js" type="text/javascript"></script>

			<object id="FlashID" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="260" height="80">

  <param name="movie" value="modules/mod_countdown/countdown.swf?dayText=Dias&texto=Contagem Regressiva&hoursText=Horas&ano=2011&mont=11&dia=12&minutesText=Minutos&secondsText=Segs&displayColor=0x7011100&textoColor=0x333333&dayColor=0x333333&hoursColor=0x333333&minutesColor=0x333333&secondsColor=0x333333&pozadinaColor=0xd6d6d4&linijaColor=0xd6d6d4&trans=" />

  <param name="quality" value="high" />

  <param name="wmode" value="transparent" />

  <param name="swfversion" value="6.0.65.0" />

  

  <!-- This param tag prompts users with Flash Player 6.0 r65 and higher to download the latest version of Flash Player. Delete it if you don’t want users to see the prompt. -->

  <param name="expressinstall" value="modules/mod_countdown/scripts/expressInstall.swf" />

  <!-- Next object tag is for non-IE browsers. So hide it from IE using IECC. -->

  <!--[if !IE]>-->

  <object type="application/x-shockwave-flash" data="modules/mod_countdown/countdown.swf?dayText=Dias&texto=Contagem Regressiva&hoursText=Horas&ano=2011&mont=11&dia=12&minutesText=Minutos&secondsText=Segs&displayColor=0x701110&textoColor=0x333333&dayColor=0x333333&hoursColor=0x333333&minutesColor=0x333333&secondsColor=0x333333&pozadinaColor=0xd6d6d4&linijaColor=0xd6d6d4&trans=" width="260" height="80">

    <!--<![endif]-->

    <param name="quality" value="high" />

    <param name="wmode" value="transparent" />

    <param name="swfversion" value="6.0.65.0" />

    <param name="expressinstall" value="modules/mod_countdown/scripts/expressInstall.swf" />

	

    <!-- The browser displays the following alternative content for users with Flash Player 6.0 and older. -->

    <div>

      <h4>Content on this page requires a newer version of Adobe Flash Player.</h4>

      <p><a href="http://www.adobe.com/go/getflashplayer"><img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" width="112" height="33" /></a></p>

    </div>

    <!--[if !IE]>-->

  </object>

  <!--<![endif]-->

</object>

<script type="text/javascript">

<!--

swfobject.registerObject("FlashID");

//-->

</script>

								</div>
					</div>
				</div>
            </div>
        	                    <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3"><h2 class="title">Main Menu</h2></div></div></div></div>
						<div class="clear"></div>
		                						<div class="module-content">
		                	<ul class="menu level1" >
				<li class="item1 active" >
					<a class="orphan item bullet" href="http://elijah.com.br/tandava/2010/"  >
				<span>
			    				Home				   
				</span>
			</a>
			
			
	</li>	
					<li class="item164" >
					<a class="orphan item bullet" href="/tandava/2010/o-festival.html"  >
				<span>
			    				Festival				   
				</span>
			</a>
			
			
	</li>	
					<li class="item206" >
					<a class="orphan item bullet" href="/tandava/2010/programacao-tandava.html"  >
				<span>
			    				Programação				   
				</span>
			</a>
			
			
	</li>	
					<li class="item172 parent" >
					<a class="orphan item bullet" href="/tandava/2010/atracoes-tandava.html"  >
				<span>
			    				Atrações				   
				</span>
			</a>
			
			
	</li>	
					<li class="item168" >
					<a class="orphan item bullet" href="/tandava/2010/ingressos.html"  >
				<span>
			    				Ingressos				   
				</span>
			</a>
			
			
	</li>	
					<li class="item174 parent" >
					<a class="orphan item bullet" href="/tandava/2010/local.html"  >
				<span>
			    				Local				   
				</span>
			</a>
			
			
	</li>	
					<li class="item173" >
					<a class="orphan item bullet" href="/tandava/2010/fotos.html"  >
				<span>
			    				Fotos				   
				</span>
			</a>
			
			
	</li>	
					<li class="item175" >
					<span class="orphan item bullet nolink">
			    <span>
			        			    Área Restrita			    			    </span>
			</span>
			
			
	</li>	
					<li class="item49" >
					<a class="orphan item bullet" href="/tandava/2010/contato.html"  >
				<span>
			    				Contato				   
				</span>
			</a>
			
			
	</li>	
		</ul>
						</div>
					</div>
				</div>
            </div>
        	                    <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3"><h2 class="title">Usuários Online</h2></div></div></div></div>
						<div class="clear"></div>
		                						<div class="module-content">
		                	<div>
	<ul style="list-style:none; margin: 0 0 10px; padding: 0;">
		
	
	</ul>
	
	<div>
		0 participantes		
					e 1 visitante				
		online 
		<br /><br />
        <div>
		<a href="/tandava/2010/area-restrita/comunidade/search/browse.html?sort=online">Ver Todos Participantes</a>
		</div>
	</div>
</div>						</div>
					</div>
				</div>
            </div>
        	                    <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3"><h2 class="title">Área Restrita</h2></div></div></div></div>
						<div class="clear"></div>
		                						<div class="module-content">
		                	<form action="/tandava/2010/index.php" method="post" name="login" id="form-login" >
		<fieldset class="input">
	<p id="form-login-username">
		<label for="modlgn_username">Email</label><br />
		<input id="modlgn_username" type="text" name="username" class="inputbox" alt="username" size="18" />
	</p>
	<p id="form-login-password">
		<label for="modlgn_passwd">Senha</label><br />
		<input id="modlgn_passwd" type="password" name="passwd" class="inputbox" size="18" alt="password" />
	</p>
		<p id="form-login-remember">
		<input type="checkbox" name="remember" class="checkbox" value="yes" alt="Lembrar-me" />
		<label class="remember">
			Lembrar-me		</label>
	</p>
		<div class="readon"><input type="submit" name="Submit" class="button" value="Entrar" /></div>
	</fieldset>
	<ul>
		<li>
			<a href="/tandava/2010/component/user/reset.html">
			Esqueci minha senha</a>
		</li>
		<li>
			<a href="/tandava/2010/component/user/remind.html">
			Esqueci meu nome de usuário</a>
		</li>
			</ul>
	
	<input type="hidden" name="option" value="com_user" />
	<input type="hidden" name="task" value="login" />
	<input type="hidden" name="return" value="L3RhbmRhdmEvMjAxMC9hcmVhLXJlc3RyaXRhL2NvbXVuaWRhZGUuaHRtbA==" />
	<input type="hidden" name="adee329c38009e0c6cec390c0185c1b3" value="1" /></form>
						</div>
					</div>
				</div>
            </div>
        	
                </div>
            </div>

                    <div class="clear"></div>
                </div>
            </div>
																		<div id="rt-mainbottom"><div id="rt-mainbottom2"><div id="rt-mainbottom3">
							<div class="rt-grid-4 rt-alpha">
                    <div class="flag2">
                    <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3"><h2 class="title">Sua Opinião</h2></div></div></div></div>
						<div class="clear"></div>
		                						<div class="module-content">
		                	
<h4 class="rt-polltitle">
	Qual é a principal qualidade do Tandava para você?</h4>
<form action="index.php" method="post" name="form2" class="rt-poll">
	<fieldset>
				<div class="rt-pollrow">
			<input type="radio" name="voteid" id="voteid13" value="13" alt="13" />
			<label for="voteid13">
				Um encontro sem fins lucrativos.			</label>
		</div>
				<div class="rt-pollrow">
			<input type="radio" name="voteid" id="voteid14" value="14" alt="14" />
			<label for="voteid14">
				Reúne vários tipos de música.			</label>
		</div>
				<div class="rt-pollrow">
			<input type="radio" name="voteid" id="voteid15" value="15" alt="15" />
			<label for="voteid15">
				Clima familiar dos participantes.			</label>
		</div>
				<div class="rt-pollrow">
			<input type="radio" name="voteid" id="voteid16" value="16" alt="16" />
			<label for="voteid16">
				Entrada com bebidas liberada.			</label>
		</div>
				<div class="rt-pollrow">
			<input type="radio" name="voteid" id="voteid17" value="17" alt="17" />
			<label for="voteid17">
				Adoro acampar.			</label>
		</div>
			</fieldset>
	<div class="rt-pollbuttons">
		<div class="readon">
			<input type="submit" name="task_button" class="button" value="Votar" />
		</div>
		<div class="readon">
			<input type="button" name="option" class="button" value="Resultados" onclick="document.location.href='/tandava/2010/component/poll/15-qual-e-a-principal-qualidade-do-tandava-para-voce.html'" />
		</div>
	</div>

	<input type="hidden" name="option" value="com_poll" />
	<input type="hidden" name="task" value="vote" />
	<input type="hidden" name="id" value="15" />
	<input type="hidden" name="adee329c38009e0c6cec390c0185c1b3" value="1" /></form>
						</div>
					</div>
				</div>
            </div>
                </div>
		
</div>
<div class="rt-grid-8 rt-omega">
                        <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-content">
		                		<script type="text/javascript">
		RokTabsOptions.mouseevent.push('click');
		RokTabsOptions.duration.push(600);
		RokTabsOptions.transition.push(Fx.Transitions.Quad.easeInOut);
		RokTabsOptions.auto.push(0);
		RokTabsOptions.delay.push(2000);
		RokTabsOptions.type.push('scrolling');
		RokTabsOptions.linksMargins.push(0);
	</script>
	<div class="roktabs-wrapper" style="width: 600px;">
		<div class="roktabs base">
			<!--<div class="roktabs-arrows">
				<span class="previous">&larr;</span>
				<span class="next">&rarr;</span>
			</div>-->
			<div class='roktabs-links' >
<ul class='roktabs-top'>
<li class="first active icon-left"><span>Tandava Gathering 2010</span></li>
<li class=" icon-left"><span>Tandava em 360º</span></li>
<li class="last icon-left"><span>Falha Nostra</span></li>
</ul>
</div>
			<div class="roktabs-container-tr">
				<div class="roktabs-container-tl">
					<div class="roktabs-container-br">
						<div class="roktabs-container-bl">
							<div class="roktabs-container-inner">
								<div class="roktabs-container-wrapper">
									<div class='roktabs-tab1'>
	<div class='wrapper'>
<p><em class="bold">O que mudou em relação às edições anteriores?</em></p>
<ul class="bullet-7">
<li>O número de participantes aumentou de 296 para 425.</li>
<li>O Chill Out assume o lugar para degustar sons alternativos e logo na abertura recebe a festa <a href="index.php?option=com_content&amp;view=article&amp;id=93:funk-you&amp;catid=45&amp;Itemid=176"><strong>Funk You</strong></a>. Na segunda noite hospeda a festa <a href="index.php?option=com_content&amp;view=article&amp;id=106:vive-la-musique&amp;catid=45&amp;Itemid=196"><strong>Vive La Musique</strong></a> e, no encerramento, a festa onde são proibidas músicais originais: <strong><a href="index.php?option=com_content&amp;view=article&amp;id=132:reboot&amp;catid=45&amp;Itemid=204">Reboot</a></strong>.</li>
<li>O ingresso terá valores diferenciados por lotes. (R$ 50 / 60 / 70)</li>
<li>O Line Up ganhou consideráveis reforços!</li>
<li>E principalmente: O festival tem 1 dia a mais de duração! Segunda&nbsp; é feriado!</li>
</ul>
<p><em class="bold">O que <strong>não </strong>mudou em relação às edições anteriores?</em></p>
<ul class="bullet-7">
<li>A intenção continua reunir amigos em um local agradável e ouvindo boa música.</li>
<li>Existirão diversas atividades culturais, o foco do festival não é a pista de dança e sim a interação entre as pessoas.</li>
<li>A entrada com bebida e comida continua liberada. Ainda haverá bar e refeições a preços baixos.</li>
<li>O TANDAVA continua <strong>sem fins lucrativos</strong>!</li>
</ul><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://elijah.com.br/tandava/2010/&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>	</div></div>
<div class='roktabs-tab2'>
	<div class='wrapper'>
<p><a href="#"><em class="bold">Foto Panorâmica - Visão 360º by MushPics</em></a></p>
<p>Os fotógrafos da MushPics.com estiveram na edição 2009 do <strong>Tandava Gathering </strong>e registraram momentos únicos. Clique na imagem abaixo e confira uma foto panorámica em 360º da segunda noite do Festival.</p>
<p style="text-align: center;"><a href="http://www.mushpics.com/panoramicas/tandava2009/index.html" rel="rokbox[fullscreen]" title=""><img class="album" src="images/stories/tandava360_thumb.jpg" alt="" /></a> </p><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://elijah.com.br/tandava/2010/&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>	</div></div>
<div class='roktabs-tab3'>
	<div class='wrapper'>
<p><a href="#"><em class="bold">"Falha Nostra" by Tatiana Stock no Tandava 2008</em></a></p>
<p>Durante a primeira edição do <strong>Tandava Gathering </strong>em 2008, a jornalista e agitadora cultural <strong>Tatiana Stock </strong>gravou depoimentos dos participantes em uma enquete "<em>O Trance seria o Rock n Roll do Século XXI?"</em>. Os mais engraçados e as falhas de edição você confere no vídeo "Falha Nostra" clicando abaixo.</p>
<p style="text-align: center;"><a href="http://www.youtube.com/watch?v=WptVT1YNwe0" rel="rokbox[640 385](tandava2008)" title="Falha Nostra by Tatiana Stock"><img class="album" src="images/stories/youtube_tandava2008.jpg" alt="Falha Nostra by Tatiana Stock" /></a> </p><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://elijah.com.br/tandava/2010/&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>	</div></div>
								</div>
							</div>
						</div>
					</div>
				</div>	
			</div>
					</div>
	</div>
	
	
						</div>
					</div>
				</div>
            </div>
        	
</div>
							<div class="clear"></div>
						</div></div></div>
																		<div id="rt-bottom">
							<div class="rt-grid-4 rt-alpha">
                        <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3"><h2 class="title">Últimas Atualizações</h2></div></div></div></div>
						<div class="clear"></div>
		                						<div class="module-content">
		                	<ul class="latestnews">
	<li class="latestnews">
		<a href="/tandava/2010/fotos/143-fotos-do-tandava-2010.html" class="latestnews">
			Fotos do Tandava 2010</a>
	</li>
	<li class="latestnews">
		<a href="/tandava/2010/component/content/article/48-terceira-edicao/144-o-tandava-2010.html" class="latestnews">
			O Tandava 2010</a>
	</li>
	<li class="latestnews">
		<a href="/tandava/2010/o-festival.html" class="latestnews">
			Tandava Gathering</a>
	</li>
	<li class="latestnews">
		<a href="/tandava/2010/local/mapa-como-chegar-tandava-2010.html" class="latestnews">
			Mapa Tandava 2010</a>
	</li>
	<li class="latestnews">
		<a href="/tandava/2010/local/142-sobre-o-local.html" class="latestnews">
			Sobre o Local</a>
	</li>
</ul>						</div>
					</div>
				</div>
            </div>
        	
</div>
<div class="rt-grid-4">
                        <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3"><h2 class="title">Mais Lidos</h2></div></div></div></div>
						<div class="clear"></div>
		                						<div class="module-content">
		                	<ul class="mostread">
	<li class="mostread">
		<a href="/tandava/2010/component/content/article/48-terceira-edicao/144-o-tandava-2010.html" class="mostread">
			O Tandava 2010</a>
	</li>
	<li class="mostread">
		<a href="/tandava/2010/atracoes-tandava/funk-you.html" class="mostread">
			Funk You</a>
	</li>
	<li class="mostread">
		<a href="/tandava/2010/fotos/85-fotos-tandava-2009.html" class="mostread">
			Fotos do Tandava 2009</a>
	</li>
	<li class="mostread">
		<a href="/tandava/2010/local/mapa-como-chegar-tandava-2010.html" class="mostread">
			Mapa Tandava 2010</a>
	</li>
	<li class="mostread">
		<a href="/tandava/2010/atracoes-tandava/synk-recs-day-party.html" class="mostread">
			Synk Recs Day Party</a>
	</li>
</ul>						</div>
					</div>
				</div>
            </div>
        	
</div>
<div class="rt-grid-4 rt-omega">
                        <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3"><h2 class="title">Destaque</h2></div></div></div></div>
						<div class="clear"></div>
		                						<div class="module-content">
		                	<p><strong><a href="index.php?option=com_content&amp;view=category&amp;layout=blog&amp;id=44&amp;Itemid=173"><strong>Clique Aqui</strong></a></strong> e confira as <a href="index.php?option=com_content&amp;view=category&amp;layout=blog&amp;id=44&amp;Itemid=173"><strong>fotos</strong></a> tiradas pelo <strong>MushPics </strong>nas edições anteriores do festival <strong>Tandava Gathering</strong>.</p>
<p>Esteve lá? <a href="mailto:tandava@tandava.com.br?subject=Fotos">Envie suas fotos</a> e faça parte desta história.</p>						</div>
					</div>
				</div>
            </div>
        	
</div>
							<div class="clear"></div>
						</div>
																		<div id="rt-footer"><div id="rt-footer2"><div id="rt-footer3">
							<div class="rt-grid-12 rt-alpha rt-omega">
                    <div class="title1">
                    <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3"><h2 class="title">Apoio Cultural e Parceiros</h2></div></div></div></div>
						<div class="clear"></div>
		                						<div class="module-content">
		                	<table border="0" width="100%">
<tbody>
<tr>
<td><a target="_blank" href="http://www.metropolis.art.br"><img style="margin: 10px; vertical-align: middle;" alt="metropolis" src="images/stories/parceiros/metropolis.png" width="100" height="100" /></a></td>
<td><img style="margin: 10px; vertical-align: middle;" alt="sica" src="images/stories/parceiros/sica.png" width="100" height="100" /></td>
<td><a target="_blank" href="http://www.wasabiam.com.br/"><img style="margin: 10px; vertical-align: middle;" alt="wasabi" src="images/stories/parceiros/wasabi.png" width="100" height="100" /></a></td>
<td><a target="_blank" href="http://www.myspace.com/funkyoucwb"><img style="margin: 10px; vertical-align: middle;" alt="funkyou_parceiro" src="images/stories/parceiros/funkyou_parceiro.png" width="100" height="100" /></a></td>
<td><img style="margin: 10px; vertical-align: middle;" alt="revolucione" src="images/stories/parceiros/revolucione.png" width="100" height="100" /></td>
</tr>
<tr>
<td><a target="_blank" href="http://www.egralha.com.br/"><img style="margin: 5px 10px; vertical-align: middle;" alt="egralha" src="images/stories/parceiros/egralha.png" width="100" height="50" /></a></td>
<td><a target="_blank" href="http://www.vivelamusique.com.br"><img style="margin: 5px 10px; vertical-align: middle;" alt="vivelamusique_parceiro" src="images/stories/parceiros/vivelamusique_parceiro.png" width="100" height="50" /></a></td>
<td><img style="margin: 5px 10px; vertical-align: middle;" alt="makunba" src="images/stories/parceiros/makunba.png" width="100" height="40" /></td>
<td><a target="_blank" href="http://synk.com.br/"><img style="margin: 5px 10px; vertical-align: middle;" alt="synk_parceiro" src="images/stories/parceiros/synk_parceiro.png" width="100" height="50" /></a></td>
<td><a target="_blank" href="http://www.mushpics.com/"><img style="margin: 10px; vertical-align: middle;" alt="mushpics" src="images/stories/parceiros/mushpics.png" width="100" height="50" /></a></td>
</tr>
<tr>
<td><a target="_blank" href="http://leduxcwb.wordpress.com/"><img style="margin: 5px 10px; vertical-align: middle;" alt="leduxcwb" src="images/stories/parceiros/leduxcwb.png" width="100" height="40" /></a></td>
<td><img style="margin: 5px 10px; vertical-align: middle;" alt="makana" src="images/stories/parceiros/makana.png" width="100" height="40" /></td>
<td><img style="margin: 10px; vertical-align: middle;" alt="Oxydus" src="images/stories/parceiros/oxydus_parceiro.png" width="100" height="50" /></td>
<td><a target="_blank" href="http://reboot.blog.com"><img style="margin: 10px; vertical-align: middle;" alt="reboot_parceiro" src="images/stories/parceiros/reboot_parceiro.png" width="100" height="50" /></a></td>
<td><a target="_blank" href="http://www.mzc-lab.com/"><img style="margin: 10px; vertical-align: middle;" alt="mzc-lab" src="images/stories/parceiros/mzc-lab.png" width="100" height="50" /></a></td>
</tr>
</tbody>
</table>						</div>
					</div>
				</div>
            </div>
                </div>
		
</div>
							<div class="clear"></div>
						</div></div></div>
											</div>
										<div id="rt-copyright">
						<div class="rt-grid-12 rt-alpha rt-omega">
                        <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-content">
		                	<table width="100%" border="0" cellpadding="0" cellspacing="1"><tr><td nowrap="nowrap"><a href="/tandava/2010/home.html" class="mainlevel" id="active_menu">Tandava Gathering</a><span class="mainlevel">  - </span><a href="/tandava/2010/o-festival.html" class="mainlevel" >O Festival</a><span class="mainlevel">  - </span><a href="/tandava/2010/atracoes-tandava.html" class="mainlevel" >Atrações</a><span class="mainlevel">  - </span><a href="/tandava/2010/ingressos.html" class="mainlevel" >Ingressos</a><span class="mainlevel">  - </span><a href="/tandava/2010/local.html" class="mainlevel" >Local</a><span class="mainlevel">  - </span><a href="/tandava/2010/fotos.html" class="mainlevel" >Fotos</a><span class="mainlevel">  - </span><a href="/tandava/2010/contato.html" class="mainlevel" >Contato</a><span class="mainlevel">  - </span><a href="/tandava/2010/mapa-do-site.html" class="mainlevel" >Mapa do Site</a></td></tr></table>						</div>
					</div>
				</div>
            </div>
        	
</div>
						<div class="clear"></div>
					</div>
															<div id="rt-debug">
						<div class="rt-grid-12 rt-alpha rt-omega">
                        <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-content">
		                	<p style="text-align: center;"><a href="http://validator.w3.org/check?uri=referer"><img style="margin: 0px 5px; vertical-align: middle;" src="http://www.w3.org/Icons/valid-xhtml10" alt="Valid XHTML 1.0 Transitional" height="31" width="88" /></a> <a href="http://jigsaw.w3.org/css-validator/check/referer"><img style="border: 0px none; width: 88px; height: 31px; margin: 0px 5px; vertical-align: middle;" src="http://jigsaw.w3.org/css-validator/images/vcss" alt="Valid CSS!" height="31px" width="88px" /></a><a href="http://feed2.w3.org/check.cgi?url=http%3A//www.tandava.com.br/index.php%3Fformat%3Dfeed%26type%3Drss"> <img style="margin: 0px 5px; vertical-align: middle;" src="images/stories/valid-rss-rogers.png" alt="valid-rss-rogers" title="Validate my Atom 1.0 feed" height="31" width="88" /> </a></p>
<p><span style="font-size: 9pt;">Tandava Gathering website was created by <strong>de elijah hatem</strong>. Powered by Open Source CMS <a target="_blank" href="http://www.joomla.org">Joomla 1.5</a> and developed using <a target="_blank" href="http://www.gantry-framework.org"><img style="vertical-align: bottom;" alt="gantry" src="images/stories/gantry.png" height="34" width="99" /></a> framework.</span></p>						</div>
					</div>
				</div>
            </div>
        	
</div>
						<div class="clear"></div>
					</div>
									</div>
			</div></div></div></div>
		</div>
			</body>
</html>
