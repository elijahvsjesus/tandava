<?php die("Access Denied"); ?>
<?xml version="1.0" encoding="utf-8"?>
<!-- generator="Joomla! 1.5 - Open Source Content Management" -->
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
	<channel>
		<title>Local do Tandava Gathering</title>
		<description>Não surgiu um novo conceito. As coisas não vão mudar e nada será resgatado.  Não  é nada melhor ou maior de tudo que você já viu. Não  é inédito e nem ao menos é um evento. O respeito existe, mas o publico não, existe um encontro entre amigos e amigos de amigos.</description>
		<link>http://www.elijah.com.br/tandava/2010/local.html</link>
		<lastBuildDate>Tue, 02 Aug 2011 16:50:29 +0000</lastBuildDate>
		<generator>Joomla! 1.5 - Open Source Content Management</generator>
		<language>pt-br</language>
		<item>
			<title>Sobre o Local</title>
			<link>http://www.elijah.com.br/tandava/2010/local/142-sobre-o-local.html</link>
			<guid>http://www.elijah.com.br/tandava/2010/local/142-sobre-o-local.html</guid>
			<description><![CDATA[<p style="text-align: justify;">A edição 2010 do <strong>Tandava Gathering</strong> aconteceu em uma propriedade com 120 alqueires totalmente integrados a natureza, situados em local privilegiado com vista para a Serra do Mar e cercados por rios de águas claras.</p>
<p style="text-align: justify;">Esse espaço é mata nativa e área de preservação ambiental, TODO CUIDADO COM O LIXO É NECESSÁRIO!</p>
<p style="text-align: justify;">Entre as paisagens, uma cachoeira liberada para banho dentro e durante todo o festival.  Trilhas levam a grutas, lagos, 2 diferentes rios e uma infinidade de belezas naturais. Passeios a cavalo e trekking estão entre as atividades desenvolvidas com acompanhamento de instrutores/guias.</p>
<h4 style="text-align: justify;">Fotos do Local</h4>
<p>{rokbox album=|tandava2010local|}images/stories/fotos/2010/local/*{/rokbox}</p>]]></description>
			<author>elijah@metropolis.art.br (de elijah hatem)</author>
			<category>Local</category>
			<pubDate>Tue, 26 Oct 2010 20:21:25 +0000</pubDate>
		</item>
		<item>
			<title>Estrutura</title>
			<link>http://www.elijah.com.br/tandava/2010/local/91-estrutura.html</link>
			<guid>http://www.elijah.com.br/tandava/2010/local/91-estrutura.html</guid>
			<description><![CDATA[<p>Toda estrutura necessária para uma estadia agradável e segura dos participantes do <strong>Tandava Gathering </strong>é planejada.</p>
<p>O local do encontro possui estrutura completa como:</p>
<ul class="bullet-c">
<li>Área para Camping.</li>
<li>Área para refeições e alimentação.</li>
<li>Banheiros femininos e masculinos.</li>
<li>Chuveiros quentes.</li>
<li>Bar e barracas com opções de alimentação.</li>
<li>Pontos de energia elétrica.</li>
<li>Será permitido fogueiras.</li>
</ul>
<h4>Refeições e Alimentação</h4>
<p>É permitida a entrada com comida no evento e também será disponibilizado área para alimentação.</p>
<p>Será servido almoço e café da manhã todos os dias (SÁB / DOM / SEG).</p>
<p>O café da manhã será servido das 07:00 as 10:30 e o almoço será servido das 12:00 as 15:30.</p>
<p>É altamente recomendado ficar atento aos horários das refeições para poder aproveitar todo o encontro com energia suficiente! Não será servido jantar em nenhum dia, porém paralelo as refeições existirão, funcionado em tempo integral, barracas com opções de alimentação (variando do fast food à alimentação vegetariana).</p>
<p><em>Obs.: O valor das refeições <strong>não</strong> está incluído no valor dos ingressos.</em></p>
<h4>Bebidas e Bar</h4>
<p>É permitida a entrada com bebidas no evento e também será disponibilizado bar no local com estrutura completa e a intenção de suprir os menos organizados que esquecerem seu kit bebidas.</p>
<p>Os preços praticados pelo bar são divulgados com antecedência, conforme tabela abaixo:</p>
<pre>Cerveja (Lata 355 ml) - R$3,00<br />Refrigerante - R$3,00<br />Água - R$2,00<br />Dose de Bacardi Apple / Vodka Smirnoff - R$5,00<br />Copo de Catuaba - R$5,00 <br />Dose de Whisky Natu Nobilis - R$5,00<br />Dose de Whisky J. Walker Red - R$8,00<br />Energético - R$8,00 <br />Copo de Gelo – R$ 1,00<br /></pre>
<ul class="bullet-c">
</ul>]]></description>
			<author>elijah@metropolis.art.br (de elijah hatem)</author>
			<category>Local</category>
			<pubDate>Thu, 17 Jun 2010 01:16:00 +0000</pubDate>
		</item>
		<item>
			<title>Mapa Tandava 2010</title>
			<link>http://www.elijah.com.br/tandava/2010/local/mapa-como-chegar-tandava-2010.html</link>
			<guid>http://www.elijah.com.br/tandava/2010/local/mapa-como-chegar-tandava-2010.html</guid>
			<description><![CDATA[<p style="text-align: justify;">A edição 2010 do <strong>Tandava Gathering</strong> aconteceu em uma  propriedade com 120 alqueires totalmente integrados a natureza, situados  em local privilegiado com vista para a Serra do Mar e cercados por rios  de águas claras.</p>
<p style="text-align: justify;">A localização e o mapa do local são divulgados apenas para todos os participantes cadastrados. Confira abaixo:</p>
<h2>Como Chegar ao Tandava 2010</h2>
<p style="text-align: justify;">O local se chama <strong>Haras Cartel </strong>e fica a cerca de 40km da Grande Curitiba. Confira o mapa abaixo, faça o <a target="_blank" href="http://www.elijah.com.br/tandava/2010/images/stories/mapa/tandava2010-mapa-como-chegar.pdf">download</a> da <a target="_blank" href="http://www.elijah.com.br/tandava/2010/images/stories/mapa/tandava2010-mapa-como-chegar.pdf">versão para impressão</a> e <a href="http://www.elijah.com.br/tandava/2010/#instrucoes">leia as instruções</a>!</p>
<p>Get ready!</p>
<h4>Visualizar Mapa</h4>
<p style="text-align: center;">{rokbox title=|Mapa. Como Chegar ao Tandava 2010|  thumb=|http://www.tandava.com.br/images/stories/mapa/tandava2010-mapa-como-chegar_thumb.jpg|}images/stories/mapa/tandava2010-mapa-como-chegar.jpg{/rokbox}</p>
<h4>Download Mapa - Versão para Impressão (PDF)</h4>
<div class="download">
<div class="typo-icon"><a target="_blank" href="http://www.elijah.com.br/tandava/2010/images/stories/mapa/tandava2010-mapa-como-chegar.pdf">Clique Aqui e baixe o Mapa e Instruções de Como Chegar ao Tandava 2010.</a></div>
</div>
<h4><a name="instrucoes"></a>Instruções</h4>
<ul class="bullet-c">
<li><strong>De Curitiba:</strong></li>
</ul>
<ul class="bullet-3">
<li>Pegar a Av. Das Torres e, após Portal de São José dos Pinhais, seguir pela BR 376 sentido Joinville.</li>
<li>Seguir por aproximadamente 40 km, passando pelo Contorno Sul, Pedágio e Posto Receita Estadual até o Posto Monte Carlo II.</li>
<li>1.4km após o Posto Monte Carlo II, entrar a direita no KM 648 próximo ao retorno.</li>
<li>Após 100m virar a direita e seguir placas indicativas para Haras Cartel de acordo com o mapa.</li>
</ul>
<ul class="bullet-c">
<li><strong>De Santa Catarina:</strong></li>
</ul>
<ul class="bullet-3">
<li>Pegar a BR 376 Joinville e seguir sentido Curitiba.</li>
<li>Após Posto Monte Carlo no KM 652, seguir por aproximadamente 5km e fazer o retorno.</li>
<li>Entrar a direita na entrada no KM 648.</li>
<li>Após 100m virar a direita e seguir placas indicativas para Haras Cartel de acordo com o mapa.</li>
</ul>
<h4>Fotos e Estrutura do Tandava 2010</h4>
<div class="camera">
<div class="typo-icon"><a href="http://www.elijah.com.br/tandava/2010/local.html">Saiba Mais sobre o local, suas belezas naturais e a estrutura do Tandava 2010.</a></div>
</div>
<h4>IMPORTANTE!</h4>
<ul class="bullet-c">
<li>Lembre-se que à noite a visibilidade é menor, então vá devagar para conseguir visualizar todas as placas.</li>
<li>Se beber, NÃO dirija!</li>
</ul>]]></description>
			<author>elijah@metropolis.art.br (de elijah hatem)</author>
			<category>Local</category>
			<pubDate>Thu, 17 Jun 2010 01:09:00 +0000</pubDate>
		</item>
	</channel>
</rss>
