<?php die("Access Denied"); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br" >
<head>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-2048503-7']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
	  <base href="http://elijah.com.br/tandava/2010/programacao-tandava.html" />
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="robots" content="index, follow" />
  <meta name="keywords" content="tandava, gathering, festival, 2010, curitiba, novembro" />
  <meta name="og:title" content="Arte, Cultura e Oficinas" />
  <meta name="og:site_name" content="Tandava Gathering" />
  <meta name="description" content="Não surgiu um novo conceito. As coisas não vão mudar e nada será resgatado.  Não  é nada melhor ou maior de tudo que você já viu. Não  é inédito e nem ao menos é um evento. O respeito existe, mas o publico não, existe um encontro entre amigos e amigos de amigos." />
  <meta name="generator" content="Joomla! 1.5 - Open Source Content Management" />
  <title>Programação do Tandava Gathering</title>
  <link href="/tandava/2010/programacao-tandava.feed?type=rss" rel="alternate" type="application/rss+xml" title="RSS 2.0" />
  <link href="/tandava/2010/programacao-tandava.feed?type=atom" rel="alternate" type="application/atom+xml" title="Atom 1.0" />
  <link href="/tandava/2010/favicon.ico" rel="shortcut icon" type="image/x-icon" />
  <link rel="stylesheet" href="/tandava/2010/plugins/system/rokbox/themes/dark/rokbox-style.css" type="text/css" />
  <link rel="stylesheet" href="/tandava/2010/components/com_gantry/css/gantry.css" type="text/css" />
  <link rel="stylesheet" href="/tandava/2010/components/com_gantry/css/grid-12.css" type="text/css" />
  <link rel="stylesheet" href="/tandava/2010/components/com_gantry/css/joomla.css" type="text/css" />
  <link rel="stylesheet" href="/tandava/2010/templates/rt_juxta_j15/css/joomla.css" type="text/css" />
  <link rel="stylesheet" href="/tandava/2010/templates/rt_juxta_j15/css/style3.css" type="text/css" />
  <link rel="stylesheet" href="/tandava/2010/templates/rt_juxta_j15/css/demo-styles.css" type="text/css" />
  <link rel="stylesheet" href="/tandava/2010/templates/rt_juxta_j15/css/template.css" type="text/css" />
  <link rel="stylesheet" href="/tandava/2010/templates/rt_juxta_j15/css/template-firefox.css" type="text/css" />
  <link rel="stylesheet" href="/tandava/2010/templates/rt_juxta_j15/css/typography.css" type="text/css" />
  <link rel="stylesheet" href="/tandava/2010/templates/rt_juxta_j15/css/backgrounds.css" type="text/css" />
  <link rel="stylesheet" href="/tandava/2010/templates/rt_juxta_j15/css/fusionmenu.css" type="text/css" />
  <style type="text/css">
    <!--
#rt-main-surround ul.menu li.active > a, #rt-main-surround ul.menu li.active > .separator, #rt-main-surround ul.menu li.active > .item, #rt-main-surround .square4 ul.menu li:hover > a, #rt-main-surround .square4 ul.menu li:hover > .item, #rt-main-surround .square4 ul.menu li:hover > .separator, .roktabs-links ul li.active span, .menutop li:hover > .item, .menutop li.f-menuparent-itemfocus .item, .menutop li.active > .item {color:#701110;}
a, .button, #rt-main-surround ul.menu a:hover, #rt-main-surround ul.menu .separator:hover, #rt-main-surround ul.menu .item:hover, .title1 .module-title .title {color:#701110;}body #rt-logo {width:600px;height:200px;}
    -->
  </style>
  <script type="text/javascript" src="/tandava/2010/media/system/js/mootools.js"></script>
  <script type="text/javascript" src="/tandava/2010/media/system/js/caption.js"></script>
  <script type="text/javascript" src="/tandava/2010/plugins/system/rokbox/rokbox.js"></script>
  <script type="text/javascript" src="/tandava/2010/plugins/system/rokbox/themes/dark/rokbox-config.js"></script>
  <script type="text/javascript" src="/tandava/2010/components/com_gantry/js/gantry-buildspans.js"></script>
  <script type="text/javascript" src="/tandava/2010/components/com_gantry/js/gantry-inputs.js"></script>
  <script type="text/javascript" src="/tandava/2010/modules/mod_roknavmenu/themes/fusion/js/fusion.js"></script>
  <script type="text/javascript">
var rokboxPath = '/tandava/2010/plugins/system/rokbox/';
			window.addEvent('domready', function() {
				var modules = ['rt-block'];
				var header = ['h3','h2','h1'];
				GantryBuildSpans(modules, header);
			});
		InputsExclusion.push('.content_vote','#rt-popup','#vmMainPage')
		        window.addEvent('load', function() {
					new Fusion('ul.menutop', {
						pill: 0,
						effect: 'slide and fade',
						opacity: 1,
						hideDelay: 500,
						centered: 0,
						tweakInitial: {'x': 9, 'y': 6},
        				tweakSubsequent: {'x': 0, 'y': -14},
						menuFx: {duration: 200, transition: Fx.Transitions.Sine.easeOut},
						pillFx: {duration: 400, transition: Fx.Transitions.Back.easeOut}
					});
	            });
  </script>
  <script type='text/javascript'>
/*<![CDATA[*/
	var jax_live_site = 'http://elijah.com.br/tandava/2010/index.php';
	var jax_site_type = '1.5';
/*]]>*/
</script><script type="text/javascript" src="http://elijah.com.br/tandava/2010/plugins/system/pc_includes/ajax_1.3.js"></script>
</head>
	<body  class="backgroundlevel-high backgroundstyle-style3 bodylevel-high cssstyle-style3 font-family-optima font-size-is-large menu-type-fusionmenu col12 ">
		<div id="rt-mainbg-overlay">
			<div class="rt-surround-wrap"><div class="rt-surround"><div class="rt-surround2"><div class="rt-surround3">
				<div class="rt-container">
										<div id="rt-drawer">
												<div class="clear"></div>
					</div>
															<div id="rt-header-wrap"><div id="rt-header-wrap2">
												<div id="rt-header-graphic">
																				<div class="rt-header-padding">
																							<div id="rt-header">
									<div class="rt-grid-12 rt-alpha rt-omega">
    			<div class="rt-block">
    	    	<a href="/tandava/2010/" id="rt-logo"></a>
    		</div>
	    
</div>
									<div class="clear"></div>
								</div>
																								<div id="rt-navigation"><div id="rt-navigation2"><div id="rt-navigation3">
									
<div class="nopill">
	<ul class="menutop level1 " >
						<li class="item1 root" >
					<a class="orphan item bullet" href="http://elijah.com.br/tandava/2010/"  >
				<span>
			    				Home				   
				</span>
			</a>
			
			
	</li>	
							<li class="item164 root" >
					<a class="orphan item bullet" href="/tandava/2010/o-festival.html"  >
				<span>
			    				Festival				   
				</span>
			</a>
			
			
	</li>	
							<li class="item206 active root" >
					<a class="orphan item bullet" href="/tandava/2010/programacao-tandava.html"  >
				<span>
			    				Programação				   
				</span>
			</a>
			
			
	</li>	
							<li class="item172 parent root" >
					<a class="daddy item bullet" href="/tandava/2010/atracoes-tandava.html"  >
				<span>
			    				Atrações				   
				</span>
			</a>
			
					<div class="fusion-submenu-wrapper level2 columns2">
				<div class="drop-top"></div>
				<ul class="level2 columns2">
								
							<li class="item176" >
					<a class="orphan item image" href="/tandava/2010/atracoes-tandava/funk-you.html"  >
				<span>
			    			        <img src="/tandava/2010/templates/rt_juxta_j15/images/icons/funkyou_icon.png" alt="funkyou_icon.png" />
			    				Funk You				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item196" >
					<a class="orphan item image" href="/tandava/2010/atracoes-tandava/vive-la-musique.html"  >
				<span>
			    			        <img src="/tandava/2010/templates/rt_juxta_j15/images/icons/vivelamusique_icon.png" alt="vivelamusique_icon.png" />
			    				Vive La Musique				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item204" >
					<a class="orphan item image" href="/tandava/2010/atracoes-tandava/reboot.html"  >
				<span>
			    			        <img src="/tandava/2010/templates/rt_juxta_j15/images/icons/reboot_icon.png" alt="reboot_icon.png" />
			    				Reboot				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item197" >
					<a class="orphan item image" href="/tandava/2010/atracoes-tandava/synk-recs-day-party.html"  >
				<span>
			    			        <img src="/tandava/2010/templates/rt_juxta_j15/images/icons/synk_icon.png" alt="synk_icon.png" />
			    				Synk Recs Day Party				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item192" >
					<a class="orphan item bullet" href="/tandava/2010/atracoes-tandava/rex-africa-do-sul.html"  >
				<span>
			    				Rex (África do Sul)				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item195" >
					<a class="orphan item bullet" href="/tandava/2010/atracoes-tandava/zaraus-live-portugal.html"  >
				<span>
			    				Zaraus LIVE (Portugal)				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item199" >
					<a class="orphan item bullet" href="/tandava/2010/atracoes-tandava/sallun-pedra-branca.html"  >
				<span>
			    				Sallun (Pedra Branca)				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item193" >
					<a class="orphan item bullet" href="/tandava/2010/atracoes-tandava/minimal-criminal-live.html"  >
				<span>
			    				Minimal Criminal LIVE				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item200" >
					<a class="orphan item bullet" href="/tandava/2010/atracoes-tandava/truati-live-sp.html"  >
				<span>
			    				Truati LIVE (SP)				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item198" >
					<a class="orphan item bullet" href="/tandava/2010/atracoes-tandava/demonizz-live-sp.html"  >
				<span>
			    				Demonizz LIVE (SP)				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item191" >
					<a class="orphan item bullet" href="/tandava/2010/atracoes-tandava/ver-todas.html"  >
				<span>
			    				Ver Todas...				   
				</span>
			</a>
			
			
	</li>	
										</ul>
			</div>
			
	</li>	
							<li class="item168 root" >
					<a class="orphan item bullet" href="/tandava/2010/ingressos.html"  >
				<span>
			    				Ingressos				   
				</span>
			</a>
			
			
	</li>	
							<li class="item174 parent root" >
					<a class="daddy item bullet" href="/tandava/2010/local.html"  >
				<span>
			    				Local				   
				</span>
			</a>
			
					<div class="fusion-submenu-wrapper level2">
				<div class="drop-top"></div>
				<ul class="level2">
								
							<li class="item207" >
					<a class="orphan item bullet" href="/tandava/2010/local/mapa-como-chegar-tandava-2010.html"  >
				<span>
			    				Mapa Tandava 2010				   
				</span>
			</a>
			
			
	</li>	
										</ul>
			</div>
			
	</li>	
							<li class="item173 root" >
					<a class="orphan item bullet" href="/tandava/2010/fotos.html"  >
				<span>
			    				Fotos				   
				</span>
			</a>
			
			
	</li>	
							<li class="item175 root" >
					<span class="orphan item bullet nolink">
			    <span>
			        			    Área Restrita			    			    </span>
			</span>
			
			
	</li>	
							<li class="item49 root" >
					<a class="orphan item bullet" href="/tandava/2010/contato.html"  >
				<span>
			    				Contato				   
				</span>
			</a>
			
			
	</li>	
				</ul>
</div>

								    <div class="clear"></div>
								</div></div></div>
																						</div>
																			</div>
											</div></div>
															<div id="rt-main-surround">
												<div id="rt-breadcrumbs"><div id="rt-breadcrumbs2"><div id="rt-breadcrumbs3">
								<div class="rt-breadcrumb-surround">
		<a href="http://elijah.com.br/tandava/2010/" id="breadcrumbs-home"></a>
		<span class="breadcrumbs pathway">
<span class="no-link">Programação</span></span>
	</div>
	
							<div class="clear"></div>
						</div></div></div>
																							              <div id="rt-main" class="mb8-sa4">
                <div class="rt-main-inner">
                    <div class="rt-grid-8 ">
                                                						<div class="rt-block">
							<div class="default">
	                            <div id="rt-mainbody">
	                                
<div class="rt-joomla ">
	<div class="rt-blog">

				<h1 class="rt-pagetitle">Programação do Tandava Gathering</h1>
		
				<div class="rt-description">
										<p style="text-align: justify;">O <strong>Tandava</strong> é para todos que querem experimentar, para aqueles que  respeitam a si mesmo e aos outros. Na verdade é mais do que isso, é  feito para quem apenas deseja alguns dias fora da maçante realidade do  cotidiano.</p>
<p style="text-align: justify;">E tudo isso é materializado através da música e da arte.</p>
<p style="text-align: justify;">A música por  sua vez assume o papel de sincronizar, não só o a pista de dança, mas o  ritmo de tudo que acontece no festival. E a arte visa elevar, ainda que  por 3 dias, a percepção de você para com o mundo ao seu redor e,  principalmente, com você mesmo.</p>
<div class="note">
<div class="typo-icon"><span style="font-size: 8pt;">A programação e o cronograma das atividades e atrações poderão sofrer alterações devido a imprevistos, conflitos de agenda e deslocamento dos artistas.</span></div>
</div>
<blockquote>
<p>"É uma fuga para onde tempo e espaço estão aquém de afetar o relacionamento interpessoal."</p>
</blockquote>
<p><cite><a href="../">Tandava Gathering</a></cite>, 2010.</p><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://elijah.com.br/tandava/2010/programacao-tandava.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>					</div>
		
				<div class="rt-leading-articles">
										
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/programacao-tandava/133-line-up-completo.html">Line Up Completo</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<p>A edição 2010 do <strong>Tandava </strong>terá mais de 80 horas de música em 4 dias de  festival multiplicados por 2 pistas. Os artistas foram escolhidos  cuidadosamente pela oganização e a diversidade sonora é o que se ouve  durante o encontro.</p>
<p>Confira abaixo o <strong>LINE UP</strong> completo desta edição:</p>
<hr />
<h3>SEXTA.12.NOVEMBRO</h3>
<div id="2columns" style="display: inline-block;">
<div style="width: 300px; float: left;" id="mainstage">
<h4>Main Stage</h4>
<p>20:00 - Suissa vs Azahel (PR)</p>
<ul class="bullet-c">
<li><strong>Psy-Trance Full On</strong></li>
</ul>
<p>21:00 - Titicow (PR)<br /> 22:00 - Fernanda (PR)<br /> 23:00 - Dulio (PR)</p>
</div>
<div style="width: 300px; float: right;" id="chillout">
<h4>Chill Out Stage</h4>
<p>18:00 às 23:59 - FECHADO</p>
</div>
</div>
<div style="clear: both;"></div>
<hr />
<h3>SÁBADO.13.NOVEMBRO</h3>
<div id="2columns" style="display: inline-block;">
<div style="width: 300px; float: left;" id="mainstage">
<h4>Main Stage</h4>
<ul class="bullet-c">
<li><strong>Psy-Trance Full On</strong></li>
</ul>
<p>00:00 - <a href="index.php?option=com_content&amp;view=article&amp;id=137:anginha-rodrigues-pr&amp;catid=45&amp;Itemid=172">Anginha</a> vs Luana Cherry (PR)<br /> 01:00 - <strong><span style="font-size: 12pt;"><a href="index.php?option=com_content&amp;view=article&amp;id=107:zaraus-live-portugual&amp;catid=45&amp;Itemid=195">Zaraus LIVE (Portugal)</a></span></strong></p>
<ul class="bullet-c">
<li><strong>Dark Psychedelic</strong></li>
</ul>
<p>03:00 - <strong><span style="font-size: 12pt;"><a href="index.php?option=com_content&amp;view=article&amp;id=118:demonizz-live-sp&amp;catid=45&amp;Itemid=198">Demonizz LIVE (SP)</a></span></strong><br /> 04:00 - <strong><span style="font-size: 12pt;">Rodrigo CPU (PR)</span></strong></p>
<ul class="bullet-c">
<li><strong>Psy-Trance Full On</strong></li>
</ul>
<p>06:00 - <strong>Starting Up LIVE (PR)</strong><br />07:00 - <span style="font-size: 12pt;"><strong>SweDagon LIVE (PR)</strong></span><br /> 08:00 - <a href="index.php?option=com_content&amp;view=article&amp;id=138:clara-pr&amp;catid=45&amp;Itemid=172">Clara<span style="font-size: 8pt;"> (Psy Adonai - Bola de Neve PR)</span></a></p>
<ul class="bullet-c">
<li><strong>Progressive</strong></li>
</ul>
<p>09:00 - <span style="font-size: 12pt;"><strong>Beat Gate LIVE (PR)</strong></span><br />10:00 - <a href="index.php?option=com_content&amp;view=article&amp;id=141:chucky-pr&amp;catid=45&amp;Itemid=172">Chucky (PR)</a><br />11:00 - Saana (PR)<br /> 12:30 - Soha (PR)<br /> 14:00 - <strong>Thiago Alves (SP)</strong><br /> 15:30 - Magoo (PR)<br /> 17:00 - <strong>Fábio Leal (SP)</strong><br /> 18:30 - <span style="font-size: 12pt;"><strong><a target="_blank" href="index.php?option=com_content&amp;view=article&amp;id=124:ahlan-droid-pr&amp;catid=45&amp;Itemid=172">Ahlan Droid (PR)</a></strong></span></p>
<ul class="bullet-c">
<li><strong>Techno / House / Electro</strong></li>
</ul>
<p>20:30 - <a href="index.php?option=com_content&amp;view=article&amp;id=140:urbit-live-pr&amp;catid=45&amp;Itemid=172"><strong>Urbit LIVE (PR)</strong></a><br /> 21:30 - <span style="font-size: 12pt;"><strong><a href="index.php?option=com_content&amp;view=article&amp;id=112:elijah-hatem-pr&amp;catid=45&amp;Itemid=172">Elijah Hatem (PR)</a></strong></span><br /> 23:30 - <span style="font-size: 12pt;"><strong><strong>Kriminal Groove LIVE</strong></strong><strong>&nbsp;</strong></span></p>
</div>
<div style="width: 300px; float: right;" id="chillout">
<h4>Chill Out Stage</h4>
<ul class="bullet-c">
<li><span style="font-size: 12pt;"><strong><a href="index.php?option=com_content&amp;view=article&amp;id=93:funk-you&amp;catid=45&amp;Itemid=176">Funk You<span style="font-size: 8pt;"> (00h às 07h)</span></a></strong></span></li>
</ul>
<p>Abrindo a primeira noite do Chill Out a festa <a href="index.php?option=com_content&amp;view=article&amp;id=93:funk-you&amp;catid=45&amp;Itemid=176"><strong>Funk You</strong></a>, comandada pelos DJs <strong>Murillo</strong>, <strong><a href="index.php?option=com_content&amp;view=article&amp;id=94:schasko&amp;catid=45&amp;Itemid=172">Schasko</a></strong> e convidados.</p>
<ul class="bullet-c">
<li><strong><span style="font-size: 12pt;">Nova Delhi</span><span style="font-size: 8pt;"> (07h às 23h)</span></strong></li>
</ul>
<p><span style="font-size: 8pt;">Psychill, progbient, spacechill, organicchill são as trilhas sonoras que embalam da tarde de sábado no Tandava com o grupo curitibano <strong>Nova Delhi</strong>.</span></p>
<p>07:00 - Marcio Scheffler (PR)<br />09:00 - <em>A confirmar</em><br />11:00 - Azul (PR)<br />13:00 - Roninho (PR)<br />15:00 - Andrey Vinicius (PR)<br />17:00 - <em>A confirmar</em><br />19:00 - Patrick Rosa vs Azul (PR)<br />21:00 - <span style="font-size: 12pt;"><strong>Virtual Chakra LIVE (PR)</strong></span><br />22:00 - Xrú (PR)</p>
<ul class="bullet-c">
<li><strong><strong>Electro / Indie</strong></strong></li>
<p>23:00 -&nbsp; <span style="font-size: 12pt;"></span><strong><strong><span style="font-size: 12pt;">Lexosset LIVE (PR)</span><br /></strong></strong></p>
</ul>
</div>
</div>
<div style="clear: both;"></div>
<hr />
<h3><strong>DOMINGO.14.NOVEMBRO</strong></h3>
<div id="2columns" style="display: inline-block;">
<div style="width: 300px; float: left;" id="mainstage">
<h4><strong>Main Stage</strong></h4>
<ul class="bullet-c">
<li><strong><strong>Techno / House / Electro</strong></strong></li>
</ul>
<p><span style="font-size: 12pt;">&nbsp;</span><strong><span style="font-size: 12pt;"><strong>&nbsp;</strong></span></strong>00:30 - <strong><a href="index.php?option=com_content&amp;view=article&amp;id=126:roger-thiago-sc&amp;catid=45&amp;Itemid=172"><strong>Roger Thiago (SC)</strong></a><br /> </strong>01:30 - <strong><span style="font-size: 12pt;"><strong><a href="index.php?option=com_content&amp;view=article&amp;id=109:felipe-kantek-pr&amp;catid=45&amp;Itemid=172">Felipe Kantek (PR)</a></strong></span> <br /> </strong>03:30 - <strong><span style="font-size: 12pt;"><strong><a href="index.php?option=com_content&amp;view=article&amp;id=105:dj-afahell&amp;catid=45&amp;Itemid=172">AfaHell (PR)</a></strong></span><br /> </strong>05:30 - <a href="index.php?option=com_content&amp;view=article&amp;id=125:kazz-pr&amp;catid=45&amp;Itemid=172">Kazz (PR)</a><br /> 06:30 - Correção<strong><br /> </strong>08:30 - <a href="index.php?option=com_content&amp;view=article&amp;id=139:gabriel-bibi-pr&amp;catid=45&amp;Itemid=172">Gabriel Bibi (PR)</a><br /> 09:30 - <a href="index.php?option=com_content&amp;view=article&amp;id=128:dymi-pr&amp;catid=45&amp;Itemid=172">Dymi (PR)</a></p>
<ul class="bullet-c">
<li><strong><strong>Progressive Dark</strong></strong></li>
</ul>
<p>10:30 - <strong><a href="index.php?option=com_content&amp;view=article&amp;id=127:tiptronic-live-pr&amp;catid=45&amp;Itemid=172"><strong>Tip_Tronic LIVE (PR)</strong></a><br /> </strong>11:30 - <strong><span style="font-size: 12pt;"><strong><a href="index.php?option=com_content&amp;view=article&amp;id=103:minimal-criminal-live-rj&amp;catid=45&amp;Itemid=193">Minimal Criminal LIVE (RJ)</a></strong></span> <br /> </strong></p>
<ul class="bullet-c">
<li><strong><strong>Psy-Trance Full On</strong></strong></li>
</ul>
<p>12:30 - <strong><span style="font-size: 12pt;"><strong><a href="index.php?option=com_content&amp;view=article&amp;id=108:psapo-pr&amp;catid=45&amp;Itemid=172">Psapo (PR)</a></strong></span><br /> </strong>14:30 - <strong><span style="font-size: 12pt;"><strong><a href="index.php?option=com_content&amp;view=article&amp;id=104:rex-africa-do-sul&amp;catid=45&amp;Itemid=192">Rex (África do Sul)</a></strong></span><br /> </strong>16:30 - <strong><strong><a href="index.php?option=com_content&amp;view=article&amp;id=95:ahal-rama&amp;catid=45&amp;Itemid=172">Ahal Rãma (SC)<br /></a></strong></strong>17:30 - Correção<strong><strong><a href="index.php?option=com_content&amp;view=article&amp;id=95:ahal-rama&amp;catid=45&amp;Itemid=172"></a></strong></strong></p>
<ul class="bullet-c">
<li><strong><strong>Dark Psychedelic</strong></strong></li>
</ul>
<p>18:30 - <strong><strong>Thiago Pelotas (RS)</strong><br /> </strong>19:30 - <strong><strong>Sutemi (SP)</strong><br /> </strong>21:00 - <strong><a href="index.php?option=com_content&amp;view=article&amp;id=130:urucubaca-live-pr&amp;catid=45&amp;Itemid=172"><span style="font-size: 12pt;"><strong>Urucubaca LIVE (PR)</strong></span></a><br /></strong>22:00 - Correção</p>
</div>
<div style="width: 300px; float: right;" id="chillout">
<h4><strong><strong>Chill Out Stage</strong></strong></h4>
<ul class="bullet-c">
<li><span style="font-size: 12pt;"><strong><strong><strong><a href="index.php?option=com_content&amp;view=article&amp;id=106:vive-la-musique&amp;catid=45&amp;Itemid=196">Vive La Musique<span style="font-size: 8pt;"> (00h às 09h)</span></a></strong></strong></strong></span></li>
</ul>
<p style="text-align: justify;">Durante a noite de Sábado, o Chill Out se transforma com o som dançante da "Vive" e o espírito da cena Electro Indie curitibana.</p>
<p>00:00 - Débora Mello (Galeria Lúdica)<br />01:00 - Sandra Carraro (PR)<br />02:00 - LeduxCWB (PR)<strong>&nbsp;</strong><br />03:00 - Sérgio CAOS (PR)<br />04:00 - <strong><strong> <a href="index.php?option=com_content&amp;view=article&amp;id=111:joel-guglielmini-pr&amp;catid=45&amp;Itemid=172">Joel Guglielmini (PR)</a></strong></strong><strong>&nbsp;</strong><strong>&nbsp;</strong><br />05:30 - Manolo Neto (PR)<br />06:30 -<strong> <a href="index.php?option=com_content&amp;view=article&amp;id=110:andre-nego-pr&amp;catid=45&amp;Itemid=172">André Nego (PR)</a></strong><strong><br /></strong>08:00 - Raggamonk (PR)</p>
<ul class="bullet-c">
<li><strong>Chill Out</strong></li>
</ul>
<p>09:00 - Anderoath (PR)<br />10:30 - <strong><strong><span style="font-size: 12pt;"><a href="index.php?option=com_content&amp;view=article&amp;id=135:live-constroi-pr&amp;catid=45&amp;Itemid=172"><strong>Live Constrói (PR)</strong></a></span><br /></strong></strong>12:00 - <strong><strong><a href="index.php?option=com_content&amp;view=article&amp;id=112:elijah-hatem-pr&amp;catid=45&amp;Itemid=172"><strong>DJ Elijah Hatem (Chill Out Set)</strong></a></strong></strong></p>
<p>13:00 - <strong><strong><strong><span style="font-size: 12pt;"><a href="index.php?option=com_content&amp;view=article&amp;id=117:sallun-pedra-branca-sp&amp;catid=45&amp;Itemid=199">Sallun (Pedra Branca - SP)</a></span></strong></strong></strong></p>
<p>16:00 - <em>A confirmar</em><br />17:00 - Correção<br />18:00 - <a href="index.php?option=com_content&amp;view=article&amp;id=129:felipe-kojake-pr&amp;catid=45&amp;Itemid=172">Felipe Kojake (PR)</a><br />21:00 - <a href="index.php?option=com_content&amp;view=article&amp;id=96:friq-trip&amp;catid=45&amp;Itemid=172"></a><strong><strong><a href="index.php?option=com_content&amp;view=article&amp;id=96:friq-trip&amp;catid=45&amp;Itemid=172">Friq Trip (SC)</a></strong></strong></p>
</div>
</div>
<div style="clear: both;"></div>
<hr />
<h3><strong><strong>SEGUNDA.15.NOVEMBRO</strong></strong></h3>
<div id="2columns" style="display: inline-block;">
<div style="width: 300px; float: left;" id="mainstage">
<h4><strong><strong>Main Stage</strong></strong></h4>
<ul class="bullet-c">
<li><strong><strong><strong>Progressive Dark</strong></strong></strong></li>
</ul>
<p>00:30 - Correção<br />01:30 - <strong><strong><strong><a href="index.php?option=com_content&amp;view=article&amp;id=121:alanita-sc&amp;catid=45&amp;Itemid=172"><strong>Alanita (SC)</strong></a><br /></strong></strong></strong>03:30 - DeeJoker LIVE (PR)<br />04:30 - <strong><strong><strong><span style="font-size: 12pt;"><strong><a href="index.php?option=com_content&amp;view=article&amp;id=100:slow-control-live-sc&amp;catid=45&amp;Itemid=172">Slow Control LIVE (SC)</a></strong></span></strong></strong></strong></p>
<ul class="bullet-c">
<li><strong><strong><strong>Techno / House / Electro</strong></strong></strong></li>
</ul>
<p>05:30 - <strong><strong><strong><a href="index.php?option=com_content&amp;view=article&amp;id=119:caroles-rj&amp;catid=45&amp;Itemid=172"><strong>Caroles (RJ)</strong></a></strong></strong></strong></p>
<ul class="bullet-c">
<li><span style="font-size: 12pt;"><strong><strong><strong><a href="index.php?option=com_content&amp;view=article&amp;id=116:synk-recs-day-party&amp;catid=45&amp;Itemid=197">Synk Day Party<span style="font-size: 8pt;"> (08h às 20h)</span></a></strong></strong></strong></span><strong> </strong></li>
</ul>
<p>08:00 - Nav (PR)<br />09:30 - <a href="index.php?option=com_content&amp;view=article&amp;id=120:gromma-pr&amp;catid=45&amp;Itemid=172">Gromma (PR)</a><br />11:00 - <strong><a href="index.php?option=com_content&amp;view=article&amp;id=122:rodrigo-nickel-pr&amp;catid=45&amp;Itemid=172">Rodrigo Nickel (PR)</a></strong><br />12:30 - <a href="index.php?option=com_content&amp;view=article&amp;id=123:gabriel-boni-pr&amp;catid=45&amp;Itemid=172">Gabriel Boni (PR)</a><br />14:00 - <strong><strong><strong><a href="index.php?option=com_content&amp;view=article&amp;id=114:rodrigo-carreira-pr&amp;catid=45&amp;Itemid=172">Rodrigo Carreira (PR)</a><br /></strong></strong></strong>15:30 - <strong><strong><strong><span style="font-size: 12pt;"><strong><a href="index.php?option=com_content&amp;view=article&amp;id=115:truati-live-sp&amp;catid=45&amp;Itemid=200">Truati LIVE (SP)</a></strong></span><br /></strong></strong></strong>16:30 - <strong><strong><strong><a href="index.php?option=com_content&amp;view=article&amp;id=113:gabi-lima-pr&amp;catid=45&amp;Itemid=172">Gabi Lima (PR)</a><br /></strong></strong></strong>18:00 - <a href="index.php?option=com_content&amp;view=article&amp;id=131:crash-pr&amp;catid=45&amp;Itemid=172">Crash (PR)</a><br />19:30 - Jam Sessions Synk</p>
</div>
<div style="width: 300px; float: right;" id="chillout">
<h4><strong><strong><strong>Chill Out Stage</strong></strong></strong></h4>
<ul class="bullet-c">
<li><span style="font-size: 12pt;"><span style="font-size: 12pt;"><strong><strong><strong><a href="index.php?option=com_content&amp;view=article&amp;id=132:reboot&amp;catid=45&amp;Itemid=204">REBOOT<span style="font-size: 8pt;"> (23h às 08h)</span></a></strong></strong></strong></span></span><strong> </strong></li>
</ul>
<p>No encerramento do Chill Out, a festa onde são proibidas músicais originais: <a target="_blank" href="atracoes-tandava/reboot.html">Reboot</a><a target="_blank" href="atracoes-tandava/reboot.html"></a>.</p>
<p>DJs Feiges, RenanF e Murillo e performances de Yamballoon Circus Arts.</p>
<p>08:00 às 20:00 - FECHADO</p>
</div>
</div>
<div style="clear: both;"></div><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://elijah.com.br/tandava/2010/programacao-tandava/133-line-up-completo.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/programacao-tandava/133-line-up-completo.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/programacao-tandava/133-line-up-completo.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL2VsaWphaC5jb20uYnIvdGFuZGF2YS8yMDEwL3Byb2dyYW1hY2FvLXRhbmRhdmEvMTMzLWxpbmUtdXAtY29tcGxldG8uaHRtbA%3D%3D" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Terça, 19 de Outubro de 2010 11:35				</span>
					
								<span class="rt-date-modified">
					Última atualização em Terça, 09 de Novembro de 2010 16:28				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>									
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/programacao-tandava/134-arte-cultura-e-oficinas.html">Arte, Cultura e Oficinas</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<p style="text-align: justify;">A edição 2010 do Tandava terá inúmeras oficinas, exposições e outras atividades culturais durante todos os dias do evento. Participe!</p>
<p>Confira abaixo a Programação Cultural do <strong>Tandava Gathering</strong>:</p>
<h4>Programações Diárias</h4>
<p>Durante o Sábado, Domingo e Segunda.</p>
<ul class="bullet-c">
<li><strong>Roda de Malabares<br /></strong>Vanderlei de Mattos Biscaia fará pirofagia com Devil Fire e rodas de malabares, além da confecção e venda de peças.<strong><br /></strong> </li>
<br />
<li><strong>Exposição Fotográfica by MushPics<br /></strong>Conheça o MushPics: <a target="_blank" href="http://www.mushpics.com">www.mushpics.com</a><br /><br /></li>
<li><strong>Cobertura Fotográfica by Luna Cris</strong><br />Conheça Luna Cris:&nbsp;<a target="_blank" href="http://www.clikssnabalada.com/">www.clikssnabalada.com</a><br /><br /></li>
<li><strong>Cobertura Fotográfica by MushPics</strong><br />Conheça o MushPics: <a target="_blank" href="http://www.mushpics.com/">www.mushpics.com<br /></a><br /><strong></strong></li>
<li><strong>Cobertura Fotográfica by LeduxCWB</strong><br />Conheça o LeduxCWB: <a target="_blank" href="http://leduxcwb.wordpress.com/">leduxcwb.wordpress.com</a></li>
</ul>
<h4>Programação Sábado</h4>
<ul class="bullet-c">
<li><strong>Oficina de Objetos Tridimensionais (08:00 às 18:00)<br /></strong>Ministrantes: Marcio Scheffler e Azul.<br /><br />Em breve mais informações.<strong><br /><br /></strong><strong></strong></li>
<li><strong>Oficina de Modelagem em Argila (13:00 às 15:00)<br /></strong>Ministrante: Felipe S. (Ano Novo Maia / União da Vitória)<br /><br />A prática da modelagem em argila terá como objetivo principal estabelecer uma inter-relação entre o ser humano e os elementos naturais que dele fazem parte.<br /><br />Dentre eles estão a terra, meio de renovação e origem de vida, a base de tudo; a água, que purifica, deixa maleável; o ar que nutre, leva e traz e fornece leveza.<br /><br /> Elementos que compõe tanto o ser humano quanto o material a ser utilizado. Também se trata do momento de criar, dar forma ao pensamento integrando-o a materialidade da argila, respeitando suas propriedades e compreendendo as suas técnicas de manuseio. <br /><br /></li>
<li><strong>Oficina de Tie Die (15:00 às 17:00)<br /></strong>Ministrante: Andrey Torvalski.<br /><br />Em breve mais informações.<br /><br /><em>Obs.: O participante deverá levar uma camiseta (preferencialmente sem estampa) para participar.</em></li>
</ul>
<h4>Programação Domingo</h4>
<ul class="bullet-c">
<li style="text-align: justify;"><strong>Live Paint (00:00)<br /></strong>Artista: Orlando Muzca - artista plástico<br /> <br /> Uma laicra será pintada durante o festival. Em um processo de arqueologia do subconsciente, o artista trás à superfície os símbolos que estão presentes em cada momento de sua experiência visual.<br /><br />Mais Informações: <a target="_blank" href="http://www.mzc-lab.com/">www.mzc-lab.com</a><br /><strong><br /></strong> </li>
<strong></strong>
<li style="text-align: justify;"><strong>Oficina de Estampa de Tecido - Estêncil (15:00)<br /></strong>Ministrante: Felipe S. (Ano Novo Maia / União da Vitória)<br /> <br /> Conjunto de processos artesanais e técnicas decorativas aplicadas sobre tecido de algodão utilizando motivos e desenhos variados. Tem como objetivo difundir a técnica de estampa e a construção do molde vazado (estêncil). Consiste numa técnica exata, simples, a qual proporciona um excelente resultado visual.<br /><br />O fundamento da oficina permeia a ideia de que cada um pode criar sua própria estampa e utilizá-las em camisetas e outros tipos de pano (cangas, decoração, etc.) em vez de comprar em uma loja por um valor mais elevado.<br /> <br /> <em>Obs.: Cada participante deverá levar uma camiseta branca de algodão ou pano branco de algodão. Camisetas e panos de outras cores claras também poderão ser utilizados.<br /><br /></em></li>
<li><strong>Oficina de Micologia (17:00)<br /></strong>Ministrante: Matusael (Ano Novo Maia - União da Vitória)<br /><br />O objetivo desta oficina será abordar sobre os fungos em nosso meio ambiente, sua origem e sua relação com a consciência humana, e detalhar técnicas básicas de cultivo de cogumelos que desde a antiguidade são conhecidos pelos seus poderes mágicos..<br /><br /></li>
<li style="text-align: justify;"><strong>Vivência Circular de Pintura Abstrata (15:00 às 19:00)<br /></strong>Ministrante: Orlando Muzca aka Muzg, artista plástico e membro do projeto Urbit.<br /><br />A Vivência Circular de Pintura Abstrata é um mandala coletivo com o objetivo de unir seus praticantes numa psicosfera mântrica e interativa, proporcionando a abstração da mente através de um ritual circular, artístico e contemplativo.<br /><br />O processo acontece de forma lúdica no qual cada pessoa interage com a outra através do suporte onde estará sendo pintado o mandala.<br /><br />Cada rodada dura em média 30 minutos, com o número mínimo de 2 e máximo de 8 participantes. As cores são distribuídas aleatoriamente e passam de mão em mão.<br /><br />Em seguida outra rodada se inicia para complementar a pintura anterior ou até mesmo reciclar totalmente o tecido, compondo um novo mandala sobre o que foi formado anteriormente.<br /><br /><em>Obs.: Os inscritos deverão levar roupas velhas para participar, pois a tinta mancha os tecidos.<br /></em></li>
</ul>
<h4>Programação Segunda</h4>
<ul class="bullet-c">
<li style="text-align: justify;"><strong>Oficina de Hatha Yoga (08:00)<br /></strong>Ministrantes: Stefany Oliveira.<br /><br />Hatha Yoga, pré-clássico, significa sol (ha), lua (tha), positivo e negativo, homem e mulher, ou seja representa energias opostas. Procura o equilíbrio do corpo e mente através de exercícios físicos (asanas), respiração controlada (pranayama) e quietude da mente através do relaxamento e meditação.<br /><br />"<em>O Yoga é como música. O ritmo do corpo, a melodia da mente e a harmonia da alma criam a sinfonia da vida.</em>" - B. K. S. Iyengar<br /><strong><br /></strong> </li>
<strong> </strong>
<li style="text-align: justify;"><strong>Vivência Circular de Pintura Abstrata (15:00 às 19:00)<br /></strong>Ministrante: Orlando Muzca aka Muzg, artista plástico e membro do projeto Urbit.<br /><br />A  Vivência Circular de Pintura Abstrata é um mandala coletivo com o  objetivo de unir seus praticantes numa psicosfera mântrica e interativa,  proporcionando a abstração da mente através de um ritual circular,  artístico e contemplativo.<br /><br />O processo acontece de forma lúdica no  qual cada pessoa interage com a outra através do suporte onde estará  sendo pintado o mandala.<br /><br />Cada rodada dura em média 30 minutos,  com o número mínimo de 2 e máximo de 8 participantes. As cores são  distribuídas aleatoriamente e passam de mão em mão.<br /><br />Em seguida  outra rodada se inicia para complementar a pintura anterior ou até mesmo  reciclar totalmente o tecido, compondo um novo mandala sobre o que foi  formado anteriormente.<br /><br /><em>Obs.: Os inscritos deverão levar roupas velhas para participar, pois a tinta mancha os tecidos.</em><br /><br /><em></em></li>
</ul>
<h4>Stand S.O.S. Trance Dancers por Natural Flux Institute</h4>
<p>Natural Flux Institute nasceu da necessidade dos Naturoterapêutas e Yogaterapêutas formados nas Faculdades Integradas “Espírita” em Integrar as diversas Terapias Naturais e Práticas Transcendentais, no intuito de proporcionar Qualidade Vida e Longevidade através da prevenção de doenças e manutenção da saúde física e mental.</p>
<p>Esperamos contribuir, desta forma, ao desenvolvimento de indivíduos saudáveis, satisfeitos e mantenedores conscientes de sí próprios e do Planeta.</p>
<p><em>Obs.: Estas atividades terão custos individuais. Em breve serão publicados os custos de cada uma das terapias</em>.</p>
<h5>Massagens Terapeuticas - S.O.S. Trance Dancers</h5>
<ul class="bullet-c">
<li><strong>Don't Stop! - N.1 (R$ 15,00)<br /></strong>Com óleo: Reflexo, Ayurveda, Zen Shiatsu, Tuina ou  Relaxante - de  acordo com as possibilidades. <br />Pés, Panturrilhas e Coxas  Laterais (Zen  Shiatsu).<br /><br /><strong></strong></li>
<li><strong>Don't Stop! - N.2 (R$ 20,00)<br /></strong>Com óleo: Reflexo, Ayurveda, Zen Shiatsu, Tuina  ou Relaxante - de  acordo com as possibilidade.<br /> Ombros, trapézios,  pescoço,  Interescapular e canal da bexiga nas costas.<br /><br /><strong></strong></li>
<li><strong>Don't Stop! - N.3 (R$ 15,00)<br /></strong>Com óleo: Reflexo ou Ayurveda ou Zen Shiatsu ou  Tuina ou Relaxante -  de acordo com as possibilidades.<br />Face, Braços,  Mãos, Reiki.<br /><br /><strong></strong></li>
<li><strong>Don't Stop! - N.4 (R$ 25,00)<br /></strong>Com óleo: Reflexo ou Ayurveda ou Zen Shiatsu ou  Tuina ou Relaxante -  de acordo com as possibilidades.<br /> Alongamentos específicos com Nuad Bo Harn.<br /><br /><strong></strong></li>
<li><strong>Don't Stop! - N.5 (R$ 15,00)<br /></strong>Shiatsu Expresso.<br /><br /><strong></strong></li>
<li><strong>Don't Stop! - Combo Keep On Dancing (R$ 40,00)<br /></strong>Combo com Don't Stop! N.1 + N.2 + N.3.<br /><br /><strong></strong></li>
<li><strong>Don't Stop! - Combo Ressureição (R$ 50,00)<br /></strong>Combo com Don't Stop! N.1 + N.2 + N.3. + N.4</li>
</ul>
<h5>Atendimentos Naturoterápicos - S.O.S. Trance Dancers</h5>
<p>Custo: R$ 20,00.</p>
<p><em>* Inclui prescrição em Fitoterapia e Florais de Bach.</em></p>
<h5>Acupuntura Abdominal - S.O.S. Trance Dancers</h5>
<p>Custo: R$ 30,00.</p>
<h5>Auriculoterapia - S.O.S. Trance Dancers</h5>
<p>Custo: R$ 15,00.</p>
<h5>Reiki</h5>
<p>Custo: R$ 10,00.</p>
<h5>Demais Atendimentos e Práticas no Instituto ou a Domicílio</h5>
<p>Naturoterapia: Acupuntura Sistemica e Abdominal, Massoterapia Indiana, Chinesa, Japonesa e Ocidental, Fitoterapia, Florais, Geoterapia, Trofoterapia, Auriculoterapia, Reiki.</p>
<p>Práticas transcendentais: Hatha Yoga, Taijiquan (Taichichuan familia Yang), Kung fu Shaolin, Qigong, Alquimia Interna Taoista (breve), Muaythai, Defesa pessoal.</p><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://elijah.com.br/tandava/2010/programacao-tandava/134-arte-cultura-e-oficinas.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/programacao-tandava/134-arte-cultura-e-oficinas.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/programacao-tandava/134-arte-cultura-e-oficinas.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL2VsaWphaC5jb20uYnIvdGFuZGF2YS8yMDEwL3Byb2dyYW1hY2FvLXRhbmRhdmEvMTM0LWFydGUtY3VsdHVyYS1lLW9maWNpbmFzLmh0bWw%3D" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Terça, 19 de Outubro de 2010 16:08				</span>
					
								<span class="rt-date-modified">
					Última atualização em Quarta, 03 de Novembro de 2010 17:13				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>							</div>
		
				<div class="clear"></div>
		
				
	</div>
</div>
	                            </div>
								<div class="clear"></div>
							</div>
						</div>
                                                                    </div>
                                <div class="rt-grid-4 ">
                <div id="rt-sidebar-a">
                                        <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3"><h2 class="title">Próximo Tandava...</h2></div></div></div></div>
						<div class="clear"></div>
		                						<div class="module-content">
		                	<script src="modules/mod_countdown/scripts/swfobject_modified.js" type="text/javascript"></script>

			<object id="FlashID" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="260" height="80">

  <param name="movie" value="modules/mod_countdown/countdown.swf?dayText=Dias&texto=Contagem Regressiva&hoursText=Horas&ano=2011&mont=11&dia=12&minutesText=Minutos&secondsText=Segs&displayColor=0x7011100&textoColor=0x333333&dayColor=0x333333&hoursColor=0x333333&minutesColor=0x333333&secondsColor=0x333333&pozadinaColor=0xd6d6d4&linijaColor=0xd6d6d4&trans=" />

  <param name="quality" value="high" />

  <param name="wmode" value="transparent" />

  <param name="swfversion" value="6.0.65.0" />

  

  <!-- This param tag prompts users with Flash Player 6.0 r65 and higher to download the latest version of Flash Player. Delete it if you don’t want users to see the prompt. -->

  <param name="expressinstall" value="modules/mod_countdown/scripts/expressInstall.swf" />

  <!-- Next object tag is for non-IE browsers. So hide it from IE using IECC. -->

  <!--[if !IE]>-->

  <object type="application/x-shockwave-flash" data="modules/mod_countdown/countdown.swf?dayText=Dias&texto=Contagem Regressiva&hoursText=Horas&ano=2011&mont=11&dia=12&minutesText=Minutos&secondsText=Segs&displayColor=0x701110&textoColor=0x333333&dayColor=0x333333&hoursColor=0x333333&minutesColor=0x333333&secondsColor=0x333333&pozadinaColor=0xd6d6d4&linijaColor=0xd6d6d4&trans=" width="260" height="80">

    <!--<![endif]-->

    <param name="quality" value="high" />

    <param name="wmode" value="transparent" />

    <param name="swfversion" value="6.0.65.0" />

    <param name="expressinstall" value="modules/mod_countdown/scripts/expressInstall.swf" />

	

    <!-- The browser displays the following alternative content for users with Flash Player 6.0 and older. -->

    <div>

      <h4>Content on this page requires a newer version of Adobe Flash Player.</h4>

      <p><a href="http://www.adobe.com/go/getflashplayer"><img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" width="112" height="33" /></a></p>

    </div>

    <!--[if !IE]>-->

  </object>

  <!--<![endif]-->

</object>

<script type="text/javascript">

<!--

swfobject.registerObject("FlashID");

//-->

</script>

								</div>
					</div>
				</div>
            </div>
        	                    <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3"><h2 class="title">Main Menu</h2></div></div></div></div>
						<div class="clear"></div>
		                						<div class="module-content">
		                	<ul class="menu level1" >
				<li class="item1" >
					<a class="orphan item bullet" href="http://elijah.com.br/tandava/2010/"  >
				<span>
			    				Home				   
				</span>
			</a>
			
			
	</li>	
					<li class="item164" >
					<a class="orphan item bullet" href="/tandava/2010/o-festival.html"  >
				<span>
			    				Festival				   
				</span>
			</a>
			
			
	</li>	
					<li class="item206 active" >
					<a class="orphan item bullet" href="/tandava/2010/programacao-tandava.html"  >
				<span>
			    				Programação				   
				</span>
			</a>
			
			
	</li>	
					<li class="item172 parent" >
					<a class="orphan item bullet" href="/tandava/2010/atracoes-tandava.html"  >
				<span>
			    				Atrações				   
				</span>
			</a>
			
			
	</li>	
					<li class="item168" >
					<a class="orphan item bullet" href="/tandava/2010/ingressos.html"  >
				<span>
			    				Ingressos				   
				</span>
			</a>
			
			
	</li>	
					<li class="item174 parent" >
					<a class="orphan item bullet" href="/tandava/2010/local.html"  >
				<span>
			    				Local				   
				</span>
			</a>
			
			
	</li>	
					<li class="item173" >
					<a class="orphan item bullet" href="/tandava/2010/fotos.html"  >
				<span>
			    				Fotos				   
				</span>
			</a>
			
			
	</li>	
					<li class="item175" >
					<span class="orphan item bullet nolink">
			    <span>
			        			    Área Restrita			    			    </span>
			</span>
			
			
	</li>	
					<li class="item49" >
					<a class="orphan item bullet" href="/tandava/2010/contato.html"  >
				<span>
			    				Contato				   
				</span>
			</a>
			
			
	</li>	
		</ul>
						</div>
					</div>
				</div>
            </div>
        	                    <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3"><h2 class="title">Usuários Online</h2></div></div></div></div>
						<div class="clear"></div>
		                						<div class="module-content">
		                	<div>
	<ul style="list-style:none; margin: 0 0 10px; padding: 0;">
		
	
	</ul>
	
	<div>
		0 participantes		
					e 1 visitante				
		online 
		<br /><br />
        <div>
		<a href="/tandava/2010/area-restrita/comunidade/search/browse.html?sort=online">Ver Todos Participantes</a>
		</div>
	</div>
</div>						</div>
					</div>
				</div>
            </div>
        	                    <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3"><h2 class="title">Sua Opinião</h2></div></div></div></div>
						<div class="clear"></div>
		                						<div class="module-content">
		                	
<h4 class="rt-polltitle">
	Qual é a principal qualidade do Tandava para você?</h4>
<form action="index.php" method="post" name="form2" class="rt-poll">
	<fieldset>
				<div class="rt-pollrow">
			<input type="radio" name="voteid" id="voteid13" value="13" alt="13" />
			<label for="voteid13">
				Um encontro sem fins lucrativos.			</label>
		</div>
				<div class="rt-pollrow">
			<input type="radio" name="voteid" id="voteid14" value="14" alt="14" />
			<label for="voteid14">
				Reúne vários tipos de música.			</label>
		</div>
				<div class="rt-pollrow">
			<input type="radio" name="voteid" id="voteid15" value="15" alt="15" />
			<label for="voteid15">
				Clima familiar dos participantes.			</label>
		</div>
				<div class="rt-pollrow">
			<input type="radio" name="voteid" id="voteid16" value="16" alt="16" />
			<label for="voteid16">
				Entrada com bebidas liberada.			</label>
		</div>
				<div class="rt-pollrow">
			<input type="radio" name="voteid" id="voteid17" value="17" alt="17" />
			<label for="voteid17">
				Adoro acampar.			</label>
		</div>
			</fieldset>
	<div class="rt-pollbuttons">
		<div class="readon">
			<input type="submit" name="task_button" class="button" value="Votar" />
		</div>
		<div class="readon">
			<input type="button" name="option" class="button" value="Resultados" onclick="document.location.href='/tandava/2010/component/poll/15-qual-e-a-principal-qualidade-do-tandava-para-voce.html'" />
		</div>
	</div>

	<input type="hidden" name="option" value="com_poll" />
	<input type="hidden" name="task" value="vote" />
	<input type="hidden" name="id" value="15" />
	<input type="hidden" name="eefcbbec877e25b725a65633021f418b" value="1" /></form>
						</div>
					</div>
				</div>
            </div>
        	                    <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3"><h2 class="title">Área Restrita</h2></div></div></div></div>
						<div class="clear"></div>
		                						<div class="module-content">
		                	<form action="/tandava/2010/programacao-tandava.html" method="post" name="login" id="form-login" >
		<fieldset class="input">
	<p id="form-login-username">
		<label for="modlgn_username">Email</label><br />
		<input id="modlgn_username" type="text" name="username" class="inputbox" alt="username" size="18" />
	</p>
	<p id="form-login-password">
		<label for="modlgn_passwd">Senha</label><br />
		<input id="modlgn_passwd" type="password" name="passwd" class="inputbox" size="18" alt="password" />
	</p>
		<p id="form-login-remember">
		<input type="checkbox" name="remember" class="checkbox" value="yes" alt="Lembrar-me" />
		<label class="remember">
			Lembrar-me		</label>
	</p>
		<div class="readon"><input type="submit" name="Submit" class="button" value="Entrar" /></div>
	</fieldset>
	<ul>
		<li>
			<a href="/tandava/2010/component/user/reset.html">
			Esqueci minha senha</a>
		</li>
		<li>
			<a href="/tandava/2010/component/user/remind.html">
			Esqueci meu nome de usuário</a>
		</li>
			</ul>
	
	<input type="hidden" name="option" value="com_user" />
	<input type="hidden" name="task" value="login" />
	<input type="hidden" name="return" value="L3RhbmRhdmEvMjAxMC9hcmVhLXJlc3RyaXRhL2NvbXVuaWRhZGUuaHRtbA==" />
	<input type="hidden" name="eefcbbec877e25b725a65633021f418b" value="1" /></form>
						</div>
					</div>
				</div>
            </div>
        	
                </div>
            </div>

                    <div class="clear"></div>
                </div>
            </div>
																								<div id="rt-bottom">
							<div class="rt-grid-4 rt-alpha">
                        <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3"><h2 class="title">Últimas Atualizações</h2></div></div></div></div>
						<div class="clear"></div>
		                						<div class="module-content">
		                	<ul class="latestnews">
	<li class="latestnews">
		<a href="/tandava/2010/fotos/143-fotos-do-tandava-2010.html" class="latestnews">
			Fotos do Tandava 2010</a>
	</li>
	<li class="latestnews">
		<a href="/tandava/2010/programacao-tandava/144-o-tandava-2010.html" class="latestnews">
			O Tandava 2010</a>
	</li>
	<li class="latestnews">
		<a href="/tandava/2010/o-festival.html" class="latestnews">
			Tandava Gathering</a>
	</li>
	<li class="latestnews">
		<a href="/tandava/2010/local/mapa-como-chegar-tandava-2010.html" class="latestnews">
			Mapa Tandava 2010</a>
	</li>
	<li class="latestnews">
		<a href="/tandava/2010/local/142-sobre-o-local.html" class="latestnews">
			Sobre o Local</a>
	</li>
</ul>						</div>
					</div>
				</div>
            </div>
        	
</div>
<div class="rt-grid-4">
                        <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3"><h2 class="title">Mais Lidos</h2></div></div></div></div>
						<div class="clear"></div>
		                						<div class="module-content">
		                	<ul class="mostread">
	<li class="mostread">
		<a href="/tandava/2010/component/content/article/48-terceira-edicao/144-o-tandava-2010.html" class="mostread">
			O Tandava 2010</a>
	</li>
	<li class="mostread">
		<a href="/tandava/2010/atracoes-tandava/funk-you.html" class="mostread">
			Funk You</a>
	</li>
	<li class="mostread">
		<a href="/tandava/2010/fotos/85-fotos-tandava-2009.html" class="mostread">
			Fotos do Tandava 2009</a>
	</li>
	<li class="mostread">
		<a href="/tandava/2010/local/mapa-como-chegar-tandava-2010.html" class="mostread">
			Mapa Tandava 2010</a>
	</li>
	<li class="mostread">
		<a href="/tandava/2010/atracoes-tandava/synk-recs-day-party.html" class="mostread">
			Synk Recs Day Party</a>
	</li>
</ul>						</div>
					</div>
				</div>
            </div>
        	
</div>
<div class="rt-grid-4 rt-omega">
                        <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3"><h2 class="title">Destaque</h2></div></div></div></div>
						<div class="clear"></div>
		                						<div class="module-content">
		                	<p><strong><a href="index.php?option=com_content&amp;view=category&amp;layout=blog&amp;id=44&amp;Itemid=173"><strong>Clique Aqui</strong></a></strong> e confira as <a href="index.php?option=com_content&amp;view=category&amp;layout=blog&amp;id=44&amp;Itemid=173"><strong>fotos</strong></a> tiradas pelo <strong>MushPics </strong>nas edições anteriores do festival <strong>Tandava Gathering</strong>.</p>
<p>Esteve lá? <a href="mailto:tandava@tandava.com.br?subject=Fotos">Envie suas fotos</a> e faça parte desta história.</p>						</div>
					</div>
				</div>
            </div>
        	
</div>
							<div class="clear"></div>
						</div>
																		<div id="rt-footer"><div id="rt-footer2"><div id="rt-footer3">
							<div class="rt-grid-12 rt-alpha rt-omega">
                    <div class="title1">
                    <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3"><h2 class="title">Apoio Cultural e Parceiros</h2></div></div></div></div>
						<div class="clear"></div>
		                						<div class="module-content">
		                	<table border="0" width="100%">
<tbody>
<tr>
<td><a target="_blank" href="http://www.metropolis.art.br"><img style="margin: 10px; vertical-align: middle;" alt="metropolis" src="images/stories/parceiros/metropolis.png" width="100" height="100" /></a></td>
<td><img style="margin: 10px; vertical-align: middle;" alt="sica" src="images/stories/parceiros/sica.png" width="100" height="100" /></td>
<td><a target="_blank" href="http://www.wasabiam.com.br/"><img style="margin: 10px; vertical-align: middle;" alt="wasabi" src="images/stories/parceiros/wasabi.png" width="100" height="100" /></a></td>
<td><a target="_blank" href="http://www.myspace.com/funkyoucwb"><img style="margin: 10px; vertical-align: middle;" alt="funkyou_parceiro" src="images/stories/parceiros/funkyou_parceiro.png" width="100" height="100" /></a></td>
<td><img style="margin: 10px; vertical-align: middle;" alt="revolucione" src="images/stories/parceiros/revolucione.png" width="100" height="100" /></td>
</tr>
<tr>
<td><a target="_blank" href="http://www.egralha.com.br/"><img style="margin: 5px 10px; vertical-align: middle;" alt="egralha" src="images/stories/parceiros/egralha.png" width="100" height="50" /></a></td>
<td><a target="_blank" href="http://www.vivelamusique.com.br"><img style="margin: 5px 10px; vertical-align: middle;" alt="vivelamusique_parceiro" src="images/stories/parceiros/vivelamusique_parceiro.png" width="100" height="50" /></a></td>
<td><img style="margin: 5px 10px; vertical-align: middle;" alt="makunba" src="images/stories/parceiros/makunba.png" width="100" height="40" /></td>
<td><a target="_blank" href="http://synk.com.br/"><img style="margin: 5px 10px; vertical-align: middle;" alt="synk_parceiro" src="images/stories/parceiros/synk_parceiro.png" width="100" height="50" /></a></td>
<td><a target="_blank" href="http://www.mushpics.com/"><img style="margin: 10px; vertical-align: middle;" alt="mushpics" src="images/stories/parceiros/mushpics.png" width="100" height="50" /></a></td>
</tr>
<tr>
<td><a target="_blank" href="http://leduxcwb.wordpress.com/"><img style="margin: 5px 10px; vertical-align: middle;" alt="leduxcwb" src="images/stories/parceiros/leduxcwb.png" width="100" height="40" /></a></td>
<td><img style="margin: 5px 10px; vertical-align: middle;" alt="makana" src="images/stories/parceiros/makana.png" width="100" height="40" /></td>
<td><img style="margin: 10px; vertical-align: middle;" alt="Oxydus" src="images/stories/parceiros/oxydus_parceiro.png" width="100" height="50" /></td>
<td><a target="_blank" href="http://reboot.blog.com"><img style="margin: 10px; vertical-align: middle;" alt="reboot_parceiro" src="images/stories/parceiros/reboot_parceiro.png" width="100" height="50" /></a></td>
<td><a target="_blank" href="http://www.mzc-lab.com/"><img style="margin: 10px; vertical-align: middle;" alt="mzc-lab" src="images/stories/parceiros/mzc-lab.png" width="100" height="50" /></a></td>
</tr>
</tbody>
</table>						</div>
					</div>
				</div>
            </div>
                </div>
		
</div>
							<div class="clear"></div>
						</div></div></div>
											</div>
										<div id="rt-copyright">
						<div class="rt-grid-12 rt-alpha rt-omega">
                        <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-content">
		                	<table width="100%" border="0" cellpadding="0" cellspacing="1"><tr><td nowrap="nowrap"><a href="/tandava/2010/home.html" class="mainlevel" id="active_menu">Tandava Gathering</a><span class="mainlevel">  - </span><a href="/tandava/2010/o-festival.html" class="mainlevel" >O Festival</a><span class="mainlevel">  - </span><a href="/tandava/2010/atracoes-tandava.html" class="mainlevel" >Atrações</a><span class="mainlevel">  - </span><a href="/tandava/2010/ingressos.html" class="mainlevel" >Ingressos</a><span class="mainlevel">  - </span><a href="/tandava/2010/local.html" class="mainlevel" >Local</a><span class="mainlevel">  - </span><a href="/tandava/2010/fotos.html" class="mainlevel" >Fotos</a><span class="mainlevel">  - </span><a href="/tandava/2010/contato.html" class="mainlevel" >Contato</a><span class="mainlevel">  - </span><a href="/tandava/2010/mapa-do-site.html" class="mainlevel" >Mapa do Site</a></td></tr></table>						</div>
					</div>
				</div>
            </div>
        	
</div>
						<div class="clear"></div>
					</div>
															<div id="rt-debug">
						<div class="rt-grid-12 rt-alpha rt-omega">
                        <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-content">
		                	<p style="text-align: center;"><a href="http://validator.w3.org/check?uri=referer"><img style="margin: 0px 5px; vertical-align: middle;" src="http://www.w3.org/Icons/valid-xhtml10" alt="Valid XHTML 1.0 Transitional" height="31" width="88" /></a> <a href="http://jigsaw.w3.org/css-validator/check/referer"><img style="border: 0px none; width: 88px; height: 31px; margin: 0px 5px; vertical-align: middle;" src="http://jigsaw.w3.org/css-validator/images/vcss" alt="Valid CSS!" height="31px" width="88px" /></a><a href="http://feed2.w3.org/check.cgi?url=http%3A//www.tandava.com.br/index.php%3Fformat%3Dfeed%26type%3Drss"> <img style="margin: 0px 5px; vertical-align: middle;" src="images/stories/valid-rss-rogers.png" alt="valid-rss-rogers" title="Validate my Atom 1.0 feed" height="31" width="88" /> </a></p>
<p><span style="font-size: 9pt;">Tandava Gathering website was created by <strong>de elijah hatem</strong>. Powered by Open Source CMS <a target="_blank" href="http://www.joomla.org">Joomla 1.5</a> and developed using <a target="_blank" href="http://www.gantry-framework.org"><img style="vertical-align: bottom;" alt="gantry" src="images/stories/gantry.png" height="34" width="99" /></a> framework.</span></p>						</div>
					</div>
				</div>
            </div>
        	
</div>
						<div class="clear"></div>
					</div>
									</div>
			</div></div></div></div>
		</div>
			</body>
</html>
