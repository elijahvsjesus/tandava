<?php die("Access Denied"); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br" >
<head>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-2048503-7']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
	  <base href="http://www.elijah.com.br/tandava/2010/fotos.html" />
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="robots" content="index, follow" />
  <meta name="keywords" content="tandava, gathering, festival, 2010, curitiba, novembro" />
  <meta name="og:title" content="Foto Panorâmica 360º do Tandava 2009" />
  <meta name="og:site_name" content="Tandava Gathering" />
  <meta name="og:image" content="http://www.elijah.com.br/tandava/2010/images/stories/tandava360_thumb.jpg" />
  <meta name="description" content="Não surgiu um novo conceito. As coisas não vão mudar e nada será resgatado.  Não  é nada melhor ou maior de tudo que você já viu. Não  é inédito e nem ao menos é um evento. O respeito existe, mas o publico não, existe um encontro entre amigos e amigos de amigos." />
  <meta name="generator" content="Joomla! 1.5 - Open Source Content Management" />
  <title>Fotos do Tandava Gathering</title>
  <link href="/tandava/2010/fotos.feed?type=rss" rel="alternate" type="application/rss+xml" title="RSS 2.0" />
  <link href="/tandava/2010/fotos.feed?type=atom" rel="alternate" type="application/atom+xml" title="Atom 1.0" />
  <link href="/tandava/2010/favicon.ico" rel="shortcut icon" type="image/x-icon" />
  <link rel="stylesheet" href="/tandava/2010/plugins/system/rokbox/themes/dark/rokbox-style.css" type="text/css" />
  <link rel="stylesheet" href="/tandava/2010/components/com_gantry/css/gantry.css" type="text/css" />
  <link rel="stylesheet" href="/tandava/2010/components/com_gantry/css/grid-12.css" type="text/css" />
  <link rel="stylesheet" href="/tandava/2010/components/com_gantry/css/joomla.css" type="text/css" />
  <link rel="stylesheet" href="/tandava/2010/templates/rt_juxta_j15/css/joomla.css" type="text/css" />
  <link rel="stylesheet" href="/tandava/2010/templates/rt_juxta_j15/css/style3.css" type="text/css" />
  <link rel="stylesheet" href="/tandava/2010/templates/rt_juxta_j15/css/demo-styles.css" type="text/css" />
  <link rel="stylesheet" href="/tandava/2010/templates/rt_juxta_j15/css/template.css" type="text/css" />
  <link rel="stylesheet" href="/tandava/2010/templates/rt_juxta_j15/css/typography.css" type="text/css" />
  <link rel="stylesheet" href="/tandava/2010/templates/rt_juxta_j15/css/backgrounds.css" type="text/css" />
  <link rel="stylesheet" href="/tandava/2010/templates/rt_juxta_j15/css/fusionmenu.css" type="text/css" />
  <style type="text/css">
    <!--
#rt-main-surround ul.menu li.active > a, #rt-main-surround ul.menu li.active > .separator, #rt-main-surround ul.menu li.active > .item, #rt-main-surround .square4 ul.menu li:hover > a, #rt-main-surround .square4 ul.menu li:hover > .item, #rt-main-surround .square4 ul.menu li:hover > .separator, .roktabs-links ul li.active span, .menutop li:hover > .item, .menutop li.f-menuparent-itemfocus .item, .menutop li.active > .item {color:#701110;}
a, .button, #rt-main-surround ul.menu a:hover, #rt-main-surround ul.menu .separator:hover, #rt-main-surround ul.menu .item:hover, .title1 .module-title .title {color:#701110;}body #rt-logo {width:600px;height:200px;}
    -->
  </style>
  <script type="text/javascript" src="/tandava/2010/media/system/js/mootools.js"></script>
  <script type="text/javascript" src="/tandava/2010/media/system/js/caption.js"></script>
  <script type="text/javascript" src="/tandava/2010/plugins/system/rokbox/rokbox.js"></script>
  <script type="text/javascript" src="/tandava/2010/plugins/system/rokbox/themes/dark/rokbox-config.js"></script>
  <script type="text/javascript" src="/tandava/2010/components/com_gantry/js/gantry-buildspans.js"></script>
  <script type="text/javascript" src="/tandava/2010/components/com_gantry/js/gantry-inputs.js"></script>
  <script type="text/javascript" src="/tandava/2010/modules/mod_roknavmenu/themes/fusion/js/fusion.js"></script>
  <script type="text/javascript">
var rokboxPath = '/tandava/2010/plugins/system/rokbox/';
			window.addEvent('domready', function() {
				var modules = ['rt-block'];
				var header = ['h3','h2','h1'];
				GantryBuildSpans(modules, header);
			});
		InputsExclusion.push('.content_vote','#rt-popup','#vmMainPage')
		        window.addEvent('load', function() {
					new Fusion('ul.menutop', {
						pill: 0,
						effect: 'slide and fade',
						opacity: 1,
						hideDelay: 500,
						centered: 0,
						tweakInitial: {'x': 9, 'y': 6},
        				tweakSubsequent: {'x': 0, 'y': -14},
						menuFx: {duration: 200, transition: Fx.Transitions.Sine.easeOut},
						pillFx: {duration: 400, transition: Fx.Transitions.Back.easeOut}
					});
	            });
  </script>
  <script type='text/javascript'>
/*<![CDATA[*/
	var jax_live_site = 'http://www.elijah.com.br/tandava/2010/index.php';
	var jax_site_type = '1.5';
/*]]>*/
</script><script type="text/javascript" src="http://www.elijah.com.br/tandava/2010/plugins/system/pc_includes/ajax_1.3.js"></script>
</head>
	<body  class="backgroundlevel-high backgroundstyle-style3 bodylevel-high cssstyle-style3 font-family-optima font-size-is-large menu-type-fusionmenu col12 ">
		<div id="rt-mainbg-overlay">
			<div class="rt-surround-wrap"><div class="rt-surround"><div class="rt-surround2"><div class="rt-surround3">
				<div class="rt-container">
										<div id="rt-drawer">
												<div class="clear"></div>
					</div>
															<div id="rt-header-wrap"><div id="rt-header-wrap2">
												<div id="rt-header-graphic">
																				<div class="rt-header-padding">
																							<div id="rt-header">
									<div class="rt-grid-12 rt-alpha rt-omega">
    			<div class="rt-block">
    	    	<a href="/tandava/2010/" id="rt-logo"></a>
    		</div>
	    
</div>
									<div class="clear"></div>
								</div>
																								<div id="rt-navigation"><div id="rt-navigation2"><div id="rt-navigation3">
									
<div class="nopill">
	<ul class="menutop level1 " >
						<li class="item1 root" >
					<a class="orphan item bullet" href="http://www.elijah.com.br/tandava/2010/"  >
				<span>
			    				Home				   
				</span>
			</a>
			
			
	</li>	
							<li class="item164 root" >
					<a class="orphan item bullet" href="/tandava/2010/o-festival.html"  >
				<span>
			    				Festival				   
				</span>
			</a>
			
			
	</li>	
							<li class="item206 root" >
					<a class="orphan item bullet" href="/tandava/2010/programacao-tandava.html"  >
				<span>
			    				Programação				   
				</span>
			</a>
			
			
	</li>	
							<li class="item172 parent root" >
					<a class="daddy item bullet" href="/tandava/2010/atracoes-tandava.html"  >
				<span>
			    				Atrações				   
				</span>
			</a>
			
					<div class="fusion-submenu-wrapper level2 columns2">
				<div class="drop-top"></div>
				<ul class="level2 columns2">
								
							<li class="item176" >
					<a class="orphan item image" href="/tandava/2010/atracoes-tandava/funk-you.html"  >
				<span>
			    			        <img src="/tandava/2010/templates/rt_juxta_j15/images/icons/funkyou_icon.png" alt="funkyou_icon.png" />
			    				Funk You				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item196" >
					<a class="orphan item image" href="/tandava/2010/atracoes-tandava/vive-la-musique.html"  >
				<span>
			    			        <img src="/tandava/2010/templates/rt_juxta_j15/images/icons/vivelamusique_icon.png" alt="vivelamusique_icon.png" />
			    				Vive La Musique				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item204" >
					<a class="orphan item image" href="/tandava/2010/atracoes-tandava/reboot.html"  >
				<span>
			    			        <img src="/tandava/2010/templates/rt_juxta_j15/images/icons/reboot_icon.png" alt="reboot_icon.png" />
			    				Reboot				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item197" >
					<a class="orphan item image" href="/tandava/2010/atracoes-tandava/synk-recs-day-party.html"  >
				<span>
			    			        <img src="/tandava/2010/templates/rt_juxta_j15/images/icons/synk_icon.png" alt="synk_icon.png" />
			    				Synk Recs Day Party				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item192" >
					<a class="orphan item bullet" href="/tandava/2010/atracoes-tandava/rex-africa-do-sul.html"  >
				<span>
			    				Rex (África do Sul)				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item195" >
					<a class="orphan item bullet" href="/tandava/2010/atracoes-tandava/zaraus-live-portugal.html"  >
				<span>
			    				Zaraus LIVE (Portugal)				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item199" >
					<a class="orphan item bullet" href="/tandava/2010/atracoes-tandava/sallun-pedra-branca.html"  >
				<span>
			    				Sallun (Pedra Branca)				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item193" >
					<a class="orphan item bullet" href="/tandava/2010/atracoes-tandava/minimal-criminal-live.html"  >
				<span>
			    				Minimal Criminal LIVE				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item200" >
					<a class="orphan item bullet" href="/tandava/2010/atracoes-tandava/truati-live-sp.html"  >
				<span>
			    				Truati LIVE (SP)				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item198" >
					<a class="orphan item bullet" href="/tandava/2010/atracoes-tandava/demonizz-live-sp.html"  >
				<span>
			    				Demonizz LIVE (SP)				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item191" >
					<a class="orphan item bullet" href="/tandava/2010/atracoes-tandava/ver-todas.html"  >
				<span>
			    				Ver Todas...				   
				</span>
			</a>
			
			
	</li>	
										</ul>
			</div>
			
	</li>	
							<li class="item168 root" >
					<a class="orphan item bullet" href="/tandava/2010/ingressos.html"  >
				<span>
			    				Ingressos				   
				</span>
			</a>
			
			
	</li>	
							<li class="item174 parent root" >
					<a class="daddy item bullet" href="/tandava/2010/local.html"  >
				<span>
			    				Local				   
				</span>
			</a>
			
					<div class="fusion-submenu-wrapper level2">
				<div class="drop-top"></div>
				<ul class="level2">
								
							<li class="item207" >
					<a class="orphan item bullet" href="/tandava/2010/local/mapa-como-chegar-tandava-2010.html"  >
				<span>
			    				Mapa Tandava 2010				   
				</span>
			</a>
			
			
	</li>	
										</ul>
			</div>
			
	</li>	
							<li class="item173 active root" >
					<a class="orphan item bullet" href="/tandava/2010/fotos.html"  >
				<span>
			    				Fotos				   
				</span>
			</a>
			
			
	</li>	
							<li class="item175 root" >
					<span class="orphan item bullet nolink">
			    <span>
			        			    Área Restrita			    			    </span>
			</span>
			
			
	</li>	
							<li class="item49 root" >
					<a class="orphan item bullet" href="/tandava/2010/contato.html"  >
				<span>
			    				Contato				   
				</span>
			</a>
			
			
	</li>	
				</ul>
</div>

								    <div class="clear"></div>
								</div></div></div>
																						</div>
																			</div>
											</div></div>
															<div id="rt-main-surround">
												<div id="rt-breadcrumbs"><div id="rt-breadcrumbs2"><div id="rt-breadcrumbs3">
								<div class="rt-breadcrumb-surround">
		<a href="http://www.elijah.com.br/tandava/2010/" id="breadcrumbs-home"></a>
		<span class="breadcrumbs pathway">
<span class="no-link">Fotos</span></span>
	</div>
	
							<div class="clear"></div>
						</div></div></div>
																							              <div id="rt-main" class="mb8-sa4">
                <div class="rt-main-inner">
                    <div class="rt-grid-8 ">
                                                						<div class="rt-block">
							<div class="default">
	                            <div id="rt-mainbody">
	                                
<div class="rt-joomla ">
	<div class="rt-blog">

				<h1 class="rt-pagetitle">Fotos do Tandava Gathering</h1>
		
				<div class="rt-description">
										<p>Do festival, restam sempre as lembranças, as memórias e as estórias de um final de semana prolongado com boa música e ótimos amigos. Confira abaixo as fotos das últimas edições do <strong>Tandava Gathering</strong>.<strong><br /></strong></p>
<blockquote>
<p>"O respeito existe, mas  o  publico não, existe um encontro entre amigos e  amigos de amigos."</p>
</blockquote>
<p><cite><a href="index.php?option=com_content&amp;view=category&amp;layout=blog&amp;id=41&amp;Itemid=165">Tandava Gathering</a></cite>, Edição 2010.</p><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/fotos.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>					</div>
		
				<div class="rt-leading-articles">
										
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/fotos/143-fotos-do-tandava-2010.html">Fotos do Tandava 2010</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<p>A terceira edição do <strong>Tandava Gathering </strong>aconteceu no <strong>Haras Cartel</strong>, um local fantástico há 40km de Curitiba, entre os dias 12 e 15 de Novembro de 2010.</p>
<p>Abaixo, você encontra as fotos do encontro a partir de inúmeros pontos de vista de diferentes fotógrafos que estiveram presentes no evento. Confira::</p>
<h3>Fotos by MushPics.com</h3>
<p>Acesse também através do <a target="_blank" href="http://mushpics.com/tandava2010.html">website</a> e <a target="_blank" href="http://www.flickr.com/photos/mushpics">Flickr</a> do Mushpics.</p>
<p><a href="images/stories/fotos/2010/mushpics/DSC_3546.jpg" rel="rokbox[640 426](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3546_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3551.jpg" rel="rokbox[640 426](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3551_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3587.jpg" rel="rokbox[640 426](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3587_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3591.jpg" rel="rokbox[640 426](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3591_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3596.jpg" rel="rokbox[640 426](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3596_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3597.jpg" rel="rokbox[640 426](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3597_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3598.jpg" rel="rokbox[640 426](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3598_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3599.jpg" rel="rokbox[640 426](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3599_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3612.jpg" rel="rokbox[640 426](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3612_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3623.jpg" rel="rokbox[640 426](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3623_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3625.jpg" rel="rokbox[640 426](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3625_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3626.jpg" rel="rokbox[640 426](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3626_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3637.jpg" rel="rokbox[426 640](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3637_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3639.jpg" rel="rokbox[640 426](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3639_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3641.jpg" rel="rokbox[426 640](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3641_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3642.jpg" rel="rokbox[426 640](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3642_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3647.jpg" rel="rokbox[640 426](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3647_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3650.jpg" rel="rokbox[640 426](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3650_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3656.jpg" rel="rokbox[640 426](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3656_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3658.jpg" rel="rokbox[640 426](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3658_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3660.jpg" rel="rokbox[640 426](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3660_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3661.jpg" rel="rokbox[640 426](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3661_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3663.jpg" rel="rokbox[426 640](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3663_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3665.jpg" rel="rokbox[640 426](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3665_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3668.jpg" rel="rokbox[426 640](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3668_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3670.jpg" rel="rokbox[640 426](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3670_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3675.jpg" rel="rokbox[426 640](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3675_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3682.jpg" rel="rokbox[640 426](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3682_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3683.jpg" rel="rokbox[426 640](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3683_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3688.jpg" rel="rokbox[426 640](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3688_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3696.jpg" rel="rokbox[426 640](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3696_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3699.jpg" rel="rokbox[640 426](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3699_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3703.jpg" rel="rokbox[640 426](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3703_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3706.jpg" rel="rokbox[426 640](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3706_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3710.jpg" rel="rokbox[640 426](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3710_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3722.jpg" rel="rokbox[426 640](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3722_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3724.jpg" rel="rokbox[426 640](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3724_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3727.jpg" rel="rokbox[640 426](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3727_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3728.jpg" rel="rokbox[426 640](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3728_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3733.jpg" rel="rokbox[426 640](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3733_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3741.jpg" rel="rokbox[426 640](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3741_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3742.jpg" rel="rokbox[426 640](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3742_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3746.jpg" rel="rokbox[426 640](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3746_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3747.jpg" rel="rokbox[426 640](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3747_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3749.jpg" rel="rokbox[640 426](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3749_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3751.jpg" rel="rokbox[426 640](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3751_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3752.jpg" rel="rokbox[640 426](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3752_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3756.jpg" rel="rokbox[426 640](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3756_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3757.jpg" rel="rokbox[426 640](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3757_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3758.jpg" rel="rokbox[426 640](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3758_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3760.jpg" rel="rokbox[426 640](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3760_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3763.jpg" rel="rokbox[640 426](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3763_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3779.jpg" rel="rokbox[640 426](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3779_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3783.jpg" rel="rokbox[640 426](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3783_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3787.jpg" rel="rokbox[640 426](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3787_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3794.jpg" rel="rokbox[640 426](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3794_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3797.jpg" rel="rokbox[640 426](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3797_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3798.jpg" rel="rokbox[640 426](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3798_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3806.jpg" rel="rokbox[640 426](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3806_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3809.jpg" rel="rokbox[640 426](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3809_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3811.jpg" rel="rokbox[426 640](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3811_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3814.jpg" rel="rokbox[426 640](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3814_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3815.jpg" rel="rokbox[426 640](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3815_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3817.jpg" rel="rokbox[640 426](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3817_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/DSC_3821.jpg" rel="rokbox[640 426](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_3821_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/FRF_0732.jpg" rel="rokbox[640 425](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//FRF_0732_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/FRF_0739.jpg" rel="rokbox[640 425](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//FRF_0739_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/FRF_0752.jpg" rel="rokbox[425 640](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//FRF_0752_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/FRF_0780.jpg" rel="rokbox[425 640](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//FRF_0780_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/FRF_0799.jpg" rel="rokbox[640 425](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//FRF_0799_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/FRF_0805.jpg" rel="rokbox[640 425](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//FRF_0805_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/FRF_0831.jpg" rel="rokbox[640 425](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//FRF_0831_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/FRF_0833.jpg" rel="rokbox[640 425](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//FRF_0833_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/FRF_0837.jpg" rel="rokbox[640 425](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//FRF_0837_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/FRF_0842.jpg" rel="rokbox[425 640](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//FRF_0842_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/FRF_0845.jpg" rel="rokbox[640 425](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//FRF_0845_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/FRF_0850.jpg" rel="rokbox[640 425](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//FRF_0850_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/FRF_0854.jpg" rel="rokbox[640 425](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//FRF_0854_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/FRF_0859.jpg" rel="rokbox[640 425](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//FRF_0859_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/FRF_0861.jpg" rel="rokbox[640 425](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//FRF_0861_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/FRF_0880.jpg" rel="rokbox[425 640](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//FRF_0880_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/FRF_0889.jpg" rel="rokbox[425 640](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//FRF_0889_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/FRF_0893.jpg" rel="rokbox[425 640](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//FRF_0893_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/FRF_0894.jpg" rel="rokbox[425 640](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//FRF_0894_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/FRF_0897.jpg" rel="rokbox[640 425](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//FRF_0897_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/FRF_0902.jpg" rel="rokbox[640 425](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//FRF_0902_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/FRF_0906.jpg" rel="rokbox[640 425](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//FRF_0906_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/FRF_0912.jpg" rel="rokbox[640 425](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//FRF_0912_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/FRF_0917.jpg" rel="rokbox[640 425](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//FRF_0917_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/FRF_0921.jpg" rel="rokbox[640 425](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//FRF_0921_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/FRF_0922.jpg" rel="rokbox[640 425](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//FRF_0922_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/FRF_0930.jpg" rel="rokbox[640 425](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//FRF_0930_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/FRF_0940.jpg" rel="rokbox[640 425](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//FRF_0940_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/FRF_0943.jpg" rel="rokbox[640 425](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//FRF_0943_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/FRF_0950.jpg" rel="rokbox[640 425](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//FRF_0950_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/FRF_0959.jpg" rel="rokbox[640 425](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//FRF_0959_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/FRF_0969.jpg" rel="rokbox[640 425](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//FRF_0969_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/FRF_0971.jpg" rel="rokbox[425 640](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//FRF_0971_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/FRF_0972.jpg" rel="rokbox[640 425](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//FRF_0972_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/FRF_0979.jpg" rel="rokbox[640 425](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//FRF_0979_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/FRF_0981.jpg" rel="rokbox[640 425](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//FRF_0981_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/FRF_0983.jpg" rel="rokbox[640 425](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//FRF_0983_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/FRF_0998.jpg" rel="rokbox[640 425](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//FRF_0998_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/FRF_1006.jpg" rel="rokbox[640 425](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//FRF_1006_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/FRF_1015.jpg" rel="rokbox[425 640](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//FRF_1015_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/FRF_1020.jpg" rel="rokbox[425 640](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//FRF_1020_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/FRF_1023.jpg" rel="rokbox[425 640](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//FRF_1023_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/FRF_1034.jpg" rel="rokbox[425 640](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//FRF_1034_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/FRF_1036.jpg" rel="rokbox[425 640](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//FRF_1036_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/FRF_1037.jpg" rel="rokbox[425 640](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//FRF_1037_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/FRF_1040.jpg" rel="rokbox[425 640](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//FRF_1040_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/FRF_1045.jpg" rel="rokbox[425 640](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//FRF_1045_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/FRF_1046.jpg" rel="rokbox[640 425](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//FRF_1046_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/FRF_1055.jpg" rel="rokbox[640 425](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//FRF_1055_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/FRF_1057.jpg" rel="rokbox[640 425](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//FRF_1057_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/FRF_1081.jpg" rel="rokbox[425 640](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//FRF_1081_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/FRF_1083.jpg" rel="rokbox[425 640](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//FRF_1083_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/FRF_1084.jpg" rel="rokbox[640 425](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//FRF_1084_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/FRF_1086.jpg" rel="rokbox[640 425](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//FRF_1086_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/FRF_1093.jpg" rel="rokbox[425 640](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//FRF_1093_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mushpics/FRF_1095.jpg" rel="rokbox[640 425](tandava2010mushpics)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//FRF_1095_thumb.jpg" alt="" /></a> </p>
<h3>Fotos by Pinduca</h3>
<p><a href="images/stories/fotos/2010/pinduca/CSC_0271.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//CSC_0271_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0059.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0059_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0072.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0072_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0085.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0085_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0093.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0093_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0095.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0095_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0104.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0104_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0113.jpg" rel="rokbox[537 800](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0113_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0114.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0114_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0118.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0118_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0119.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0119_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0120.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0120_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0121.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0121_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0123.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0123_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0139.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0139_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0143.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0143_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0144.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0144_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0145.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0145_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0146.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0146_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0149.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0149_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0150.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0150_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0152.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0152_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0153.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0153_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0154.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0154_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0157.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0157_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0159.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0159_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0162.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0162_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0166.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0166_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0168.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0168_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0170.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0170_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0172.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0172_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0176.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0176_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0178.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0178_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0180.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0180_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0181.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0181_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0185.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0185_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0197.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0197_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0206.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0206_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0207.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0207_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0208.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0208_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0210.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0210_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0216.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0216_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0217.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0217_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0246.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0246_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0250.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0250_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0262.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0262_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0267.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0267_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0280.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0280_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0284.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0284_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0285.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0285_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0293.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0293_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0294.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0294_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0296.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0296_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0298.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0298_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0300.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0300_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0307.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0307_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0308.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0308_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0309.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0309_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0315.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0315_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0316.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0316_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0318.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0318_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0325.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0325_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0326.jpg" rel="rokbox[537 800](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0326_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0327.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0327_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0330.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0330_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0331.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0331_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0336.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0336_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0337.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0337_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0338.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0338_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0339.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0339_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0341.jpg" rel="rokbox[537 800](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0341_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0357.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0357_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/pinduca/DSC_0358.jpg" rel="rokbox[800 537](tandava2010pinduca)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC_0358_thumb.jpg" alt="" /></a> </p>
<h3>Fotos by Mamão</h3>
<p><a href="images/stories/fotos/2010/mamao/P1050886.jpg" rel="rokbox[800 450](tandava2010mamao)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//P1050886_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mamao/P1050891.jpg" rel="rokbox[800 450](tandava2010mamao)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//P1050891_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mamao/P1050895.jpg" rel="rokbox[800 534](tandava2010mamao)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//P1050895_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mamao/P1050897.jpg" rel="rokbox[534 800](tandava2010mamao)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//P1050897_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mamao/P1050899.jpg" rel="rokbox[800 534](tandava2010mamao)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//P1050899_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mamao/P1050901.jpg" rel="rokbox[800 534](tandava2010mamao)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//P1050901_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mamao/P1050907.jpg" rel="rokbox[800 534](tandava2010mamao)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//P1050907_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mamao/P1050908.jpg" rel="rokbox[534 800](tandava2010mamao)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//P1050908_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mamao/P1050912.jpg" rel="rokbox[800 534](tandava2010mamao)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//P1050912_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mamao/P1050914.jpg" rel="rokbox[800 534](tandava2010mamao)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//P1050914_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mamao/P1050920.jpg" rel="rokbox[800 534](tandava2010mamao)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//P1050920_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mamao/P1050922.jpg" rel="rokbox[800 533](tandava2010mamao)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//P1050922_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mamao/P1050924.jpg" rel="rokbox[592 800](tandava2010mamao)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//P1050924_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mamao/P1050925.jpg" rel="rokbox[534 800](tandava2010mamao)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//P1050925_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mamao/P1050926.jpg" rel="rokbox[800 534](tandava2010mamao)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//P1050926_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mamao/P1050933.jpg" rel="rokbox[800 450](tandava2010mamao)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//P1050933_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mamao/P1050936.jpg" rel="rokbox[800 450](tandava2010mamao)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//P1050936_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mamao/P1050942.jpg" rel="rokbox[800 450](tandava2010mamao)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//P1050942_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mamao/P1050944.jpg" rel="rokbox[800 450](tandava2010mamao)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//P1050944_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mamao/P1050945.jpg" rel="rokbox[800 450](tandava2010mamao)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//P1050945_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mamao/P1050946.jpg" rel="rokbox[800 450](tandava2010mamao)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//P1050946_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mamao/P1050947.jpg" rel="rokbox[800 450](tandava2010mamao)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//P1050947_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mamao/P1050953.jpg" rel="rokbox[800 450](tandava2010mamao)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//P1050953_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mamao/P1050955.jpg" rel="rokbox[800 450](tandava2010mamao)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//P1050955_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mamao/P1050957.jpg" rel="rokbox[800 450](tandava2010mamao)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//P1050957_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mamao/P1050959.jpg" rel="rokbox[800 450](tandava2010mamao)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//P1050959_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mamao/P1050962.jpg" rel="rokbox[800 450](tandava2010mamao)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//P1050962_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mamao/P1050963.jpg" rel="rokbox[800 450](tandava2010mamao)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//P1050963_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mamao/P1050967.jpg" rel="rokbox[800 450](tandava2010mamao)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//P1050967_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mamao/P1050970.jpg" rel="rokbox[800 534](tandava2010mamao)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//P1050970_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mamao/P1050979.jpg" rel="rokbox[800 534](tandava2010mamao)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//P1050979_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mamao/P1050982.jpg" rel="rokbox[800 534](tandava2010mamao)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//P1050982_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2010/mamao/P1050998.jpg" rel="rokbox[800 450](tandava2010mamao)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//P1050998_thumb.jpg" alt="" /></a> </p>
<h3>Fotos by leduxCWB</h3>
<p>Acesse também através do <a target="_blank" href="http://leduxcwb.wordpress.com/2010/11/23/tandava-vive-la-musique-13-e-14nov2010/">blog do leduxCWB</a>.</p>
<p>Em breve mais fotos aqui nessa página.</p>
<h3>Fotos da Galera</h3>
Em breve! <br /><br />
<div class="camera">
<div class="typo-icon">
<p><strong>Envie suas fotos!</strong></p>
<p>Se você participou do Tandava Gathering 2010, colabore! Envie suas fotos e faça parte desta história.</p>
<p><a href="index.php?option=com_contact&amp;view=contact&amp;id=1&amp;Itemid=49">Clique Aqui</a> e entre em contato conosco.</p>
</div>
</div><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/fotos/143-fotos-do-tandava-2010.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/fotos/143-fotos-do-tandava-2010.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/fotos/143-fotos-do-tandava-2010.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL3d3dy5lbGlqYWguY29tLmJyL3RhbmRhdmEvMjAxMC9mb3Rvcy8xNDMtZm90b3MtZG8tdGFuZGF2YS0yMDEwLmh0bWw%3D" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Quarta, 17 de Novembro de 2010 18:01				</span>
					
								<span class="rt-date-modified">
					Última atualização em Quarta, 24 de Novembro de 2010 16:44				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>									
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/fotos/92-tandava-panoramica-360-tandava-2009.html">Foto Panorâmica 360º do Tandava 2009</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<p>Os fotógrafos da <a target="_blank" href="http://www.mushpics.com/"><strong>MushPics.com</strong></a> estiveram na edição 2009 do <strong>Tandava Gathering </strong>e registraram momentos únicos. Clique na imagem abaixo e confira uma foto panorâmica em 360º da segunda noite do Festival.</p>
<p style="text-align: center;"><a href="http://www.mushpics.com/panoramicas/tandava2009/index.html" rel="rokbox[fullscreen]" title=""><img class="album" src="images/stories/tandava360_thumb.jpg" alt="" /></a> </p><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/fotos/92-tandava-panoramica-360-tandava-2009.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/fotos/92-tandava-panoramica-360-tandava-2009.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/fotos/92-tandava-panoramica-360-tandava-2009.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL3d3dy5lbGlqYWguY29tLmJyL3RhbmRhdmEvMjAxMC9mb3Rvcy85Mi10YW5kYXZhLXBhbm9yYW1pY2EtMzYwLXRhbmRhdmEtMjAwOS5odG1s" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Quarta, 16 de Junho de 2010 00:00				</span>
					
								<span class="rt-date-modified">
					Última atualização em Quarta, 17 de Novembro de 2010 18:01				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>				</div>
		
				<div class="clear"></div>
		
				<div class="rt-pagination">
						<p class="rt-results">
				Página 1 de 2			</p>
						
<div class="tab">
<div class="tab2"><div class="page-active">Início</div></div></div>
<div class="tab">
<div class="tab2"><div class="page-active">Anterior</div></div></div>
<div class="page-block"><div class="page-active">1</div></div>
<div class="page-block"><div class="page-inactive"><a href="/tandava/2010/fotos.html?start=2" title="2">2</a></div></div>
<div class="tab">
<div class="tab2"><div class="page-inactive"><a href="/tandava/2010/fotos.html?start=2" title="Próximo">Próximo</a></div></div></div>
<div class="tab">
<div class="tab2"><div class="page-inactive"><a href="/tandava/2010/fotos.html?start=2" title="Fim">Fim</a></div></div></div>		</div>
				
	</div>
</div>
	                            </div>
								<div class="clear"></div>
							</div>
						</div>
                                                                    </div>
                                <div class="rt-grid-4 ">
                <div id="rt-sidebar-a">
                                        <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3"><h2 class="title">Próximo Tandava...</h2></div></div></div></div>
						<div class="clear"></div>
		                						<div class="module-content">
		                	<script src="modules/mod_countdown/scripts/swfobject_modified.js" type="text/javascript"></script>

			<object id="FlashID" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="260" height="80">

  <param name="movie" value="modules/mod_countdown/countdown.swf?dayText=Dias&texto=Contagem Regressiva&hoursText=Horas&ano=2011&mont=11&dia=12&minutesText=Minutos&secondsText=Segs&displayColor=0x7011100&textoColor=0x333333&dayColor=0x333333&hoursColor=0x333333&minutesColor=0x333333&secondsColor=0x333333&pozadinaColor=0xd6d6d4&linijaColor=0xd6d6d4&trans=" />

  <param name="quality" value="high" />

  <param name="wmode" value="transparent" />

  <param name="swfversion" value="6.0.65.0" />

  

  <!-- This param tag prompts users with Flash Player 6.0 r65 and higher to download the latest version of Flash Player. Delete it if you don’t want users to see the prompt. -->

  <param name="expressinstall" value="modules/mod_countdown/scripts/expressInstall.swf" />

  <!-- Next object tag is for non-IE browsers. So hide it from IE using IECC. -->

  <!--[if !IE]>-->

  <object type="application/x-shockwave-flash" data="modules/mod_countdown/countdown.swf?dayText=Dias&texto=Contagem Regressiva&hoursText=Horas&ano=2011&mont=11&dia=12&minutesText=Minutos&secondsText=Segs&displayColor=0x701110&textoColor=0x333333&dayColor=0x333333&hoursColor=0x333333&minutesColor=0x333333&secondsColor=0x333333&pozadinaColor=0xd6d6d4&linijaColor=0xd6d6d4&trans=" width="260" height="80">

    <!--<![endif]-->

    <param name="quality" value="high" />

    <param name="wmode" value="transparent" />

    <param name="swfversion" value="6.0.65.0" />

    <param name="expressinstall" value="modules/mod_countdown/scripts/expressInstall.swf" />

	

    <!-- The browser displays the following alternative content for users with Flash Player 6.0 and older. -->

    <div>

      <h4>Content on this page requires a newer version of Adobe Flash Player.</h4>

      <p><a href="http://www.adobe.com/go/getflashplayer"><img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" width="112" height="33" /></a></p>

    </div>

    <!--[if !IE]>-->

  </object>

  <!--<![endif]-->

</object>

<script type="text/javascript">

<!--

swfobject.registerObject("FlashID");

//-->

</script>

								</div>
					</div>
				</div>
            </div>
        	                    <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3"><h2 class="title">Main Menu</h2></div></div></div></div>
						<div class="clear"></div>
		                						<div class="module-content">
		                	<ul class="menu level1" >
				<li class="item1" >
					<a class="orphan item bullet" href="http://www.elijah.com.br/tandava/2010/"  >
				<span>
			    				Home				   
				</span>
			</a>
			
			
	</li>	
					<li class="item164" >
					<a class="orphan item bullet" href="/tandava/2010/o-festival.html"  >
				<span>
			    				Festival				   
				</span>
			</a>
			
			
	</li>	
					<li class="item206" >
					<a class="orphan item bullet" href="/tandava/2010/programacao-tandava.html"  >
				<span>
			    				Programação				   
				</span>
			</a>
			
			
	</li>	
					<li class="item172 parent" >
					<a class="orphan item bullet" href="/tandava/2010/atracoes-tandava.html"  >
				<span>
			    				Atrações				   
				</span>
			</a>
			
			
	</li>	
					<li class="item168" >
					<a class="orphan item bullet" href="/tandava/2010/ingressos.html"  >
				<span>
			    				Ingressos				   
				</span>
			</a>
			
			
	</li>	
					<li class="item174 parent" >
					<a class="orphan item bullet" href="/tandava/2010/local.html"  >
				<span>
			    				Local				   
				</span>
			</a>
			
			
	</li>	
					<li class="item173 active" >
					<a class="orphan item bullet" href="/tandava/2010/fotos.html"  >
				<span>
			    				Fotos				   
				</span>
			</a>
			
			
	</li>	
					<li class="item175" >
					<span class="orphan item bullet nolink">
			    <span>
			        			    Área Restrita			    			    </span>
			</span>
			
			
	</li>	
					<li class="item49" >
					<a class="orphan item bullet" href="/tandava/2010/contato.html"  >
				<span>
			    				Contato				   
				</span>
			</a>
			
			
	</li>	
		</ul>
						</div>
					</div>
				</div>
            </div>
        	                    <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3"><h2 class="title">Usuários Online</h2></div></div></div></div>
						<div class="clear"></div>
		                						<div class="module-content">
		                	<div>
	<ul style="list-style:none; margin: 0 0 10px; padding: 0;">
		
	
	</ul>
	
	<div>
		0 participantes		
					e 1 visitante				
		online 
		<br /><br />
        <div>
		<a href="/tandava/2010/area-restrita/comunidade/search/browse.html?sort=online">Ver Todos Participantes</a>
		</div>
	</div>
</div>						</div>
					</div>
				</div>
            </div>
        	                    <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3"><h2 class="title">Sua Opinião</h2></div></div></div></div>
						<div class="clear"></div>
		                						<div class="module-content">
		                	
<h4 class="rt-polltitle">
	Qual é a principal qualidade do Tandava para você?</h4>
<form action="index.php" method="post" name="form2" class="rt-poll">
	<fieldset>
				<div class="rt-pollrow">
			<input type="radio" name="voteid" id="voteid13" value="13" alt="13" />
			<label for="voteid13">
				Um encontro sem fins lucrativos.			</label>
		</div>
				<div class="rt-pollrow">
			<input type="radio" name="voteid" id="voteid14" value="14" alt="14" />
			<label for="voteid14">
				Reúne vários tipos de música.			</label>
		</div>
				<div class="rt-pollrow">
			<input type="radio" name="voteid" id="voteid15" value="15" alt="15" />
			<label for="voteid15">
				Clima familiar dos participantes.			</label>
		</div>
				<div class="rt-pollrow">
			<input type="radio" name="voteid" id="voteid16" value="16" alt="16" />
			<label for="voteid16">
				Entrada com bebidas liberada.			</label>
		</div>
				<div class="rt-pollrow">
			<input type="radio" name="voteid" id="voteid17" value="17" alt="17" />
			<label for="voteid17">
				Adoro acampar.			</label>
		</div>
			</fieldset>
	<div class="rt-pollbuttons">
		<div class="readon">
			<input type="submit" name="task_button" class="button" value="Votar" />
		</div>
		<div class="readon">
			<input type="button" name="option" class="button" value="Resultados" onclick="document.location.href='/tandava/2010/component/poll/15-qual-e-a-principal-qualidade-do-tandava-para-voce.html'" />
		</div>
	</div>

	<input type="hidden" name="option" value="com_poll" />
	<input type="hidden" name="task" value="vote" />
	<input type="hidden" name="id" value="15" />
	<input type="hidden" name="0d1d349d860fedbb5b15617084a500cb" value="1" /></form>
						</div>
					</div>
				</div>
            </div>
        	                    <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3"><h2 class="title">Área Restrita</h2></div></div></div></div>
						<div class="clear"></div>
		                						<div class="module-content">
		                	<form action="/tandava/2010/fotos.html" method="post" name="login" id="form-login" >
		<fieldset class="input">
	<p id="form-login-username">
		<label for="modlgn_username">Email</label><br />
		<input id="modlgn_username" type="text" name="username" class="inputbox" alt="username" size="18" />
	</p>
	<p id="form-login-password">
		<label for="modlgn_passwd">Senha</label><br />
		<input id="modlgn_passwd" type="password" name="passwd" class="inputbox" size="18" alt="password" />
	</p>
		<p id="form-login-remember">
		<input type="checkbox" name="remember" class="checkbox" value="yes" alt="Lembrar-me" />
		<label class="remember">
			Lembrar-me		</label>
	</p>
		<div class="readon"><input type="submit" name="Submit" class="button" value="Entrar" /></div>
	</fieldset>
	<ul>
		<li>
			<a href="/tandava/2010/component/user/reset.html">
			Esqueci minha senha</a>
		</li>
		<li>
			<a href="/tandava/2010/component/user/remind.html">
			Esqueci meu nome de usuário</a>
		</li>
			</ul>
	
	<input type="hidden" name="option" value="com_user" />
	<input type="hidden" name="task" value="login" />
	<input type="hidden" name="return" value="L3RhbmRhdmEvMjAxMC9hcmVhLXJlc3RyaXRhL2NvbXVuaWRhZGUuaHRtbA==" />
	<input type="hidden" name="0d1d349d860fedbb5b15617084a500cb" value="1" /></form>
						</div>
					</div>
				</div>
            </div>
        	
                </div>
            </div>

                    <div class="clear"></div>
                </div>
            </div>
																								<div id="rt-bottom">
							<div class="rt-grid-4 rt-alpha">
                        <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3"><h2 class="title">Últimas Atualizações</h2></div></div></div></div>
						<div class="clear"></div>
		                						<div class="module-content">
		                	<ul class="latestnews">
	<li class="latestnews">
		<a href="/tandava/2010/fotos/143-fotos-do-tandava-2010.html" class="latestnews">
			Fotos do Tandava 2010</a>
	</li>
	<li class="latestnews">
		<a href="/tandava/2010/fotos/144-o-tandava-2010.html" class="latestnews">
			O Tandava 2010</a>
	</li>
	<li class="latestnews">
		<a href="/tandava/2010/o-festival.html" class="latestnews">
			Tandava Gathering</a>
	</li>
	<li class="latestnews">
		<a href="/tandava/2010/local/mapa-como-chegar-tandava-2010.html" class="latestnews">
			Mapa Tandava 2010</a>
	</li>
	<li class="latestnews">
		<a href="/tandava/2010/local/142-sobre-o-local.html" class="latestnews">
			Sobre o Local</a>
	</li>
</ul>						</div>
					</div>
				</div>
            </div>
        	
</div>
<div class="rt-grid-4">
                        <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3"><h2 class="title">Mais Lidos</h2></div></div></div></div>
						<div class="clear"></div>
		                						<div class="module-content">
		                	<ul class="mostread">
	<li class="mostread">
		<a href="/tandava/2010/component/content/article/48-terceira-edicao/144-o-tandava-2010.html" class="mostread">
			O Tandava 2010</a>
	</li>
	<li class="mostread">
		<a href="/tandava/2010/atracoes-tandava/funk-you.html" class="mostread">
			Funk You</a>
	</li>
	<li class="mostread">
		<a href="/tandava/2010/fotos/85-fotos-tandava-2009.html" class="mostread">
			Fotos do Tandava 2009</a>
	</li>
	<li class="mostread">
		<a href="/tandava/2010/local/mapa-como-chegar-tandava-2010.html" class="mostread">
			Mapa Tandava 2010</a>
	</li>
	<li class="mostread">
		<a href="/tandava/2010/atracoes-tandava/synk-recs-day-party.html" class="mostread">
			Synk Recs Day Party</a>
	</li>
</ul>						</div>
					</div>
				</div>
            </div>
        	
</div>
<div class="rt-grid-4 rt-omega">
                        <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3"><h2 class="title">Destaque</h2></div></div></div></div>
						<div class="clear"></div>
		                						<div class="module-content">
		                	<p><strong><a href="index.php?option=com_content&amp;view=category&amp;layout=blog&amp;id=44&amp;Itemid=173"><strong>Clique Aqui</strong></a></strong> e confira as <a href="index.php?option=com_content&amp;view=category&amp;layout=blog&amp;id=44&amp;Itemid=173"><strong>fotos</strong></a> tiradas pelo <strong>MushPics </strong>nas edições anteriores do festival <strong>Tandava Gathering</strong>.</p>
<p>Esteve lá? <a href="mailto:tandava@tandava.com.br?subject=Fotos">Envie suas fotos</a> e faça parte desta história.</p>						</div>
					</div>
				</div>
            </div>
        	
</div>
							<div class="clear"></div>
						</div>
																		<div id="rt-footer"><div id="rt-footer2"><div id="rt-footer3">
							<div class="rt-grid-12 rt-alpha rt-omega">
                    <div class="title1">
                    <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3"><h2 class="title">Apoio Cultural e Parceiros</h2></div></div></div></div>
						<div class="clear"></div>
		                						<div class="module-content">
		                	<table border="0" width="100%">
<tbody>
<tr>
<td><a target="_blank" href="http://www.metropolis.art.br"><img style="margin: 10px; vertical-align: middle;" alt="metropolis" src="images/stories/parceiros/metropolis.png" width="100" height="100" /></a></td>
<td><img style="margin: 10px; vertical-align: middle;" alt="sica" src="images/stories/parceiros/sica.png" width="100" height="100" /></td>
<td><a target="_blank" href="http://www.wasabiam.com.br/"><img style="margin: 10px; vertical-align: middle;" alt="wasabi" src="images/stories/parceiros/wasabi.png" width="100" height="100" /></a></td>
<td><a target="_blank" href="http://www.myspace.com/funkyoucwb"><img style="margin: 10px; vertical-align: middle;" alt="funkyou_parceiro" src="images/stories/parceiros/funkyou_parceiro.png" width="100" height="100" /></a></td>
<td><img style="margin: 10px; vertical-align: middle;" alt="revolucione" src="images/stories/parceiros/revolucione.png" width="100" height="100" /></td>
</tr>
<tr>
<td><a target="_blank" href="http://www.egralha.com.br/"><img style="margin: 5px 10px; vertical-align: middle;" alt="egralha" src="images/stories/parceiros/egralha.png" width="100" height="50" /></a></td>
<td><a target="_blank" href="http://www.vivelamusique.com.br"><img style="margin: 5px 10px; vertical-align: middle;" alt="vivelamusique_parceiro" src="images/stories/parceiros/vivelamusique_parceiro.png" width="100" height="50" /></a></td>
<td><img style="margin: 5px 10px; vertical-align: middle;" alt="makunba" src="images/stories/parceiros/makunba.png" width="100" height="40" /></td>
<td><a target="_blank" href="http://synk.com.br/"><img style="margin: 5px 10px; vertical-align: middle;" alt="synk_parceiro" src="images/stories/parceiros/synk_parceiro.png" width="100" height="50" /></a></td>
<td><a target="_blank" href="http://www.mushpics.com/"><img style="margin: 10px; vertical-align: middle;" alt="mushpics" src="images/stories/parceiros/mushpics.png" width="100" height="50" /></a></td>
</tr>
<tr>
<td><a target="_blank" href="http://leduxcwb.wordpress.com/"><img style="margin: 5px 10px; vertical-align: middle;" alt="leduxcwb" src="images/stories/parceiros/leduxcwb.png" width="100" height="40" /></a></td>
<td><img style="margin: 5px 10px; vertical-align: middle;" alt="makana" src="images/stories/parceiros/makana.png" width="100" height="40" /></td>
<td><img style="margin: 10px; vertical-align: middle;" alt="Oxydus" src="images/stories/parceiros/oxydus_parceiro.png" width="100" height="50" /></td>
<td><a target="_blank" href="http://reboot.blog.com"><img style="margin: 10px; vertical-align: middle;" alt="reboot_parceiro" src="images/stories/parceiros/reboot_parceiro.png" width="100" height="50" /></a></td>
<td><a target="_blank" href="http://www.mzc-lab.com/"><img style="margin: 10px; vertical-align: middle;" alt="mzc-lab" src="images/stories/parceiros/mzc-lab.png" width="100" height="50" /></a></td>
</tr>
</tbody>
</table>						</div>
					</div>
				</div>
            </div>
                </div>
		
</div>
							<div class="clear"></div>
						</div></div></div>
											</div>
										<div id="rt-copyright">
						<div class="rt-grid-12 rt-alpha rt-omega">
                        <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-content">
		                	<table width="100%" border="0" cellpadding="0" cellspacing="1"><tr><td nowrap="nowrap"><a href="/tandava/2010/home.html" class="mainlevel" id="active_menu">Tandava Gathering</a><span class="mainlevel">  - </span><a href="/tandava/2010/o-festival.html" class="mainlevel" >O Festival</a><span class="mainlevel">  - </span><a href="/tandava/2010/atracoes-tandava.html" class="mainlevel" >Atrações</a><span class="mainlevel">  - </span><a href="/tandava/2010/ingressos.html" class="mainlevel" >Ingressos</a><span class="mainlevel">  - </span><a href="/tandava/2010/local.html" class="mainlevel" >Local</a><span class="mainlevel">  - </span><a href="/tandava/2010/fotos.html" class="mainlevel" >Fotos</a><span class="mainlevel">  - </span><a href="/tandava/2010/contato.html" class="mainlevel" >Contato</a><span class="mainlevel">  - </span><a href="/tandava/2010/mapa-do-site.html" class="mainlevel" >Mapa do Site</a></td></tr></table>						</div>
					</div>
				</div>
            </div>
        	
</div>
						<div class="clear"></div>
					</div>
															<div id="rt-debug">
						<div class="rt-grid-12 rt-alpha rt-omega">
                        <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-content">
		                	<p style="text-align: center;"><a href="http://validator.w3.org/check?uri=referer"><img style="margin: 0px 5px; vertical-align: middle;" src="http://www.w3.org/Icons/valid-xhtml10" alt="Valid XHTML 1.0 Transitional" height="31" width="88" /></a> <a href="http://jigsaw.w3.org/css-validator/check/referer"><img style="border: 0px none; width: 88px; height: 31px; margin: 0px 5px; vertical-align: middle;" src="http://jigsaw.w3.org/css-validator/images/vcss" alt="Valid CSS!" height="31px" width="88px" /></a><a href="http://feed2.w3.org/check.cgi?url=http%3A//www.tandava.com.br/index.php%3Fformat%3Dfeed%26type%3Drss"> <img style="margin: 0px 5px; vertical-align: middle;" src="images/stories/valid-rss-rogers.png" alt="valid-rss-rogers" title="Validate my Atom 1.0 feed" height="31" width="88" /> </a></p>
<p><span style="font-size: 9pt;">Tandava Gathering website was created by <strong>de elijah hatem</strong>. Powered by Open Source CMS <a target="_blank" href="http://www.joomla.org">Joomla 1.5</a> and developed using <a target="_blank" href="http://www.gantry-framework.org"><img style="vertical-align: bottom;" alt="gantry" src="images/stories/gantry.png" height="34" width="99" /></a> framework.</span></p>						</div>
					</div>
				</div>
            </div>
        	
</div>
						<div class="clear"></div>
					</div>
									</div>
			</div></div></div></div>
		</div>
			</body>
</html>
