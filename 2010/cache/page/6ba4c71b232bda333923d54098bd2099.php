<?php die("Access Denied"); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br" >
<head>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-2048503-7']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
	  <base href="http://www.elijah.com.br/tandava/2010/fotos.html" />
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="robots" content="index, follow" />
  <meta name="keywords" content="tandava, gathering, festival, 2010, curitiba, novembro" />
  <meta name="og:title" content="Fotos do Tandava 2008" />
  <meta name="og:site_name" content="Tandava Gathering" />
  <meta name="og:image" content="http://www.elijah.com.br/tandava/2010/http://www.elijah.com.br/tandava/2010/cache//3044318491_82ecd3a68d_o_thumb.jpg" />
  <meta name="description" content="Não surgiu um novo conceito. As coisas não vão mudar e nada será resgatado.  Não  é nada melhor ou maior de tudo que você já viu. Não  é inédito e nem ao menos é um evento. O respeito existe, mas o publico não, existe um encontro entre amigos e amigos de amigos." />
  <meta name="generator" content="Joomla! 1.5 - Open Source Content Management" />
  <title>Fotos do Tandava Gathering</title>
  <link href="/tandava/2010/fotos.feed?type=rss" rel="alternate" type="application/rss+xml" title="RSS 2.0" />
  <link href="/tandava/2010/fotos.feed?type=atom" rel="alternate" type="application/atom+xml" title="Atom 1.0" />
  <link href="/tandava/2010/favicon.ico" rel="shortcut icon" type="image/x-icon" />
  <link rel="stylesheet" href="/tandava/2010/plugins/system/rokbox/themes/dark/rokbox-style.css" type="text/css" />
  <link rel="stylesheet" href="/tandava/2010/components/com_gantry/css/gantry.css" type="text/css" />
  <link rel="stylesheet" href="/tandava/2010/components/com_gantry/css/grid-12.css" type="text/css" />
  <link rel="stylesheet" href="/tandava/2010/components/com_gantry/css/joomla.css" type="text/css" />
  <link rel="stylesheet" href="/tandava/2010/templates/rt_juxta_j15/css/joomla.css" type="text/css" />
  <link rel="stylesheet" href="/tandava/2010/templates/rt_juxta_j15/css/style3.css" type="text/css" />
  <link rel="stylesheet" href="/tandava/2010/templates/rt_juxta_j15/css/demo-styles.css" type="text/css" />
  <link rel="stylesheet" href="/tandava/2010/templates/rt_juxta_j15/css/template.css" type="text/css" />
  <link rel="stylesheet" href="/tandava/2010/templates/rt_juxta_j15/css/template-firefox.css" type="text/css" />
  <link rel="stylesheet" href="/tandava/2010/templates/rt_juxta_j15/css/typography.css" type="text/css" />
  <link rel="stylesheet" href="/tandava/2010/templates/rt_juxta_j15/css/backgrounds.css" type="text/css" />
  <link rel="stylesheet" href="/tandava/2010/templates/rt_juxta_j15/css/fusionmenu.css" type="text/css" />
  <style type="text/css">
    <!--
#rt-main-surround ul.menu li.active > a, #rt-main-surround ul.menu li.active > .separator, #rt-main-surround ul.menu li.active > .item, #rt-main-surround .square4 ul.menu li:hover > a, #rt-main-surround .square4 ul.menu li:hover > .item, #rt-main-surround .square4 ul.menu li:hover > .separator, .roktabs-links ul li.active span, .menutop li:hover > .item, .menutop li.f-menuparent-itemfocus .item, .menutop li.active > .item {color:#701110;}
a, .button, #rt-main-surround ul.menu a:hover, #rt-main-surround ul.menu .separator:hover, #rt-main-surround ul.menu .item:hover, .title1 .module-title .title {color:#701110;}body #rt-logo {width:600px;height:200px;}
    -->
  </style>
  <script type="text/javascript" src="/tandava/2010/media/system/js/mootools.js"></script>
  <script type="text/javascript" src="/tandava/2010/media/system/js/caption.js"></script>
  <script type="text/javascript" src="/tandava/2010/plugins/system/rokbox/rokbox.js"></script>
  <script type="text/javascript" src="/tandava/2010/plugins/system/rokbox/themes/dark/rokbox-config.js"></script>
  <script type="text/javascript" src="/tandava/2010/components/com_gantry/js/gantry-buildspans.js"></script>
  <script type="text/javascript" src="/tandava/2010/components/com_gantry/js/gantry-inputs.js"></script>
  <script type="text/javascript" src="/tandava/2010/modules/mod_roknavmenu/themes/fusion/js/fusion.js"></script>
  <script type="text/javascript">
var rokboxPath = '/tandava/2010/plugins/system/rokbox/';
			window.addEvent('domready', function() {
				var modules = ['rt-block'];
				var header = ['h3','h2','h1'];
				GantryBuildSpans(modules, header);
			});
		InputsExclusion.push('.content_vote','#rt-popup','#vmMainPage')
		        window.addEvent('load', function() {
					new Fusion('ul.menutop', {
						pill: 0,
						effect: 'slide and fade',
						opacity: 1,
						hideDelay: 500,
						centered: 0,
						tweakInitial: {'x': 9, 'y': 6},
        				tweakSubsequent: {'x': 0, 'y': -14},
						menuFx: {duration: 200, transition: Fx.Transitions.Sine.easeOut},
						pillFx: {duration: 400, transition: Fx.Transitions.Back.easeOut}
					});
	            });
  </script>
  <script type='text/javascript'>
/*<![CDATA[*/
	var jax_live_site = 'http://www.elijah.com.br/tandava/2010/index.php';
	var jax_site_type = '1.5';
/*]]>*/
</script><script type="text/javascript" src="http://www.elijah.com.br/tandava/2010/plugins/system/pc_includes/ajax_1.3.js"></script>
</head>
	<body  class="backgroundlevel-high backgroundstyle-style3 bodylevel-high cssstyle-style3 font-family-optima font-size-is-large menu-type-fusionmenu col12 ">
		<div id="rt-mainbg-overlay">
			<div class="rt-surround-wrap"><div class="rt-surround"><div class="rt-surround2"><div class="rt-surround3">
				<div class="rt-container">
										<div id="rt-drawer">
												<div class="clear"></div>
					</div>
															<div id="rt-header-wrap"><div id="rt-header-wrap2">
												<div id="rt-header-graphic">
																				<div class="rt-header-padding">
																							<div id="rt-header">
									<div class="rt-grid-12 rt-alpha rt-omega">
    			<div class="rt-block">
    	    	<a href="/tandava/2010/" id="rt-logo"></a>
    		</div>
	    
</div>
									<div class="clear"></div>
								</div>
																								<div id="rt-navigation"><div id="rt-navigation2"><div id="rt-navigation3">
									
<div class="nopill">
	<ul class="menutop level1 " >
						<li class="item1 root" >
					<a class="orphan item bullet" href="http://www.elijah.com.br/tandava/2010/"  >
				<span>
			    				Home				   
				</span>
			</a>
			
			
	</li>	
							<li class="item164 root" >
					<a class="orphan item bullet" href="/tandava/2010/o-festival.html"  >
				<span>
			    				Festival				   
				</span>
			</a>
			
			
	</li>	
							<li class="item206 root" >
					<a class="orphan item bullet" href="/tandava/2010/programacao-tandava.html"  >
				<span>
			    				Programação				   
				</span>
			</a>
			
			
	</li>	
							<li class="item172 parent root" >
					<a class="daddy item bullet" href="/tandava/2010/atracoes-tandava.html"  >
				<span>
			    				Atrações				   
				</span>
			</a>
			
					<div class="fusion-submenu-wrapper level2 columns2">
				<div class="drop-top"></div>
				<ul class="level2 columns2">
								
							<li class="item176" >
					<a class="orphan item image" href="/tandava/2010/atracoes-tandava/funk-you.html"  >
				<span>
			    			        <img src="/tandava/2010/templates/rt_juxta_j15/images/icons/funkyou_icon.png" alt="funkyou_icon.png" />
			    				Funk You				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item196" >
					<a class="orphan item image" href="/tandava/2010/atracoes-tandava/vive-la-musique.html"  >
				<span>
			    			        <img src="/tandava/2010/templates/rt_juxta_j15/images/icons/vivelamusique_icon.png" alt="vivelamusique_icon.png" />
			    				Vive La Musique				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item204" >
					<a class="orphan item image" href="/tandava/2010/atracoes-tandava/reboot.html"  >
				<span>
			    			        <img src="/tandava/2010/templates/rt_juxta_j15/images/icons/reboot_icon.png" alt="reboot_icon.png" />
			    				Reboot				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item197" >
					<a class="orphan item image" href="/tandava/2010/atracoes-tandava/synk-recs-day-party.html"  >
				<span>
			    			        <img src="/tandava/2010/templates/rt_juxta_j15/images/icons/synk_icon.png" alt="synk_icon.png" />
			    				Synk Recs Day Party				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item192" >
					<a class="orphan item bullet" href="/tandava/2010/atracoes-tandava/rex-africa-do-sul.html"  >
				<span>
			    				Rex (África do Sul)				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item195" >
					<a class="orphan item bullet" href="/tandava/2010/atracoes-tandava/zaraus-live-portugal.html"  >
				<span>
			    				Zaraus LIVE (Portugal)				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item199" >
					<a class="orphan item bullet" href="/tandava/2010/atracoes-tandava/sallun-pedra-branca.html"  >
				<span>
			    				Sallun (Pedra Branca)				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item193" >
					<a class="orphan item bullet" href="/tandava/2010/atracoes-tandava/minimal-criminal-live.html"  >
				<span>
			    				Minimal Criminal LIVE				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item200" >
					<a class="orphan item bullet" href="/tandava/2010/atracoes-tandava/truati-live-sp.html"  >
				<span>
			    				Truati LIVE (SP)				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item198" >
					<a class="orphan item bullet" href="/tandava/2010/atracoes-tandava/demonizz-live-sp.html"  >
				<span>
			    				Demonizz LIVE (SP)				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item191" >
					<a class="orphan item bullet" href="/tandava/2010/atracoes-tandava/ver-todas.html"  >
				<span>
			    				Ver Todas...				   
				</span>
			</a>
			
			
	</li>	
										</ul>
			</div>
			
	</li>	
							<li class="item168 root" >
					<a class="orphan item bullet" href="/tandava/2010/ingressos.html"  >
				<span>
			    				Ingressos				   
				</span>
			</a>
			
			
	</li>	
							<li class="item174 parent root" >
					<a class="daddy item bullet" href="/tandava/2010/local.html"  >
				<span>
			    				Local				   
				</span>
			</a>
			
					<div class="fusion-submenu-wrapper level2">
				<div class="drop-top"></div>
				<ul class="level2">
								
							<li class="item207" >
					<a class="orphan item bullet" href="/tandava/2010/local/mapa-como-chegar-tandava-2010.html"  >
				<span>
			    				Mapa Tandava 2010				   
				</span>
			</a>
			
			
	</li>	
										</ul>
			</div>
			
	</li>	
							<li class="item173 active root" >
					<a class="orphan item bullet" href="/tandava/2010/fotos.html"  >
				<span>
			    				Fotos				   
				</span>
			</a>
			
			
	</li>	
							<li class="item175 root" >
					<span class="orphan item bullet nolink">
			    <span>
			        			    Área Restrita			    			    </span>
			</span>
			
			
	</li>	
							<li class="item49 root" >
					<a class="orphan item bullet" href="/tandava/2010/contato.html"  >
				<span>
			    				Contato				   
				</span>
			</a>
			
			
	</li>	
				</ul>
</div>

								    <div class="clear"></div>
								</div></div></div>
																						</div>
																			</div>
											</div></div>
															<div id="rt-main-surround">
												<div id="rt-breadcrumbs"><div id="rt-breadcrumbs2"><div id="rt-breadcrumbs3">
								<div class="rt-breadcrumb-surround">
		<a href="http://www.elijah.com.br/tandava/2010/" id="breadcrumbs-home"></a>
		<span class="breadcrumbs pathway">
<span class="no-link">Fotos</span></span>
	</div>
	
							<div class="clear"></div>
						</div></div></div>
																							              <div id="rt-main" class="mb8-sa4">
                <div class="rt-main-inner">
                    <div class="rt-grid-8 ">
                                                						<div class="rt-block">
							<div class="default">
	                            <div id="rt-mainbody">
	                                
<div class="rt-joomla ">
	<div class="rt-blog">

				<h1 class="rt-pagetitle">Fotos do Tandava Gathering</h1>
		
				<div class="rt-description">
										<p>Do festival, restam sempre as lembranças, as memórias e as estórias de um final de semana prolongado com boa música e ótimos amigos. Confira abaixo as fotos das últimas edições do <strong>Tandava Gathering</strong>.<strong><br /></strong></p>
<blockquote>
<p>"O respeito existe, mas  o  publico não, existe um encontro entre amigos e  amigos de amigos."</p>
</blockquote>
<p><cite><a href="index.php?option=com_content&amp;view=category&amp;layout=blog&amp;id=41&amp;Itemid=165">Tandava Gathering</a></cite>, Edição 2010.</p><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/fotos.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>					</div>
		
				<div class="rt-leading-articles">
										
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/fotos/85-fotos-tandava-2009.html">Fotos do Tandava 2009</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<p>A segunda edição do <strong>Tandava Gathering </strong>aconteceu em um ensolarado final de semana nos dias 13, 14 e 15 de Novembro de 2009 na Chácara Santa Rita nos arredores de Curitiba.</p>
<p>Uma experiência sem igual e um encontro de amigos e amigos de amigos. Confira as fotos:</p>
<h3>Fotos by MushPics.com</h3>
<p><a href="images/stories/fotos/2009/mushpics/4117814581_d2b9a191a0_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4117814581_d2b9a191a0_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4117815193_a4c6f65538_o.jpg" rel="rokbox[465 700](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4117815193_a4c6f65538_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4117816697_83b34ac07d_o.jpg" rel="rokbox[700 467](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4117816697_83b34ac07d_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4117817117_246b473b1e_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4117817117_246b473b1e_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4117817315_833017e668_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4117817315_833017e668_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4117821597_c92c3df5ea_o.jpg" rel="rokbox[700 467](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4117821597_c92c3df5ea_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4117823117_a81907a771_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4117823117_a81907a771_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4117823801_807defb198_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4117823801_807defb198_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4117824589_d642d47a50_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4117824589_d642d47a50_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4117825125_f9f1b8c0a2_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4117825125_f9f1b8c0a2_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4117825285_372f0322b8_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4117825285_372f0322b8_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4117825423_db59f5b0b0_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4117825423_db59f5b0b0_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4117825843_d67d7bdbe7_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4117825843_d67d7bdbe7_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4117827221_9e45ef6974_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4117827221_9e45ef6974_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4117827563_a6f8eb8fc3_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4117827563_a6f8eb8fc3_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4117828435_4fe4d58311_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4117828435_4fe4d58311_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4117829043_08dace3785_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4117829043_08dace3785_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4117829347_44b4971d2f_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4117829347_44b4971d2f_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4117829521_1b4984b36b_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4117829521_1b4984b36b_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4117829755_ae86480dfd_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4117829755_ae86480dfd_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4117830479_3df9728d85_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4117830479_3df9728d85_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4117832057_978c301ede_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4117832057_978c301ede_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4117834449_7265000121_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4117834449_7265000121_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4117835001_7d59ee54e7_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4117835001_7d59ee54e7_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4117835781_5fef1ffff0_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4117835781_5fef1ffff0_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4117836397_6e7a4de2c2_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4117836397_6e7a4de2c2_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4117842223_1b8ed64400_o.jpg" rel="rokbox[465 700](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4117842223_1b8ed64400_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4117842809_7dd43ee02b_o.jpg" rel="rokbox[465 700](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4117842809_7dd43ee02b_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4117843589_7ca9d9a308_o.jpg" rel="rokbox[465 700](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4117843589_7ca9d9a308_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4117844785_db004f29e0_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4117844785_db004f29e0_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4117845225_df4e243dd4_o.jpg" rel="rokbox[465 700](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4117845225_df4e243dd4_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4117846615_96c53e9f30_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4117846615_96c53e9f30_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4117850475_5962629947_o.jpg" rel="rokbox[700 446](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4117850475_5962629947_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4117851343_19f112574b_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4117851343_19f112574b_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4117851589_41368ebffc_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4117851589_41368ebffc_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4117851983_83a713e83e_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4117851983_83a713e83e_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4117852783_fb718d64f7_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4117852783_fb718d64f7_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4117854107_c138e9e97f_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4117854107_c138e9e97f_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4117855107_3e52437fb2_o.jpg" rel="rokbox[465 700](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4117855107_3e52437fb2_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4117856645_609a7b09ab_o.jpg" rel="rokbox[465 700](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4117856645_609a7b09ab_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4117857573_9e3dcd3aac_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4117857573_9e3dcd3aac_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4117858705_e275c0f29c_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4117858705_e275c0f29c_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4117859153_ec29672009_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4117859153_ec29672009_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4117860345_d3cd49ba33_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4117860345_d3cd49ba33_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4117860913_41fc3c7336_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4117860913_41fc3c7336_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4117862273_0795b8c2ef_o.jpg" rel="rokbox[465 700](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4117862273_0795b8c2ef_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4117863289_5e1d0222c3_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4117863289_5e1d0222c3_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4117865517_7b6139a122_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4117865517_7b6139a122_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4118584738_91d8545dd1_o.jpg" rel="rokbox[465 700](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4118584738_91d8545dd1_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4118585228_43900d7955_o.jpg" rel="rokbox[465 700](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4118585228_43900d7955_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4118585818_2d30ac26af_o.jpg" rel="rokbox[465 700](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4118585818_2d30ac26af_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4118586650_a1d64b6d6f_o.jpg" rel="rokbox[465 700](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4118586650_a1d64b6d6f_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4118588076_e9674f7f34_o.jpg" rel="rokbox[465 700](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4118588076_e9674f7f34_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4118588650_0a7090c28a_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4118588650_0a7090c28a_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4118589142_3ba6cf5544_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4118589142_3ba6cf5544_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4118589770_06a85a44e2_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4118589770_06a85a44e2_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4118590022_2d163b0989_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4118590022_2d163b0989_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4118590794_aa3406ea75_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4118590794_aa3406ea75_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4118591038_1bcfd1f8f9_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4118591038_1bcfd1f8f9_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4118591686_4e0f46af0a_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4118591686_4e0f46af0a_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4118591930_d7cf8db020_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4118591930_d7cf8db020_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4118592354_79ccb852b0_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4118592354_79ccb852b0_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4118592974_465fa08448_o.jpg" rel="rokbox[465 700](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4118592974_465fa08448_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4118593492_c41751e5fa_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4118593492_c41751e5fa_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4118594476_c441608501_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4118594476_c441608501_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4118595616_27fbd00289_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4118595616_27fbd00289_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4118595838_92d9dd18b1_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4118595838_92d9dd18b1_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4118596336_a83e8e7ee7_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4118596336_a83e8e7ee7_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4118596598_1ecfcb6f51_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4118596598_1ecfcb6f51_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4118598318_21b991dba6_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4118598318_21b991dba6_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4118599152_e00e18b256_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4118599152_e00e18b256_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4118599634_33eac75c94_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4118599634_33eac75c94_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4118600242_f04deb4928_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4118600242_f04deb4928_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4118600434_5b3d4313fa_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4118600434_5b3d4313fa_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4118601302_7e65356eee_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4118601302_7e65356eee_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4118601826_21f7544cbb_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4118601826_21f7544cbb_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4118602018_860ca04bd1_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4118602018_860ca04bd1_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4118602202_7ba31f26ed_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4118602202_7ba31f26ed_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4118602392_8e753b6670_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4118602392_8e753b6670_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4118602734_be12cc99d2_o.jpg" rel="rokbox[465 700](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4118602734_be12cc99d2_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4118605426_6e1fbcfd8a_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4118605426_6e1fbcfd8a_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4118605764_80fd259301_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4118605764_80fd259301_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4118606608_ca89c64f0c_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4118606608_ca89c64f0c_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4118606944_f94543cff4_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4118606944_f94543cff4_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4118607284_531def8a5a_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4118607284_531def8a5a_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4118607516_a1ff62a4c2_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4118607516_a1ff62a4c2_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4118607760_6e4c0e0cd7_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4118607760_6e4c0e0cd7_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4118608588_78eb325c59_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4118608588_78eb325c59_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4118609120_f448ec2d10_o.jpg" rel="rokbox[465 700](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4118609120_f448ec2d10_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4118609552_0e47479eb7_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4118609552_0e47479eb7_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4118611720_86cb908e72_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4118611720_86cb908e72_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4118612310_c0fc307430_o.jpg" rel="rokbox[465 700](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4118612310_c0fc307430_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4118615858_dddcb2701b_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4118615858_dddcb2701b_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4118617440_36f3246e37_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4118617440_36f3246e37_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4118618002_328ef466ce_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4118618002_328ef466ce_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4118618200_acb73fb0d5_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4118618200_acb73fb0d5_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4118618834_5b78b73965_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4118618834_5b78b73965_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4118619248_835e44f3ee_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4118619248_835e44f3ee_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4118619610_a376b1898e_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4118619610_a376b1898e_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4118620914_9975dbba8f_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4118620914_9975dbba8f_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4118622522_65c9c2af7b_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4118622522_65c9c2af7b_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4118623044_28592700e3_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4118623044_28592700e3_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4118623914_979b4e08a5_o.jpg" rel="rokbox[465 700](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4118623914_979b4e08a5_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4118625810_4dbcc6c00e_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4118625810_4dbcc6c00e_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4118627174_7837ec6d90_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4118627174_7837ec6d90_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4118629502_4ec4f2b9b9_o(2).jpg" rel="rokbox[700 466](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4118629502_4ec4f2b9b9_o(2)_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4118629502_4ec4f2b9b9_o.jpg" rel="rokbox[700 466](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4118629502_4ec4f2b9b9_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4118629970_e7f2e623b5_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4118629970_e7f2e623b5_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4118632826_8a3426b5b6_o.jpg" rel="rokbox[465 700](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4118632826_8a3426b5b6_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/mushpics/4118635110_9258f54c98_o.jpg" rel="rokbox[700 465](tandava2009)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//4118635110_9258f54c98_o_thumb.jpg" alt="" /></a> </p>
<h3>Fotos da Galera</h3>
<h5>by TaePsy</h5>
<p><a href="images/stories/fotos/2009/taepsy/DSC02879.JPG" rel="rokbox[2592 1944](tandava2009-taepsy)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC02879_thumb.JPG" alt="" /></a> <a href="images/stories/fotos/2009/taepsy/DSC02936.JPG" rel="rokbox[2592 1944](tandava2009-taepsy)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC02936_thumb.JPG" alt="" /></a> <a href="images/stories/fotos/2009/taepsy/DSC02977.JPG" rel="rokbox[1944 2592](tandava2009-taepsy)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC02977_thumb.JPG" alt="" /></a> <a href="images/stories/fotos/2009/taepsy/DSC02981.JPG" rel="rokbox[1944 2592](tandava2009-taepsy)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC02981_thumb.JPG" alt="" /></a> <a href="images/stories/fotos/2009/taepsy/DSC02998.JPG" rel="rokbox[2592 1944](tandava2009-taepsy)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC02998_thumb.JPG" alt="" /></a> <a href="images/stories/fotos/2009/taepsy/DSC03025.JPG" rel="rokbox[2592 1944](tandava2009-taepsy)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//DSC03025_thumb.JPG" alt="" /></a> <a href="images/stories/fotos/2009/taepsy/tandava021.jpg" rel="rokbox[614 819](tandava2009-taepsy)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//tandava021_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/taepsy/tandava034.jpg" rel="rokbox[819 614](tandava2009-taepsy)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//tandava034_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/taepsy/tandava038.jpg" rel="rokbox[819 614](tandava2009-taepsy)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//tandava038_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/taepsy/tandava055.jpg" rel="rokbox[614 819](tandava2009-taepsy)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//tandava055_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/taepsy/tandava061.jpg" rel="rokbox[614 819](tandava2009-taepsy)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//tandava061_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/taepsy/tandava071.jpg" rel="rokbox[819 614](tandava2009-taepsy)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//tandava071_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2009/taepsy/tandava095.jpg" rel="rokbox[614 819](tandava2009-taepsy)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//tandava095_thumb.jpg" alt="" /></a> </p>
<div class="camera">
<div class="typo-icon">
<p><strong>Envie suas fotos!</strong></p>
<p>Se você participou do Tandava Gathering 2009, colabore! Envie suas fotos e faça parte desta história.</p>
<p><a href="index.php?option=com_contact&amp;view=contact&amp;id=1&amp;Itemid=49">Clique Aqui</a> e entre em contato conosco.</p>
</div>
</div><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/fotos/85-fotos-tandava-2009.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/fotos/85-fotos-tandava-2009.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/fotos/85-fotos-tandava-2009.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL3d3dy5lbGlqYWguY29tLmJyL3RhbmRhdmEvMjAxMC9mb3Rvcy84NS1mb3Rvcy10YW5kYXZhLTIwMDkuaHRtbA%3D%3D" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Quinta, 10 de Junho de 2010 00:09				</span>
					
								<span class="rt-date-modified">
					Última atualização em Quarta, 17 de Novembro de 2010 18:04				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>									
<div class="rt-article ">
	<div class="rt-article-bg">
	
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/fotos/97-fotos-tandava-2008.html">Fotos do Tandava 2008</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
				<div class="rt-article-content">
					
						
					
			<p>A primeira edição do <strong>Tandava Gathering </strong>aconteceu em um ensolarado final de semana de 2008 nos arredores de Curitiba.</p>
<p>Uma experiência sem igual e o começo de um encontro de amigos e amigos de amigos. Confira as fotos:</p>
<h3>Fotos by MushPics.com</h3>
<p><a href="images/stories/fotos/2008/mushpics/3044318491_82ecd3a68d_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3044318491_82ecd3a68d_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3044319217_ea5cc1580e_o.jpg" rel="rokbox[426 640](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3044319217_ea5cc1580e_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3044319709_0a3d0b0c70_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3044319709_0a3d0b0c70_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3044319901_0550b34718_o.jpg" rel="rokbox[426 640](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3044319901_0550b34718_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3044319989_a1528ec8af_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3044319989_a1528ec8af_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3044320079_452eed9680_o.jpg" rel="rokbox[426 640](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3044320079_452eed9680_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3044320303_1a369fcb78_o.jpg" rel="rokbox[426 640](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3044320303_1a369fcb78_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3044320727_3913e72bd9_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3044320727_3913e72bd9_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3044320837_fb9e60d27d_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3044320837_fb9e60d27d_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3044321679_718515fba3_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3044321679_718515fba3_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3044321763_22555146df_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3044321763_22555146df_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3044322235_dbc1a7e648_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3044322235_dbc1a7e648_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3044322943_dcb7bea27e_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3044322943_dcb7bea27e_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3044323115_ae3c500dc1_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3044323115_ae3c500dc1_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3044323731_f166dcf5b6_o.jpg" rel="rokbox[640 424](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3044323731_f166dcf5b6_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3044323865_a0c72976bc_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3044323865_a0c72976bc_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3044323951_615835273a_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3044323951_615835273a_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3044324181_1a98556578_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3044324181_1a98556578_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3044324341_d1ff086dd8_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3044324341_d1ff086dd8_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3044324427_10104ddfaf_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3044324427_10104ddfaf_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3044324615_83b9ceed8d_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3044324615_83b9ceed8d_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3044324733_86f11375cd_o.jpg" rel="rokbox[428 640](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3044324733_86f11375cd_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3044324935_f9cb87237f_o.jpg" rel="rokbox[640 428](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3044324935_f9cb87237f_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3044325087_3cd1bbec4c_o.jpg" rel="rokbox[640 428](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3044325087_3cd1bbec4c_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3044325173_7c39d001ed_o.jpg" rel="rokbox[428 640](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3044325173_7c39d001ed_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3044325259_70d874f4e6_o.jpg" rel="rokbox[428 640](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3044325259_70d874f4e6_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3044325315_c5cdea854c_o.jpg" rel="rokbox[428 640](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3044325315_c5cdea854c_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3044325449_b3a48c4587_o.jpg" rel="rokbox[640 427](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3044325449_b3a48c4587_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3044325749_741bff06ca_o.jpg" rel="rokbox[428 640](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3044325749_741bff06ca_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3044326487_0188b54cc5_o.jpg" rel="rokbox[640 428](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3044326487_0188b54cc5_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3044326653_a0de882903_o.jpg" rel="rokbox[640 427](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3044326653_a0de882903_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3044326739_bdd7bfaca5_o.jpg" rel="rokbox[640 427](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3044326739_bdd7bfaca5_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3044327549_0dd50efb37_o.jpg" rel="rokbox[428 640](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3044327549_0dd50efb37_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3044327765_768206d374_o.jpg" rel="rokbox[640 428](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3044327765_768206d374_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3044327941_0b54dde364_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3044327941_0b54dde364_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3044328155_1a19af23b4_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3044328155_1a19af23b4_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3044328273_8e524141f9_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3044328273_8e524141f9_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3044328359_24f6bb0a78_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3044328359_24f6bb0a78_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3044328637_4020de0681_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3044328637_4020de0681_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3044328901_bc256f9cb7_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3044328901_bc256f9cb7_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3044329193_c83923cdc4_o.jpg" rel="rokbox[426 640](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3044329193_c83923cdc4_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3044329809_2189591a34_o.jpg" rel="rokbox[426 640](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3044329809_2189591a34_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3044329873_56ff4f863b_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3044329873_56ff4f863b_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3044330121_bdbebae5f8_o.jpg" rel="rokbox[426 640](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3044330121_bdbebae5f8_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3044330307_83de156808_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3044330307_83de156808_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3045155270_808063485c_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3045155270_808063485c_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3045155422_bd8622ab87_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3045155422_bd8622ab87_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3045155540_f096abdc8b_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3045155540_f096abdc8b_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3045155630_02e1af8980_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3045155630_02e1af8980_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3045155716_a310e51ca4_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3045155716_a310e51ca4_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3045155818_76117c1a65_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3045155818_76117c1a65_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3045155884_0fc4f93374_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3045155884_0fc4f93374_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3045156098_eecd911f15_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3045156098_eecd911f15_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3045156244_677fa05a7f_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3045156244_677fa05a7f_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3045156324_db8f6f3650_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3045156324_db8f6f3650_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3045156442_c6350d9207_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3045156442_c6350d9207_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3045156658_c2fbaaac53_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3045156658_c2fbaaac53_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3045157130_f0cd16045d_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3045157130_f0cd16045d_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3045157306_d10733c177_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3045157306_d10733c177_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3045157434_cf7490885e_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3045157434_cf7490885e_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3045157794_0428747729_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3045157794_0428747729_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3045158076_8c77536b77_o.jpg" rel="rokbox[426 640](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3045158076_8c77536b77_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3045158788_dcf89aa7f5_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3045158788_dcf89aa7f5_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3045158880_1fe501f096_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3045158880_1fe501f096_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3045158976_165c734615_o.jpg" rel="rokbox[426 640](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3045158976_165c734615_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3045159532_f8dbc5dbee_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3045159532_f8dbc5dbee_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3045159954_841fef01a5_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3045159954_841fef01a5_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3045160118_9b08ea86c9_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3045160118_9b08ea86c9_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3045160190_dd282c7688_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3045160190_dd282c7688_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3045160290_44e0bcee29_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3045160290_44e0bcee29_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3045160400_7859afd34e_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3045160400_7859afd34e_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3045160528_a4d96f9f6f_o.jpg" rel="rokbox[426 640](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3045160528_a4d96f9f6f_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3045160722_3fbcd25f80_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3045160722_3fbcd25f80_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3045160958_97fafe6e7c_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3045160958_97fafe6e7c_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3045161156_af2101ed24_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3045161156_af2101ed24_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3045161398_085b48a8e6_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3045161398_085b48a8e6_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3045161704_a6522d92cf_o.jpg" rel="rokbox[640 416](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3045161704_a6522d92cf_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3045162480_57e0f4cf12_o.jpg" rel="rokbox[640 428](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3045162480_57e0f4cf12_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3045162682_77f31f8675_o.jpg" rel="rokbox[640 428](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3045162682_77f31f8675_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3045163356_7948e22eca_o.jpg" rel="rokbox[640 428](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3045163356_7948e22eca_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3045163616_4331209b6f_o.jpg" rel="rokbox[428 640](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3045163616_4331209b6f_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3045163690_31b2ffb9ab_o.jpg" rel="rokbox[640 428](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3045163690_31b2ffb9ab_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3045164342_28fcf0443d_o.jpg" rel="rokbox[428 640](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3045164342_28fcf0443d_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3045164502_071fd57998_o.jpg" rel="rokbox[428 640](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3045164502_071fd57998_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3045164714_5bd8e1da93_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3045164714_5bd8e1da93_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3045164876_e51b4c3ec3_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3045164876_e51b4c3ec3_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3045165266_9987036070_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3045165266_9987036070_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3045165342_b8efd0ddb0_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3045165342_b8efd0ddb0_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3045165554_53209119a4_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3045165554_53209119a4_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3045165684_366f15ddfa_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3045165684_366f15ddfa_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3045165900_4d4ea7f839_o.jpg" rel="rokbox[426 640](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3045165900_4d4ea7f839_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3045166142_853bc2f38e_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3045166142_853bc2f38e_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3045166248_5cf75c5408_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3045166248_5cf75c5408_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3045166418_865de31134_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3045166418_865de31134_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3045166572_d07d4307d9_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3045166572_d07d4307d9_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3045166788_e008b7c109_o.jpg" rel="rokbox[426 640](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3045166788_e008b7c109_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3045166850_37411c373f_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3045166850_37411c373f_o_thumb.jpg" alt="" /></a> <a href="images/stories/fotos/2008/mushpics/3045167026_d5c32ee122_o.jpg" rel="rokbox[640 426](tandava2008)" title=""><img class="album" src="http://www.elijah.com.br/tandava/2010/cache//3045167026_d5c32ee122_o_thumb.jpg" alt="" /></a> </p>
<div class="camera">
<div class="typo-icon">
<p><strong>Envie suas fotos!</strong></p>
<p>Se você participou do Tandava Gathering 2008, colabore! Envie suas fotos e faça parte desta história.</p>
<p><a href="index.php?option=com_contact&amp;view=contact&amp;id=1&amp;Itemid=49">Clique Aqui</a> e entre em contato conosco.</p>
</div>
</div><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elijah.com.br/tandava/2010/fotos/97-fotos-tandava-2008.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				
						
						<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
								<div class="rt-article-icons">
					<a href="/tandava/2010/fotos/97-fotos-tandava-2008.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/fotos/97-fotos-tandava-2008.html?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL3d3dy5lbGlqYWguY29tLmJyL3RhbmRhdmEvMjAxMC9mb3Rvcy85Ny1mb3Rvcy10YW5kYXZhLTIwMDguaHRtbA%3D%3D" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>									</div>
							
								<span class="rt-date-posted">
					Quarta, 09 de Junho de 2010 00:09				</span>
					
								<span class="rt-date-modified">
					Última atualização em Terça, 13 de Julho de 2010 20:04				</span>
					
								<span class="rt-author">
					Postado por de elijah hatem				</span>
					
							</div></div></div>
						
		</div>
		
			</div>
</div>				</div>
		
				<div class="clear"></div>
		
				<div class="rt-pagination">
						<p class="rt-results">
				Página 2 de 2			</p>
						
<div class="tab">
<div class="tab2"><div class="page-inactive"><a href="/tandava/2010/fotos.html" title="Início">Início</a></div></div></div>
<div class="tab">
<div class="tab2"><div class="page-inactive"><a href="/tandava/2010/fotos.html" title="Anterior">Anterior</a></div></div></div>
<div class="page-block"><div class="page-inactive"><a href="/tandava/2010/fotos.html" title="1">1</a></div></div>
<div class="page-block"><div class="page-active">2</div></div>
<div class="tab">
<div class="tab2"><div class="page-active">Próximo</div></div></div>
<div class="tab">
<div class="tab2"><div class="page-active">Fim</div></div></div>		</div>
				
	</div>
</div>
	                            </div>
								<div class="clear"></div>
							</div>
						</div>
                                                                    </div>
                                <div class="rt-grid-4 ">
                <div id="rt-sidebar-a">
                                        <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3"><h2 class="title">Próximo Tandava...</h2></div></div></div></div>
						<div class="clear"></div>
		                						<div class="module-content">
		                	<script src="modules/mod_countdown/scripts/swfobject_modified.js" type="text/javascript"></script>

			<object id="FlashID" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="260" height="80">

  <param name="movie" value="modules/mod_countdown/countdown.swf?dayText=Dias&texto=Contagem Regressiva&hoursText=Horas&ano=2011&mont=11&dia=12&minutesText=Minutos&secondsText=Segs&displayColor=0x7011100&textoColor=0x333333&dayColor=0x333333&hoursColor=0x333333&minutesColor=0x333333&secondsColor=0x333333&pozadinaColor=0xd6d6d4&linijaColor=0xd6d6d4&trans=" />

  <param name="quality" value="high" />

  <param name="wmode" value="transparent" />

  <param name="swfversion" value="6.0.65.0" />

  

  <!-- This param tag prompts users with Flash Player 6.0 r65 and higher to download the latest version of Flash Player. Delete it if you don’t want users to see the prompt. -->

  <param name="expressinstall" value="modules/mod_countdown/scripts/expressInstall.swf" />

  <!-- Next object tag is for non-IE browsers. So hide it from IE using IECC. -->

  <!--[if !IE]>-->

  <object type="application/x-shockwave-flash" data="modules/mod_countdown/countdown.swf?dayText=Dias&texto=Contagem Regressiva&hoursText=Horas&ano=2011&mont=11&dia=12&minutesText=Minutos&secondsText=Segs&displayColor=0x701110&textoColor=0x333333&dayColor=0x333333&hoursColor=0x333333&minutesColor=0x333333&secondsColor=0x333333&pozadinaColor=0xd6d6d4&linijaColor=0xd6d6d4&trans=" width="260" height="80">

    <!--<![endif]-->

    <param name="quality" value="high" />

    <param name="wmode" value="transparent" />

    <param name="swfversion" value="6.0.65.0" />

    <param name="expressinstall" value="modules/mod_countdown/scripts/expressInstall.swf" />

	

    <!-- The browser displays the following alternative content for users with Flash Player 6.0 and older. -->

    <div>

      <h4>Content on this page requires a newer version of Adobe Flash Player.</h4>

      <p><a href="http://www.adobe.com/go/getflashplayer"><img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" width="112" height="33" /></a></p>

    </div>

    <!--[if !IE]>-->

  </object>

  <!--<![endif]-->

</object>

<script type="text/javascript">

<!--

swfobject.registerObject("FlashID");

//-->

</script>

								</div>
					</div>
				</div>
            </div>
        	                    <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3"><h2 class="title">Main Menu</h2></div></div></div></div>
						<div class="clear"></div>
		                						<div class="module-content">
		                	<ul class="menu level1" >
				<li class="item1" >
					<a class="orphan item bullet" href="http://www.elijah.com.br/tandava/2010/"  >
				<span>
			    				Home				   
				</span>
			</a>
			
			
	</li>	
					<li class="item164" >
					<a class="orphan item bullet" href="/tandava/2010/o-festival.html"  >
				<span>
			    				Festival				   
				</span>
			</a>
			
			
	</li>	
					<li class="item206" >
					<a class="orphan item bullet" href="/tandava/2010/programacao-tandava.html"  >
				<span>
			    				Programação				   
				</span>
			</a>
			
			
	</li>	
					<li class="item172 parent" >
					<a class="orphan item bullet" href="/tandava/2010/atracoes-tandava.html"  >
				<span>
			    				Atrações				   
				</span>
			</a>
			
			
	</li>	
					<li class="item168" >
					<a class="orphan item bullet" href="/tandava/2010/ingressos.html"  >
				<span>
			    				Ingressos				   
				</span>
			</a>
			
			
	</li>	
					<li class="item174 parent" >
					<a class="orphan item bullet" href="/tandava/2010/local.html"  >
				<span>
			    				Local				   
				</span>
			</a>
			
			
	</li>	
					<li class="item173 active" >
					<a class="orphan item bullet" href="/tandava/2010/fotos.html"  >
				<span>
			    				Fotos				   
				</span>
			</a>
			
			
	</li>	
					<li class="item175" >
					<span class="orphan item bullet nolink">
			    <span>
			        			    Área Restrita			    			    </span>
			</span>
			
			
	</li>	
					<li class="item49" >
					<a class="orphan item bullet" href="/tandava/2010/contato.html"  >
				<span>
			    				Contato				   
				</span>
			</a>
			
			
	</li>	
		</ul>
						</div>
					</div>
				</div>
            </div>
        	                    <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3"><h2 class="title">Usuários Online</h2></div></div></div></div>
						<div class="clear"></div>
		                						<div class="module-content">
		                	<div>
	<ul style="list-style:none; margin: 0 0 10px; padding: 0;">
		
	
	</ul>
	
	<div>
		0 participantes		
					e 1 visitante				
		online 
		<br /><br />
        <div>
		<a href="/tandava/2010/area-restrita/comunidade/search/browse.html?sort=online">Ver Todos Participantes</a>
		</div>
	</div>
</div>						</div>
					</div>
				</div>
            </div>
        	                    <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3"><h2 class="title">Sua Opinião</h2></div></div></div></div>
						<div class="clear"></div>
		                						<div class="module-content">
		                	
<h4 class="rt-polltitle">
	Qual é a principal qualidade do Tandava para você?</h4>
<form action="index.php" method="post" name="form2" class="rt-poll">
	<fieldset>
				<div class="rt-pollrow">
			<input type="radio" name="voteid" id="voteid13" value="13" alt="13" />
			<label for="voteid13">
				Um encontro sem fins lucrativos.			</label>
		</div>
				<div class="rt-pollrow">
			<input type="radio" name="voteid" id="voteid14" value="14" alt="14" />
			<label for="voteid14">
				Reúne vários tipos de música.			</label>
		</div>
				<div class="rt-pollrow">
			<input type="radio" name="voteid" id="voteid15" value="15" alt="15" />
			<label for="voteid15">
				Clima familiar dos participantes.			</label>
		</div>
				<div class="rt-pollrow">
			<input type="radio" name="voteid" id="voteid16" value="16" alt="16" />
			<label for="voteid16">
				Entrada com bebidas liberada.			</label>
		</div>
				<div class="rt-pollrow">
			<input type="radio" name="voteid" id="voteid17" value="17" alt="17" />
			<label for="voteid17">
				Adoro acampar.			</label>
		</div>
			</fieldset>
	<div class="rt-pollbuttons">
		<div class="readon">
			<input type="submit" name="task_button" class="button" value="Votar" />
		</div>
		<div class="readon">
			<input type="button" name="option" class="button" value="Resultados" onclick="document.location.href='/tandava/2010/component/poll/15-qual-e-a-principal-qualidade-do-tandava-para-voce.html'" />
		</div>
	</div>

	<input type="hidden" name="option" value="com_poll" />
	<input type="hidden" name="task" value="vote" />
	<input type="hidden" name="id" value="15" />
	<input type="hidden" name="e035af4cbdfaa9386b9d4e1c74822a69" value="1" /></form>
						</div>
					</div>
				</div>
            </div>
        	                    <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3"><h2 class="title">Área Restrita</h2></div></div></div></div>
						<div class="clear"></div>
		                						<div class="module-content">
		                	<form action="/tandava/2010/fotos.html" method="post" name="login" id="form-login" >
		<fieldset class="input">
	<p id="form-login-username">
		<label for="modlgn_username">Email</label><br />
		<input id="modlgn_username" type="text" name="username" class="inputbox" alt="username" size="18" />
	</p>
	<p id="form-login-password">
		<label for="modlgn_passwd">Senha</label><br />
		<input id="modlgn_passwd" type="password" name="passwd" class="inputbox" size="18" alt="password" />
	</p>
		<p id="form-login-remember">
		<input type="checkbox" name="remember" class="checkbox" value="yes" alt="Lembrar-me" />
		<label class="remember">
			Lembrar-me		</label>
	</p>
		<div class="readon"><input type="submit" name="Submit" class="button" value="Entrar" /></div>
	</fieldset>
	<ul>
		<li>
			<a href="/tandava/2010/component/user/reset.html">
			Esqueci minha senha</a>
		</li>
		<li>
			<a href="/tandava/2010/component/user/remind.html">
			Esqueci meu nome de usuário</a>
		</li>
			</ul>
	
	<input type="hidden" name="option" value="com_user" />
	<input type="hidden" name="task" value="login" />
	<input type="hidden" name="return" value="L3RhbmRhdmEvMjAxMC9hcmVhLXJlc3RyaXRhL2NvbXVuaWRhZGUuaHRtbA==" />
	<input type="hidden" name="e035af4cbdfaa9386b9d4e1c74822a69" value="1" /></form>
						</div>
					</div>
				</div>
            </div>
        	
                </div>
            </div>

                    <div class="clear"></div>
                </div>
            </div>
																								<div id="rt-bottom">
							<div class="rt-grid-4 rt-alpha">
                        <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3"><h2 class="title">Últimas Atualizações</h2></div></div></div></div>
						<div class="clear"></div>
		                						<div class="module-content">
		                	<ul class="latestnews">
	<li class="latestnews">
		<a href="/tandava/2010/fotos/143-fotos-do-tandava-2010.html" class="latestnews">
			Fotos do Tandava 2010</a>
	</li>
	<li class="latestnews">
		<a href="/tandava/2010/fotos/144-o-tandava-2010.html" class="latestnews">
			O Tandava 2010</a>
	</li>
	<li class="latestnews">
		<a href="/tandava/2010/o-festival.html" class="latestnews">
			Tandava Gathering</a>
	</li>
	<li class="latestnews">
		<a href="/tandava/2010/local/mapa-como-chegar-tandava-2010.html" class="latestnews">
			Mapa Tandava 2010</a>
	</li>
	<li class="latestnews">
		<a href="/tandava/2010/local/142-sobre-o-local.html" class="latestnews">
			Sobre o Local</a>
	</li>
</ul>						</div>
					</div>
				</div>
            </div>
        	
</div>
<div class="rt-grid-4">
                        <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3"><h2 class="title">Mais Lidos</h2></div></div></div></div>
						<div class="clear"></div>
		                						<div class="module-content">
		                	<ul class="mostread">
	<li class="mostread">
		<a href="/tandava/2010/component/content/article/48-terceira-edicao/144-o-tandava-2010.html" class="mostread">
			O Tandava 2010</a>
	</li>
	<li class="mostread">
		<a href="/tandava/2010/atracoes-tandava/funk-you.html" class="mostread">
			Funk You</a>
	</li>
	<li class="mostread">
		<a href="/tandava/2010/fotos/85-fotos-tandava-2009.html" class="mostread">
			Fotos do Tandava 2009</a>
	</li>
	<li class="mostread">
		<a href="/tandava/2010/local/mapa-como-chegar-tandava-2010.html" class="mostread">
			Mapa Tandava 2010</a>
	</li>
	<li class="mostread">
		<a href="/tandava/2010/atracoes-tandava/synk-recs-day-party.html" class="mostread">
			Synk Recs Day Party</a>
	</li>
</ul>						</div>
					</div>
				</div>
            </div>
        	
</div>
<div class="rt-grid-4 rt-omega">
                        <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3"><h2 class="title">Destaque</h2></div></div></div></div>
						<div class="clear"></div>
		                						<div class="module-content">
		                	<p><strong><a href="index.php?option=com_content&amp;view=category&amp;layout=blog&amp;id=44&amp;Itemid=173"><strong>Clique Aqui</strong></a></strong> e confira as <a href="index.php?option=com_content&amp;view=category&amp;layout=blog&amp;id=44&amp;Itemid=173"><strong>fotos</strong></a> tiradas pelo <strong>MushPics </strong>nas edições anteriores do festival <strong>Tandava Gathering</strong>.</p>
<p>Esteve lá? <a href="mailto:tandava@tandava.com.br?subject=Fotos">Envie suas fotos</a> e faça parte desta história.</p>						</div>
					</div>
				</div>
            </div>
        	
</div>
							<div class="clear"></div>
						</div>
																		<div id="rt-footer"><div id="rt-footer2"><div id="rt-footer3">
							<div class="rt-grid-12 rt-alpha rt-omega">
                    <div class="title1">
                    <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3"><h2 class="title">Apoio Cultural e Parceiros</h2></div></div></div></div>
						<div class="clear"></div>
		                						<div class="module-content">
		                	<table border="0" width="100%">
<tbody>
<tr>
<td><a target="_blank" href="http://www.metropolis.art.br"><img style="margin: 10px; vertical-align: middle;" alt="metropolis" src="images/stories/parceiros/metropolis.png" width="100" height="100" /></a></td>
<td><img style="margin: 10px; vertical-align: middle;" alt="sica" src="images/stories/parceiros/sica.png" width="100" height="100" /></td>
<td><a target="_blank" href="http://www.wasabiam.com.br/"><img style="margin: 10px; vertical-align: middle;" alt="wasabi" src="images/stories/parceiros/wasabi.png" width="100" height="100" /></a></td>
<td><a target="_blank" href="http://www.myspace.com/funkyoucwb"><img style="margin: 10px; vertical-align: middle;" alt="funkyou_parceiro" src="images/stories/parceiros/funkyou_parceiro.png" width="100" height="100" /></a></td>
<td><img style="margin: 10px; vertical-align: middle;" alt="revolucione" src="images/stories/parceiros/revolucione.png" width="100" height="100" /></td>
</tr>
<tr>
<td><a target="_blank" href="http://www.egralha.com.br/"><img style="margin: 5px 10px; vertical-align: middle;" alt="egralha" src="images/stories/parceiros/egralha.png" width="100" height="50" /></a></td>
<td><a target="_blank" href="http://www.vivelamusique.com.br"><img style="margin: 5px 10px; vertical-align: middle;" alt="vivelamusique_parceiro" src="images/stories/parceiros/vivelamusique_parceiro.png" width="100" height="50" /></a></td>
<td><img style="margin: 5px 10px; vertical-align: middle;" alt="makunba" src="images/stories/parceiros/makunba.png" width="100" height="40" /></td>
<td><a target="_blank" href="http://synk.com.br/"><img style="margin: 5px 10px; vertical-align: middle;" alt="synk_parceiro" src="images/stories/parceiros/synk_parceiro.png" width="100" height="50" /></a></td>
<td><a target="_blank" href="http://www.mushpics.com/"><img style="margin: 10px; vertical-align: middle;" alt="mushpics" src="images/stories/parceiros/mushpics.png" width="100" height="50" /></a></td>
</tr>
<tr>
<td><a target="_blank" href="http://leduxcwb.wordpress.com/"><img style="margin: 5px 10px; vertical-align: middle;" alt="leduxcwb" src="images/stories/parceiros/leduxcwb.png" width="100" height="40" /></a></td>
<td><img style="margin: 5px 10px; vertical-align: middle;" alt="makana" src="images/stories/parceiros/makana.png" width="100" height="40" /></td>
<td><img style="margin: 10px; vertical-align: middle;" alt="Oxydus" src="images/stories/parceiros/oxydus_parceiro.png" width="100" height="50" /></td>
<td><a target="_blank" href="http://reboot.blog.com"><img style="margin: 10px; vertical-align: middle;" alt="reboot_parceiro" src="images/stories/parceiros/reboot_parceiro.png" width="100" height="50" /></a></td>
<td><a target="_blank" href="http://www.mzc-lab.com/"><img style="margin: 10px; vertical-align: middle;" alt="mzc-lab" src="images/stories/parceiros/mzc-lab.png" width="100" height="50" /></a></td>
</tr>
</tbody>
</table>						</div>
					</div>
				</div>
            </div>
                </div>
		
</div>
							<div class="clear"></div>
						</div></div></div>
											</div>
										<div id="rt-copyright">
						<div class="rt-grid-12 rt-alpha rt-omega">
                        <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-content">
		                	<table width="100%" border="0" cellpadding="0" cellspacing="1"><tr><td nowrap="nowrap"><a href="/tandava/2010/home.html" class="mainlevel" id="active_menu">Tandava Gathering</a><span class="mainlevel">  - </span><a href="/tandava/2010/o-festival.html" class="mainlevel" >O Festival</a><span class="mainlevel">  - </span><a href="/tandava/2010/atracoes-tandava.html" class="mainlevel" >Atrações</a><span class="mainlevel">  - </span><a href="/tandava/2010/ingressos.html" class="mainlevel" >Ingressos</a><span class="mainlevel">  - </span><a href="/tandava/2010/local.html" class="mainlevel" >Local</a><span class="mainlevel">  - </span><a href="/tandava/2010/fotos.html" class="mainlevel" >Fotos</a><span class="mainlevel">  - </span><a href="/tandava/2010/contato.html" class="mainlevel" >Contato</a><span class="mainlevel">  - </span><a href="/tandava/2010/mapa-do-site.html" class="mainlevel" >Mapa do Site</a></td></tr></table>						</div>
					</div>
				</div>
            </div>
        	
</div>
						<div class="clear"></div>
					</div>
															<div id="rt-debug">
						<div class="rt-grid-12 rt-alpha rt-omega">
                        <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-content">
		                	<p style="text-align: center;"><a href="http://validator.w3.org/check?uri=referer"><img style="margin: 0px 5px; vertical-align: middle;" src="http://www.w3.org/Icons/valid-xhtml10" alt="Valid XHTML 1.0 Transitional" height="31" width="88" /></a> <a href="http://jigsaw.w3.org/css-validator/check/referer"><img style="border: 0px none; width: 88px; height: 31px; margin: 0px 5px; vertical-align: middle;" src="http://jigsaw.w3.org/css-validator/images/vcss" alt="Valid CSS!" height="31px" width="88px" /></a><a href="http://feed2.w3.org/check.cgi?url=http%3A//www.tandava.com.br/index.php%3Fformat%3Dfeed%26type%3Drss"> <img style="margin: 0px 5px; vertical-align: middle;" src="images/stories/valid-rss-rogers.png" alt="valid-rss-rogers" title="Validate my Atom 1.0 feed" height="31" width="88" /> </a></p>
<p><span style="font-size: 9pt;">Tandava Gathering website was created by <strong>de elijah hatem</strong>. Powered by Open Source CMS <a target="_blank" href="http://www.joomla.org">Joomla 1.5</a> and developed using <a target="_blank" href="http://www.gantry-framework.org"><img style="vertical-align: bottom;" alt="gantry" src="images/stories/gantry.png" height="34" width="99" /></a> framework.</span></p>						</div>
					</div>
				</div>
            </div>
        	
</div>
						<div class="clear"></div>
					</div>
									</div>
			</div></div></div></div>
		</div>
			</body>
</html>
