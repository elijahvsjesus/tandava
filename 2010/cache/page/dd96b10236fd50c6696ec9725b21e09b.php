<?php die("Access Denied"); ?>
<?xml version="1.0" encoding="utf-8"?>
<!-- generator="Joomla! 1.5 - Open Source Content Management" -->
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
	<channel>
		<title>Ingressos para o Tandava Gathering</title>
		<description>Não surgiu um novo conceito. As coisas não vão mudar e nada será resgatado.  Não  é nada melhor ou maior de tudo que você já viu. Não  é inédito e nem ao menos é um evento. O respeito existe, mas o publico não, existe um encontro entre amigos e amigos de amigos.</description>
		<link>http://elijah.com.br/tandava/2010/ingressos.html</link>
		<lastBuildDate>Tue, 19 Jul 2011 02:56:39 +0000</lastBuildDate>
		<generator>Joomla! 1.5 - Open Source Content Management</generator>
		<language>pt-br</language>
		<item>
			<title>Onde Comprar</title>
			<link>http://elijah.com.br/tandava/2010/ingressos/86-onde-comprar.html</link>
			<guid>http://elijah.com.br/tandava/2010/ingressos/86-onde-comprar.html</guid>
			<description><![CDATA[<div class="rt-article-content">
<p>Em Curitiba os ingressos podem ser encontrados na loja <strong>Sica Moda Alternativa</strong>:</p>
<ul class="bullet-a">
<li><strong>Loja Sica Moda Alternativa</strong><br />Rua Trajano Reis, 111 - São Francisco.<br />Horário de Atend.: Terça a Sexta 11h às 19h / Sábado e Domingo 11h às 16h.</li>
</ul>
<p>Muitos dos núcleos que iniciaram o Tandava continuam colaborando nessa edição, desta vez no papel de parceiros. Espalhados por diversas localidades eles garantem uma vasta distribuição dos ingressos além de Curitiba.</p>
<p>Segue abaixo uma relação para que você interessado possa não só conseguir ingressos, mas principalmente conseguir mais informações sobre o festival.</p>
<h2 style="visibility: visible;">Paraná</h2>
<h4 style="visibility: visible;">Curitiba</h4>
<ul class="bullet-a">
<li><strong>Ahlan Droid</strong> (djahlandroid@hotmail.com)<br />Água Verde.<br />Tel: (41) 9676-7800<br />Links: <a target="_blank" href="http://www.myspace.com/djahlandroid">MySpace</a></li>
<br />
<li><strong>Felipe Kantek</strong> (kantek@metropolis.art.br)<br />Centro.<br />Tel: (41) 9965-2313<br />Links: <a target="_blank" href="http://www.myspace.com/kantek">MySpace</a> - <a target="_blank" href="http://www.facebook.com/djkantek">Facebook</a> - <a target="_blank" href="http://www.twitter.com/kantek">Twitter</a></li>
<br />
<li><strong>Rodrigo "Psapo" Schultz</strong> (djpsapo@hotmail.com)<br />Bairro Mossunguê.<br />Tel: (41) 91811978 / (41) 8888-8001<br />Links: <a target="_blank" href="http://www.orkut.com.br/Main#Profile?rl=ls&amp;uid=7566503494211634731">Orkut</a> - <a target="_blank" href="http://www.myspace.com/djpsapo">MySpace</a> - <a target="_blank" href="http://www.twitter.com/djpsapo">Twitter</a></li>
<br />
<li><strong>Alejandro Bargueno</strong> (alejandrobargueno@hotmail.com)<br />Bairro Champagnat.<br />Tel: (41) 9612-2826<br />Links: <a target="_blank" href="http://soundcloud.com/tip_tronic">Soundcloud</a> - <a target="_blank" href="http://www.orkut.com.br/Main#Profile?uid=5906662220881259873">Orkut</a><a target="_blank" href="http://www.twitter.com/kantek"></a></li>
<br />
<li><strong>Gabriel Mello </strong>(psylocibinproject@hotmail.com)<br />Bairro Cristo Rei.<br />Tel: (41) 8461-3354<br />Links: <a target="_blank" href="http://www.orkut.com.br/Main#Profile?uid=10137116061140916781">Orkut 1</a> - <a target="_blank" href="http://www.orkut.com.br/Main#Profile?uid=11614065982556444558">Orkut 2</a> - <a target="_blank" href="http://www.myspace.com/startinguplive">MySpace</a> - <a target="_blank" href="http://soundcloud.com/starting-up-live">Soundcloud</a> - <a target="_blank" href="http://www.facebook.com/home.php#!/profile.php?id=100001272074352">Facebook</a></li>
<br />
<li><strong>Fernanda D'Avilla </strong>(davilafefe@hotmail.com)<br />Bairro Cristo Rei.<br />Tel: (41) 9653-8935<br />Links: <a target="_blank" href="http://www.orkut.com.br/Main#Profile?rl=ttp&amp;uid=17357190155344533774">Orkut</a> - <a target="_blank" href="http://www.myspace.com/djfernanda">MySpace</a> - <a target="_blank" href="http://www.facebook.com/home.php#!/profile.php?id=100000981554277">Facebook</a></li>
<br />
<li><strong>Lafayete Carneiro </strong>(lafa321@hotmail.com)<br />Bairro Campina do Siqueira.<br />Tel: (41) 9675-4535</li>
<br />
<li><strong>Cezar de Araujo Santos aka DJ Kazz </strong>(cezar.santos@ymail.com)<br />Cidade Industrial.<br />Tel: (41) 9903-8154 / (41) 3095-4688<br />Links: <a target="_blank" href="http://www.orkut.com.br/Main#Profile?rl=mp&amp;uid=15558153954449337127">Orkut</a> - <a target="_blank" href="http://www.facebook.com/home.php#!/profile.php?id=100000870310449">Facebook</a> - <a target="_blank" href="http://www.soundcloud.com/djkazz">Soundcloud</a></li>
<br />
<li><strong>Joel Guglielmini </strong>(joelguglielmini@hotmail.com)<br />Tel: (41) 9929-9747<br />Links: <a target="_blank" href="http://www.orkut.com.br/Main#Profile?uid=15791433437215101125">Orkut</a> - <a target="_blank" href="http://www.myspace.com/joelguglielmini">MySpace</a> - <a target="_blank" href="http://www.twitter.com/joelguglielmini">Twitter</a></li>
</ul>
<h4 style="visibility: visible;">São José dos Pinhais</h4>
<ul class="bullet-a">
<li><strong>Rafahell </strong>(sexta_e_sabado@hotmail.com)<br />Tel: (41) 9929-3185 / (41) 8422-6777<br />Links: <a target="_blank" href="http://www.orkut.com.br/Main#Profile?uid=11984508187892384100">Orkut</a> - <a target="_blank" href="http://www.myspace.com/afahell">MySpace</a> - <a target="_blank" href="http://www.facebook.com/RafahellHenrique">Facebook</a></li>
<br />
<li><strong>Chucky aka Luiz Gustavo Kochanny </strong>(lgkochanny@hotmail.com)<br />Tel: (41) 7811-7755<br />Links: <a target="_blank" href="http://www.orkut.com.br/Main#Profile?rl=mp&amp;uid=5697523841202609930">Orkut</a></li>
<br />
<li><strong>Maicon Luiz Ribeiro da Silva </strong>(maiconcuritiba@hotmail.com)<br />Tel: (41) 8841-7226<br />Links: <a target="_blank" href="http://www.orkut.com.br/Main#Profile?uid=12877644444843431040">Orkut</a> - <a target="_blank" href="http://twitter.com/Maicon_Curitiba">Twitter</a></li>
</ul>
<h4 style="visibility: visible;">Campo Largo</h4>
<ul class="bullet-a">
<li><strong>Marcelo Schiavon Ramos </strong>(psyseedrecords@hotmail.com)<br />Tel: (41) 3393-3937 / (41) 2913-6698<br />Links: <a target="_blank" href="http://www.orkut.com.br/Main#Profile?uid=3101381545169315055">Orkut</a> - <a target="_blank" href="http://www.myspace.com/urucubacadark">MySpace</a> - <a target="_blank" href="http://www.facebook.com/profile.php?id=688364190">Facebook</a></li>
</ul>
<h4 style="visibility: visible;">Guarapuava</h4>
<ul class="bullet-a">
<li><strong>João Gabriel de Oliveira </strong>(bibi.dj@hotmail.com)<br />Tel: (42) 8412-6332<br />Links: <a target="_blank" href="http://www.orkut.com.br/Main#Profile?uid=12603969666220510463">Orkut</a> - <a target="_blank" href="http://www.facebook.com/#!/profile.php?id=1146217263">Facebook</a></li>
<br />
<li><strong>Dayani Leite </strong>(daya_sista@yahoo.com.br)<br />Centro.<br />Tel: (41) 9673-6680</li>
</ul>
<h4 style="visibility: visible;">Ponta Grossa</h4>
<ul class="bullet-a">
<li><strong>Saana aka Guilherme Conforto dos Santos </strong>(tiobozox@hotmail.com)<br />Links: <a target="_blank" href="http://www.orkut.com.br/Main#Profile?uid=15226248948939348043">Orkut</a> - <a target="_blank" href="http://www.myspace.com/djsaana">MySpace</a></li>
<br />
<li><strong>Soha aka Rômulo Andrade</strong> (djsoha@gmail.com)<br />Tel: (42) 9936-5639 / (42) 3226-2885<br />Links: <a target="_blank" href="http://www.orkut.com.br/Main#Profile?rl=mp&amp;uid=11570403255916802060">Orkut</a> - <a target="_blank" href="http://www.myspace.com/djsoha">MySpace</a></li>
</ul>
<h4 style="visibility: visible;">São Mateus do Sul</h4>
<ul class="bullet-a">
<li><strong>Dymi </strong>(dymi_cordeiro@hotmail.com)<br />Tel: (42) 8816-7060 / (42) 8816-7060 / (41) 3257-2100<br />Links: <a target="_blank" href="http://www.orkut.com.br/Main#Profile?rl=mp&amp;uid=2595876933433285207">Orkut</a> - <a target="_blank" href="http://WWW.MYSPACE.COM/MUSICDYMI">MySpace</a> - <a target="_blank" href="http://WWW.TWITTER.COM/MUSICDYMI">Twitter</a><br /><br /></li>
<li><strong>Cibele </strong>(bell_zinha3@hotmail.com)<br />Tel: (42) 8812-2010<br />Links: <a target="_blank" href="http://www.myspace.com/ciidubiel">MySpace</a></li>
</ul>
<h4 style="visibility: visible;">União da Vitória</h4>
<ul class="bullet-a">
<li><strong>Felipe Sicuro Sturmer </strong>(fsspirasi@hotmail.com)<br />Tel: (42) 3523-2992<br />Links: <a target="_blank" href="http://www.orkut.com.br/Main#Profile?uid=1193840043773786793">Orkut</a></li>
</ul>
<h2 style="visibility: visible;">Santa Catarina</h2>
<h4 style="visibility: visible;">Florianópolis</h4>
<ul class="bullet-a">
<li><strong>Felpe Telles Poeta </strong>(ftpoeta@hotmail.com)<br />Tel: (48) 8464-6796<strong></strong><br /><br /></li>
<li><strong>Alana </strong>(musc4ri4@gmail.com)<br />Tel: (48) 3237-9663 / (48) 8836-9372<br />Links: <a target="_blank" href="http://www.orkut.com.br/Main#Profile?uid=8939579281051093172">Orkut</a> - <a target="_blank" href="http://www.soundcloud.com/djalanita">Soundcloud</a> - <a target="_blank" href="http://www.facebook.com/alana.curi">Facebook</a></li>
</ul>
<h4 style="visibility: visible;">Balneário Camboriú / Itajaí</h4>
<ul class="bullet-a">
<li><strong>Flavio Fayet </strong>(ffayet007@hotmail.com)<br />Tel: (47) 3361-6334<br />Links: <a target="_blank" href="http://www.orkut.com.br/Main#Profile?rl=ls&amp;uid=17643359035068202650">Orkut</a> - <a target="_blank" href="http://WWW.myspace.com/sednainc">MySpace</a> - <a target="_blank" href="http://twitter.com/ffayet007">Twitter</a></li>
</ul>
<h4 style="visibility: visible;">Joinville</h4>
<ul class="bullet-a">
<li><strong>Roger Thiago Oliveira </strong>(rogerthiagooliveira@gmail.com)<br />Tel: (47) 9951-3697<br />Links: <a target="_blank" href="http://www.facebook.com/rogerthiago">Facebook</a></li>
</ul>
<h4 style="visibility: visible;">Blumenau</h4>
<ul class="bullet-a">
<li><strong>Tiago Rafael Azul </strong>(gerencial@spacovagun.com.br)<br />Tel: (41) 9660-4544 / MSN: listen.art@hotmail.com<a target="_blank" href="http://www.facebook.com/rogerthiago"></a></li>
</ul>
<h4 style="visibility: visible;">Videira / Lages</h4>
<ul class="bullet-a">
<li><strong>Marcos Paulo Santini </strong>(marcospaulosantini@bol.com.br)<br />Tel: (49) 9985-5162</li>
</ul>
<h2 style="visibility: visible;">Rio Grande do Sul</h2>
<h4>Capital</h4>
<ul class="bullet-a">
<li><strong>Thiago Pires Gonçalves </strong>(bijari@msn.com)<br />Links: <a target="_blank" href="http://www.orkut.com.br/Main#Profile?uid=213587247634284605&amp;rl=t">Orkut</a> - <a target="_blank" href="http://www.soundcloud.com/djthiagobrazil">Soundcloud</a><a target="_blank" href="http://www.twitter.com/djcaroles"></a></li>
</ul>
<h2 style="visibility: visible;">Rio de Janeiro</h2>
<h4>Capital</h4>
<ul class="bullet-a">
<li><strong>Caroline </strong>(carolesramos@yahoo.com.br)<br />Tel: (21) 9119-5145 / (21) 2494-4534<br />Links: <a target="_blank" href="http://www.orkut.com.br/Main#Profile?uid=3086396642885006389&amp;rl=t">Orkut</a> - <a target="_blank" href="http://www.myspace.com/djcaroles">MySpace</a> - <a target="_blank" href="http://www.facebook.com/djcaroles">Facebook</a> - <a target="_blank" href="http://www.twitter.com/djcaroles">Twitter</a></li>
</ul>
<h2 style="visibility: visible;">São Paulo</h2>
<h4>Capital</h4>
<ul class="bullet-a">
<li><strong>João Carlos Sutemi </strong>(djsutemi@gmail.com)<br />Tel: (11) 8092-5380<br />Links: <a target="_blank" href="http://WWW.myspace.com/sutemidj">MySpace</a> - <a target="_blank" href="http://www.facebook.com/sutemi">Facebook</a> - <a target="_blank" href="http://www.twitter.com/sutemi">Twitter</a></li>
</ul>
<div class="notice">
<div class="typo-icon">Quer se tornar um Ponto de Venda de ingressos para o Festival <strong>Tandava  Gathering</strong> na sua cidade? <a href="http://elijah.com.br/tandava/2010/index.php?option=com_contact&amp;view=contact&amp;id=1&amp;Itemid=49">Entre  em contato</a> conosco.</div>
</div>
</div>]]></description>
			<author>elijah@metropolis.art.br (de elijah hatem)</author>
			<category>Ingressos</category>
			<pubDate>Thu, 17 Jun 2010 01:02:54 +0000</pubDate>
		</item>
		<item>
			<title>Ingressos Online</title>
			<link>http://elijah.com.br/tandava/2010/ingressos/87-venda-online.html</link>
			<guid>http://elijah.com.br/tandava/2010/ingressos/87-venda-online.html</guid>
			<description><![CDATA[<p>Os ingressos também estão disponíveis para todo país para compra <em>online</em> através do website parceiro <strong>Hot Ticket</strong>.</p>
<h4>Acesse: <a target="_blank" href="http://www.hotticket.biz/">www.hotticket.biz</a></h4>]]></description>
			<author>elijah@metropolis.art.br (de elijah hatem)</author>
			<category>Ingressos</category>
			<pubDate>Thu, 17 Jun 2010 00:14:55 +0000</pubDate>
		</item>
		<item>
			<title>Excursões para o Tandava</title>
			<link>http://elijah.com.br/tandava/2010/ingressos/88-excursoes.html</link>
			<guid>http://elijah.com.br/tandava/2010/ingressos/88-excursoes.html</guid>
			<description><![CDATA[<p>Se você mora em outra cidade ou estado, entre em contato com o parceiro da sua região e venha para o festival. Você encontra uma lista de parceiros<strong></strong> na página <a target="_self" href="http://elijah.com.br/tandava/2010/index.php?option=com_content&view=article&id=86:onde-comprar&catid=43&Itemid=168">Onde Comprar</a>.</p>
<p>Faça parte do <strong>Tandava Gathering 2010</strong>.</p>
<div class="notice">
<div class="typo-icon">Sua cidade não tem um parceiro próximo e você quer organizar uma excursão exclusiva para o Festival <strong>Tandava Gathering</strong> na sua cidade? <a href="http://elijah.com.br/tandava/2010/index.php?option=com_contact&view=contact&id=1&Itemid=49">Entre em contato</a> conosco.</div>
</div>]]></description>
			<author>elijah@metropolis.art.br (de elijah hatem)</author>
			<category>Ingressos</category>
			<pubDate>Wed, 16 Jun 2010 23:15:15 +0000</pubDate>
		</item>
	</channel>
</rss>
