<?php die("Access Denied"); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br" >
<head>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-2048503-7']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
	  <base href="http://elijah.com.br/tandava/2010/o-festival.html" />
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="robots" content="index, follow" />
  <meta name="keywords" content="tandava, gathering, festival, 2010, curitiba, novembro" />
  <meta name="og:title" content="Tandava Gathering" />
  <meta name="og:site_name" content="Tandava Gathering" />
  <meta name="og:image" content="http://elijah.com.br/tandava/2010/images/stories/home/tandava-home-1.jpg" />
  <meta name="title" content="Tandava Gathering" />
  <meta name="author" content="de elijah hatem" />
  <meta name="description" content="Não surgiu um novo conceito. As coisas não vão mudar e nada será resgatado.  Não  é nada melhor ou maior de tudo que você já viu. Não  é inédito e nem ao menos é um evento. O respeito existe, mas o publico não, existe um encontro entre amigos e amigos de amigos." />
  <meta name="generator" content="Joomla! 1.5 - Open Source Content Management" />
  <title>Tandava Gathering</title>
  <link href="/tandava/2010/favicon.ico" rel="shortcut icon" type="image/x-icon" />
  <link rel="stylesheet" href="/tandava/2010/plugins/system/rokbox/themes/dark/rokbox-style.css" type="text/css" />
  <link rel="stylesheet" href="/tandava/2010/components/com_gantry/css/gantry.css" type="text/css" />
  <link rel="stylesheet" href="/tandava/2010/components/com_gantry/css/grid-12.css" type="text/css" />
  <link rel="stylesheet" href="/tandava/2010/components/com_gantry/css/joomla.css" type="text/css" />
  <link rel="stylesheet" href="/tandava/2010/templates/rt_juxta_j15/css/joomla.css" type="text/css" />
  <link rel="stylesheet" href="/tandava/2010/templates/rt_juxta_j15/css/style3.css" type="text/css" />
  <link rel="stylesheet" href="/tandava/2010/templates/rt_juxta_j15/css/demo-styles.css" type="text/css" />
  <link rel="stylesheet" href="/tandava/2010/templates/rt_juxta_j15/css/template.css" type="text/css" />
  <link rel="stylesheet" href="/tandava/2010/templates/rt_juxta_j15/css/typography.css" type="text/css" />
  <link rel="stylesheet" href="/tandava/2010/templates/rt_juxta_j15/css/backgrounds.css" type="text/css" />
  <link rel="stylesheet" href="/tandava/2010/templates/rt_juxta_j15/css/fusionmenu.css" type="text/css" />
  <style type="text/css">
    <!--
#rt-main-surround ul.menu li.active > a, #rt-main-surround ul.menu li.active > .separator, #rt-main-surround ul.menu li.active > .item, #rt-main-surround .square4 ul.menu li:hover > a, #rt-main-surround .square4 ul.menu li:hover > .item, #rt-main-surround .square4 ul.menu li:hover > .separator, .roktabs-links ul li.active span, .menutop li:hover > .item, .menutop li.f-menuparent-itemfocus .item, .menutop li.active > .item {color:#701110;}
a, .button, #rt-main-surround ul.menu a:hover, #rt-main-surround ul.menu .separator:hover, #rt-main-surround ul.menu .item:hover, .title1 .module-title .title {color:#701110;}body #rt-logo {width:600px;height:200px;}
    -->
  </style>
  <script type="text/javascript" src="/tandava/2010/media/system/js/mootools.js"></script>
  <script type="text/javascript" src="/tandava/2010/media/system/js/caption.js"></script>
  <script type="text/javascript" src="/tandava/2010/plugins/system/rokbox/rokbox.js"></script>
  <script type="text/javascript" src="/tandava/2010/plugins/system/rokbox/themes/dark/rokbox-config.js"></script>
  <script type="text/javascript" src="/tandava/2010/components/com_gantry/js/gantry-buildspans.js"></script>
  <script type="text/javascript" src="/tandava/2010/components/com_gantry/js/gantry-inputs.js"></script>
  <script type="text/javascript" src="/tandava/2010/modules/mod_roknavmenu/themes/fusion/js/fusion.js"></script>
  <script type="text/javascript">
var rokboxPath = '/tandava/2010/plugins/system/rokbox/';
			window.addEvent('domready', function() {
				var modules = ['rt-block'];
				var header = ['h3','h2','h1'];
				GantryBuildSpans(modules, header);
			});
		InputsExclusion.push('.content_vote','#rt-popup','#vmMainPage')
		        window.addEvent('load', function() {
					new Fusion('ul.menutop', {
						pill: 0,
						effect: 'slide and fade',
						opacity: 1,
						hideDelay: 500,
						centered: 0,
						tweakInitial: {'x': 9, 'y': 6},
        				tweakSubsequent: {'x': 0, 'y': -14},
						menuFx: {duration: 200, transition: Fx.Transitions.Sine.easeOut},
						pillFx: {duration: 400, transition: Fx.Transitions.Back.easeOut}
					});
	            });
  </script>
  <script type='text/javascript'>
/*<![CDATA[*/
	var jax_live_site = 'http://elijah.com.br/tandava/2010/index.php';
	var jax_site_type = '1.5';
/*]]>*/
</script><script type="text/javascript" src="http://elijah.com.br/tandava/2010/plugins/system/pc_includes/ajax_1.3.js"></script>
</head>
	<body  class="backgroundlevel-high backgroundstyle-style3 bodylevel-high cssstyle-style3 font-family-optima font-size-is-large menu-type-fusionmenu col12 ">
		<div id="rt-mainbg-overlay">
			<div class="rt-surround-wrap"><div class="rt-surround"><div class="rt-surround2"><div class="rt-surround3">
				<div class="rt-container">
										<div id="rt-drawer">
												<div class="clear"></div>
					</div>
															<div id="rt-header-wrap"><div id="rt-header-wrap2">
												<div id="rt-header-graphic">
																				<div class="rt-header-padding">
																							<div id="rt-header">
									<div class="rt-grid-12 rt-alpha rt-omega">
    			<div class="rt-block">
    	    	<a href="/tandava/2010/" id="rt-logo"></a>
    		</div>
	    
</div>
									<div class="clear"></div>
								</div>
																								<div id="rt-navigation"><div id="rt-navigation2"><div id="rt-navigation3">
									
<div class="nopill">
	<ul class="menutop level1 " >
						<li class="item1 root" >
					<a class="orphan item bullet" href="http://elijah.com.br/tandava/2010/"  >
				<span>
			    				Home				   
				</span>
			</a>
			
			
	</li>	
							<li class="item164 active root" >
					<a class="orphan item bullet" href="/tandava/2010/o-festival.html"  >
				<span>
			    				Festival				   
				</span>
			</a>
			
			
	</li>	
							<li class="item206 root" >
					<a class="orphan item bullet" href="/tandava/2010/programacao-tandava.html"  >
				<span>
			    				Programação				   
				</span>
			</a>
			
			
	</li>	
							<li class="item172 parent root" >
					<a class="daddy item bullet" href="/tandava/2010/atracoes-tandava.html"  >
				<span>
			    				Atrações				   
				</span>
			</a>
			
					<div class="fusion-submenu-wrapper level2 columns2">
				<div class="drop-top"></div>
				<ul class="level2 columns2">
								
							<li class="item176" >
					<a class="orphan item image" href="/tandava/2010/atracoes-tandava/funk-you.html"  >
				<span>
			    			        <img src="/tandava/2010/templates/rt_juxta_j15/images/icons/funkyou_icon.png" alt="funkyou_icon.png" />
			    				Funk You				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item196" >
					<a class="orphan item image" href="/tandava/2010/atracoes-tandava/vive-la-musique.html"  >
				<span>
			    			        <img src="/tandava/2010/templates/rt_juxta_j15/images/icons/vivelamusique_icon.png" alt="vivelamusique_icon.png" />
			    				Vive La Musique				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item204" >
					<a class="orphan item image" href="/tandava/2010/atracoes-tandava/reboot.html"  >
				<span>
			    			        <img src="/tandava/2010/templates/rt_juxta_j15/images/icons/reboot_icon.png" alt="reboot_icon.png" />
			    				Reboot				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item197" >
					<a class="orphan item image" href="/tandava/2010/atracoes-tandava/synk-recs-day-party.html"  >
				<span>
			    			        <img src="/tandava/2010/templates/rt_juxta_j15/images/icons/synk_icon.png" alt="synk_icon.png" />
			    				Synk Recs Day Party				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item192" >
					<a class="orphan item bullet" href="/tandava/2010/atracoes-tandava/rex-africa-do-sul.html"  >
				<span>
			    				Rex (África do Sul)				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item195" >
					<a class="orphan item bullet" href="/tandava/2010/atracoes-tandava/zaraus-live-portugal.html"  >
				<span>
			    				Zaraus LIVE (Portugal)				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item199" >
					<a class="orphan item bullet" href="/tandava/2010/atracoes-tandava/sallun-pedra-branca.html"  >
				<span>
			    				Sallun (Pedra Branca)				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item193" >
					<a class="orphan item bullet" href="/tandava/2010/atracoes-tandava/minimal-criminal-live.html"  >
				<span>
			    				Minimal Criminal LIVE				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item200" >
					<a class="orphan item bullet" href="/tandava/2010/atracoes-tandava/truati-live-sp.html"  >
				<span>
			    				Truati LIVE (SP)				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item198" >
					<a class="orphan item bullet" href="/tandava/2010/atracoes-tandava/demonizz-live-sp.html"  >
				<span>
			    				Demonizz LIVE (SP)				   
				</span>
			</a>
			
			
	</li>	
									
							<li class="item191" >
					<a class="orphan item bullet" href="/tandava/2010/atracoes-tandava/ver-todas.html"  >
				<span>
			    				Ver Todas...				   
				</span>
			</a>
			
			
	</li>	
										</ul>
			</div>
			
	</li>	
							<li class="item168 root" >
					<a class="orphan item bullet" href="/tandava/2010/ingressos.html"  >
				<span>
			    				Ingressos				   
				</span>
			</a>
			
			
	</li>	
							<li class="item174 parent root" >
					<a class="daddy item bullet" href="/tandava/2010/local.html"  >
				<span>
			    				Local				   
				</span>
			</a>
			
					<div class="fusion-submenu-wrapper level2">
				<div class="drop-top"></div>
				<ul class="level2">
								
							<li class="item207" >
					<a class="orphan item bullet" href="/tandava/2010/local/mapa-como-chegar-tandava-2010.html"  >
				<span>
			    				Mapa Tandava 2010				   
				</span>
			</a>
			
			
	</li>	
										</ul>
			</div>
			
	</li>	
							<li class="item173 root" >
					<a class="orphan item bullet" href="/tandava/2010/fotos.html"  >
				<span>
			    				Fotos				   
				</span>
			</a>
			
			
	</li>	
							<li class="item175 root" >
					<span class="orphan item bullet nolink">
			    <span>
			        			    Área Restrita			    			    </span>
			</span>
			
			
	</li>	
							<li class="item49 root" >
					<a class="orphan item bullet" href="/tandava/2010/contato.html"  >
				<span>
			    				Contato				   
				</span>
			</a>
			
			
	</li>	
				</ul>
</div>

								    <div class="clear"></div>
								</div></div></div>
																						</div>
																			</div>
											</div></div>
															<div id="rt-main-surround">
												<div id="rt-breadcrumbs"><div id="rt-breadcrumbs2"><div id="rt-breadcrumbs3">
								<div class="rt-breadcrumb-surround">
		<a href="http://elijah.com.br/tandava/2010/" id="breadcrumbs-home"></a>
		<span class="breadcrumbs pathway">
<span class="no-link">Festival</span></span>
	</div>
	
							<div class="clear"></div>
						</div></div></div>
																							              <div id="rt-main" class="mb8-sa4">
                <div class="rt-main-inner">
                    <div class="rt-grid-8 ">
                                                						<div class="rt-block">
							<div class="default">
	                            <div id="rt-mainbody">
	                                
<div class="rt-joomla ">
	<div class="rt-article">
		
				<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3">
						<h1 class="title">
			<a href="/tandava/2010/o-festival.html">Tandava Gathering</a>			</h1>
					</div></div></div></div>
		<div class="clear"></div>
		
		
		
		
		<img style="margin: 0px 10px 10px 0px; float: left;" alt="tandava-home-1" src="images/stories/home/tandava-home-1.jpg" width="150" height="219" />
<blockquote>
<p style="text-align: justify;">"Não surgiu um novo conceito. As coisas não vão mudar e nada  será&nbsp;resgatado. Não&nbsp; é&nbsp;nada melhor ou maior de tudo que você&nbsp;já&nbsp;viu. Não&nbsp; é&nbsp;inédito e nem ao menos é um evento. O respeito existe, mas  o  publico não, existe um encontro entre amigos e amigos de amigos."</p>
</blockquote>
<p style="text-align: justify;">O <strong>Tandava Gathering</strong> é um festival, ou um encontro como alguns podem preferir, sem fins lucrativos, feito por pessoas que possuem outra atividade profissional e desenvolvem essa idéia por simples e convicta crença na cena <em>underground</em>.</p>
<p style="text-align: justify;"><img style="margin: 20px; float: right;" alt="tandava-home-2" src="images/stories/home/tandava-home-2.jpg" width="200" height="151" />O Tandava é para todos que querem experimentar, para aqueles que respeitam a si mesmo e aos outros. Na verdade é mais do que isso, é feito para quem apenas deseja alguns dias fora da maçante realidade do cotidiano.</p>
<p style="text-align: justify;">É uma fuga para onde tempo e espaço estão aquém de afetar o relacionamento interpessoal.</p>
<h4>Música e Arte</h4>
<p style="text-align: justify;">E tudo isso é materializado através da música e da arte. A música por sua vez assume o papel de sincronizar, não só o a pista de dança, mas o ritmo de tudo que acontece no festival. E a arte visa elevar, ainda que por 3 dias, a percepção de você para com o mundo ao seu redor e, principalmente, com você mesmo.</p>
<p style="text-align: justify;">Fugindo do tom lúdico e emblemático que os mais céticos podem preferir evitar, o principal motivo para qual o Tandava foi criado é a diversão, o sorriso, o bem-estar. Sinta-se convidado a passar um feriado em um local agradável, ao som de boa musica e em convivência com pessoas dispostas a se divertir. Joint It!</p>
<p><img style="vertical-align: middle; display: block; margin-left: auto; margin-right: auto;" alt="tandava-home-3" src="images/stories/home/tandava-home-3.jpg" width="480" height="310" /></p>

<h4>O Espírito do Tandava</h4>
<p style="text-align: justify;">Costumava-se pensar que a palavra festival era forte demais para o Tandava, que em sua essência é um encontro de amigos.</p>
<p style="text-align: justify;">Seja como for, algumas coisas são pensadas para que o espírito familiar seja mantido. A entrada com bebida e comida é permitida, assim como ausentar-se do local e retornar também é permitido. Fogueiras são permitidas e incentivadas, desde que mantido o bom senso em relação à segurança.</p>
<p style="text-align: justify;">A força motriz é evitar regras e rótulos, a fim de que quem quer que esteja presente sinta-se no direito de mudar o rumo de tudo a qualquer momento.</p>
<p style="text-align: justify;">Não se deve taxar o Tandava como um evento de música eletrônica, afinal como algo que aspira ser a forma física da vontade dos presentes, é impossível de ser preciso, como é natural das vontades.&nbsp;&nbsp; &nbsp;</p>
<p style="text-align: justify;">Outra peculiaridade é a área restrita do site, com mural e outras formas de comunicação entre os usuários, esse recurso permite que os participantes possam se conhecer nos meses que antecedem o festival. Essa característica interativa do site auxilia o coletivismo e gera ações como central de caronas e campeonatos dos mais engraçados esportes (de disputas de bolinha de gude a torneios de bets).</p>
<p style="text-align: justify;">Uma breve retrospectiva a cerca das duas primeiras edições do Tandava é útil para contextualizar os recém chegados, uma vez que essa terceira edição almeja atingir números mais expressivos em se tratando de participantes do encontro.</p>
<h4>Últimas Edições</h4>
<ul>
<li><a href="index.php?option=com_content&amp;view=article&amp;id=98:tandava-gathering-2008-primeira-edicao&amp;catid=41&amp;Itemid=164">Tandava Gathering 2008 - Primeira Edição</a><br />Veja as <a href="index.php?option=com_content&amp;view=article&amp;id=97:fotos-tandava-2008&amp;catid=44&amp;Itemid=173">Fotos</a></li>
<br />
<li><a href="index.php?option=com_content&amp;view=article&amp;id=99:tandava-gathering-2009-segunda-edicao&amp;catid=41&amp;Itemid=164">Tandava Gathering 2009 - Segunda Edição</a><br />Veja as <a href="index.php?option=com_content&amp;view=article&amp;id=85:fotos-tandava-2009&amp;catid=44&amp;Itemid=173">Fotos</a></li>
<br />
<li><a href="index.php?option=com_content&amp;view=frontpage&amp;Itemid=1">Tandava Gathering 2010 - Terceira Edição</a><br />12 a 15 de Novembro de 2010. Em breve Fotos!</li>
<br /> 
</ul><div style="margin-top:0px;margin-bottom:0px;display: inline;"><iframe src="http://www.facebook.com/plugins/like.php?href=http://elijah.com.br/tandava/2010/o-festival.html&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:30px;" allowTransparency="true"></iframe></div>		
				<div class="rt-articleinfo"><div class="rt-articleinfo2"><div class="rt-articleinfo3">
						<div class="rt-article-icons">
								<a href="/tandava/2010/o-festival.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon pdf"></span></a><a href="/tandava/2010/o-festival.html?tmpl=component&amp;print=1&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon print"></span></a><a href="/tandava/2010/component/mailto/?tmpl=component&amp;link=aHR0cDovL2VsaWphaC5jb20uYnIvdGFuZGF2YS8yMDEwL28tZmVzdGl2YWwuaHRtbA%3D%3D" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><span class="icon email"></span></a>																</div>
			
						<span class="rt-date-posted">
				Domingo, 25 de Abril de 2010 14:18			</span>
			
						<span class="rt-date-modified">
				Última atualização em Quinta, 18 de Novembro de 2010 16:58			</span>
			
						<span class="rt-author">
				Postado por de elijah hatem			</span>
				
					</div></div></div>
		
					</div>
</div>
	                            </div>
								<div class="clear"></div>
							</div>
						</div>
                                                                    </div>
                                <div class="rt-grid-4 ">
                <div id="rt-sidebar-a">
                                        <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3"><h2 class="title">Próximo Tandava...</h2></div></div></div></div>
						<div class="clear"></div>
		                						<div class="module-content">
		                	<script src="modules/mod_countdown/scripts/swfobject_modified.js" type="text/javascript"></script>

			<object id="FlashID" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="260" height="80">

  <param name="movie" value="modules/mod_countdown/countdown.swf?dayText=Dias&texto=Contagem Regressiva&hoursText=Horas&ano=2011&mont=11&dia=12&minutesText=Minutos&secondsText=Segs&displayColor=0x7011100&textoColor=0x333333&dayColor=0x333333&hoursColor=0x333333&minutesColor=0x333333&secondsColor=0x333333&pozadinaColor=0xd6d6d4&linijaColor=0xd6d6d4&trans=" />

  <param name="quality" value="high" />

  <param name="wmode" value="transparent" />

  <param name="swfversion" value="6.0.65.0" />

  

  <!-- This param tag prompts users with Flash Player 6.0 r65 and higher to download the latest version of Flash Player. Delete it if you don’t want users to see the prompt. -->

  <param name="expressinstall" value="modules/mod_countdown/scripts/expressInstall.swf" />

  <!-- Next object tag is for non-IE browsers. So hide it from IE using IECC. -->

  <!--[if !IE]>-->

  <object type="application/x-shockwave-flash" data="modules/mod_countdown/countdown.swf?dayText=Dias&texto=Contagem Regressiva&hoursText=Horas&ano=2011&mont=11&dia=12&minutesText=Minutos&secondsText=Segs&displayColor=0x701110&textoColor=0x333333&dayColor=0x333333&hoursColor=0x333333&minutesColor=0x333333&secondsColor=0x333333&pozadinaColor=0xd6d6d4&linijaColor=0xd6d6d4&trans=" width="260" height="80">

    <!--<![endif]-->

    <param name="quality" value="high" />

    <param name="wmode" value="transparent" />

    <param name="swfversion" value="6.0.65.0" />

    <param name="expressinstall" value="modules/mod_countdown/scripts/expressInstall.swf" />

	

    <!-- The browser displays the following alternative content for users with Flash Player 6.0 and older. -->

    <div>

      <h4>Content on this page requires a newer version of Adobe Flash Player.</h4>

      <p><a href="http://www.adobe.com/go/getflashplayer"><img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" width="112" height="33" /></a></p>

    </div>

    <!--[if !IE]>-->

  </object>

  <!--<![endif]-->

</object>

<script type="text/javascript">

<!--

swfobject.registerObject("FlashID");

//-->

</script>

								</div>
					</div>
				</div>
            </div>
        	                    <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3"><h2 class="title">Main Menu</h2></div></div></div></div>
						<div class="clear"></div>
		                						<div class="module-content">
		                	<ul class="menu level1" >
				<li class="item1" >
					<a class="orphan item bullet" href="http://elijah.com.br/tandava/2010/"  >
				<span>
			    				Home				   
				</span>
			</a>
			
			
	</li>	
					<li class="item164 active" >
					<a class="orphan item bullet" href="/tandava/2010/o-festival.html"  >
				<span>
			    				Festival				   
				</span>
			</a>
			
			
	</li>	
					<li class="item206" >
					<a class="orphan item bullet" href="/tandava/2010/programacao-tandava.html"  >
				<span>
			    				Programação				   
				</span>
			</a>
			
			
	</li>	
					<li class="item172 parent" >
					<a class="orphan item bullet" href="/tandava/2010/atracoes-tandava.html"  >
				<span>
			    				Atrações				   
				</span>
			</a>
			
			
	</li>	
					<li class="item168" >
					<a class="orphan item bullet" href="/tandava/2010/ingressos.html"  >
				<span>
			    				Ingressos				   
				</span>
			</a>
			
			
	</li>	
					<li class="item174 parent" >
					<a class="orphan item bullet" href="/tandava/2010/local.html"  >
				<span>
			    				Local				   
				</span>
			</a>
			
			
	</li>	
					<li class="item173" >
					<a class="orphan item bullet" href="/tandava/2010/fotos.html"  >
				<span>
			    				Fotos				   
				</span>
			</a>
			
			
	</li>	
					<li class="item175" >
					<span class="orphan item bullet nolink">
			    <span>
			        			    Área Restrita			    			    </span>
			</span>
			
			
	</li>	
					<li class="item49" >
					<a class="orphan item bullet" href="/tandava/2010/contato.html"  >
				<span>
			    				Contato				   
				</span>
			</a>
			
			
	</li>	
		</ul>
						</div>
					</div>
				</div>
            </div>
        	                    <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3"><h2 class="title">Usuários Online</h2></div></div></div></div>
						<div class="clear"></div>
		                						<div class="module-content">
		                	<div>
	<ul style="list-style:none; margin: 0 0 10px; padding: 0;">
		
	
	</ul>
	
	<div>
		0 participantes		
					e 3 visitantes				
		online 
		<br /><br />
        <div>
		<a href="/tandava/2010/area-restrita/comunidade/search/browse.html?sort=online">Ver Todos Participantes</a>
		</div>
	</div>
</div>						</div>
					</div>
				</div>
            </div>
        	                    <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3"><h2 class="title">Sua Opinião</h2></div></div></div></div>
						<div class="clear"></div>
		                						<div class="module-content">
		                	
<h4 class="rt-polltitle">
	Qual é a principal qualidade do Tandava para você?</h4>
<form action="index.php" method="post" name="form2" class="rt-poll">
	<fieldset>
				<div class="rt-pollrow">
			<input type="radio" name="voteid" id="voteid13" value="13" alt="13" />
			<label for="voteid13">
				Um encontro sem fins lucrativos.			</label>
		</div>
				<div class="rt-pollrow">
			<input type="radio" name="voteid" id="voteid14" value="14" alt="14" />
			<label for="voteid14">
				Reúne vários tipos de música.			</label>
		</div>
				<div class="rt-pollrow">
			<input type="radio" name="voteid" id="voteid15" value="15" alt="15" />
			<label for="voteid15">
				Clima familiar dos participantes.			</label>
		</div>
				<div class="rt-pollrow">
			<input type="radio" name="voteid" id="voteid16" value="16" alt="16" />
			<label for="voteid16">
				Entrada com bebidas liberada.			</label>
		</div>
				<div class="rt-pollrow">
			<input type="radio" name="voteid" id="voteid17" value="17" alt="17" />
			<label for="voteid17">
				Adoro acampar.			</label>
		</div>
			</fieldset>
	<div class="rt-pollbuttons">
		<div class="readon">
			<input type="submit" name="task_button" class="button" value="Votar" />
		</div>
		<div class="readon">
			<input type="button" name="option" class="button" value="Resultados" onclick="document.location.href='/tandava/2010/component/poll/15-qual-e-a-principal-qualidade-do-tandava-para-voce.html'" />
		</div>
	</div>

	<input type="hidden" name="option" value="com_poll" />
	<input type="hidden" name="task" value="vote" />
	<input type="hidden" name="id" value="15" />
	<input type="hidden" name="0771853ce26e921f570bd0921d153a94" value="1" /></form>
						</div>
					</div>
				</div>
            </div>
        	                    <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3"><h2 class="title">Área Restrita</h2></div></div></div></div>
						<div class="clear"></div>
		                						<div class="module-content">
		                	<form action="/tandava/2010/o-festival.html" method="post" name="login" id="form-login" >
		<fieldset class="input">
	<p id="form-login-username">
		<label for="modlgn_username">Email</label><br />
		<input id="modlgn_username" type="text" name="username" class="inputbox" alt="username" size="18" />
	</p>
	<p id="form-login-password">
		<label for="modlgn_passwd">Senha</label><br />
		<input id="modlgn_passwd" type="password" name="passwd" class="inputbox" size="18" alt="password" />
	</p>
		<p id="form-login-remember">
		<input type="checkbox" name="remember" class="checkbox" value="yes" alt="Lembrar-me" />
		<label class="remember">
			Lembrar-me		</label>
	</p>
		<div class="readon"><input type="submit" name="Submit" class="button" value="Entrar" /></div>
	</fieldset>
	<ul>
		<li>
			<a href="/tandava/2010/component/user/reset.html">
			Esqueci minha senha</a>
		</li>
		<li>
			<a href="/tandava/2010/component/user/remind.html">
			Esqueci meu nome de usuário</a>
		</li>
			</ul>
	
	<input type="hidden" name="option" value="com_user" />
	<input type="hidden" name="task" value="login" />
	<input type="hidden" name="return" value="L3RhbmRhdmEvMjAxMC9hcmVhLXJlc3RyaXRhL2NvbXVuaWRhZGUuaHRtbA==" />
	<input type="hidden" name="0771853ce26e921f570bd0921d153a94" value="1" /></form>
						</div>
					</div>
				</div>
            </div>
        	
                </div>
            </div>

                    <div class="clear"></div>
                </div>
            </div>
																								<div id="rt-bottom">
							<div class="rt-grid-4 rt-alpha">
                        <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3"><h2 class="title">Últimas Atualizações</h2></div></div></div></div>
						<div class="clear"></div>
		                						<div class="module-content">
		                	<ul class="latestnews">
	<li class="latestnews">
		<a href="/tandava/2010/fotos/143-fotos-do-tandava-2010.html" class="latestnews">
			Fotos do Tandava 2010</a>
	</li>
	<li class="latestnews">
		<a href="/tandava/2010/o-festival/144-o-tandava-2010.html" class="latestnews">
			O Tandava 2010</a>
	</li>
	<li class="latestnews">
		<a href="/tandava/2010/o-festival.html" class="latestnews">
			Tandava Gathering</a>
	</li>
	<li class="latestnews">
		<a href="/tandava/2010/local/mapa-como-chegar-tandava-2010.html" class="latestnews">
			Mapa Tandava 2010</a>
	</li>
	<li class="latestnews">
		<a href="/tandava/2010/local/142-sobre-o-local.html" class="latestnews">
			Sobre o Local</a>
	</li>
</ul>						</div>
					</div>
				</div>
            </div>
        	
</div>
<div class="rt-grid-4">
                        <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3"><h2 class="title">Mais Lidos</h2></div></div></div></div>
						<div class="clear"></div>
		                						<div class="module-content">
		                	<ul class="mostread">
	<li class="mostread">
		<a href="/tandava/2010/o-festival/144-o-tandava-2010.html" class="mostread">
			O Tandava 2010</a>
	</li>
	<li class="mostread">
		<a href="/tandava/2010/atracoes-tandava/funk-you.html" class="mostread">
			Funk You</a>
	</li>
	<li class="mostread">
		<a href="/tandava/2010/fotos/85-fotos-tandava-2009.html" class="mostread">
			Fotos do Tandava 2009</a>
	</li>
	<li class="mostread">
		<a href="/tandava/2010/local/mapa-como-chegar-tandava-2010.html" class="mostread">
			Mapa Tandava 2010</a>
	</li>
	<li class="mostread">
		<a href="/tandava/2010/atracoes-tandava/synk-recs-day-party.html" class="mostread">
			Synk Recs Day Party</a>
	</li>
</ul>						</div>
					</div>
				</div>
            </div>
        	
</div>
<div class="rt-grid-4 rt-omega">
                        <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3"><h2 class="title">Destaque</h2></div></div></div></div>
						<div class="clear"></div>
		                						<div class="module-content">
		                	<p><strong><a href="index.php?option=com_content&amp;view=category&amp;layout=blog&amp;id=44&amp;Itemid=173"><strong>Clique Aqui</strong></a></strong> e confira as <a href="index.php?option=com_content&amp;view=category&amp;layout=blog&amp;id=44&amp;Itemid=173"><strong>fotos</strong></a> tiradas pelo <strong>MushPics </strong>nas edições anteriores do festival <strong>Tandava Gathering</strong>.</p>
<p>Esteve lá? <a href="mailto:tandava@tandava.com.br?subject=Fotos">Envie suas fotos</a> e faça parte desta história.</p>						</div>
					</div>
				</div>
            </div>
        	
</div>
							<div class="clear"></div>
						</div>
																		<div id="rt-footer"><div id="rt-footer2"><div id="rt-footer3">
							<div class="rt-grid-12 rt-alpha rt-omega">
                    <div class="title1">
                    <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-title-surround"><div class="module-title"><div class="module-title2"><div class="module-title3"><h2 class="title">Apoio Cultural e Parceiros</h2></div></div></div></div>
						<div class="clear"></div>
		                						<div class="module-content">
		                	<table border="0" width="100%">
<tbody>
<tr>
<td><a target="_blank" href="http://www.metropolis.art.br"><img style="margin: 10px; vertical-align: middle;" alt="metropolis" src="images/stories/parceiros/metropolis.png" width="100" height="100" /></a></td>
<td><img style="margin: 10px; vertical-align: middle;" alt="sica" src="images/stories/parceiros/sica.png" width="100" height="100" /></td>
<td><a target="_blank" href="http://www.wasabiam.com.br/"><img style="margin: 10px; vertical-align: middle;" alt="wasabi" src="images/stories/parceiros/wasabi.png" width="100" height="100" /></a></td>
<td><a target="_blank" href="http://www.myspace.com/funkyoucwb"><img style="margin: 10px; vertical-align: middle;" alt="funkyou_parceiro" src="images/stories/parceiros/funkyou_parceiro.png" width="100" height="100" /></a></td>
<td><img style="margin: 10px; vertical-align: middle;" alt="revolucione" src="images/stories/parceiros/revolucione.png" width="100" height="100" /></td>
</tr>
<tr>
<td><a target="_blank" href="http://www.egralha.com.br/"><img style="margin: 5px 10px; vertical-align: middle;" alt="egralha" src="images/stories/parceiros/egralha.png" width="100" height="50" /></a></td>
<td><a target="_blank" href="http://www.vivelamusique.com.br"><img style="margin: 5px 10px; vertical-align: middle;" alt="vivelamusique_parceiro" src="images/stories/parceiros/vivelamusique_parceiro.png" width="100" height="50" /></a></td>
<td><img style="margin: 5px 10px; vertical-align: middle;" alt="makunba" src="images/stories/parceiros/makunba.png" width="100" height="40" /></td>
<td><a target="_blank" href="http://synk.com.br/"><img style="margin: 5px 10px; vertical-align: middle;" alt="synk_parceiro" src="images/stories/parceiros/synk_parceiro.png" width="100" height="50" /></a></td>
<td><a target="_blank" href="http://www.mushpics.com/"><img style="margin: 10px; vertical-align: middle;" alt="mushpics" src="images/stories/parceiros/mushpics.png" width="100" height="50" /></a></td>
</tr>
<tr>
<td><a target="_blank" href="http://leduxcwb.wordpress.com/"><img style="margin: 5px 10px; vertical-align: middle;" alt="leduxcwb" src="images/stories/parceiros/leduxcwb.png" width="100" height="40" /></a></td>
<td><img style="margin: 5px 10px; vertical-align: middle;" alt="makana" src="images/stories/parceiros/makana.png" width="100" height="40" /></td>
<td><img style="margin: 10px; vertical-align: middle;" alt="Oxydus" src="images/stories/parceiros/oxydus_parceiro.png" width="100" height="50" /></td>
<td><a target="_blank" href="http://reboot.blog.com"><img style="margin: 10px; vertical-align: middle;" alt="reboot_parceiro" src="images/stories/parceiros/reboot_parceiro.png" width="100" height="50" /></a></td>
<td><a target="_blank" href="http://www.mzc-lab.com/"><img style="margin: 10px; vertical-align: middle;" alt="mzc-lab" src="images/stories/parceiros/mzc-lab.png" width="100" height="50" /></a></td>
</tr>
</tbody>
</table>						</div>
					</div>
				</div>
            </div>
                </div>
		
</div>
							<div class="clear"></div>
						</div></div></div>
											</div>
										<div id="rt-copyright">
						<div class="rt-grid-12 rt-alpha rt-omega">
                        <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-content">
		                	<table width="100%" border="0" cellpadding="0" cellspacing="1"><tr><td nowrap="nowrap"><a href="/tandava/2010/home.html" class="mainlevel" >Tandava Gathering</a><span class="mainlevel">  - </span><a href="/tandava/2010/o-festival.html" class="mainlevel" id="active_menu">O Festival</a><span class="mainlevel">  - </span><a href="/tandava/2010/atracoes-tandava.html" class="mainlevel" >Atrações</a><span class="mainlevel">  - </span><a href="/tandava/2010/ingressos.html" class="mainlevel" >Ingressos</a><span class="mainlevel">  - </span><a href="/tandava/2010/local.html" class="mainlevel" >Local</a><span class="mainlevel">  - </span><a href="/tandava/2010/fotos.html" class="mainlevel" >Fotos</a><span class="mainlevel">  - </span><a href="/tandava/2010/contato.html" class="mainlevel" >Contato</a><span class="mainlevel">  - </span><a href="/tandava/2010/mapa-do-site.html" class="mainlevel" >Mapa do Site</a></td></tr></table>						</div>
					</div>
				</div>
            </div>
        	
</div>
						<div class="clear"></div>
					</div>
															<div id="rt-debug">
						<div class="rt-grid-12 rt-alpha rt-omega">
                        <div class="rt-block">
				<div class="rt-module-surround">
					<div class="rt-module-inner">
	                							<div class="module-content">
		                	<p style="text-align: center;"><a href="http://validator.w3.org/check?uri=referer"><img style="margin: 0px 5px; vertical-align: middle;" src="http://www.w3.org/Icons/valid-xhtml10" alt="Valid XHTML 1.0 Transitional" height="31" width="88" /></a> <a href="http://jigsaw.w3.org/css-validator/check/referer"><img style="border: 0px none; width: 88px; height: 31px; margin: 0px 5px; vertical-align: middle;" src="http://jigsaw.w3.org/css-validator/images/vcss" alt="Valid CSS!" height="31px" width="88px" /></a><a href="http://feed2.w3.org/check.cgi?url=http%3A//www.tandava.com.br/index.php%3Fformat%3Dfeed%26type%3Drss"> <img style="margin: 0px 5px; vertical-align: middle;" src="images/stories/valid-rss-rogers.png" alt="valid-rss-rogers" title="Validate my Atom 1.0 feed" height="31" width="88" /> </a></p>
<p><span style="font-size: 9pt;">Tandava Gathering website was created by <strong>de elijah hatem</strong>. Powered by Open Source CMS <a target="_blank" href="http://www.joomla.org">Joomla 1.5</a> and developed using <a target="_blank" href="http://www.gantry-framework.org"><img style="vertical-align: bottom;" alt="gantry" src="images/stories/gantry.png" height="34" width="99" /></a> framework.</span></p>						</div>
					</div>
				</div>
            </div>
        	
</div>
						<div class="clear"></div>
					</div>
									</div>
			</div></div></div></div>
		</div>
			</body>
</html>
