<?php
/**
 * @category	Module
 * @package		JomSocial
 * @subpackage	HelloMe
 * @copyright (C) 2008 by Slashes & Dots Sdn Bhd - All rights reserved!
 * @license		GNU/GPL, see LICENSE.php
 */
defined('_JEXEC') or die('Restricted access');

// Include the syndicate functions only once
require_once (dirname(__FILE__).DS.'helper.php');

require_once( JPATH_BASE . DS . 'components' . DS . 'com_community' . DS . 'libraries' . DS . 'core.php');
CFactory::load( 'libraries' , 'userpoints' );
CFactory::load( 'libraries' , 'window' );
CFactory::load( 'helpers' , 'owner' );
CFactory::load( 'libraries' , 'facebook' );

CWindow::load();

$document	=& JFactory::getDocument();
$config		=& CFactory::getConfig();
$script		= '/assets/script-1.2';
$script		.= ( $config->get('usepackedjavascript') == 1 ) ? '.pack.js' : '.js';
CAssets::attach( $script , 'js' );
$my			= CFactory::getUser();

if( $my->isOnline() && $my->id != 0 )
{
	$inboxModel			= CFactory::getModel('inbox');
	$filter				= array();
	$filter['user_id']	= $my->id;
	$friendModel		= CFactory::getModel ( 'friends' );
	$profileid 			= JRequest::getVar('userid' , 0 , 'GET');
	
	$params->def('unreadCount',	$inboxModel->countUnRead ( $filter ));
	$params->def('pending', $friendModel->countPending( $my->id ));
	$params->def('myLink', CRoute::_('index.php?option=com_community&view=profile&userid='.$my->id));
	$params->def('myName', $my->getDisplayName());
	$params->def('myAvatar', $my->getAvatar());
	$params->def('myId', $my->id);
	$params->def('myKarma', CUserPoints::getPointsImage($my));
	$params->def('enablephotos', $config->get('enablephotos'));
	$params->def('enablevideos', $config->get('enablevideos'));
	$params->def('enablegroups', $config->get('enablegroups'));
	$params->def('enableevents', $config->get('enableevents'));
	
	$enablekarma	= $config->get('enablekarma') ? $params->get('show_karma' , 1 ) : $config->get('enablekarma');
	$params->def('enablekarma', $enablekarma);

	$js				= modHelloMeHelper::getHelloMeScript( $my->getStatus() , COwnerHelper::isMine($my->id, $profileid) );
	$document->addScriptDeclaration($js);
	
	if($params->get('enable_facebookconnect', '1'))
	{
		$params->def('facebookuser', modHelloMeHelper::isFacebookUser());
	}
	else
	{
		$params->def('facebookuser', false);
	}
	
	//check caching
	$cache		= JFactory::getCache('mod_hellome');
				
	$cache->setCaching( $params->get('customCache', 1) );
	$callback	= array('modHelloMeHelper', 'getHelloMeHTML');
	$content	= $cache->call($callback, $params);
}
else
{
	$content = '';
	
	if($params->get('enable_login', '1'))
	{
		$type	= modHelloMeHelper::getType();
		$user	=& JFactory::getUser();
		$content .= modHelloMeHelper::getHelloMeLoginHTML($params, $type, $user);
	}
	
	if($params->get('enable_facebookconnect', '1'))
	{
		$content	.= modHelloMeHelper::getFacebookConnectHTML( $my , $config );
	}
}