Hi {target},

{actor} invited you to join an event (<?php echo $eventTitle;?>). Below is a message that is sent by {actor},

Message:

<?php echo $message; ?>

To view the event, access the URL at the following location:

<a href="<?php echo $url; ?>"><?php echo $url; ?></a>


Have a nice day!

