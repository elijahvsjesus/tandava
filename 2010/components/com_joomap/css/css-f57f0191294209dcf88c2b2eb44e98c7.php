<?php 
ob_start ("ob_gzhandler");
header("Content-type: text/css; charset: UTF-8");
header("Cache-Control: must-revalidate");
$expires_time = 1440;
$offset = 60 * $expires_time ;
$ExpStr = "Expires: " . 
gmdate("D, d M Y H:i:s",
time() + $offset) . " GMT";
header($ExpStr);
                ?>

/*** joomap.css ***/

.sitemap ul {display: block;list-style: none;margin: 0;padding: 0;}.sitemap ul li {margin: 0;padding: 0;white-space: nowrap;background: transparent;}.sitemap a img {border: none;}.sitemap ul.level_0 ul {list-style: inside square;padding: 0;}.sitemap ul.level_1 li {padding: 0 0 0 2em;white-space: nowrap;}.sitemap li.active a {font-style: italic;}