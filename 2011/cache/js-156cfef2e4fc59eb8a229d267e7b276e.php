<?php
ob_start ("ob_gzhandler");
header("Content-type: application/x-javascript; charset: UTF-8");
header("Cache-Control: must-revalidate");
$expires_time = 1440;
$offset = 60 * $expires_time ;
$ExpStr = "Expires: " .
gmdate("D, d M Y H:i:s",
time() + $offset) . " GMT";
header($ExpStr);
                ?>

/*** belated-png.js ***/

/**
* DD_belatedPNG: Adds IE6 support: PNG images for CSS background-image and HTML <IMG/>.
* Author: Drew Diller
* Email: drew.diller@gmail.com
* URL: http://www.dillerdesign.com/experiment/DD_belatedPNG/
* Version: 0.0.8a
* Licensed under the MIT License: http://dillerdesign.com/experiment/DD_belatedPNG/#license
*
* Example usage:
* DD_belatedPNG.fix('.png_bg'); // argument is a CSS selector
* DD_belatedPNG.fixPng( someNode ); // argument is an HTMLDomElement
**/

var DD_belatedPNG={ns:"DD_belatedPNG",imgSize:{},delay:10,nodesFixed:0,createVmlNameSpace:function(){if(document.namespaces&&!document.namespaces[this.ns]){document.namespaces.add(this.ns,"urn:schemas-microsoft-com:vml")}},createVmlStyleSheet:function(){var b,a;b=document.createElement("style");b.setAttribute("media","screen");document.documentElement.firstChild.insertBefore(b,document.documentElement.firstChild.firstChild);if(b.styleSheet){b=b.styleSheet;b.addRule(this.ns+"\\:*","{behavior:url(#default#VML)}");b.addRule(this.ns+"\\:shape","position:absolute;");b.addRule("img."+this.ns+"_sizeFinder","behavior:none; border:none; position:absolute; z-index:-1; top:-10000px; visibility:hidden;");this.screenStyleSheet=b;a=document.createElement("style");a.setAttribute("media","print");document.documentElement.firstChild.insertBefore(a,document.documentElement.firstChild.firstChild);a=a.styleSheet;a.addRule(this.ns+"\\:*","{display: none !important;}");a.addRule("img."+this.ns+"_sizeFinder","{display: none !important;}")}},readPropertyChange:function(){var b,c,a;b=event.srcElement;if(!b.vmlInitiated){return}if(event.propertyName.search("background")!=-1||event.propertyName.search("border")!=-1){DD_belatedPNG.applyVML(b)}if(event.propertyName=="style.display"){c=(b.currentStyle.display=="none")?"none":"block";for(a in b.vml){if(b.vml.hasOwnProperty(a)){b.vml[a].shape.style.display=c}}}if(event.propertyName.search("filter")!=-1){DD_belatedPNG.vmlOpacity(b)}},vmlOpacity:function(b){if(b.currentStyle.filter.search("lpha")!=-1){var a=b.currentStyle.filter;a=parseInt(a.substring(a.lastIndexOf("=")+1,a.lastIndexOf(")")),10)/100;b.vml.color.shape.style.filter=b.currentStyle.filter;b.vml.image.fill.opacity=a}},handlePseudoHover:function(a){setTimeout(function(){DD_belatedPNG.applyVML(a)},1)},fix:function(a){if(this.screenStyleSheet){var c,b;c=a.split(",");for(b=0;b<c.length;b++){this.screenStyleSheet.addRule(c[b],"behavior:expression(DD_belatedPNG.fixPng(this))")}}},applyVML:function(a){a.runtimeStyle.cssText="";this.vmlFill(a);this.vmlOffsets(a);this.vmlOpacity(a);if(a.isImg){this.copyImageBorders(a)}},attachHandlers:function(i){var d,c,g,e,b,f;d=this;c={resize:"vmlOffsets",move:"vmlOffsets"};if(i.nodeName=="A"){e={mouseleave:"handlePseudoHover",mouseenter:"handlePseudoHover",focus:"handlePseudoHover",blur:"handlePseudoHover"};for(b in e){if(e.hasOwnProperty(b)){c[b]=e[b]}}}for(f in c){if(c.hasOwnProperty(f)){g=function(){d[c[f]](i)};i.attachEvent("on"+f,g)}}i.attachEvent("onpropertychange",this.readPropertyChange)},giveLayout:function(a){a.style.zoom=1;if(a.currentStyle.position=="static"){a.style.position="relative"}},copyImageBorders:function(b){var c,a;c={borderStyle:true,borderWidth:true,borderColor:true};for(a in c){if(c.hasOwnProperty(a)){b.vml.color.shape.style[a]=b.currentStyle[a]}}},vmlFill:function(e){if(!e.currentStyle){return}else{var d,f,g,b,a,c;d=e.currentStyle}for(b in e.vml){if(e.vml.hasOwnProperty(b)){e.vml[b].shape.style.zIndex=d.zIndex}}e.runtimeStyle.backgroundColor="";e.runtimeStyle.backgroundImage="";f=true;if(d.backgroundImage!="none"||e.isImg){if(!e.isImg){e.vmlBg=d.backgroundImage;e.vmlBg=e.vmlBg.substr(5,e.vmlBg.lastIndexOf('")')-5)}else{e.vmlBg=e.src}g=this;if(!g.imgSize[e.vmlBg]){a=document.createElement("img");g.imgSize[e.vmlBg]=a;a.className=g.ns+"_sizeFinder";a.runtimeStyle.cssText="behavior:none; position:absolute; left:-10000px; top:-10000px; border:none; margin:0; padding:0;";c=function(){this.width=this.offsetWidth;this.height=this.offsetHeight;g.vmlOffsets(e)};a.attachEvent("onload",c);a.src=e.vmlBg;a.removeAttribute("width");a.removeAttribute("height");document.body.insertBefore(a,document.body.firstChild)}e.vml.image.fill.src=e.vmlBg;f=false}e.vml.image.fill.on=!f;e.vml.image.fill.color="none";e.vml.color.shape.style.backgroundColor=d.backgroundColor;e.runtimeStyle.backgroundImage="none";e.runtimeStyle.backgroundColor="transparent"},vmlOffsets:function(d){var h,n,a,e,g,m,f,l,j,i,k;h=d.currentStyle;n={W:d.clientWidth+1,H:d.clientHeight+1,w:this.imgSize[d.vmlBg].width,h:this.imgSize[d.vmlBg].height,L:d.offsetLeft,T:d.offsetTop,bLW:d.clientLeft,bTW:d.clientTop};a=(n.L+n.bLW==1)?1:0;e=function(b,p,q,c,s,u){b.coordsize=c+","+s;b.coordorigin=u+","+u;b.path="m0,0l"+c+",0l"+c+","+s+"l0,"+s+" xe";b.style.width=c+"px";b.style.height=s+"px";b.style.left=p+"px";b.style.top=q+"px"};e(d.vml.color.shape,(n.L+(d.isImg?0:n.bLW)),(n.T+(d.isImg?0:n.bTW)),(n.W-1),(n.H-1),0);e(d.vml.image.shape,(n.L+n.bLW),(n.T+n.bTW),(n.W),(n.H),1);g={X:0,Y:0};if(d.isImg){g.X=parseInt(h.paddingLeft,10)+1;g.Y=parseInt(h.paddingTop,10)+1}else{for(j in g){if(g.hasOwnProperty(j)){this.figurePercentage(g,n,j,h["backgroundPosition"+j])}}}d.vml.image.fill.position=(g.X/n.W)+","+(g.Y/n.H);m=h.backgroundRepeat;f={T:1,R:n.W+a,B:n.H,L:1+a};l={X:{b1:"L",b2:"R",d:"W"},Y:{b1:"T",b2:"B",d:"H"}};if(m!="repeat"||d.isImg){i={T:(g.Y),R:(g.X+n.w),B:(g.Y+n.h),L:(g.X)};if(m.search("repeat-")!=-1){k=m.split("repeat-")[1].toUpperCase();i[l[k].b1]=1;i[l[k].b2]=n[l[k].d]}if(i.B>n.H){i.B=n.H}d.vml.image.shape.style.clip="rect("+i.T+"px "+(i.R+a)+"px "+i.B+"px "+(i.L+a)+"px)"}else{d.vml.image.shape.style.clip="rect("+f.T+"px "+f.R+"px "+f.B+"px "+f.L+"px)"}},figurePercentage:function(d,c,f,a){var b,e;e=true;b=(f=="X");switch(a){case"left":case"top":d[f]=0;break;case"center":d[f]=0.5;break;case"right":case"bottom":d[f]=1;break;default:if(a.search("%")!=-1){d[f]=parseInt(a,10)/100}else{e=false}}d[f]=Math.ceil(e?((c[b?"W":"H"]*d[f])-(c[b?"w":"h"]*d[f])):parseInt(a,10));if(d[f]%2===0){d[f]++}return d[f]},fixPng:function(c){c.style.behavior="none";var g,b,f,a,d;if(c.nodeName=="BODY"||c.nodeName=="TD"||c.nodeName=="TR"){return}c.isImg=false;if(c.nodeName=="IMG"){if(c.src.toLowerCase().search(/\.png$/)!=-1){c.isImg=true;c.style.visibility="hidden"}else{return}}else{if(c.currentStyle.backgroundImage.toLowerCase().search(".png")==-1){return}}g=DD_belatedPNG;c.vml={color:{},image:{}};b={shape:{},fill:{}};for(a in c.vml){if(c.vml.hasOwnProperty(a)){for(d in b){if(b.hasOwnProperty(d)){f=g.ns+":"+d;c.vml[a][d]=document.createElement(f)}}c.vml[a].shape.stroked=false;c.vml[a].shape.appendChild(c.vml[a].fill);c.parentNode.insertBefore(c.vml[a].shape,c)}}c.vml.image.shape.fillcolor="none";c.vml.image.fill.type="tile";c.vml.color.fill.on=false;g.attachHandlers(c);g.giveLayout(c);g.giveLayout(c.offsetParent);c.vmlInitiated=true;g.applyVML(c)}};try{document.execCommand("BackgroundImageCache",false,true)}catch(r){}DD_belatedPNG.createVmlNameSpace();DD_belatedPNG.createVmlStyleSheet();
;

/*** gantry-ie6warn.js ***/

/**
 * @version   3.1.15 July 15, 2011
 * @author    RocketTheme http://www.rockettheme.com
 * @copyright Copyright (C) 2007 - 2011 RocketTheme, LLC
 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 */

var RokIEWarn=new Class({site:"sitename",initialize:function(f){var d=f;this.box=new Element("div",{id:"iewarn"}).inject(document.body,"top");var g=new Element("div").inject(this.box).setHTML(d);var e=this.toggle.bind(this);var c=new Element("a",{id:"iewarn_close"}).addEvents({mouseover:function(){this.addClass("cHover");},mouseout:function(){this.removeClass("cHover");},click:function(){e();}}).inject(g,"top");this.height=$("iewarn").getSize().size.y;this.fx=new Fx.Styles(this.box,{duration:1000}).set({top:$("iewarn").getStyle("top").toInt()});this.open=false;var b=Cookie.get("rokIEWarn"),a=this.height;if(!b||b=="open"){this.show();}else{this.fx.set({top:-a});}return;},show:function(){this.fx.start({top:0});this.open=true;Cookie.set("rokIEWarn","open",{duration:7});},close:function(){var a=this.height;this.fx.start({top:-a});this.open=false;Cookie.set("rokIEWarn","close",{duration:7});},status:function(){return this.open;},toggle:function(){if(this.open){this.close();}else{this.show();}}});
;

/*** gantry-smartload.js ***/

/**
 * @version   3.1.15 July 15, 2011
 * @author    RocketTheme http://www.rockettheme.com
 * @copyright Copyright (C) 2007 - 2011 RocketTheme, LLC
 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 */

eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('7 B=l 1f({g:{C:\'1c.1b\',j:H,t:\'17\',p:{x:J,y:J},K:[]},11:8(d){2.X(d);2.j=$(2.g.j);2.9=$$(2.g.t);2.o=2.j.w();7 e=2.g.K[0].R(\',\');5(e.z&&(e.z!=1&&e[0]!=""))e.r(8(b){7 c=$$(b+\' \'+2.g.t);c.r(8(a){2.9.k(a)},2)},2);2.s=0;2.i=l 19({});2.9.r(8(a,b){5(G a==\'A\')q;5(!a.6(\'3\')&&!a.6(\'4\')){2.i.k(a.6(\'n\'));2.9.k(a);q}7 c=a.w().F;5(a.6(\'3\')){c.x=a.6(\'3\');c.y=a.6(\'4\')}5(!a.6(\'3\')&&c.x&&c.y&&!H.1h){a.h(\'3\',c.x).h(\'4\',c.y)}a.h(\'n\',b);2.i.1d(b,{\'m\':a.m,\'3\':c.x,\'4\':c.y,\'D\':l E.16(a,\'14\',{12:Z,U:E.S.Q.W})});5(!2.u(a)){a.h(\'m\',2.g.C).P(\'O\')}N{2.i.k(a.6(\'n\'));2.9.k(a)}},2);5(2.9.z)$(2.j).T(\'v\',2.M.V(2));7 f=2.j},u:8(a){7 b=a.Y(),o=2.j.w(),p=2.g.p;q((b.y>=o.v.y-p.y)&&(b.y<=o.v.y+2.o.F.y+p.y))},M:8(e){7 d=2;5(!2.9||!2.s){2.s=1;q}2.9.r(8(b){5(G b==\'A\')q;5(2.u(b)&&2.i.L(b.6(\'n\'))){7 c=2.i.L(b.6(\'n\'));l 10.13(c.m,{15:8(){7 a={3:c.3,4:c.4};5(a.3&&!a.4)a.4=a.3;5(!a.3&&a.4)a.3=a.4;5(!a.3&&!a.4){a.3=2.3;a.4=2.4}5(a.3!=2.3&&a.4==2.4)a.3=2.3;N 5(a.3==2.3&&a.4!=2.4)a.4=2.4;c.D.I(0).18(8(){b.h(\'3\',a.3).h(\'4\',a.4);b.h(\'m\',c.m).1a(\'O\');2.I(1)});d.9.k(b);d.i.k(b.6(\'n\'))}})}},2)}});B.1e(l 1g,l 1i);',62,81,'||this|width|height|if|getProperty|var|function|images|||||||options|setProperty|storage|container|remove|new|src|smartload|dimensions|offset|return|each|init|cssrule|checkPosition|scroll|getSize|||length|undefined|GantrySmartLoad|placeholder|fx|Fx|size|typeof|window|start|200|exclusion|get|scrolling|else|spinner|addClass|Sine|split|Transitions|addEvent|transition|bind|easeIn|setOptions|getPosition|250|Asset|initialize|duration|image|opacity|onload|Style|img|chain|Hash|removeClass|gif|blank|set|implement|Class|Events|opera|Options'.split('|'),0,{}))
;

/*** gantry-ie6menu.js ***/

/**
 * @version   3.1.15 July 15, 2011
 * @author    RocketTheme http://www.rockettheme.com
 * @copyright Copyright (C) 2007 - 2011 RocketTheme, LLC
 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 */

var sfHover=function(B,C){if(!C){C="sfHover";}var A=$$("."+B).getElements("li");if(!A.length){return false;}A.each(function(D){D.addEvents({mouseenter:function(){var F=this.getProperty("class").split(" ");F=F.filter(function(G){return !G.test("-"+C);});F.each(function(G){if(this.hasClass(G)){this.addClass(G+"-"+C);}},this);var E=F.join("-")+"-"+C;if(!this.hasClass(E)){this.addClass(E);}this.addClass(C);},mouseleave:function(){var F=this.getProperty("class").split(" ");F=F.filter(function(G){return G.test("-"+C);});F.each(function(G){if(this.hasClass(G)){this.removeClass(G);}},this);var E=F.join("-")+"-"+C;if(!this.hasClass(E)){this.removeClass(E);}this.removeClass(C);}});});};window.addEvent("domready",function(){sfHover("menutop");});
;

/*** gantry-module-scroller.js ***/

/**
 * @package		Gantry Template Framework - RocketTheme
 * @version		1.5.1 April 20, 2011
 * @author		RocketTheme http://www.rockettheme.com
 * @copyright 	Copyright (C) 2007 - 2011 RocketTheme, LLC
 * @license		http://www.rockettheme.com/legal/license.php RocketTheme Proprietary Use License
 */

eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('(3(){5 g=2.O=l 1H({k:{1F:1E,m:Q,K:1A,u:1y,z:H.1v.1u.1t},1s:3(d,e){2.7=$(d);6(!2.7)8;2.7=(2.7.D().B(\'F-1j\'))?2.7.D():2.7;2.1i(e);2.A=2.7.1h().1g(3(a){8!a.B(\'1e\')&&!a.B(\'n\')});2.v=$$(2.A).Y(\'.F-10-11\');2.q=0;2.r=[];2.9=[];2.J=[];2.I=[];2.n=2.7.Y(\'.n 14\');2.4=0;5 f=0;2.v.h(3(b,i){f=W.N(f,b.1z);2.q=f;b.h(3(a,j){6(!2.9[j])2.9[j]=[];2.9[j].o(a)},2)},2);6(f<=1){8}2.R();$$(2.A).w(\'p\',2.9[2.4][0].s(\'p\').C());2.A.h(3(a,i){a.w(\'1k\',\'1J\');2.P(a)},2);2.n.h(3(c){c.1C(\'S\',3(){6(c.B(\'1B\'))2.4+=1;G 2.4-=1;6(2.4<0)2.4=2.q-1;G 6(2.4>2.q-1)2.4=0;2.J.h(3(a,i){5 b=2.v[i][2.4];6(!b)b=2.U(2.v[i]);a.1c(b);2.I[i].1b(2.r[2.4])},2)}.M(2))},2);6(2.k.m){2.m=2.E.Z(2.k.K,2);2.7.1a({\'19\':3(){16(2.m)}.M(2),\'18\':3(){2.m=2.E.Z(2.k.K,2)}.M(2)})}},E:3(){2.n[1].15(\'S\')},P:3(a){5 b={t:2.k.z,d:2.k.u};5 c=a.D().s(\'L-13\').C();5 d=l H.17(a,{12:\'X\',1d:Q,1f:{x:0,y:-c},u:b.d,z:b.t});5 e=l H.1l(a,\'p\',{12:\'X\',u:b.d,z:b.t});d.1m();2.J.o(d);2.I.o(e)},U:3(a){5 b=l 1n(\'1o\',{\'1p\':\'F-10-11\'});b.w(\'p\',2.r[2.4]).1q(a.1r(),\'1w\');a.o(b);8 b},R:3(b){2.9.h(3(a,i){6(!$1x(b)||b==i)$$(a).w(\'p\',2.V(i))},2)},V:3(c){5 d=0;2.9[c||0].h(3(a){5 b=2.T(a)||0;d=W.N(a.1D().1G.y+b,d)},2);2.r.o(d);8 d},T:3(a){6(!a)8 0;G 8 a.s(\'L-1I\').C()+a.s(\'L-13\').C()}});2.O.1K(l 1L,l 1M)})();',62,111,'||this|function|current|var|if|el|return|rows||||||||each|||options|new|autoplay|controls|push|height|blocksNo|heights|getStyle||duration|blocks|setStyle|||transition|columns|hasClass|toInt|getFirst|play|rt|else|Fx|fxsHeight|fxs|delay|margin|bind|max|ScrollModules|addFx|false|updateHeights|click|getMargins|missingModule|getRowHeight|Math|cancel|getElements|periodical|block|scroller|link|top|span|fireEvent|clearInterval|Scroll|mouseleave|mouseenter|addEvents|start|toElement|wheelStops|clear|offset|filter|getChildren|setOptions|container|overflow|Style|toTop|Element|div|class|inject|getLast|initialize|easeInOut|Expo|Transitions|after|chk|600|length|5000|down|addEvent|getSize|true|dynamic|size|Class|bottom|hidden|implement|Options|Events'.split('|'),0,{}))
;