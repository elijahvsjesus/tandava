if (!$defined(Napi)) var Napi;
var Napi = new Class;


/**
 * Form behavior
 */
Napi.Grid = new Class({

    getOptions: function(){
        return {
        	
        	id: {
        		form:		'form',
        		checkAll: {
        			id: 	'input.id',
        			toggle: 'input.toggle',
        			count: 	'[name="boxchecked"]'	
        		}	
        	}
        };
    },

    initialize: function(options){
        this.setOptions(this.getOptions(), options);
        if (this.options.initialize) this.options.initialize.call(this);
    },
	
	/**
	* Toggles the check state of a group of boxes
	*/
	checkAll: function(){
		var checked = $$(this.options.id.checkAll.toggle)[0].getProperty('checked');
		var els = $$(this.options.id.form + ' ' + this.options.id.checkAll.id).filter(function(el){
			return $(el).getProperty('checked') !== checked;
		});
		
		
		els.each(function(el){
			el.setProperty('checked', checked);
		});
		$(els[0].form).fireEvent('change');		
		this.count();
	},
	
	count: function(){
		var els = $$(this.options.id.form + ' ' + this.options.id.checkAll.id);
		var count = els.filter(function(el){
			return $(el).getProperty('checked') !== false ;
		}).length;
		$$(this.options.id.checkAll.toggle)[0].setProperty('checked', els.length === count);
	}
});

/*
Class: Element
	Custom class to allow all of its methods to be used with any DOM element via the dollar function <$>.
*/

Element.extend({

	checkall: function(options){
		(new Napi.Grid(options)).checkAll();
		
		return this;
	},
	
	count: function(options){
		(new Napi.Grid(options)).count();
		//grid.count(true);
		
		return this;
	}
});


Napi.Grid.implement(new Events, new Options);

