<?php 
/**
 * @version		$Id$
 * @category	Ninja
 * @copyright	Copyright (C) 2007 - 2010 NinjaForge. All rights reserved.
 * @license		GNU GPLv2 <http://www.gnu.org/licenses/old-licenses/gpl-2.0.html>
 * @link     	http://ninjaforge.com
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

// If MySQLi isn't available at all, do nothing
if (!function_exists('mysqli_connect')) {
	JHTML::stylesheet('grid.css', 'media/com_ninja/css/');
	echo '<div class="placeholder placeholder-up-two-lines"><h1 class="title">' . JText::_("We're sorry but your server isn't<br /> configured with the MySQLi database driver.<br /> Please contact your host an ask them<br /> to enable MySQLi for your PHP install.") . '</h1></div>';
	return;
} 

jimport('joomla.filesystem.file');
$user = & JFactory::getUser();

// Check if Koowa is active
if(JFactory::getApplication()->getCfg('dbtype') != 'mysqli')
{
	if(JRequest::getCmd('setup') == 'mysqli' && $user->authorize( 'com_config', 'manage' ))
	{
		// set gzip on/off based on browser
		$conf = JFactory::getConfig();
		$path = JPATH_CONFIGURATION.DS.'configuration.php';
		if(JFile::exists($path)) {
			JPath::setPermissions($path, '0644');
			$search  = JFile::read($path);
			$replace = str_replace('var $dbtype = \'mysql\';', 'var $dbtype = \'mysqli\';', $search);
			JFile::write($path, $replace);
			JPath::setPermissions($path, '0444');
		}
		$uri = clone JFactory::getURI();
		$uri->delVar('setup');
		JFactory::getApplication()->redirect($uri->toString(), JText::_('Database configuration setting changed to \'mysqli\'.') . '<style>.error{display:none;}</style>');
		return;
	}	
	elseif($user->authorize( 'com_config', 'manage' ))
	{
		JHTML::stylesheet('grid.css', 'media/com_ninja/css/');
		JHTML::_('behavior.modal');
		$uri = clone JFactory::getURI();
		$uri->setVar('setup', 'mysqli');
		echo '<div class="placeholder placeholder-up-two-lines"><h1 class="title">' . JText::_("Koowa plugin requires MySQLi Database Driver. <br />Please change your database <br />configuration to 'mysqli'.") . '</h1><a class="mysqli auto" style="padding-left: 13px;padding-right:13px;" href="' . $uri->toString() . '"><span><span></span></span>' . JText::_('Auto Configure now&hellip;') . '</a>&nbsp;&nbsp;&nbsp;&nbsp;<a class="mysqli how-to modal" rel="{handler: \'iframe\', size: {x: 949, y: 285}}" href="' . JURI::root(1) . '/media/com_ninja/images/mysqli.png"><span><span></span></span>' . JText::_('Show me how&hellip;') . '</a></div>';
		return;
	}
}

if(!JPluginHelper::isEnabled('system', 'koowa'))
{
	if(JFile::exists(JPATH_PLUGINS.'/system/koowa.php'))
	{
		$db = JFactory::getDBO();
		$id = 'NULL';
		$key    = "SELECT id FROM #__plugins WHERE element = 'koowa'";
		$db->setQuery($key);
		$key = $db->loadResult();
		if($key) $id = $key;
		
		$insert = "INSERT INTO `#__plugins` (`id`, `name`, `element`, `folder`, `access`, `ordering`, `published`, `iscore`, `client_id`, `checked_out`, `checked_out_time`, `params`) VALUES ($id, 'System - Koowa', 'koowa', 'system', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', '') ON DUPLICATE KEY UPDATE `published` = 1;";
		
		$db->execute($insert);
		$plugin = (object)array('type' => 'system', 'name' => 'koowa');
		JPluginHelper::_import($plugin, 1, 1);
		
		$manager = JFactory::getApplication()->isAdmin() ? '<a href="' . JRoute::_('index.php?option=com_plugins&view=plugin&client=site&task=edit&cid[]=' . $db->insertid()) . '">'.JText::_("Edit &laquo;System - Koowa&raquo; in the Plugin Manager.").'</a>' : null;
		$msg = JText::_("Koowa System Plugin activated. ") . $manager;
		if($user->authorize( 'com_plugins', 'manage' )) JFactory::getApplication()->enqueueMessage($msg);
	}
	else
	{
		$msg = JText::_("Koowa wasn't found. Please reinstall ".JRequest::getCmd('option')." as Koowa are installed with it.");
		if($user->authorize('com_installer', 'installer')) JError::raiseWarning(0, $msg);
		else JError::raiseNotice(0, JText::_('Whoops, something went wrong. We\'ll fix it right away!'));
	}
}

if(!JPluginHelper::isEnabled('system', 'ninja'))
{
	if(JFile::exists(JPATH_PLUGINS.'/system/ninja.php'))
	{
		$db = JFactory::getDBO();
		$id = 'NULL';
		$key    = "SELECT id FROM #__plugins WHERE element = 'ninja'";
		$db->setQuery($key);
		$key = $db->loadResult();
		if($key) $id = $key;
		
		$insert = "INSERT INTO `#__plugins` (`id`, `name`, `element`, `folder`, `access`, `ordering`, `published`, `iscore`, `client_id`, `checked_out`, `checked_out_time`, `params`) VALUES ($id, 'System - Ninja Mootools 1.2 loader', 'ninja', 'system', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', '') ON DUPLICATE KEY UPDATE `published` = 1;";
		
		$db->execute($insert);
		
		$manager = JFactory::getApplication()->isAdmin() ? '<a href="' . JRoute::_('index.php?option=com_plugins&view=plugin&client=site&task=edit&cid[]=' . $db->insertid()) . '">'.JText::_("Edit &laquo;System - Ninja Mootools 1.2 loader&raquo; in the Plugin Manager.").'</a>' : null;
		$msg = JText::_("Ninja Mootools 1.2 loader System Plugin activated. ") . $manager;
		if($user->authorize( 'com_plugins', 'manage' )) JFactory::getApplication()->redirect(KRequest::url(), $msg);
	}
	else
	{
		$msg = JText::_("Ninja Mootools 1.2 loader wasn't found. Please reinstall ".JRequest::getCmd('option')." as Ninja Mootools 1.2 loader are installed with it.");
		if($user->authorize('com_installer', 'installer')) JError::raiseWarning(0, $msg);
		else JError::raiseNotice(0, JText::_('Whoops, something went wrong. We\'ll fix it right away!'));
	}
}


// Add Ninja template filters and some legacy
if(defined('KOOWA'))
{

	//@TODO get rid of this legacy mapping	
	KFactory::map('lib.koowa.document', 'lib.joomla.document');


	$rules = array(
		KFactory::get('admin::com.ninja.template.filter.document')
	);

	KFactory::get('lib.koowa.template.default')->addFilters($rules);
}