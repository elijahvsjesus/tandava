<?php defined( 'KOOWA' ) or die( 'Restricted access' );
/**
 * @version		$Id: listbox.php 434 2010-08-17 15:32:50Z stian $
 * @category	Napi
 * @package		Napi_Parameter
 * @copyright	Copyright (C) 2007 - 2010 NinjaForge. All rights reserved.
 * @license		GNU GPLv2 <http://www.gnu.org/licenses/old-licenses/gpl-2.0.html>
 * @link     	http://ninjaforge.com
 */

class ComNinjaElementListbox extends ComNinjaElementGetlist
{
	// Change this later to use the koowa helper
}
