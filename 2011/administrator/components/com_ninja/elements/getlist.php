<?php defined( 'KOOWA' ) or die( 'Restricted access' );
/**
 * @version		$Id: getlist.php 434 2010-08-17 15:32:50Z stian $
 * @category	Napi
 * @package		Napi_Parameter
 * @copyright	Copyright (C) 2007 - 2010 NinjaForge. All rights reserved.
 * @license		GNU GPLv2 <http://www.gnu.org/licenses/old-licenses/gpl-2.0.html>
 * @link     	http://ninjaforge.com
 */

class ComNinjaElementGetlist extends ComNinjaElementAbstract
{
	function fetchElement($name, $value, &$node, $control_name)
	{
		
		$key = isset($node['key']) ? (string)$node['key'] : 'value';
		$text = isset($node['val']) ? (string)$node['val'] : 'text';

		$attr = array('class' => 'value');
				
		$options = isset($node['get']) ? KFactory::get((string)$node['get'])->getList() : array();
		if(method_exists($options, 'getData')) $options = $options->getData();
		foreach ($node->children() as $element => $child)
		{
			$option = ($element == 'option') ? JHTML::_('select.option', null, (string)$child, $key, $text, false, array('class' => 'value')) : JHTML::_('select.optgroup', (string)$child, $key, $text);
			array_unshift($options, $option);
		}

		return JHTML::_('select.genericlist', $options, $this->name, $attr, $key, $text, $value, $this->id, true);
	}
}
