<?php defined( 'KOOWA' ) or die( 'Restricted access' );
/**
 * @version		$Id: radio.php 434 2010-08-17 15:32:50Z stian $
 * @category	Napi
 * @package		Napi_Parameter
 * @copyright	Copyright (C) 2007 - 2010 NinjaForge. All rights reserved.
 * @license		GNU GPLv2 <http://www.gnu.org/licenses/old-licenses/gpl-2.0.html>
 * @link     	http://ninjaforge.com
 */

class ComNinjaElementRadio extends ComNinjaElementAbstract
{
	function fetchElement($name, $value, &$node, $control_name)
	{
		$options = array();
		foreach ($node->children() as $option)
		{
			$val	= $option['value'];
			$text	= (string)$option;
			$options[] = JHTML::_('select.option', $val, JText::_($text));
		}

		//select.radiolist arguments reference:
		//$arr, $name, $attribs = null, $key = 'value', $text = 'text', $selected = null, $idtag = false, $translate = false
		return JHTML::_('select.radiolist', $options, $this->name, '', 'value', 'text', $value, $this->id );
	}
}