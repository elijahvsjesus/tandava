<?php defined( 'KOOWA' ) or die( 'Restricted access' );
/**
 * @version		$Id: hidden.php 434 2010-08-17 15:32:50Z stian $
 * @category	Napi
 * @package		Napi_Parameter
 * @copyright	Copyright (C) 2007 - 2010 NinjaForge. All rights reserved.
 * @license		GNU GPLv2 <http://www.gnu.org/licenses/old-licenses/gpl-2.0.html>
 * @link     	http://ninjaforge.com
 */

class ComNinjaElementHidden extends ComNinjaElementAbstract
{
	public function fetchToolTip()
	{
		return false;
	}
	
	public function before()
	{
		return null;
	}
	
	public function after()
	{
		return null;
	}
	
	public function fetchElement($name, $value, &$node, $control_name)
	{
		return '<input type="hidden" name="'.$control_name.'['.$this->_parent->getGroup().']['.$name.']" id="'.$control_name.$this->_parent->getGroup().$name.'" value="'.$value.'" />';
	}
}