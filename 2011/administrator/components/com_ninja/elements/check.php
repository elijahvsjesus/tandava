<?php defined( 'KOOWA' ) or die( 'Restricted access' );
/**
 * @version		$Id: check.php 434 2010-08-17 15:32:50Z stian $
 * @category	Napi
 * @package		Napi_Parameter
 * @copyright	Copyright (C) 2007 - 2010 NinjaForge. All rights reserved.
 * @license		GNU GPLv2 <http://www.gnu.org/licenses/old-licenses/gpl-2.0.html>
 * @link     	http://ninjaforge.com
 */

class ComNinjaElementCheck extends ComNinjaElementAbstract
{
	public function fetchElement($name, $value, &$node, $control_name)
	{
		$options = array ();
		foreach ($node->children() as $option)
		{
			$val	= (string)$option['value'];
			$text	= (string)$option;
			$options[] = (object)array('value' => $val, 'text' => $text);
		}
		$vertical = isset($node['vertical']) ? ' vertical' : null;

		$html[] = '<ul class="group'.$vertical.'">'; 
		
		$realname = $this->field.'['.$this->group.']['.$name.']';
		$idname   = $this->field.'_'.$this->group.'_'.$name;
		$checklist = KTemplateHelperSelect::checklist( $options, $realname, $value, array('id' => '{id}'), 'value', 'text');
		$search = array('for="'.$realname, 'id="'.$realname);
		$replace = array('for="'.$idname, 'id="'.$idname);
		$checklist = str_replace($search, $replace, $checklist);
		foreach(explode('</label>', $checklist) as $check)
		{
			$html[] = '<li class="value">';
			
			$html[] = $check;
			
			$html[] = '</label></li>';
		}
		$html[] = '</ul>';
		return implode($html);
		
	}
}
