<?php defined( 'KOOWA' ) or die( 'Restricted access' );
/**
 * @version		$Id: sql.php 434 2010-08-17 15:32:50Z stian $
 * @category	Napi
 * @package		Napi_Parameter
 * @copyright	Copyright (C) 2007 - 2010 NinjaForge. All rights reserved.
 * @license		GNU GPLv2 <http://www.gnu.org/licenses/old-licenses/gpl-2.0.html>
 * @link     	http://ninjaforge.com
 */

class ComNinjaElementSQL extends ComNinjaElementAbstract
{
	function fetchElement($name, $value, &$node, $control_name)
	{
		 // Base name of the HTML control.
		$ctrl  = $control_name .'['. $name .']';
		$db			= & JFactory::getDBO();
		$db->setQuery($node['query']);
		$key = ($node['key_field'] ? $node['key_field'] : 'value');
		$val = ($node['value_field'] ? $node['value_field'] : $name);
		// Construct the various argument calls that are supported.
        $attribs       = ' ';
        if ($v = $node['size']) {
                $attribs       .= 'size="'.$v.'"';
        }
        if ($v = $node['class']) {
                $attribs       .= 'class="'.$v.'"';
        } else {
                $attribs       .= 'class="inputbox"';
        }
        if ($node['multiple'])
        {
                $attribs       .= ' multiple="multiple"';
                $ctrl          .= '[]';
        }
		return JHTML::_('select.genericlist',  $db->loadObjectList(), $ctrl, $attribs, $key, $val, $value, $control_name.$name);
	}
}