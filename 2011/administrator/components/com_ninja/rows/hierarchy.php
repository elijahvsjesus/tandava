<?php defined( 'KOOWA' ) or die( 'Restricted access' );
/**
 * @version		$Id: hierarchy.php 434 2010-08-17 15:32:50Z stian $
 * @category	Ninja
 * @package     Ninja_Rows
 * @copyright	Copyright (C) 2007 - 2010 NinjaForge. All rights reserved.
 * @license		GNU GPLv2 <http://www.gnu.org/licenses/old-licenses/gpl-2.0.html>
 * @link     	http://ninjaforge.com
 */

/**
 * Database Row Class capable of dealing with hierarchies
 *
 * @author		Stian Didriksen <stian@ninjaforge.com>
 * @category	Ninja
 * @package     Ninja_Rows
 */
class ComNinjaRowHierarchy extends KDatabaseRowAbstract
{

	/**
	 * Saves the row to the database.
	 *
	 * This performs an update on all children rows when the path changes.
	 *
	 * @return KDatabaseRowAbstract
	 */
	public function saves()
    {
    	if(!empty($this->id)) 
    	{
    		$id    = $this->id;
    		
    		$table = KFactory::get($this->getTable());
    		
    		$query = $table->getDatabase()->getQuery();
    		$query->where('path', 'like', '%'.$id.'%');
    		$path  = $this->path;
    		$path  = $path ? $path.'/'.$id : $id;
    		foreach($table->select($query) as $row)
    		{
    			$parts = explode($id, $row->path);
    			$part = isset($parts[1]) ? $parts[1] : null;
    			$row->path = $path.$part;
    			$row->save();
    		}	
    	}
    	
    	parent::save();
    	
        return $this;
    }
}