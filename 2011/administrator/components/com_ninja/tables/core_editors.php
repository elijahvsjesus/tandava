<?php defined( 'KOOWA' ) or die( 'Restricted access' );
/**
 * @version		$Id: core_editors.php 434 2010-08-17 15:32:50Z stian $
 * @category	Ninja
 * @copyright	Copyright (C) 2007 - 2010 NinjaForge. All rights reserved.
 * @license		GNU GPLv2 <http://www.gnu.org/licenses/old-licenses/gpl-2.0.html>
 * @link     	http://ninjaforge.com
 */

class ComNinjaTableCore_Editors extends KDatabaseTableAbstract
{
	public function __construct(KConfig $options)
	{
		$options->name		= 'plugins';
		$options->base		= 'plugins';
		$options->identity_column	= 'id';
		
		parent::__construct($options);
	}
}