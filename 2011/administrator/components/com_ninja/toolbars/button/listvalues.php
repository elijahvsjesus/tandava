<?php defined( 'KOOWA' ) or die( 'Restricted access' );
/**
 * @version		$Id: listvalues.php 434 2010-08-17 15:32:50Z stian $
 * @category	NinjaForge Plugin Manager
 * @copyright	Copyright (C) 2007 - 2010 NinjaForge. All rights reserved.
 * @license		GNU GPLv2 <http://www.gnu.org/licenses/old-licenses/gpl-2.0.html>
 * @link     	http://ninjaforge.com
 */

/**
 * Install button class for a toolbar
 * 
 * @author		Stian Didriksen <stian@ninjaforge.com>
 * @category	Napi
 * @package		Napi_Toolbar
 * @subpackage	Button
 */
class ComNinjaBoardToolbarButtonListvalues extends NToolbarButtonLink
{
	public function __construct(KConfig $options)
	{
		$options->text	= 'List Values';
		parent::__construct($options);
	}
	
	public function getLink()
	{
		$query['view']	= 'listvalues';
		return parent::getLink($query);
	}
	public function render()
	{
		return parent::render();
	}
}