<?php defined( 'KOOWA' ) or die( 'Restricted access' );
/**
 * @version		$Id: default.php 434 2010-08-17 15:32:50Z stian $
 * @package		Ninja
 * @copyright	Copyright (C) 2010 NinjaForge. All rights reserved.
 * @license 	GNU GPLv2 <http://www.gnu.org/licenses/old-licenses/gpl-2.0.html>
 * @link     	http://ninjaforge.com
 */
 
 /**
 * Default button class for a toolbar
 * 
 * @author		Stian Didriksen <stian@ninjaforge.com>
 */
class ComNinjaToolbarButtonDefault extends ComNinjaToolbarButtonPost
{
	
	/**
	 * Gives the button 'list' status.
	 *
	 * When the view is plural, and the list got no items, this button wont render
	 *
	 * @var boolean true
	 */
	public $list = true;
	
	public function __construct(KConfig $options)
	{		
		parent::__construct($options);
		$this->attribs->set(array('class' => $this->attribs->class . ' validate'));
	}
}