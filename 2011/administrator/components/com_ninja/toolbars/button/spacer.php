<?php defined( 'KOOWA' ) or die( 'Restricted access' );
/**
* @version      $Id: spacer.php 434 2010-08-17 15:32:50Z stian $
* @category		Napi
* @package		Napi_Toolbar
* @subpackage	Button
* @copyright	Copyright (C) 2010 NinjaForge. All rights reserved.
* @license      GNU GPLv2 <http://www.gnu.org/licenses/old-licenses/gpl-2.0.html>
*/

/**
 * Spacer
 * 
 * @author		Stian Didriksen <stian@ninjaforge.com>
 * @category	Napi
 * @package		Napi_Toolbar
 * @subpackage	Button
 */
class ComNinjaToolbarButtonSpacer extends KToolbarButtonAbstract
{
	public function render()
	{
		return $this->_parent->loadTemplate('button_spacer');
	}

}