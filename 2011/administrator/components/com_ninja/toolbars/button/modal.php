<?php defined( 'KOOWA' ) or die( 'Restricted access' );
/**
 * @version		$Id: modal.php 434 2010-08-17 15:32:50Z stian $
 * @category	NinjaForge Plugin Manager
 * @copyright	Copyright (C) 2007 - 2010 NinjaForge. All rights reserved.
 * @license		GNU GPLv2 <http://www.gnu.org/licenses/old-licenses/gpl-2.0.html>
 * @link     	http://ninjaforge.com
 */

/**
 * Modal button class for a toolbar
 * 
 * @author		Stian Didriksen <stian@ninjaforge.com>
 * @category	Napi
 * @package		Napi_Toolbar
 * @subpackage	Button
 */
class ComNinjaToolbarButtonModal extends KToolbarButtonNew
{
	public function __construct(KConfig $options)
	{
		parent::__construct($options);
		$name = KInflector::underscore($options->text);
		$img = KFactory::get('admin::com.ninja.helper')->img('/32/'.$name.'.png');
		if($img)
		{
			KFactory::get('admin::com.ninja.helper')->css('#toolbar-box .'.$options->icon.' { background-image: url('.$img.'); }');
		}
		if(!isset($this->_options['x'])) $this->_options['x'] = 720;
		if(!isset($this->_options['y'])) $this->_options['y'] = 'window.getSize().size.y-80';
		if(!isset($this->_options['handler'])) $this->_options['handler'] = 'iframe';
		if(!isset($this->_options['ajaxOptions'])) $this->_options['ajaxOptions'] = '{}';
	}
	
	public function render()
	{
		$text	= JText::_($this->_options['text']);
		
		//Tooltip
		//KTemplateAbstract::loadHelper('behavior.tooltip');
		
		//Call the modal behavior
		//KTemplateAbstract::loadHelper('behavior.modal');
		
		$html 	= array ();
		$html[]	= '<td class="button" id="'.$this->getId().'">';
		$html[]	= '<a href="'.JRoute::_($this->_options['link']).'" title="'.@$this->_options['title'].'" onclick="'. $this->getOnClick().'" rel="{handler:\''.$this->_options['handler'].'\', size: {x: '.$this->_options['x'].', y: '.$this->_options['y'].'},ajaxOptions:'.$this->_options['ajaxOptions'].'}" class="toolbar modal">';

		$html[]	= '<span class="'.$this->getClass().'" title="'.$text.'">';
		$html[]	= '</span>';
		$html[]	= $text;
		$html[]	= '</a>';
		$html[]	= '</td>';

		return implode(PHP_EOL, $html);
	}
}