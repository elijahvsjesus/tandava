<?php defined( 'KOOWA' ) or die( 'Restricted access' );
/**
 * @version     $Id: exception.php 434 2010-08-17 15:32:50Z stian $
 * @category	Koowa
 * @package     Form
 * @subpackage 	Element
 * @copyright   Copyright (C) 2007 - 2010 Johan Janssens and Mathias Verraes. All rights reserved.
 * @license     GNU GPLv2 <http://www.gnu.org/licenses/old-licenses/gpl-2.0.html>
 * @link        http://www.koowa.org
 */

/**
 * Koowa Form Element Exception class
 *
 * @author      Mathias Verraes <mathias@joomlatools.eu>
 * @category	Koowa
 * @package     Form
 * @subpackage 	Element
 */
class ComNinjaFormElementException extends ComNinjaFormException
{
	//Do nothing
}