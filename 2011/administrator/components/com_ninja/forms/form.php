<?php defined( 'KOOWA' ) or die( 'Restricted access' );
/**
 * @version		$Id: form.php 434 2010-08-17 15:32:50Z stian $
 * @category	Koowa
 * @package		Koowa_Form
 * @copyright	Copyright (C) 2007 - 2010 Johan Janssens and Mathias Verraes. All rights reserved.
 * @license		GNU GPLv2 <http://www.gnu.org/licenses/old-licenses/gpl-2.0.html>
 * @link     	http://www.koowa.org
 */

/**
 * Form Class
 *
 * @author		Mathias Verraes <mathias@joomlatools.org>
 * @category	Koowa
 * @package     Koowa_Form
 */
class ComNinjaForm
{
	/**
	 * Global tab index, to be used by all form elements in all forms
	 *
	 * @var int
	 */
	static public $tabIndex = 0;
}