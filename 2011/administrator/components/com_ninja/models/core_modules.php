<?php defined( 'KOOWA' ) or die( 'Restricted access' );
/**
 * @version		$Id: core_modules.php 434 2010-08-17 15:32:50Z stian $
 * @package		Koowa
 * @copyright	Copyright (C) 2010 Nooku. All rights reserved.
 * @license 	GNU GPLv2 <http://www.gnu.org/licenses/old-licenses/gpl-2.0.html>
 * @link     	http://www.nooku.org
 */

class ComNinjaModelCore_modules extends KModelAbstract
{	
	/**
	 * Constructor
	 *
	 * @param	array An optional associative array of configuration settings.
	 */
	public function __construct(KConfig $options)
	{
		parent::__construct($options);
		
		KLoader::load('lib.joomla.application.module.helper');
		$this->_list = JModuleHelper::_load();
		$this->_total = count($this->_list);
	}
}