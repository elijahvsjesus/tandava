<?php defined( 'KOOWA' ) or die( 'Restricted access' );
/**
 * @version		$Id: modules.php 434 2010-08-17 15:32:50Z stian $
 * @package		Koowa
 * @copyright	Copyright (C) 2010 Nooku. All rights reserved.
 * @license 	GNU GPLv2 <http://www.gnu.org/licenses/old-licenses/gpl-2.0.html>
 * @link     	http://www.nooku.org
 */

class ComNinjaModelJoomlaModules extends KModelAbstract
{	
	/**
	 * Constructor
	 *
	 * @param	array An optional associative array of configuration settings.
	 */
	public function __construct(KConfig $config)
	{
		parent::__construct($config);
		
		$this->_state->insert('position', 'cmd');
		
		KLoader::load('lib.joomla.application.module.helper');
		$this->_list = &JModuleHelper::_load();
		$this->_total = count($this->_list);
	}
	
	/**
	 * Append new modules to the modules array
	 *
	 * @author Stian Didriksen <stian@ninjaforge.com>
	 * @param $module Module object
	 * @return $this
	 */
	public function append($module)
	{
		$this->_list[] = $module;
		
		return $this;
	}
	
	/**
	 * Count modules in a position
	 *
	 * @author Stian Didriksen <stian@ninjaforge.com>
	 * @return int
	 */
	public function count()
	{
		return count(array_filter($this->_list, array($this, '_count')));
	}
	
	/**
	 * Count callback function
	 *
	 * @author your name
	 * @param $param
	 * @return return type
	 */
	protected function _count($module)
	{
		if(!$this->_state->position) return true;
		
		return $this->_state->position == $module->position;
	}
}