<? defined( 'KOOWA' ) or die( 'Restricted access' ) ?>
<? @$xml = simplexml_load_file(KFactory::get('admin::com.ninja.helper.application')->getPath('com_xml')) ?>

<style type="text/css">
	.info dt,
	.info dd {
		float: left;
		display: inline-block;
	}
	
	.info dt {
		clear: both;
	}
	.info dd {
		padding-left: 0.6em !important;
	}
	
	/* @group RTL */
	html[dir=rtl] .info dt,
	html[dir=rtl] .info dd {
		float: right;
	}
	html[dir=rtl] .info dd {
		padding-right: 0.6em !important;
	}
	/* @end */
</style>

<? $list = array(
	'author'		=> 'Created By',
	'license'		=> 'Code License',
	'csslicense'	=> 'CSS License',
	'jslicense'		=> 'JS License',
	'copyright'		=> 'Copyright',
) ?>

<dl class="info">
<? foreach($list as $definition => $title) : ?>

	<? if(!isset($xml->$definition)) continue ?>

	<dt>
		<strong>
			<?= @text($title) ?>
		</strong>
	</dt>
	<dd>
		<?= $xml->$definition ?>
	</dd>
	
<? endforeach ?>
</dl>

<div style="clear: both"></div>

<dl class="info">
<? if(isset(@$xml->support)) : ?>
	<dt>
		<strong>
			<?= @text('Support') ?>
		</strong>
	</dt>
	<dd>
		<a href="<?= @$xml->support['href'] ?>" target="_blank"><?= @text(@$xml->support) ?></a>
	</dd>
<? endif ?>
<? if(isset(@$xml->rate)) : ?>
	<dt>
		<strong>
			<?= @text('If you like this extension, consider giving it an honest review at the') ?>
		</strong>
	</dt>
	<dd>
		<a href="<?= @$xml->rate['href'] ?>" target="_blank"><?= @text(@$xml->rate) ?></a>
	</dd>
<? endif ?>
</dl>
<!--<p><strong><?= @text(@$xml->name.' - ') ?></strong>	<?= @text(@$xml->description) ?></p>-->
<? if(isset(@$xml->credits)) : ?>
	<hr />
	<p>
	<h3><?= @text('Credits') ?></h3>
	<script type="text/javascript">
		window.addEvent('domready', function(){
			var favicons = $$('.credits a');
			favicons.each(function(e,i){
				var uri = new URI(e.get('href')),
					src = {pre: 'http://query.yahooapis.com/v1/public/yql?q=', q: 'select * from html where url="', host: 'http://'+uri.get('host'), middle: '"and xpath="', xpath: "/html/head/link[\@rel='icon'] | /html/head/link[\@rel='ICON'] | /html/head/link[\@rel='shortcut icon'] | /html/head/link[\@rel='SHORTCUT ICON']"+'"', post: '&format=json'},
					url = src.pre + encodeURIComponent(src.q + src.host + src.middle + src.xpath) + src.post;

					var req = new Request.JSONP({url:url, onSuccess: function(ico){

							var host = favicons[i].get('host'), result = ico.query.results.link;
							if(result.length > 0) result = result[0];

							new Element('img', {'src': host+'/'+result.href.replace(host, ''), width:16, height:16}).injectTop(favicons[i]);
						}
					}).send();

				e.set('host', uri.get('scheme')+'://'+uri.get('host'));
			});
		});
	</script>
	<style type="text/css">
		hr {
			opacity: 0.2;
		}
		.credits ul {
			list-style:none;
			padding: 0;
		}
		.credits li {
			background-image: none;
			list-style:none;
			text-height: 16px;
			font-size: 14px;
			vertical-align: middle;
		}
	</style>
	<ul class="credits">
	<? foreach(@$xml->credits->children() as $a => $credit) : ?>
	<li>
		<? $attr = current($credit->attributes()) ?>
		<? if($attr) $attr = ' '.KHelperArray::toString($attr) ?>
		
		<a<?= $attr ?>>
			<?= $credit ?>
		</a>
	</li>
	<? endforeach ?>
	</ul>
	</p>
<? endif ?>