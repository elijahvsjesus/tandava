<?php defined( 'KOOWA' ) or die( 'Restricted access' );
/**
 * @version		$Id: html.php 434 2010-08-17 15:32:50Z stian $
 * @category	Ninja
 * @copyright	Copyright (C) 2007 - 2010 NinjaForge. All rights reserved.
 * @license		GNU GPLv2 <http://www.gnu.org/licenses/old-licenses/gpl-2.0.html>
 * @link     	http://ninjaforge.com
 */

class ComNinjaViewTemplatesHtml extends ComNinjaViewDefault
{
	public function display()
	{	
		$this->assign('date', KFactory::get('lib.joomla.utilities.date'));
	
		$this->_createToolbar()
			->reset();
			//->append(KFactory::get('admin::com.ninja.toolbar.button.install'))
			//->append('uninstall');

		$this->setLayout('admin::com.ninja.view.templates.default');
		return parent::display();
	}
}