<? /** $Id: toolbar_title.php 347 2010-06-10 20:24:50Z stian $ */ ?>
<? defined( 'KOOWA' ) or die( 'Restricted access' ) ?>

<div class="header icon-48-generic icon-48-<?= $name ?>">
	<?= @text($title) ?>
</div>