<? /** $Id: filter_form.php 359 2010-06-23 11:21:09Z stian $ */ ?>
<? defined( 'KOOWA' ) or die( 'Restricted access' ) ?>

<form action="<?= @route() ?>" method="get" style="display:inline;" id="<?= @id('search') ?>">
	<label for="search"><?= @text('Filter:') ?></label>
	<?= @helper('admin::com.ninja.helper.paginator.search', array(@$state->search)) ?>
	<!--&#160;&#160;-->
	<input type="hidden" name="option" value="com_<?= $this->getIdentifier()->package ?>" />
	<input type="hidden" name="view" value="<?= KFactory::get($this->getView())->getName() ?>" />
</form>