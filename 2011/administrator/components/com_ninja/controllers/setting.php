<?php defined( 'KOOWA' ) or die( 'Restricted access' );
/**
 * @version		$Id: setting.php 462 2010-08-31 12:09:27Z stian $
 * @category	Ninja
 * @copyright	Copyright (C) 2007 - 2010 NinjaForge. All rights reserved.
 * @license		GNU GPLv2 <http://www.gnu.org/licenses/old-licenses/gpl-2.0.html>
 * @link     	http://ninjaforge.com
 */

/**
 * Ninja Settings Controller
 *
 * @package Ninja
 */
class ComNinjaControllerSetting extends ComNinjaControllerView
{	
	/**
	 * Generic method to modify the default status of items
	 *
	 * @return void
	 */
	protected function _actionDefault($data)
	{
		$model  = KFactory::get($this->getModel());
		$table  = KFactory::get($model->getTable());
		
		$data['default'] = 1;
		
		//Prevent more than one item to be set as default
		$data['id'] = array($data['id'][0]);
		$this->_request->id = $data['id'];
		
		//Undefault any other default setting
		$table->select(array('default' => 1), KDatabase::FETCH_ROWSET)->setData(array('default' => 0))->save();
			
		$this->_redirect_message = JText::_('Default setting changed.');
		
		return $this->execute('edit', $data);
	}
}