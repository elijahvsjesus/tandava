<?php defined( 'KOOWA' ) or die( 'Restricted access' );
/**
 * @version		$Id: dashboard.php 481 2010-09-15 13:25:13Z stian $
 * @package		Ninja
 * @copyright	Copyright (C) 2010 NinjaForge. All rights reserved.
 * @license 	GNU GPLv2 <http://www.gnu.org/licenses/old-licenses/gpl-2.0.html>
 * @link     	http://ninjaforge.com
 */
 
/**
 * Dashboard Controller
 */
class ComNinjaControllerDashboard extends KControllerView
{
	/**
	 * Constructor
	 *
	 * @param 	object 	An optional KConfig object with configuration options
	 */
	public function __construct(KConfig $config)
	{
		parent::__construct($config);

		if(KRequest::get('get.action', 'cmd') == 'update') $this->execute('update');
		if(KRequest::get('get.action', 'cmd') == 'checkversion') $this->execute('checkversion');
	}

	/**
	 * Generic framework update action
	 *
	 * 		Using CURL and SourceForge to get the latest release
	 *		and runs a update procedure.
	 *
	 * @TODO		Support the SVN package later,
	 *				for increased performance
	 *				and to let us update from Assembla.
	 */
	protected function _actionUpdate()
	{
		// Import used APIs
		jimport('joomla.filesystem.file');
		jimport('joomla.filesystem.archive');
		jimport('joomla.installer.installer');


		$path = KFactory::get('admin::com.ninja.helper.application')->getPath('com_xml');
		
		//load the file, and save it to our object
		$xml = simplexml_load_file($path);
		if(!isset($xml->updateurl)) return false;
		$url = (string) $xml->updateurl;

		// Prepare curl
		$curl = KFactory::get('admin::com.ninja.helper.curl');
		$opt  = array(
						CURLOPT_RETURNTRANSFER => true,
						//CURLOPT_FOLLOWLOCATION => true, //this don't work if open_basedir or safe_mode is on
						CURLOPT_HTTPHEADER     => array("Expect:"),
						CURLOPT_HEADERFUNCTION => array($curl, 'readHeader')
				);
		$curl->addSession( $url, $opt );

		// Download tarball package and save it to the /tmp/ folder
		$result 	= $curl->exec();
		$info		= $curl->info();
		$filename	= $info[0]['content_disposition'];
		$foldername = JFile::stripExt($filename);
		JFile::write(JPATH_ROOT.'/tmp/'.$filename, $result);
		$curl->clear();

		// Unpack the tarball
		JArchive::extract(JPATH_ROOT.'/tmp/'.$filename, JPATH_ROOT.'/tmp/'.$foldername.'/');
		JFile::delete(JPATH_ROOT.'/tmp/'.$filename);

		// Install the update
		$installer = JInstaller::getInstance();
		$installer->install(JPATH_ROOT.'/tmp/'.$foldername.'/');

		// Cleanup
		JFolder::delete(JPATH_ROOT.'/tmp/'.$foldername.'/');

		$msg = array(
			'text' => sprintf(JText::_('%s upgraded successfully.'), JText::_($this->getIdentifier()->package)),
			'update' => true
		);
		
		if(KRequest::type() == 'AJAX') die(json_encode($msg));
		return true;
	}

	public function displayView(KCommandContext $context)
	{
		$view = KFactory::get($this->getView());

		if($view instanceof KViewTemplate) {
			KFactory::get($view->getTemplate())->addFilters(array(KFactory::get('admin::com.ninja.template.filter.document')));
		}

		KRequest::set('get.hidemainmenu', 0);
		if(!KRequest::has('get.layout', 'cmd'))
		{
			KRequest::set('get.layout', 'admin::com.ninja.views.dashboard.basic');
			
			if(KRequest::get('get.tmpl', 'cmd') == 'component') {
				KRequest::set('get.layout', 'admin::com.ninja.views.dashboard.popup');
			} else {
				$this->_renderPositionHints();
			}

			$context->result = $view
				->setLayout(KRequest::get('get.layout', 'string', 'default' ))
				->display();
		}
	}
	
	/**
	 * Renders hints about positions in the backend dashboard
	 *
	 * @author	Stian Didriksen <stian@ninjaforge.com>
	 * @return	void
	 */
	protected function _renderPositionHints()
	{
		$package	= $this->getIdentifier()->package;
		$modules	= KFactory::tmp('admin::com.ninja.model.joomla.modules');
		$document	= KFactory::get('lib.joomla.document');
		$accordions	= $package.'-dashboard-accordions';
		$accordions	= $modules->position($accordions)->count() || KRequest::has('cookie.'.$accordions);
		$tabs		= $package.'-dashboard-tabs';
		$tabs		= $modules->position($tabs)->count() || KRequest::has('cookie.'.$tabs);

		if(!$accordions || !$tabs)
		{
			$document->addScriptDeclaration("window.addEvent('domready', function(){
				$$('.placeholder .title code').each(function(title){
					new Element('button', {
						'class': 'close',
						'html': '&times;',
						'events': {
							'click': function(text){
								Cookie.write(text, true, {duration: 365});
								this.fade('out');
							}.pass(title.get('text'), title.getParent('[id^=com-$package-dashboard]'))
						}
					}).inject(title.getParent('.title'), 'before');
				});
			})");
		}

		if(!$accordions)
		{
			$modules->append(
				KFactory::get('admin::com.ninja.helper.module')->create(array(
					'title'	=> 'Accordions module position',
					'position' => $package.'-dashboard-accordions',
					'content' => KFactory::tmp('admin::com.ninja.helper.placeholder', array(
						'class' => 'placeholder smaller',
						'title' => sprintf(JText::_('Publish modules here using %s'), '<br /><code>'.$package.'-dashboard-accordions</code><br />')
					))
				))
			);
		}
		
		if(!$tabs)
		{
			$modules->append(
				KFactory::get('admin::com.ninja.helper.module')->create(array(
					'id'	=> rand(),
					'title'	=> 'Tabs module position',
					'position' => $package.'-dashboard-tabs',
					'content' => KFactory::tmp('admin::com.ninja.helper.placeholder', array(
						'class' => 'placeholder smaller',
						'title' => sprintf(JText::_('Publish modules here using %s'), '<br /><code>'.$package.'-dashboard-tabs</code><br />')
					))
				))
			);
			
		}
	}
	
	protected function _actionCheckversion()
	{
		$path = KFactory::get('admin::com.ninja.helper.application')->getPath('com_xml');

		//load the file, and save it to our object
		$xml = simplexml_load_file($path);
		if(!isset($xml->updateurl)) return false;
		$url = (string) $xml->updateurl;

		// Prepare curl
		$curl = KFactory::get('admin::com.ninja.helper.curl');
		$opt  = array(
						CURLOPT_RETURNTRANSFER => true,
						//CURLOPT_FOLLOWLOCATION => true, //this don't work if open_basedir or safe_mode is on
						CURLOPT_HTTPHEADER     => array("Expect:"),
						CURLOPT_HEADERFUNCTION => array($curl, 'readHeader')
				);
		$curl->addSession( $url, $opt );
		
		// @TODO in NEC2, let's have a api for this, so it's way faster
		$curl->exec();
		
		$info		= $curl->info();
		$filename	= $info[0]['content_disposition'];
		
		$revision = $this->extractRevisionFromFilename($filename);
		$status   = $this->extractStatusFromFilename($filename);
		$version  = $this->extractVersionFromFilename($filename);
		
		$version_compare  = version_compare((string) $xml->version, $version, '<=');
		$revision_compare = (int) $xml->revision < $revision;

		if($version_compare && $revision_compare)
		{
			$msg = array(
				'text' => sprintf(JText::_('%1$s %2$s %3$s rev%4$s available for download.'), JText::_($this->getIdentifier()->package), $version, JText::_($status), $revision),
				'update' => true
			);
			
			if(KRequest::type() == 'AJAX') die(json_encode($msg));
			return true;
		}
		else
		{
			$msg = array(
				'text' => sprintf(JText::_('This is the newest version of %1$s.'), JText::_($this->getIdentifier()->package), $version, JText::_($status), $revision),
				'update' => false
			);
			if(KRequest::type() == 'AJAX') die(json_encode($msg));
			return false;
		}
	}
	
	private function extractVersionFromFilename($file)
	{
		$pattern = '/_v([0-9]\.[0-9]\.[0-9])/';
		if (preg_match($pattern, $file, $result)) {
			return $result[1];
		} else {
			return false;
		}
	}
	
	private function extractStatusFromFilename($file)
	{
		$pattern = '/_v[0-9]\.[0-9]\.[0-9](.*?)_rev/';
		if (preg_match($pattern, $file, $result)) {
			return $result[1];
		} else {
			return false;
		}
	}

	private function extractRevisionFromFilename($file)
	{
		$pattern = '/rev([0-9]+)\.[A-Za-z]/';
		if (preg_match($pattern, $file, $result)) {
			return $result[1];
		} else {
			return false;
		}
	}
}