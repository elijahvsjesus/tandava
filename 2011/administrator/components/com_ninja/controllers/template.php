<?php defined( 'KOOWA' ) or die( 'Restricted access' );
/**
 * @version		$Id: template.php 434 2010-08-17 15:32:50Z stian $
 * @category	Ninja
 * @copyright	Copyright (C) 2007 - 2010 NinjaForge. All rights reserved.
 * @license		GNU GPLv2 <http://www.gnu.org/licenses/old-licenses/gpl-2.0.html>
 * @link     	http://ninjaforge.com
 */

/**
 * Ninja Template Controller
 *
 * @package Ninja
 */
class ComNinjaControllerTemplate extends ComNinjaControllerInstall
{

	/**
	 * Constructor
	 *
	 * @param array An optional associative array of configuration settings.
	 */
	public function __construct(KConfig $options)
	{
		parent::__construct($options);
	}
}