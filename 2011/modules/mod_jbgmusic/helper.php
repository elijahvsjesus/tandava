<?php
/**
* version 1.5 JBGMusic
* @copyright Copyright (C) 2008 Jfriendly.net. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
* 
* <object type="audio/mpeg" data="data/test.mp3" width="200" height="20">
*  <param name="src" value="data/test.mp3">
*  <param name="autoplay" value="false">
*  <param name="autoStart" value="0">
*  alt : <a href="data/test.mp3">test.mp3</a>
*</object>

*/

// no direct access
defined('_JEXEC') or die('Restricted access');

class modJBGMusicHelper
{

     function getJBGMusicParams (&$params)
     {
     }

     function cleanJBGMusicOldFiles ()
     {
        // Clean up leftover .xspf created 3 hours ago.

	$dir = JPATH_BASE.DS."tmp".DS ;
	$dp = opendir($dir) or die ('Could not open '.$dir);
	while (($filename = readdir($dp)) !== false) {
		if (preg_match("/jbg./i", $filename) && filemtime($dir.$filename) < strtotime ("-3 hours") ) unlink ($dir.$filename) ;
	
	}

	closedir($dp);
     }
}

