<?php
/**
* version 1.0 JBGMusic
* @copyright Copyright (C) 2008 Jfriendly.net. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
* 
* <object type="audio/mpeg" data="data/test.mp3" width="200" height="20">
*  <param name="src" value="data/test.mp3">
*  <param name="autoplay" value="false">
*  <param name="autoStart" value="0">
*  alt : <a href="data/test.mp3">test.mp3</a>
*</object>

*/

// no direct access
defined('_JEXEC') or die('Restricted access');

define('JPATH_BASE', dirname(__FILE__) );
define( 'DS', DIRECTORY_SEPARATOR );

require_once dirname(__FILE__).DS.'helper.php';
require_once dirname(__FILE__).DS.'include_ver.php';

modJBGMusicHelper::cleanJBGMusicOldFiles ();
$JBGParams = modJBGMusicHelper::getJBGMusicParams ($params);


$url = JURI::base();

// Set Local Environment variables
$JUSER_AGENT = $_SERVER['HTTP_USER_AGENT'];
$ip = str_replace(".","", $_SERVER['REMOTE_ADDR']);

$OPERATING_SYSTEM = 'O';
if (preg_match ("/Windows/i", $JUSER_AGENT)) $OPERATING_SYSTEM = 'W';
if (preg_match ("/Linux/i", $JUSER_AGENT)) $OPERATING_SYSTEM = 'L';

if ($OPERATING_SYSTEM == 'O') $LINE_BRK = "\r";
if ($OPERATING_SYSTEM == 'W') $LINE_BRK = "\r\n";
if ($OPERATING_SYSTEM == 'L') $LINE_BRK = "\n";


$moduleclass_sfx = $params->get('moduleclass_sfx', '');
$jsound_location = $params->get('jsound_location', '');
$jaudio_files = $params->get('jsound_file', '');
$jrandom_play = $params->get('jrandom_play', '1');
$jshuffle_play = $params->get('jshuffle_play', '1');
$jauto_resume = $params->get('jautoresume', '1');
$jtemp_location = $params->get('jtemp_location', '');


$ok = modVerifyLicense::of_CheckLicense ($params->get('jkeycode', ''), "JBGMusic");
$jaudio_list = explode(',', $jaudio_files);
$jaudio_hash = md5 ($jaudio_files);

// Create a new audio list
if ($jrandom_play == '0'){
    $jsound_file = $jaudio_list[rand(0,(count($jaudio_list)))];
}
else{
    $jsound_file = $OPERATING_SYSTEM.$ip.".xspf";
}

if ($jsound_file == "") $jsound_file = trim($jaudio_list[0]);


if ($params->get('jloop_sound'))
   $jloopy = "&repeat_playlist=true";
else
   $jloopy = "";


if ($params->get('jautostart'))
   $jautostart = '&autoplay=true';
else
   $jautostart = "";

if ($params->get('jautoresume'))
   $jautoresume = '&autoresume=1';
else
   $jautoresume = "";


if ($params->get('jhidden_controls'))
{
   $jheight = 'height="0"';
   $jwidth = 'width="0"';
   $jautostart = "&autoplay=true";
}
else
{
   $jheight = 'height="16"';  //20
   $jwidth = 'width="200"';   //144
}



// create a new .xspf file

// $keywords = explode(".", JText::_($jsound_file));

// create a unique file using ip, year, month, date, and Hour


$File = $jtemp_location.'/'.$ip.$module->id.'jbg.xspf';
$FileHash = $jtemp_location.'/'.$ip.$module->id.'jbg.hsh';


//Check for audio list differences
if (file_exists($FileHash)){
   $Handle = fopen($FileHash, 'r');
   $jstored_hash = fread ($Handle, 100);
   fclose ($Handle);
   if ($jstored_hash <> $jaudio_hash){ 
        unlink ($File);
        unlink ($FileHash);
   }
}

if (!file_exists($File)) {
        $Handle = fopen($FileHash, 'w');
        fwrite ($Handle, $jaudio_hash);
        fclose ($Handle);

	$Handle = fopen($File, 'w');


	fwrite ($Handle, '<?xml version="1.0" encoding="UTF-8"?>'.$LINE_BRK);
	fwrite ($Handle, '<playlist version="1" xmlns="http://xspf.org/ns/0/">'.$LINE_BRK);
	fwrite ($Handle, '<title>Powered by JBGMusic</title>'.$LINE_BRK);
	fwrite ($Handle, '<trackList>'.$LINE_BRK);



	if ($jrandom_play == '0'){

	    // check if valid url
	    if (preg_match ("/^http:|^ftp:/i", $jsound_file))
	    {
	       $Data = trim($jsound_file);
	    }
	    else{
	       $Data = $url.$jsound_location.'/'.trim($jsound_file);
	    }

           
           fwrite($Handle, "<track>".$LINE_BRK);
           fwrite($Handle, "<title>".$jsound_file."</title>".$LINE_BRK);
           fwrite($Handle, "<location>".$Data."</location>".$LINE_BRK);
	   fwrite($Handle, "</track>".$LINE_BRK);
	}
	else
	{
	   if ($jshuffle_play == '1') shuffle($jaudio_list);
	   $i = 0;

	  
	   foreach ($jaudio_list as $value){
		
		 if (preg_match ("/^http:|^ftp:/i", $value))
		{
		     $Data = trim($value);
		}
		else{
		     $Data = $url.$jsound_location.'/'.trim($value);
		}

                fwrite($Handle, "<track>".$LINE_BRK);
                fwrite($Handle, "<title>".$value."</title>".$LINE_BRK);
                fwrite($Handle, "<location>".$Data."</location>".$LINE_BRK);
	        fwrite($Handle, "</track>".$LINE_BRK);

	       	$i = $i + 1;
	   }
	}


	fwrite ($Handle, '</trackList>'.$LINE_BRK);
	fwrite ($Handle, '</playlist>'.$LINE_BRK);
	fclose($Handle); 

}



// You are not allowed to remove/modify the following code.
?>

<style type="text/css">
<!--
.jbgstyle {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 9px;
        align: center;
}
-->
</style>
<object type="application/x-shockwave-flash" <?php echo $jheight?> <?php echo $jwidth?>
data="<?php echo $url.'modules/mod_jbgmusic/xspf_player_slim.swf?playlist_url='.$url.$File.$jautostart.$jloopy.$jautoresume?>">
<param name="movie" 
value="<?php echo $url.'modules/mod_jbgmusic/xspf_player_slim.swf?playlist_url='.$url.$File.$jautostart.$jloopy.$jautoresume?>" />
<param name="allownetworking" value="internal" />
<param name="allowScriptAccess" value="never" />
<param name="enableJSURL" value="false" />
<param name="enableHREF" value="false" />
<param name="saveEmbedTags" value="true" />
<param name="quality" value="high" />
<param name="wmode" value="transparent" />
</object>





 


