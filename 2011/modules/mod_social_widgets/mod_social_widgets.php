<?php

/**
 * @package Social Widgets Ultimate Edition for Joomla! 1.5
 * @version $Id: 1.0.5
 * @author Turnkeye.com
 * @copyright (C) 2010 Turnkeye.com
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

// no direct access

defined('_JEXEC') or die('Restricted access');

	# add the css style to the head
	$doc = &JFactory::getDocument();
	$doc->addStyleSheet('modules/mod_social_widgets/css/main.css');

	# get module params
	$twitter_login = $params->get('twitter_login', '');
	$twitter_widget = $params->get('twitter_widget', '');

	$small_twitter = $params->get('small_twitter', '');
	$small_facebook = $params->get('small_facebook', '');
	
	$big_twitter = $params->get('big_twitter', '');
	$big_facebook = $params->get('big_facebook', '');
	$big_share_facebook = $params->get('big_share_facebook', '');

	$facebook = $params->get('facebook', '');
	$twitter = $params->get('twitter', '');
	$myspace = $params->get('myspace', '');
	$stumbleupon = $params->get('stumbleupon', '');
	$reddit = $params->get('reddit', '');
	$delicious = $params->get('delicious', '');
	$google = $params->get('google', '');
	$mail = $params->get('mail', '');
	$print = $params->get('print', '');

	$number_of_tweets = $params->get('number_of_tweets', '');
	$show_avatars = $params->get('show_avatars', '');
	$show_timestamps = $params->get('show_timestamps', '');
	$show_hashtags = $params->get('show_hashtags', '');
	
	$shell_background = $params->get('shell_background', '');
	$shell_color = $params->get('shell_color', '');
	$tweets_background = $params->get('tweets_background', '');
	$tweets_color = $params->get('tweets_color', '');
	$tweets_links = $params->get('tweets_links', '');
	
	$icons_size = $params->get('icons_size', '');

	$social_url = JURI::current();

echo '<div class="social_module">';

if ($facebook != "hide" or $twitter != "hide" or $myspace != "hide" or $stumbleupon != "hide" or $reddit != "hide" or $delicious != "hide" or $google != "hide" or $mail != "hide" or $print != "hide") {
	echo <<<EOT

<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js"></script>
<div class="addthis_toolbox">
   <div class="custom_images">
EOT;
	if ($facebook == "show") { 
		echo '<a class="addthis_button_facebook"><img src="modules/mod_social_widgets/images/'.$icons_size.'/facebook.png" alt="Share to Facebook" /></a>&nbsp;'; 
	}
	if ($twitter == "show") {
		echo '<a class="addthis_button_twitter"><img src="modules/mod_social_widgets/images/'.$icons_size.'/twitter.png" alt="Share to Twitter" /></a>&nbsp;';
	}
	if ($myspace == "show") {
		echo '<a class="addthis_button_myspace"><img src="modules/mod_social_widgets/images/'.$icons_size.'/myspace.png" alt="Share to MySpace" /></a>&nbsp;';
	}
	if ($stumbleupon == "show") {
		echo '<a class="addthis_button_stumbleupon"><img src="modules/mod_social_widgets/images/'.$icons_size.'/stumbleupon.png" alt="Stumble It" /></a>&nbsp;';
	}
	if ($reddit == "show") {
		echo '<a class="addthis_button_reddit"><img src="modules/mod_social_widgets/images/'.$icons_size.'/reddit.png" alt="Share to Reddit" /></a>&nbsp;';
	}
	if ($delicious == "show") {
		echo '<a class="addthis_button_delicious"><img src="modules/mod_social_widgets/images/'.$icons_size.'/delicious.png" alt="Share to Delicious" /></a>&nbsp;';
	}
	if ($google == "show") {
		echo '<a class="addthis_button_google"><img src="modules/mod_social_widgets/images/'.$icons_size.'/google.png" alt="Share to Google Buzz" /></a>&nbsp;';
	}
	if ($mail == "show") {
		echo '<a class="addthis_button_email"><img src="modules/mod_social_widgets/images/'.$icons_size.'/email.png" alt="Email a Friend" /></a>&nbsp;';
	}
	if ($print == "show") {
		echo '<a class="addthis_button_print"><img src="modules/mod_social_widgets/images/'.$icons_size.'/print.png" alt="Print" /></a>&nbsp;';
	}
	echo <<<EOT
   </div>
</div>
EOT;
}

if ($small_twitter != "hide" or $small_facebook != "hide") {
	echo <<<EOT
<br/>
<div class="addthis_toolbox">
EOT;
	if ($small_facebook == "show"){
		echo '<a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>';
	}
	if ($small_twitter == "show"){
		echo '<a class="addthis_button_tweet"></a>';
	}
	echo <<<EOT
</div>
<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js"></script>
<br/>     
EOT;
}

if ($twitter_widget == "show") {

	if (empty($show_avatars)) {
		$show_avatars="false";
	}
	if (empty($show_timestamps)) {
		$show_timestamps="false";
	}
	if (empty($show_hashtags)) {
		$show_hashtags="false";
	}

	echo <<<EOT
<script src="http://widgets.twimg.com/j/2/widget.js" type="text/javascript"></script>
<script type="text/javascript">
new TWTR.Widget({
	version: 2,
	type: 'profile',
	rpp: $number_of_tweets,
	interval: 6000,
	width: 'auto',
	height: 250,
	theme: {
		shell: {
			background: '$shell_background',
			color: '$shell_color'
		},
		tweets: {
			background: '$tweets_background',
			color: '$tweets_color',
			links: '$tweets_links'
		}
	},
	features: {
		scrollbar: false,
		loop: false,
		live: false,
		hashtags: $show_hashtags,
		timestamp: $show_timestamps,
		avatars: $show_avatars,
		behavior: 'all'
	}
}).render().setUser('$twitter_login').start();
</script>
EOT;
}

if ($big_twitter == "show") {
	echo <<<EOT
<a href="http://twitter.com/share" class="twitter-share-button" data-count="vertical">Tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
<br/><br/>
EOT;
}

if ($big_facebook == "show") {
	echo <<<EOT
<iframe src="http://www.facebook.com/plugins/like.php?href=$social_url&amp;layout=standard&amp;show_faces=true&amp;width=150&amp;action=like&amp;colorscheme=light&amp;height=80" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:150px; height:80px;" allowTransparency="true"></iframe>
EOT;
}

if ($big_share_facebook == "show") {
	echo <<<EOT
<a name="fb_share" type="button_count" href="http://www.facebook.com/sharer.php">Share</a><script src="http://static.ak.fbcdn.net/connect.php/js/FB.Share" type="text/javascript"></script>
<br/><br/>
EOT;
}

?>