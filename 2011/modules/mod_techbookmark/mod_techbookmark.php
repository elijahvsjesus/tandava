<?php 
/* 
* @author Bryan Keller
* Email : satechheads@yahoo.com
* URL : www.sanantoniocomputerrepair.net
* Description : This allows you to bookmark the current page on the top social boomarking sites.
* Copyright (c) 2011 Techheads - IT Consulting
* License GNU GPL
***/

/// no direct access 

defined('_JEXEC') or die('Restricted access');


$document =& JFactory::getDocument();
$mod = JURI::base() . 'modules/mod_techbookmark/icons/';
$document->addStyleSheet(JURI::base() . 'modules/mod_techbookmark/style.css');

$url = urlencode("http://".$_SERVER['HTTP_HOST'] ). getenv('REQUEST_URI');
$title = urlencode($mainframe->getPageTitle());

	$moduleclass_sfx 	     = $params->get('moduleclass_sfx','');
	$target 			     = $params->get('target','_blank');
	$size 				= $params->get('size','32');
	$margin				= $params->get('margin','3px');

// added by de elijah hatem

echo '<div class="twitterfollow"><a href="http://twitter.com/festivaltandava" class="twitter-follow-button" data-lang="pt">Follow @festivaltandava</a></div>
<script src="http://platform.twitter.com/widgets.js" type="text/javascript"></script>';

echo '<div style="margin-left: 10px;width:100%;">';

		if ($params->get('sb1', 'yes') == "yes") echo '<a href="http://www.bibsonomy.org/editBookmark?url='.$url.'&amp;description='.$title.'" target="_blank" rel="nofollow"><img style="margin:'. $margin .';" src="modules/mod_techbookmark/icons/bibsonomy.png" width="'.$size.'" height="'.$size.'" alt="Bibsonomy" title="Bibsonomy" align="left" /></a>';

		if ($params->get('sb2', 'yes') == "yes") echo '<a href="http://blinklist.com/index.php?Action=Blink/addblink.php&Name='.$title.'&Url='.$url.'" target="_blank" rel="nofollow"><img style="margin:'. $margin .';" src="modules/mod_techbookmark/icons/blinklist.png" width="'.$size.'" height="'.$size.'" alt="Blinklist" title="Blinklist" align="left" /></a>';

		if ($params->get('sb3', 'yes') == "yes") echo '<a href="http://www.connotea.org/add?continue=return&uri='.$url.'&amp;title='.$title.'" target="_blank" rel="nofollow"><img style="margin:'. $margin .';" src="modules/mod_techbookmark/icons/connotea.png" width="'.$size.'" height="'.$size.'" alt="Connotea" title="Connotea" align="left" /></a>';

		if ($params->get('sb4', 'yes') == "yes") echo '<a href="http://del.icio.us/post?url='.$url.'&amp;title='.$title.'" target="_blank" rel="nofollow"><img style="margin:'. $margin .';" src="modules/mod_techbookmark/icons/delicious.png" alt="deli.cio.us" title="deli.cio.us" align="left" height="'.$size.'" width="'.$size.'" /></a>';

		if ($params->get('sb5', 'yes') == "yes") echo '<a href="http://digg.com/submit?phase=2&amp;url='.$url.'&amp;title='.$title.'" target="_blank" rel="nofollow"><img style="margin:'. $margin .';" src="modules/mod_techbookmark/icons/digg.png" width="'.$size.'" height="'.$size.'" alt="Digg" title="Digg" align="left" /></a>';

		if ($params->get('sb6', 'yes') == "yes") echo '<a href="http://www.diigo.com/post?url='.$url.'&amp;title='.$title.'" target="_blank" rel="nofollow"><img style="margin:'. $margin .';" src="modules/mod_techbookmark/icons/diigo.png" width="'.$size.'" height="'.$size.'" alt="Diigo" title="Diigo" align="left" /></a>';

		if ($params->get('sb7', 'yes') == "yes") echo '<a href="http://www.dropjack.com/submit.php?url='.$url.'" target="_blank" rel="nofollow"><img style="margin:'. $margin .';" src="modules/mod_techbookmark/icons/dropjack.png" width="'.$size.'" height="'.$size.'" alt="Dropjack" title="Dropjack" align="left" /></a>';

		if ($params->get('sb8', 'yes') == "yes") echo '<a href="http://www.dzone.com/links/add.html?url='.$url.'&title='.$title.'" target="_blank" rel="nofollow"><img style="margin:'. $margin .';" src="modules/mod_techbookmark/icons/dzone.png" width="'.$size.'" height="'.$size.'" alt="Dzone" title="Dzone" align="left" /></a>';

		if ($params->get('sb9', 'yes') == "yes") echo '<a href="http://de.facebook.com/sharer.php?u='.$url.'&amp;t='.$title.'" target="_blank" rel="nofollow"><img style="margin:'. $margin .';" src="modules/mod_techbookmark/icons/facebook.png" alt="Facebook" title="Facebook" align="left" height="'.$size.'"   width="'.$size.'" /></a>';

		if ($params->get('sb10', 'yes') == "yes") echo '<a href="http://cgi.fark.com/cgi/fark/edit.pl?new_url='.$url.'&new_comment='.$title.'&linktype=Misc" target="_blank" rel="nofollow"><img style="margin:'. $margin .';" src="modules/mod_techbookmark/icons/fark.png" width="'.$size.'" height="'.$size.'" alt="Fark" title="Fark" align="left" /></a>';

		if ($params->get('sb11', 'yes') == "yes") echo '<a href="http://faves.com/Authoring.aspx?u='.$url.'&t='.$title.'" target="_blank" rel="nofollow"><img style="margin:'. $margin .';" src="modules/mod_techbookmark/icons/faves.png" width="'.$size.'" height="'.$size.'" alt="Faves" title="Faves" align="left" /></a>';

		if ($params->get('sb12', 'yes') == "yes") echo '<a href="http://www.folkd.com/page/social-bookmarking.html?addurl='.$url.'" target="_blank" rel="nofollow"><img style="margin:'. $margin .';" src="modules/mod_techbookmark/icons/folkd.png" width="'.$size.'" height="'.$size.'" alt="Folkd" title="Folkd" align="left" /></a>';

		if ($params->get('sb13', 'yes') == "yes") echo '<a href="http://friendfeed.com/?title='.$title.' '.$url.'" target="_blank" rel="nofollow"><img style="margin:'. $margin .';" src="modules/mod_techbookmark/icons/friendfeed.png" width="'.$size.'" height="'.$size.'" alt="Friendfeed" title="Friendfeed" align="left" /></a>';

		if ($params->get('sb14', 'yes') == "yes") echo '<a href="http://www.furl.net/storeIt.jsp?u='.$url.'&t='.$title.'" target="_blank" rel="nofollow"><img style="margin:'. $margin .';" src="modules/mod_techbookmark/icons/furl.png" width="'.$size.'" height="'.$size.'" alt="Furl" title="Furl" align="left" /></a>';
	
		if ($params->get('sb15', 'yes') == "yes") echo '<a href="http://www.google.com/bookmarks/mark?op=add&amp;bkmk='.$url.'&amp;title='.$title.'" target="_blank" rel="nofollow"><img style="margin:'. $margin .';" src="modules/mod_techbookmark/icons/google.png" width="'.$size.'" height="'.$size.'" alt="Google Bookmarks" title="Google Bookmarks" align="left" /></a>';

		if ($params->get('sb16', 'yes') == "yes") echo '<a href="http://www.google.com/buzz/post?url='.$url.'&amp;title='.$title.'&message={TITLE}" target="_blank" rel="nofollow"><img style="margin:'. $margin .';" src="modules/mod_techbookmark/icons/google_buzz.png" width="'.$size.'" height="'.$size.'" alt="Google Buzz" title="Google Buzz" align="left" /></a>';

		if ($params->get('sb17', 'yes') == "yes") echo '<a href="http://identi.ca//index.php?action=bookmarklet&status_textarea='.$title.'-'.$url.'" target="_blank" rel="nofollow"><img style="margin:'. $margin .';" src="modules/mod_techbookmark/icons/identica.png" width="'.$size.'" height="'.$size.'" alt="Identica" title="Identica" align="left" /></a>';

		if ($params->get('sb18', 'yes') == "yes") echo '<a href="http://www.jumptags.com/add/?url='.$url.'&amp;title='.$title.'" target="_blank" rel="nofollow"><img style="margin:'. $margin .';" src="modules/mod_techbookmark/icons/jumptags.png" width="'.$size.'" height="'.$size.'" alt="Jumptags" title="Jumptags" align="left" /></a>';

		if ($params->get('sb19', 'yes') == "yes") echo '<a href="http://kirtsy.com/submit.php?url='.$url.'" target="_blank" rel="nofollow"><img style="margin:'. $margin .';" src="modules/mod_techbookmark/icons/kirtsy.png" width="'.$size.'" height="'.$size.'" alt="Kirtsy" title="Kirtsy" align="left" /></a>';

		if ($params->get('sb20', 'yes') == "yes") echo '<a href="http://www.linkagogo.com/go/AddNoPopup?title='.$title.'&url='.$url.'" target="_blank" rel="nofollow"><img style="margin:'. $margin .';" src="modules/mod_techbookmark/icons/linkagogo.png" width="'.$size.'" height="'.$size.'" alt="Linkagogo" title="Linkagogo" align="left" /></a>';

		if ($params->get('sb21', 'yes') == "yes") echo '<a href="http://www.linkedin.com/shareArticle?mini=true&url='.$url.'&title='.$title.'&ro=false" target="_blank" rel="nofollow"><img style="margin:'. $margin .';" src="modules/mod_techbookmark/icons/linkedin.png" width="'.$size.'" height="'.$size.'" alt="Linkedin" title="Linkedin" align="left" /></a>';

		if ($params->get('sb22', 'yes') == "yes") echo '<a href="http://www.mister-wong.de/index.php?action=addurl&amp;bm_url='.$url.'&amp;bm_description='.$title.'" target="_blank" rel="nofollow"><img style="margin:'. $margin .';" src="modules/mod_techbookmark/icons/misterwong.png" alt="Mister Wong" title="Mister Wong" align="left" height="'.$size.'" width="'.$size.'" /></a>';

		if ($params->get('sb23', 'yes') == "yes") echo '<a href="http://www.myspace.com/index.cfm?fuseaction=postto&amp;u='.$url.'&amp;t='.$title.'" target="_blank" rel="nofollow"><img style="margin:'. $margin .';" src="modules/mod_techbookmark/icons/myspace.png" alt="MySpace" title="MySpace" align="left" height="'.$size.'"   width="'.$size.'" /></a>';

		if ($params->get('sb24', 'yes') == "yes") echo '<a href="http://www.newsvine.com/_tools/seed&amp;save?u='.$url.'&amp;h='.$title.'" target="_blank" rel="nofollow"><img style="margin:'. $margin .';" src="modules/mod_techbookmark/icons/newsvine.png" width="'.$size.'" height="'.$size.'" border="0" alt="Newsvine" title="Newsvine" align="left" /></a>';

		if ($params->get('sb25', 'yes') == "yes") echo '<a href="http://www.propeller.com/submit/?U='.$url.'&T='.$title.'" target="_blank" rel="nofollow"><img style="margin:'. $margin .';" src="modules/mod_techbookmark/icons/propeller.png" width="'.$size.'" height="'.$size.'" alt="Propeller" title="Propeller" align="left" /></a>';

		if ($params->get('sb26', 'yes') == "yes") echo '<a href="http://reddit.com/submit?url='.$url.'&amp;title='.$title.'" target="_blank" rel="nofollow"><img style="margin:'. $margin .';" src="modules/mod_techbookmark/icons/reddit.png" width="'.$size.'" height="'.$size.'" border="0" alt="reddit" title="reddit" align="left" /></a>';

		if ($params->get('sb27', 'yes') == "yes") echo '<a href="http://simpy.com/simpy/LinkAdd.do?note=&href='.$url.'" target="_blank" rel="nofollow"><img style="margin:'. $margin .';" src="modules/mod_techbookmark/icons/simpy.png" width="'.$size.'" height="'.$size.'" alt="Simpy" title="simpy" align="left" /></a>';

		if ($params->get('sb28', 'yes') == "yes") echo '<a href="http://slashdot.org/slashdot-it.pl?op=basic&url='.$url.'" target="_blank" rel="nofollow"><img style="margin:'. $margin .';" src="modules/mod_techbookmark/icons/slashdot.png" width="'.$size.'" height="'.$size.'" alt="Slashdot" title="Slashdot" align="left" /></a>';

		if ($params->get('sb29', 'yes') == "yes") echo '<a href="http://www.spurl.net/spurl.php?url='.$url.'&title='.$title.'" target="_blank" rel="nofollow"><img style="margin:'. $margin .';" src="modules/mod_techbookmark/icons/spurl.png" width="'.$size.'" height="'.$size.'" alt="Spurl" title="Spurl" align="left" /></a>';

		if ($params->get('sb30', 'yes') == "yes") echo '<a href="http://www.stumbleupon.com/submit?url='.$url.'&amp;title='.$title.'" target="_blank" rel="nofollow"><img style="margin:'. $margin .';" src="modules/mod_techbookmark/icons/stumbleupon.png" width="'.$size.'" height="'.$size.'" alt="StumbleUpon" title="StumbleUpon" align="left" /></a>';

		if ($params->get('sb31', 'yes') == "yes") echo '<a href="http://twitter.com/home?status='.$url.'" target="_blank" rel="nofollow" title="Twitter"><img style="margin:'. $margin .';" src="modules/mod_techbookmark/icons/twitter.png" width="'.$size.'" height="'.$size.'" alt="Twitter" title="Twitter" align="left" /></a>';

echo '</div>';

echo '<div style="clear: both;"></div>';

$sizegoogle	= $params->get("sizegoogle");
$count 		= $params->get("count");


		if ($params->get('sb0', 'yes') == "yes") {
echo '<script type="text/javascript" src="http://apis.google.com/js/plusone.js">
</script>';
echo '<div class="googleclass" style="margin-left: 12px;">
<g:plusone size="'.$sizegoogle.'" href="'.$url.'" count="'.$count.'"></g:plusone>';}
echo '<br /></div>';

echo '<div style="margin-left: 10px; text-align: center; font-size: 6px; color: #FFFFFF;">';

		if ($params->get('support', 'yes') == 'yes') echo '<a style="color:#FFFFFF;" href="http://www.sanantoniocomputerrepair.net/" title="Virus Removal">Virus Removal</a>';

echo '</div>';

?>
