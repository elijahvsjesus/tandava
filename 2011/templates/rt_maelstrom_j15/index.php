<?php
/**
 * @package Gantry Template Framework - RocketTheme
 * @version 1.5.1 April 20, 2011
 * @author RocketTheme http://www.rockettheme.com
 * @copyright Copyright (C) 2007 - 2011 RocketTheme, LLC
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 *
 * Gantry uses the Joomla Framework (http://www.joomla.org), a GNU/GPLv2 content management system
 *
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted index access' );

// load and inititialize gantry class
require_once('lib/gantry/gantry.php');
$gantry->init();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $gantry->language; ?>" lang="<?php echo $gantry->language;?>" >
<head>
	<?php 
		$gantry->displayHead();
		$gantry->addStyles(array('template.css','joomla.css','typography.css'));
		if ($gantry->get('fixedheader')) $gantry->addScript('rt-fixedheader.js');
		if ($gantry->browser->name == 'ie' && $gantry->browser->shortversion == '7'){
			$gantry->addScript('ie7-fixes.js');
		}
		
	?>
<script type="text/javascript">
/*
	For functions getElementsByClassName, addClassName, and removeClassName
	Copyright Robert Nyman, http://www.robertnyman.com
	Free to use if this text is included
*/
function addLoadEvent(func) {
  var oldonload = window.onload;
  if (typeof window.onload != 'function') {
    window.onload = func;
  } else {
    window.onload = function() {
      oldonload();
      func();
    }
  }
}

function getElementsByClassName(className, tag, elm){
	var testClass = new RegExp("(^|\\s)" + className + "(\\s|$)");
	var tag = tag || "*";
	var elm = elm || document;
	var elements = (tag == "*" && elm.all)? elm.all : elm.getElementsByTagName(tag);
	var returnElements = [];
	var current;
	var length = elements.length;
	for(var i=0; i<length; i++){
		current = elements[i];
		if(testClass.test(current.className)){
			returnElements.push(current);
		}
	}
	return returnElements;
}

function addClassName(elm, className){
    var currentClass = elm.className;
    if(!new RegExp(("(^|\\s)" + className + "(\\s|$)"), "i").test(currentClass)){
        elm.className = currentClass + ((currentClass.length > 0)? " " : "") + className;
    }
    return elm.className;
}

function removeClassName(elm, className){
    var classToRemove = new RegExp(("(^|\\s)" + className + "(\\s|$)"), "i");
    elm.className = elm.className.replace(classToRemove, "").replace(/^\s+|\s+$/g, "");
    return elm.className;
}

function makeTheTableHeadsHighlight() {
	// get all the td's in the heart of the table...
	var table = document.getElementById('programacao');
	var tbody = table.getElementsByTagName('tbody');
	var tbodytds = table.getElementsByTagName('td');
	
	// and loop through them...
	for (var i=0; i<tbodytds.length; i++) {
	
	// take note of their default class name
		tbodytds[i].oldClassName = tbodytds[i].className;
		
	// when someone moves their mouse over one of those cells...
		tbodytds[i].onmouseover = function() {
	
	// attach 'on' to the pointed cell
			addClassName(this,'on');
			
	// attach 'on' to the th in the thead with the same class name
			var topheading = getElementsByClassName(this.oldClassName,'th',table);
			addClassName(topheading[0],'on');
			
	// attach 'on' to the far left th in the same row as this cell
			var row = this.parentNode;
			var rowheading = row.getElementsByTagName('th')[0];
			addClassName(rowheading,'on');
		}
	
	// ok, now when someone moves their mouse away, undo everything we just did.
	
		tbodytds[i].onmouseout = function() {
	
	// remove 'on' from this cell
			removeClassName(this,'on');
			
	// remove 'on' from the th in the thead
			var topheading = getElementsByClassName(this.oldClassName,'th',table);
			removeClassName(topheading[0],'on');

	// remove 'on' to the far left th in the same row as this cell
			var row = this.parentNode;
			var rowheading = row.getElementsByTagName('th')[0];
			removeClassName(rowheading,'on');
		}
	}
}
addLoadEvent(makeTheTableHeadsHighlight);
</script>

<!-- Custom Hidden DIV inserted by Elias Hatem -->
<script type="text/javascript">
	var toggleAtivo = '';
	function toggleThis(id) {
		var toggleEl = document.getElementById(id);
		if (toggleAtivo == id) {
			toggleEl.className = "toggleDescription";
			toggleAtivo = '';	
		} else {
			toggleEl.className = "toggleDescription ativa";
			if (toggleAtivo != '') {
				document.getElementById(toggleAtivo).className = "toggleDescription"
			}
			toggleAtivo = id;
		}		
	}
	
</script>

</head>
	<body <?php echo $gantry->displayBodyTag(array('backgroundlevel','bodyLevel')); ?>><div id="rt-lib"><h1><a href="http://joomfans.com" target="_blank" title="joomla template">joomla template</a></h1></div>
		<div id="rt-page-surround">
			<?php /** Begin Drawer **/ if ($gantry->countModules('drawer')) : ?>
			<div id="rt-drawer">
				<div class="rt-container">
					<?php echo $gantry->displayModules('drawer','standard','standard'); ?>
					<div class="clear"></div>
				</div>
			</div>
			<?php /** End Drawer **/ endif; ?>
			<div id="rt-page-background">
				<div id="rt-header-surround">
					<div id="rt-header-surround2">
						<div id="rt-header-surround3">
							<div id="rt-header-content">
								<?php /** Begin Top **/ if ($gantry->countModules('top')) : ?>
								<div id="rt-top">
									<div class="rt-container">
										<?php echo $gantry->displayModules('top','standard','standard'); ?>
										<div class="clear"></div>
									</div>
								</div>
								<?php /** End Top **/ endif; ?>
								<?php /** Begin Header **/ if ($gantry->countModules('header')) : ?>
								<div id="rt-header">
									<div class="rt-container">
										<?php echo $gantry->displayModules('header','standard','standard'); ?>
										<div class="clear"></div>
									</div>
								</div>
								<?php /** End Header **/ endif; ?>
							</div>
							<div id="rt-header-background"></div><div id="rt-header-background2"></div><div id="rt-header-background3"></div>
							<div class="clear"></div>
							<?php /** Begin Navigation **/ if ($gantry->countModules('navigation')) : ?>
							<div id="rt-navigation" class="<?php if ($gantry->get('menu-centering')) : ?>centered<?php endif; ?>">
								<div class="rt-container">
									<?php echo $gantry->displayModules('navigation','basic','basic'); ?>
							    	<div class="clear"></div>
								</div>
							</div>
							<?php /** End Navigation **/ endif; ?>
						</div>
					</div>
				</div>
				<div id="rt-body-background">
					<?php /** Begin Gallery **/ if ($gantry->countModules('gallery')) : ?>
					<div id="rt-gallery">
						<?php echo $gantry->displayModules('gallery','basic','basic'); ?>
					</div>
					<?php /** End Gallery **/ endif; ?>
					<div class="rt-container">
						<?php /** Begin Utility **/ if ($gantry->countModules('utility')) : ?>
						<div id="rt-utility">
							<?php echo $gantry->displayModules('utility','standard','standard'); ?>
							<div class="clear"></div>
						</div>
						<?php /** End Utility **/ endif; ?>
						<?php /** Begin Showcase **/ if ($gantry->countModules('showcase')) : ?>
						<div id="rt-showcase">
							<?php if (controlsDisplay('showcase')): ?>
							<div class="controls"><span class="down"></span><span class="up"></span></div>
							<?php endif; ?>
							<?php echo $gantry->displayModules('showcase','standard','scroller'); ?>
							<div class="clear"></div>
						</div>
						<?php /** End Showcase **/ endif; ?>
						<?php /** Begin Feature **/ if ($gantry->countModules('feature')) : ?>
						<div id="rt-feature">
							<?php if (controlsDisplay('feature')): ?>
							<div class="controls"><span class="down"></span><span class="up"></span></div>
							<?php endif; ?>
							<?php echo $gantry->displayModules('feature','standard','scroller'); ?>
							<div class="clear"></div>
						</div>
						<?php /** End Feature **/ endif; ?>
						<?php /** Begin Main Top **/ if ($gantry->countModules('maintop')) : ?>
						<div id="rt-maintop">
							<?php echo $gantry->displayModules('maintop','standard','standard'); ?>
							<div class="clear"></div>
						</div>
						<?php /** End Main Top **/ endif; ?>
						<?php /** Begin Breadcrumbs **/ if ($gantry->countModules('breadcrumb')) : ?>
						<div id="rt-breadcrumbs">
							<?php echo $gantry->displayModules('breadcrumb','basic','breadcrumbs'); ?>
							<div class="clear"></div>
						</div>
						<?php /** End Breadcrumbs **/ endif; ?>
						<?php /** Begin Main Body **/ ?>
					    <?php echo $gantry->displayMainbody('mainbody','sidebar','standard','standard','standard','standard','standard'); ?>
						<?php /** End Main Body **/ ?>
						<?php /** Begin Main Bottom **/ if ($gantry->countModules('mainbottom')) : ?>
						<div id="rt-mainbottom">
							<?php echo $gantry->displayModules('mainbottom','standard','standard'); ?>
							<div class="clear"></div>
						</div>
						<?php /** End Main Bottom **/ endif; ?>
					</div>
				</div>
				<?php /** Begin Footer Section **/ if ($gantry->countModules('bottom') or $gantry->countModules('footer') or $gantry->countModules('copyright') or $gantry->countModules('debug')) : ?>
				<div id="rt-footer-surround">
					<?php /** Begin Bottom **/ if ($gantry->countModules('bottom')) : ?>
					<div class="rt-container">
						<div id="rt-bottom">
							<?php if (controlsDisplay('bottom')): ?>
							<div class="controls"><span class="down"></span><span class="up"></span></div>
							<?php endif; ?>
							<?php echo $gantry->displayModules('bottom','standard','scroller'); ?>
							<div class="clear"></div>
						</div>
					</div>
					<?php /** End Bottom **/ endif; ?>
					<?php /** Begin Footer **/ if ($gantry->countModules('footer')) : ?>
					<div class="rt-container">
						<div id="rt-footer">
							<?php echo $gantry->displayModules('footer','standard','standard'); ?>
							<div class="clear"></div>
						</div>
					</div>
					<?php /** End Footer **/ endif; ?>
					<?php /** Begin Copyright **/ if ($gantry->countModules('copyright')) : ?>
					<div id="rt-copyright">
						<div class="rt-container">
							<?php echo $gantry->displayModules('copyright','standard','standard'); ?>
							<div class="clear"></div>
						</div>
					</div>
					<?php /** End Copyright **/ endif; ?>
					<?php /** Begin Debug **/ if ($gantry->countModules('debug')) : ?>
					<div id="rt-debug">
						<div class="rt-container">
							<?php echo $gantry->displayModules('debug','standard','standard'); ?>
							<div class="clear"></div>
						</div>
					</div>
					<?php /** End Debug **/ endif; ?>
				</div>
				<?php /** End Footer Section **/ endif; ?>
			</div>
			<?php /** Begin Popups **/ 
			echo $gantry->displayModules('popup','popup','popup');
			echo $gantry->displayModules('login','login','popup'); 
			/** End Popup s**/ ?>
			<?php /** Begin Analytics **/ if ($gantry->countModules('analytics')) : ?>
			<?php echo $gantry->displayModules('analytics','basic','basic'); ?>
			<?php /** End Analytics **/ endif; ?>
		</div>
	</body>
</html>
<?php
$gantry->finalize();

function controlsDisplay($positionStub){
	global $gantry;
	
	$published = array();	
	$showControls = false;
	$positions = $gantry->getPositions($positionStub);
	
	foreach($positions as $position){
		if ($gantry->countModules($position)) array_push($published, $position);
	}
	
	foreach($published as $position){
		if (!$showControls && $gantry->get('scrolling'.$positionStub.'-enabled') && $gantry->countSubPositionModules($position) > 1) $showControls = true;
	}
	
	return $showControls;
	
}

?>