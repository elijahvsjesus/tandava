<?php
/**
 * @package   Maelstrom Template - RocketTheme
 * @version   1.5.1 April 20, 2011
 * @author    RocketTheme http://www.rockettheme.com
 * @copyright Copyright (C) 2007 - 2011 RocketTheme, LLC
 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 *
 * Rockettheme Maelstrom Template uses the Joomla Framework (http://www.joomla.org), a GNU/GPLv2 content management system
 *
 */
defined('JPATH_BASE') or die();

$gantry_config_mapping = array(
    'belatedPNG' => 'belatedPNG',
	'ie6Warning' => 'ie6Warning'
);

$gantry_presets = array (
    'presets' => array (
        'preset1' => array (
            'name' => 'Preset 1',
			'bodylevel' => 'high',
			'backgroundlevel' => 'high',
			'cssstyle' => 'style1',
			'hftextcolor' => '#2D3235',
			'hflinkcolor' => '#5ACBFF',
			'bodytextcolor' => '#CACBCB',
			'bodylinkcolor' => '#66AA08',
            'font-family' => 'maelstrom',
			'styleanim-enabled' => 1,
			'styleanim-delay' => 7000,
			'styleanim-duration' => 2000,
			'styleanim-animation' => 'Quad.easeInOut'
        ),
		
		'preset2' => array (
            'name' => 'Preset 2',
			'bodylevel' => 'high',
			'backgroundlevel' => 'high',
			'cssstyle' => 'style2',
			'hftextcolor' => '#333',
			'hflinkcolor' => '#EA9A0F',
			'bodytextcolor' => '#ccc',
			'bodylinkcolor' => '#EA9A0F',
            'font-family' => 'maelstrom',
			'styleanim-enabled' => 1,
			'styleanim-duration' => 60000,
			'hordirection' => 'ltr'
        ),

		'preset3' => array (
            'name' => 'Preset 3',
			'bodylevel' => 'high',
			'backgroundlevel' => 'high',
			'cssstyle' => 'style3',
			'hftextcolor' => '#333',
			'hflinkcolor' => '#D5BC61',
			'bodytextcolor' => '#ccc',
			'bodylinkcolor' => '#D5BC61',
            'font-family' => 'maelstrom',
			'styleanim-enabled' => 1,
			'styleanim-duration' => 240000,
			'verdirection' => 'bottomup'
        ),

		'preset4' => array (
            'name' => 'Preset 4',
			'bodylevel' => 'high',
			'backgroundlevel' => 'high',
			'cssstyle' => 'style4',
			'hftextcolor' => '#000',
			'hflinkcolor' => '#D1E0D1',
			'bodytextcolor' => '#ccc',
			'bodylinkcolor' => '#AEC0AE',
            'font-family' => 'maelstrom',
			'styleanim-enabled' => 1,
			'styleanim-duration' => 90000,
			'verdirection' => 'topdown'
        ),

		'preset5' => array (
            'name' => 'Preset 5',
			'bodylevel' => 'high',
			'backgroundlevel' => 'high',
			'cssstyle' => 'style5',
			'hftextcolor' => '#fff',
			'hflinkcolor' => '#D7CEB1',
			'bodytextcolor' => '#ccc',
			'bodylinkcolor' => '#D7CEB1',
            'font-family' => 'maelstrom',
			'styleanim-enabled' => 1,
			'styleanim-duration' => 80000,
			'verdirection' => 'bottomup'
        ),

		'preset6' => array (
            'name' => 'Preset 6',
			'bodylevel' => 'high',
			'backgroundlevel' => 'high',
			'cssstyle' => 'style6',
			'hftextcolor' => '#fff',
			'hflinkcolor' => '#fff',
			'bodytextcolor' => '#555',
			'bodylinkcolor' => '#B4320C',
            'font-family' => 'maelstrom',
			'styleanim-enabled' => 1,
			'styleanim-duration' => 180000,
			'verdirection' => 'bottomup'
        ),

		'preset7' => array (
            'name' => 'Preset 7',
			'bodylevel' => 'high',
			'backgroundlevel' => 'high',
			'cssstyle' => 'style7',
			'hftextcolor' => '#fff',
			'hflinkcolor' => '#A9BCD2',
			'bodytextcolor' => '#555',
			'bodylinkcolor' => '#2B5383',
            'font-family' => 'maelstrom',
			'styleanim-enabled' => 1,
			'styleanim-duration' => 70000,
			'verdirection' => 'bottomup'
        ),

		'preset8' => array (
            'name' => 'Preset 8',
			'bodylevel' => 'high',
			'backgroundlevel' => 'high',
			'cssstyle' => 'style8',
			'hftextcolor' => '#ccc',
			'hflinkcolor' => '#9B0303',
			'bodytextcolor' => '#555',
			'bodylinkcolor' => '#9B0303',
            'font-family' => 'maelstrom',
			'styleanim-enabled' => 1,
			'styleanim-duration' => 120000,
			'verdirection' => 'topdown'
        )

    )
);

$gantry_browser_params = array(
    'ie6' => array(
        'bodylevel' => 'low',
		'backgroundlevel' => 'low',
		'fixedheader' => '0',
		'readonstyle' => 'link',
		'menu-centering' => '0'
    )
);

$gantry_belatedPNG = array('.png', '#rt-logo', '#rocket', '.controls .up', '.controls .down', '.module-content ul.menu li', '.menutop li.root', '.rt-splitmenu .menutop li', '#itemListLeading', '.latestnews a', '.mostread a, .title, .module-title, .rt-gallery-controls2, .rt-gallery-controls3');

$gantry_ie6Warning = "<h3>IE6 DETECTED: Currently Running in Compatibility Mode</h3><h4>This site is compatible with IE6, however your experience will be enhanced with a newer browser</h4><p>Internet Explorer 6 was released in August of 2001, and the latest version of IE6 was released in August of 2004.  By continuing to run Internet Explorer 6 you are open to any and all security vulnerabilities discovered since that date.  In March of 2009, Microsoft released version 8 of Internet Explorer that, in addition to providing greater security, is faster and more standards compliant than both version 6 and 7 that came before it.</p> <br /><a class='external'  href='http://www.microsoft.com/windows/internet-explorer/?ocid=ie8_s_cfa09975-7416-49a5-9e3a-c7a290a656e2'>Download Internet Explorer 8 NOW!</a>";