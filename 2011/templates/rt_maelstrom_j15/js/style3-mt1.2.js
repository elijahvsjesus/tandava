/**
 * @package		Gantry Template Framework - RocketTheme
 * @version		1.5.1 April 20, 2011
 * @author		RocketTheme http://www.rockettheme.com
 * @copyright 	Copyright (C) 2007 - 2011 RocketTheme, LLC
 * @license		http://www.rockettheme.com/legal/license.php RocketTheme Proprietary Use License
 */

(function(){
	
	var MA = this.MaelstromAnim = {
		init: function(){
			MA.bg = document.id('rt-header-surround3');
			if (Browser.Engine.trident){
				MA.bgPosition = MA.bg.getStyle('background-position-x') + ' ' + MA.bg.getStyle('background-position-y');
				MA.bgPosition = MA.bgPosition.split(' ');
			} else {
				MA.bgPosition = MA.bg.getStyle('background-position').split(' ');
			}
			var loop = MA.bgPosition[1].toInt();
			loop = loop + ((loop < 0) ? 1 : - 1);
			
			var posY = MA.bgPosition[1].toInt();
			
			if (MaelstromHeader.vdir == 'bottomup'){
				if (posY > 0) loop *= -1;
				else {
					MA.bgPosition[1] = (MA.bgPosition[1].toInt() * -1) + 'px';
					MA.bg.setStyle('background-position', MA.bgPosition.join(' '));
				}
			} else {
				if (posY < 0) loop *= -1;
				else {
					MA.bgPosition[1] = (MA.bgPosition[1].toInt() * -1) + 'px';
					MA.bg.setStyle('background-position', MA.bgPosition.join(' '));
				}
			}
									
			MA.bgFx = new Fx.Tween(MA.bg, {
				duration: MaelstromHeader.duration,
				link: 'cancel',
				transition: 'linear',
				unit: 'px',
				onComplete: function(){
					MA.bg.setStyle('background-position', MA.bgPosition.join(' '));
					MA.bgFx.start('background-position',  '0 ' + loop + 'px');
				}
			}).set('background-position', MA.bgPosition.join(' '));
			
			MA.bgFx.start('background-position', '0 ' + loop + 'px');
		}
	};
	
	window.addEvent('domready', MaelstromAnim.init);
	
})();