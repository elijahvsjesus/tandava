/**
 * @package		Gantry Template Framework - RocketTheme
 * @version		1.5.1 April 20, 2011
 * @author		RocketTheme http://www.rockettheme.com
 * @copyright 	Copyright (C) 2007 - 2011 RocketTheme, LLC
 * @license		http://www.rockettheme.com/legal/license.php RocketTheme Proprietary Use License
 */

(function(){
	
	var RTIE7Fixes = {
		init: function(){
			var rokstories = $$('.rokstories-layout7');
			if (rokstories.length){
				rokstories = rokstories[0];
				var controls = rokstories.getElement('.rt-gallery-controls');
				if (controls){
					var title = controls.getElement('.rt-gallery-title');
					var circles = controls.getElement('ul');
					if (!title && circles){
						var size = (MooTools.version < "1.2") ? circles.getSize().size.x : circles.getSize().x;
						controls.setStyle('width', size + 40);
					}
				}
			}
		}
	};
	
	window.addEvent('domready', RTIE7Fixes.init);
})();