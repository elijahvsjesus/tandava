/**
 * @package		Gantry Template Framework - RocketTheme
 * @version		1.5.1 April 20, 2011
 * @author		RocketTheme http://www.rockettheme.com
 * @copyright 	Copyright (C) 2007 - 2011 RocketTheme, LLC
 * @license		http://www.rockettheme.com/legal/license.php RocketTheme Proprietary Use License
 */

(function(){
	
	var MA = this.MaelstromAnim = {
		init: function(){
			MA.bgs = [$('rt-header-background'), $('rt-header-background2'), $('rt-header-background3')];
			
			MA.fxs = [];
			MA.bgs.each(function(bg, i){
				var fx = new Fx.Style(bg, 'opacity', {
					duration: MaelstromHeader.duration,
					wait: false,
					transition: MaelstromHeader.transition,
					onComplete: function(){
						MA.fxs.each(function(fx, i){
							if (i != MA.current) fx.set(0);
						});
					}
				}).set((!i) ? 1 : 0);
				MA.fxs.push(fx);
			}, this);
			
			MA.current = -1;
			MA.next.periodical(MaelstromHeader.delay);
			MA.next();
		},
		
		next: function(){
			MA.current += 1;
			if (MA.current >= MA.bgs.length) MA.current = 0;
			
			MA.fxs.each(function(fx, i){
				if (i == MA.current) MA.bgs[i].setStyle('z-index', 2);
				else MA.bgs[i].setStyle('z-index', 1);
			}, this);
			
			MA.fxs[MA.current].start(1);
			 
		}
	};
	
	window.addEvent('domready', MaelstromAnim.init);
	
})();