/**
 * @package		Gantry Template Framework - RocketTheme
 * @version		1.5.1 April 20, 2011
 * @author		RocketTheme http://www.rockettheme.com
 * @copyright 	Copyright (C) 2007 - 2011 RocketTheme, LLC
 * @license		http://www.rockettheme.com/legal/license.php RocketTheme Proprietary Use License
 */

(function(){
	
	var MA = this.MaelstromAnim = {
		init: function(){
			MA.bg = document.id('rt-header-background2');
			if (Browser.Engine.trident){
				MA.bgPosition = MA.bg.getStyle('background-position-x') + ' ' + MA.bg.getStyle('background-position-y');
				MA.bgPosition = MA.bgPosition.split(' ');
			} else {
				MA.bgPosition = MA.bg.getStyle('background-position').split(' ');
			}
			var loop = MA.bgPosition[0].toInt();
			loop = loop + ((loop < 0) ? 1 : - 1);
			
			var posX = MA.bgPosition[0].toInt();
			
			if (MaelstromHeader.hdir == 'rtl'){
				if (posX > 0) loop *= -1;
				else {
					MA.bgPosition[0] = (MA.bgPosition[0].toInt() * -1) + 'px';
					MA.bg.setStyle('background-position', MA.bgPosition.join(' '));
				}
			} else {
				if (posX < 0) loop *= -1;
				else {
					MA.bgPosition[0] = (MA.bgPosition[0].toInt() * -1) + 'px';
					MA.bg.setStyle('background-position', MA.bgPosition.join(' '));
				}
			}
						
			MA.bgFx = new Fx.Tween(MA.bg, {
				duration: MaelstromHeader.duration,
				link: 'cancel',
				transition: 'linear',
				unit: 'px',
				onComplete: function(){
					MA.bg.setStyle('background-position', MA.bgPosition.join(' '));
					MA.bgFx.start('background-position', loop + 'px 0');
				}
			}).set('background-position', MA.bgPosition.join(' '));
			
			MA.bgFx.start('background-position', loop + 'px 0');
		}
	};
	
	window.addEvent('domready', MaelstromAnim.init);
	
})();