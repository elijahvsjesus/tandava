<?php
/**
 * @package		Gantry Template Framework - RocketTheme
 * @version		1.5.1 April 20, 2011
 * @author		RocketTheme http://www.rockettheme.com
 * @copyright 	Copyright (C) 2007 - 2011 RocketTheme, LLC
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 *
 * Gantry uses the Joomla Framework (http://www.joomla.org), a GNU/GPLv2 content management system
 *
 */
defined('JPATH_BASE') or die();

gantry_import('core.gantryfeature');

class GantryFeatureStyleDeclaration extends GantryFeature {
    var $_feature_name = 'styledeclaration';

    function isEnabled() {
        global $gantry;
        $menu_enabled = $this->get('enabled');

        if (1 == (int)$menu_enabled) return true;
        return false;
    }

	function init() {
        global $gantry;

		//inline css for colorchooser
		$css = 'a, .readonstyle-link .readon span, .readonstyle-link .readon .button, #more-articles, .readonstyle-link #rt-top .readon span, .readonstyle-link #rt-feature .readon span, .readonstyle-link #rt-footer-surround .readon span, .readonstyle-link #rt-top .readon .button, .readonstyle-link #rt-feature .readon .button, .readonstyle-link #rt-footer-surround .readon .button, #current, .roktabs-links ul li.active span, .box1 a, #rt-header-surround .box1 a, #rt-footer-surround .box1 a, .box4 a, #rt-header-surround .box4 a, #rt-footer-surround .box4 a, .box5 a, #rt-header-surround .box5 a, #rt-footer-surround .box5 a {color:'.$gantry->get('bodylinkcolor').';}'."\n";
		
		$css .= 'body, #rt-navigation, .box4 .rt-module-surround, .box5 .rt-module-surround, .inputbox, body #roksearch_search_str, body #roksearch_results h3, body #roksearch_results .roksearch_header, body #roksearch_results .roksearch_row_btm, body #roksearch_results .roksearch_row_btm span {color:'.$gantry->get('bodytextcolor').';}'."\n";
		
		$css .= '#rt-header, #rt-top, #rt-header-surround, #rt-bottom, #rt-footer, #rt-copyright, #rt-footer-surround {color:'.$gantry->get('hftextcolor').';}'."\n";
		
		if ($gantry->browser->name == 'ie' && $gantry->browser->shortversion == '6') {
			$css .= '.menutop li .item:hover {color:'.$gantry->get('hflinkcolor').';}'."\n";
			$css .= '.menutop #current .item {color: '.$gantry->get('hflinkcolor').';}'."\n";
		}
		
		$css .= '#rt-top a, #rt-header a, .readonstyle-link #rt-header-surround .readon span, .readonstyle-link #rt-header-surround .readon .button, .readonstyle-link #rt-footer-surround .readon span, .readonstyle-link #rt-footer-surround .readon .button, #rt-footer-surround a {color:'.$gantry->get('hflinkcolor').';}'."\n";
		
		$css .= '.menutop ul li.root > .item, .menutop li.active.root > .item, .menutop li.active.root.f-mainparent-itemfocus > .item, .menutop li.root:hover > .item, .menutop li.root.f-mainparent-itemfocus > .item, .rt-splitmenu .menutop li.active > .item, .rt-splitmenu .menutop li:hover > .item, .menutop ul li > .item:hover, .menutop li.f-menuparent-itemfocus > .item, .menutop li.active > .item, .menutop ul li > .item:hover, .menutop li.f-menuparent-itemfocus > .item {color:'.$gantry->get('bodylinkcolor').';}'."\n";
		
		if ($gantry->browser->platform == 'iphone'){
			$css .= 'body #idrops li.root-sub a, body #idrops li.root-sub span.separator, body #idrops li.root-sub.active a, body #idrops li.root-sub.active span.separator {color: '.$gantry->get('bodylinkcolor').' !important;}'."\n";
		}

		$gantry->addInlineStyle($css);
		$this->_disableRokBoxForiPhone();

		//style stuff
		$gantry->addStyle($gantry->get('cssstyle').".css");
		if ($gantry->get('extensions')) $gantry->addStyle('extensions.css');
		if ($gantry->get('thirdparty')) $gantry->addStyle('thirdparty.css');
		
		// per style animation
		if ($gantry->get('styleanim-enabled') && $gantry->get('backgroundlevel') == 'high'){
			$gantry->addInlineScript("
				var MaelstromHeader = {
					delay: ".$gantry->get('styleanim-delay').",
					duration: ".$gantry->get('styleanim-duration').",
					transition: Fx.Transitions.".$gantry->get('styleanim-animation').",
					vdir: '".$gantry->get('verdirection')."',
					hdir: '".$gantry->get('hordirection')."'
				};
			");
			$gantry->addScript($gantry->get('cssstyle') . '.js');
		}

	}

	function _disableRokBoxForiPhone() {
		global $gantry;

		if ($gantry->browser->platform == 'iphone') {
			$gantry->addInlineScript("window.addEvent('domready', function() {\$\$('a[rel^=rokbox]').removeEvents('click');});");
		}
	}

}