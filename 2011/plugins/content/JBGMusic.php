<?php
/**
* version 1.0 JBGMusic
* @copyright Copyright (C) 2008 Jfriendly.net. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
* 
*/

// no direct access
defined('_JEXEC') or die('Restricted access');




jimport( 'joomla.plugin.plugin' );

class plgcontentJBGMusic extends JPlugin 
{

        /**
	 * Constructor
	 *
	 * For php4 compatability we must not use the __constructor as a constructor for plugins
	 * because func_get_args ( void ) returns a copy of all passed arguments NOT references.
	 * This causes problems with cross-referencing necessary for the observer design pattern.
	 *
	 * @param object $subject The object to observe
	 * @param object $params  The object that holds the plugin parameters
	 * @since 1.5
	 */
	function plgcontentJBGMusic( &$subject, $params )
	{
		parent::__construct( $subject, $params );
	}

        /**
	 * Example prepare content method
	 *
	 * Method is called by the view
	 *
	 * @param 	object		The article object.  Note $article->text is also available
	 * @param 	object		The article params
	 * @param 	int			The 'page' number
	 */
	function onPrepareContent( &$article, &$params, $limitstart )
	{
             	$plugin =& JPluginHelper::getPlugin('content','JBGMusic');
             	$pluginParams = new JParameter( $plugin->params);

   		preg_match_all('/\{jbgmusic (.*)\}/U', $article->text, $matches);

   		$first_logo = 0;
   		foreach( $matches[1] as $name )
   		{
   			$html = contentJBGMusic_createHTML($name, $pluginParams, $first_logo);
        		$article->text = str_replace("{jbgmusic $name}", $html, $article->text);
   		}
                
        }
}

function contentJBGMusic_createHTML (&$name, &$pluginParams, &$first_logo)
{

$url = JURI::base();   

// Set Local Environment variables
$JUSER_AGENT = $_SERVER['HTTP_USER_AGENT'];
$ip = str_replace(".","", $_SERVER['REMOTE_ADDR']);

$jsound_location = $pluginParams->get('jsound_location', '');


if ($pluginParams->get('jloop_sound'))
   $jloopy = "true";
else
   $jloopy = "false";


if ($pluginParams->get('jautostart'))
   $jautostart = "true";
else
   $jautostart = "false";


if ($pluginParams->get('jhidden_controls'))
{
   $jheight = 'height="0"';
   $jwidth = 'width="0"';
   $jautostart = "true";
   $jhidden = 'true';
}
else
{
   $jheight = 'height="20"';
   $jwidth = 'width="144"';
   $jhidden = 'false';
}


$JBGParam = $name.'|';
preg_match_all('/[^|]*|/i', $JBGParam, $pluginmatches);


foreach ($pluginmatches[0] as $key => $codedparam)
{
   if ($key == 0 ) $jsound_file = $codedparam;

   $codedparam = strtolower($codedparam);
    
   
   if ($codedparam == 'loop') $jloopy = "true";
   if ($codedparam == 'noloop') $jloopy = "false";
   if ($codedparam == 'autostart') $jautostart = "true";
   if ($codedparam == 'dontstart') $jautostart = "false";
   if ($codedparam == 'hidecontrol') 
   {
       $jheight = 'height="0"';
       $jwidth = 'width="0"';
       $jautostart = "true";
       $jhidden = 'true';
   }
   if ($codedparam == 'showcontrol') 
   {
       $jheight = 'height="20"';
       $jwidth = 'width="144"';
       $jhidden = 'false';
   }
}

if ($jhidden == "true") $jautostart = "true";

// Clean up leftover .m3u created 3 hours ago.

$dir = JPATH_BASE.DS."tmp".DS ;
$dp = opendir($dir) or die ('Could not open '.$dir);
while (($filename = readdir($dp)) !== false) {
	if (eregi("jbg.m3u", $filename) && filemtime($dir.$filename) < strtotime ("-3 hours") ) unlink ($dir.$filename) ;
	
}

closedir($dp);


$keywords = explode(".", JText::_($jsound_file));
$File = 'tmp/'.$ip.date('YmdB').'jbg.m3u';
$Handle = fopen($File, 'w');

// check if valid url
if (preg_match ("/^http:|^ftp:/i", $jsound_file))
{
   $Data = JText::_($jsound_file);
}
else{
   $Data = $url.JText::_($jsound_location).'/'.JText::_($jsound_file);
}

fwrite($Handle, $Data);
fclose($Handle); 


$html  = '<style type="text/css"> '."\n";
$html .= '<!-- '."\n";
$html .= '.jbgstyle { '."\n";
$html .= 'font-family: Arial, Helvetica, sans-serif; '."\n";
$html .= 'font-size: 9px; '."\n";
$html .= 'align: center; '."\n";
$html .= '}'."\n";
$html .= '--> '."\n";
$html .= '</style>'."\n";
$html .= '<embed src="'.$File.'" type=audio/mpeg autostart="'.$jautostart.'" loop="'.$jloopy.'" ';
$html .= $jheight." ".$jwidth."/>\n";

$first_logo++;
if ($jhidden == 'false' || ($first_logo <= 1 && $jhidden == 'true')){
   $html .= "<br />\n";
   $html .= '<span class="jbgstyle">sound by</span> <a href="http://www.jfriendly.net"><img src="'.$url.JText::_("plugins/content/jbgmusic/jbgmusic.png").'" width="37" height="14" alt="jbgmusic" align="absmiddle"/></a>'."\n";
}

return $html;
}




	

 
